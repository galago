/*
 * QueryTest.java
 * JUnit based test
 *
 * Created on December 12, 2006, 1:20 PM
 */

package galago.retrieval;

import galago.retrieval.query.SimpleQuery.QueryTerm;
import galago.retrieval.query.SimpleQuery;
import junit.framework.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trevor
 */
public class QueryTest extends TestCase {
    
    public QueryTest(String testName) {
        super(testName);
    }
    
    public void testSimpleQuery() {
        System.out.println("simple query");
        
        String query = "a b c d";
        List<QueryTerm> expected = new ArrayList<QueryTerm>();
        
        expected.add( new QueryTerm( "a" ) );
        expected.add( new QueryTerm( "b" ) );
        expected.add( new QueryTerm( "c" ) );
        expected.add( new QueryTerm( "d" ) );
        
        List<QueryTerm> actual = SimpleQuery.parse(query);
        this.assertEquals( expected, actual );
    }
    
    public void testComplicatedQuery() {
        System.out.println("complicated query");
        
        String query = "f:aa^3.4 g:\"b c\"^9 \"l m\" j k d^8";
        List<QueryTerm> expected = new ArrayList<QueryTerm>();
        
        expected.add( new QueryTerm( "aa", "f", 3.4 ) );
        expected.add( new QueryTerm( "b c", "g", 9 ) );
        expected.add( new QueryTerm( "l m" ) );
        expected.add( new QueryTerm( "j" ) );
        expected.add( new QueryTerm( "k" ) );
        expected.add( new QueryTerm( "d", null, 8 ) );
        
        List<QueryTerm> actual = SimpleQuery.parse(query);
        this.assertEquals( expected, actual );
    }
    
}
