/*
 * DocumentSortedIndexWriter
 *
 * August 7, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.index;

import galago.tupleflow.InputClass;
import galago.tupleflow.Parameters;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Processor;
import galago.tupleflow.VByteInput;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.NumberWordBin;
import galago.types.XMLFragment;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author trevor
 */
@InputClass(className="galago.types.NumberWordBin", order={ "+word", "+document" } )
public class DocumentOrderedBinnedListWriter implements NumberWordBin.WordDocumentOrder.ShreddedProcessor {
    long blockSize = 32768;
    long minimumSkipListLength = 2048;
    int skipByteLength = 128;
    Processor<XMLFragment> statistics;

    DocumentSortedInvertedList invertedList;
    InvertedListWriter writer;
    
    public class DocumentSortedInvertedList implements InvertedList {
        public byte[] word;
        public CompressedByteBuffer header;
        public CompressedByteBuffer skips;
        public BackedCompressedByteBuffer data;
        long lastDocument = 0;
        long documentCount = 0;
        long listMaximumBin = 0;
        
        public DocumentSortedInvertedList( byte[] word ) {
            this.word = word;
            this.data = new BackedCompressedByteBuffer();
        }
        
        public void addDocument( long document ) throws IOException {
            assert document - lastDocument >= 1 || document == 0;
            data.add( document - lastDocument );
            lastDocument = document;
            documentCount += 1;
        }
        
        public void addBin( long bin ) throws IOException {
            listMaximumBin = Math.max( bin, listMaximumBin );
            data.add( bin );
        }
        
        public void close() throws IOException {
            boolean useSkips = minimumSkipListLength < data.length();

            if( useSkips ) {
                skips = createSkips( data, skipByteLength );
            }
            
            // build a list header, including an options section?
            int options = 0;
            options |= (useSkips ? 1 : 0);

            header = new CompressedByteBuffer();
            header.add( options );
            header.add( documentCount );
            header.add( listMaximumBin );
            
            if( useSkips ) {
                header.add( skips.length() );
            }

            header.add( data.length() );
        }

        /**
         * Reads an inverted list fragment (composed of compressed delta encoded document
         * numbers and compressed bin values) and returns a compressed skip list.
         * 
         * The skip list has three kinds of elements: document identifiers, bin values,
         * and byte distances.  The document identifiers and byte distances work as they 
         * do in any other kind of skip data; they tell you that document x is stored at position y.
         * The bin value tells you the maximum bin value found in that particular segment of the list,
         * which allows for more efficient list skipping, potentially.
         */
        
        private CompressedByteBuffer createSkips( BackedCompressedByteBuffer list, int distance ) throws IOException {
            CompressedByteBuffer skips = new CompressedByteBuffer();
            PositionalByteStream byteStream = new PositionalByteStream( list.getInputStream() );
            DataInputStream dataStream = new DataInputStream( byteStream );
            VByteInput stream = new VByteInput( dataStream );

            int lastDocument = 0;
            int lastPosition = 0;
            int document = 0;
            long maximumBin = 0;
            
            try {
                while (true) {
                    long delta = stream.readInt();
                    document += delta;
                    
                    long bin = stream.readInt();
                    maximumBin = Math.max( maximumBin, bin );

                    if (byteStream.position() > lastPosition + distance) {
                        skips.add( maximumBin );
                        skips.add( document - lastDocument );
                        skips.add( byteStream.position() - lastPosition );

                        lastPosition = byteStream.position();
                        lastDocument = document;
                        
                        maximumBin = 0;
                    }
                }
            } catch( IOException e ) {}
            
            skips.add( maximumBin );
            skips.add( document + 1 - lastDocument );
            skips.add( byteStream.position() - lastPosition );
            return skips;
        }
        
        public byte[] word() {
            return word;
        }
        
        public long dataLength() {
            long listLength = 0;
            if( header != null )
                listLength += header.length();

            if( skips != null )
                listLength += skips.length();
    
            listLength += data.length();
            return listLength;
        }

        public void write( final DataOutputStream output ) throws IOException {
            // first, write the data header
            header.write( output );
            
            // if we have skips, write those
            if( skips != null )
                skips.write( output );
            
            // finally, the list data
            data.write( output );

            // now that all the data is written, clear the buffers to delete
            // any excess files on disk
            if( skips != null )
                skips.clear();
            
            data.clear();
        }
    }
    
    @SuppressWarnings("unchecked")
    public DocumentOrderedBinnedListWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        invertedList = null;
        writer = new InvertedListWriter( parameters );
    }
    
    void flush() throws IOException {
        if( invertedList != null ) {
            invertedList.close();
            writer.add( invertedList );
        }
        invertedList = null;
    }
    
    public void close() throws IOException {
        flush();
        writer.close();
    }

    public void processWord( byte[] wordBytes ) throws IOException {
        flush();
        
        if (wordBytes.length >= 256 || wordBytes.length >= blockSize/4)
            throw new IOException( "Word is too long." );
        
        invertedList = new DocumentSortedInvertedList( wordBytes );
    }
    
    public void processDocument(int document) throws IOException {
        invertedList.addDocument( document );
    }

    public void processTuple(int bin) throws IOException {
        invertedList.addBin( bin );
    }

    public Class<NumberWordBin> getInputClass() {
        return NumberWordBin.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !parameters.getXML().containsKey( "index" ) ) {
            handler.addError( "DocumentOrderedBinnedListWriter requires an 'index' parameter." );
            return;
        }
        
        String index = parameters.getXML().get( "index" );
        Verification.requireWriteableDirectory( index, handler );
    }
}
