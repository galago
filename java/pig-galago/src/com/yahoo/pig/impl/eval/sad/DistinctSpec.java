/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.sad;

import java.io.IOException;
import java.util.Iterator;

import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.EvalItemList;

public class DistinctSpec extends SADSpec {
    EvalItemList cols;       // what columns to make distinct
 
    public DistinctSpec(EvalItemList cols) {
        this.cols = cols;
    }
    
    public String toString() {
        return "DISTINCT " + cols;
    }

    @Override
    public DataCollector collector(final DataCollector output) {
        return new DataCollector() {
            
            DataBag buf = null;
            
            public void add(Tuple t) throws IOException {
                if (buf == null){
                	buf = BagFactory.getInstance().getNewBag();
                	buf.distinct(cols);
                }

                if (t != null) {
                    buf.add(t);
                } else {
                    
                    for (Iterator<Tuple> it = buf.content(); it.hasNext(); ) {
                        output.add(it.next());
                    }
                    output.add(null);  // EOF
                }
            }
        };
    }
    
}
