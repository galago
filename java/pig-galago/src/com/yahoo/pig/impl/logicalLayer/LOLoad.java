/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.Iterator;
import java.util.List;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class LOLoad extends LogicalOperator {
    public String   filename;
    public StorageFunc lf;
    public String   args[];
    public int      outputType = FIXED;

    public LOLoad(PigContext pigContext, String filenameIn, StorageFunc lfIn, String args[]) {
    	super(pigContext);
        inputs = new LogicalOperator[0];

        filename = filenameIn;
        lf = lfIn;
        this.args = args;
        
        schema = new SchemaItemList();
    }

    public LOLoad(PigContext pigContext, String filenameIn, StorageFunc lfIn, List<String> args) {
    	super(pigContext);
        inputs = new LogicalOperator[0];

        filename = filenameIn;
        lf = lfIn;

        this.args = new String[args.size()];

        int i = 0;
        for (Iterator<String> it = args.iterator(); it.hasNext();)
            this.args[i++] = it.next();
        
        schema = new SchemaItemList();
    }

    public String name() {
        return "Load";
    }

    public String arguments() {
        StringBuffer sb = new StringBuffer();
        sb.append(lf.getName());
        sb.append('(');
        for (int i = 0; i < args.length; i++) {
            if (i != 0)
                sb.append(',');
            sb.append(args[i]);
        }
        sb.append(')');
        sb.append(':');
        sb.append(filename);
        return sb.toString();
    }

    public SchemaItemList outputSchema() {
    	schema.alias = alias;
        return this.schema;
    }

    public int getOutputType() {
        return outputType;
    }

    public void setOutputType(int type) {
        if (type < FIXED || type > AMENDABLE) {
            throw new RuntimeException("Illegal output type");
        }
        outputType = type;
    }

    public List<String> getFuncs() {
        List<String> funcs = super.getFuncs();
        funcs.add(lf.getName());
        return funcs;
    }
}
