/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.tools.grunt;

public abstract class Command {
    public abstract void execute(String commandline);
}
