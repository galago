/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketImplFactory;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.UTF8;
import org.apache.hadoop.io.WritableUtils;
import org.apache.hadoop.ipc.RPC;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobStatus;
import org.apache.hadoop.mapred.JobSubmissionProtocol;
import org.apache.hadoop.mapred.JobTracker;
import org.apache.hadoop.mapred.TaskReport;

import com.yahoo.pig.PigServer;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.IndexedTuple;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import com.yahoo.pig.impl.eval.EvalSpecPipeList;
import com.yahoo.pig.impl.eval.groupby.GroupBySpecList;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.io.PigFile;
import com.yahoo.pig.impl.logicalLayer.LogicalPlanBuilder;
import com.yahoo.pig.tools.grunt.Grunt;

/**
 * This is that Main-Class for the Pig Jar file. It will setup a Pig jar file to run with the proper
 * libraries. It will also provide a basic shell if - or -e is used as the name of the Jar file.
 * 
 * @author breed
 * 
 */
public class MapReduceLauncher {
    
    public static long totalHadoopTimeSpent = 0;
    public static int numMRJobs;
    public static int mrJobNumber;
    
    public static void initQueryStatus(int numMRJobsIn) {
        numMRJobs = numMRJobsIn;
        mrJobNumber = 0;
    }
    
    /**
     * A container class to track the Jar files that need to be merged together to submit to Hadoop.
     */
    private static class JarListEntry {
        /**
         * The name of the Jar file to merge in.
         */
        String jar;
        /**
         * If this field is not null, only entries that start with this prefix will be merged in.
         */
        String prefix;

        JarListEntry(String jar, String prefix) {
            this.jar = jar;
            this.prefix = prefix;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof JarListEntry))
                return false;
            JarListEntry other = (JarListEntry) obj;
            if (!jar.equals(other.jar))
                return false;
            if (prefix == null)
                return other.prefix == null;
            return prefix.equals(other.prefix);
        }

        public int hashCode() {
            return jar.hashCode() + (prefix == null ? 1 : prefix.hashCode());
        }
    }

    /**
     * Adds a stream to a Jar file.
     * 
     * @param os
     *            the OutputStream of the Jar file to which the stream will be added.
     * @param name
     *            the name of the stream.
     * @param is
     *            the stream to add.
     * @param contents
     *            the current contents of the Jar file. (We use this to avoid adding two streams
     *            with the same name.
     * @throws IOException
     */
    private static void addStream(JarOutputStream os, String name, InputStream is, Map<String, String> contents)
            throws IOException {
        if (contents.get(name) != null) {
            return;
        }
        contents.put(name, "");
        os.putNextEntry(new JarEntry(name));
        byte buffer[] = new byte[4096];
        int rc;
        while ((rc = is.read(buffer)) > 0) {
            os.write(buffer, 0, rc);
        }
    }

    /**
     * Find a jar that contains a class of the same name, if any. It will return a jar file, even if
     * that is not the first thing on the class path that has a class with the same name.
     * 
     * @author Owen O'Malley (Copied from JobConf)
     * @param my_class
     *            the class to find
     * @return a jar file that contains the class, or null
     * @throws IOException
     */
    private static String findContainingJar(Class my_class) {
        ClassLoader loader = my_class.getClassLoader();
        String class_file = my_class.getName().replaceAll("\\.", "/") + ".class";
        try {
            for (Enumeration itr = loader.getResources(class_file); itr.hasMoreElements();) {
                URL url = (URL) itr.nextElement();
                if ("jar".equals(url.getProtocol())) {
                    String toReturn = url.getPath();
                    if (toReturn.startsWith("file:")) {
                        toReturn = toReturn.substring("file:".length());
                    }
                    toReturn = URLDecoder.decode(toReturn, "UTF-8");
                    return toReturn.replaceAll("!.*$", "");
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return null;
    }

    /**
     * Invoke the main method in the mainClass with the given arguments. The method takes care of
     * removing the first argument, which is the command or jar file, from the args array. The
     * mainClass will be loaded from the specified classloader.
     * 
     * @param mainClass
     *            the class containing the main method to invoke
     * @param args
     *            the arguments to pass to the main method. (The first element will be removed
     *            before invoking main.
     * @param cl
     *            the classloader to use to load the mainClass.
     */
    private static void invokeMain(Class clazz, String[] args) {
        try {
            Class argsClass[] = { String[].class };
            Method meth = clazz.getMethod("main", argsClass);
            String truncArgs[] = new String[args.length - 1];
            System.arraycopy(args, 1, truncArgs, 0, truncArgs.length);
            Object invokeArgs[] = new Object[1];
            invokeArgs[0] = truncArgs;
            meth.invoke(null, invokeArgs);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static Random rand = new Random();

    /**
     * Submit a Pig job to hadoop.
     * 
     * @param mapFuncs
     *            a list of map functions to apply to the inputs. The cardinality of the list should
     *            be the same as input's cardinality.
     * @param groupFuncs
     *            a list of grouping functions to apply to the inputs. The cardinality of the list
     *            should be the same as input's cardinality.
     * @param reduceFunc
     *            the reduce function.
     * @param mapTasks
     *            the number of map tasks to use.
     * @param reduceTasks
     *            the number of reduce tasks to use.
     * @param input
     *            a list of inputs
     * @param output
     *            the path of the output.
     * @return an indicator of success or failure.
     * @throws IOException
     */
    // public boolean launchPig(String mapFuncs, String groupFuncs, String reduceFunc, int mapTasks,
    // int reduceTasks,
    // String input, String output) throws IOException {
    // return launchPig(mapFuncs, null, groupFuncs, reduceFunc, mapTasks, reduceTasks, input,
    // output);
    // }
    public boolean launchPig(EvalSpecPipeList toMap, EvalSpecPipe toCombine, GroupBySpecList toGroup,
            EvalSpecPipe toReduce, int mapTasks, int reduceTasks, String input, String output, String storeFunc, PigContext pigContext) throws IOException {
        if (dfs == null || jobTracker == null || lfs == null)
            init();
        JobConf conf = new JobConf();
        conf.setJobName("PigLatin");

        String fsName = jobTracker.getFilesystemName();
        FileSystem jobfs = FileSystem.getNamed(fsName, conf);
        Path submitJobDir = new Path(conf.getSystemDir(), "submit_" + Integer.toString(Math.abs(rand.nextInt()), 36));
        Path submitJobFile = new Path(submitJobDir, "job.xml");
        Path submitJarFile = new Path(submitJobDir, "job.jar");
        Path submitSplitFile = new Path(submitJobDir, "job.split");
        boolean success = false;
        try {
            jobfs.mkdirs(submitJobDir);
            try {
                FSDataOutputStream jaros = jobfs.create(submitJarFile);
                List<String> funcs = new ArrayList<String>();
                if (toMap != null)
                    funcs.addAll(toMap.getFuncs());
                if (toGroup != null)
                    funcs.addAll(toGroup.getFuncs());
                if (toReduce != null)
                    funcs.addAll(toReduce.getFuncs());
                createJar(jaros, funcs, pigContext);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new IOException(e.getMessage());
            }
            conf.setJar(submitJarFile.toString());
            int replication = conf.getInt("mapred.submit.replication", 10);
            jobfs.setReplication(submitJarFile, (short) replication);
            String user = System.getProperty("user.name");
            conf.setUser(user != null ? user : "Pigster");
            if (conf.getWorkingDirectory() == null) {
                conf.setWorkingDirectory(jobfs.getWorkingDirectory());
            }
            if (reduceTasks != -1)
                conf.setNumReduceTasks(reduceTasks);
            if (toMap != null)
                conf.set("pig.mapFuncs", toMap.toString());
            if (toCombine != null)
                conf.set("pig.combineFunc", toCombine.toString());
            if (toGroup != null)
                conf.set("pig.groupFuncs", toGroup.toString());
            if (toReduce != null)
                conf.set("pig.reduceFunc", toReduce.toString());
            // the keys are words (strings)
            conf.setOutputKeyClass(IndexedTuple.class);
            // the values are counts (ints)
            conf.setOutputValueClass(Tuple.class);
            conf.setMapRunnerClass(PigMapReduce.class);
            if (toCombine != null)
                conf.setCombinerClass(PigCombine.class);
            conf.setReducerClass(PigMapReduce.class);
            conf.setInputFormat(PigInputFormat.class);
            conf.setOutputFormat(PigOutputFormat.class);
            conf.setInputKeyClass(UTF8.class);
            conf.setInputValueClass(Tuple.class);
            conf.setOutputKeyClass(Tuple.class);
            conf.setOutputValueClass(IndexedTuple.class);
            conf.set("pig.inputs", input);
            conf.setInputPath(new Path((String) input));
            conf.setOutputPath(new Path((String) output));
            conf.set("pig.storeFunc", storeFunc == null ? "" : storeFunc);
            // Setup the splits
            FSDataOutputStream splitStream = jobfs.create(submitSplitFile, (short)replication);
            try {
            	// This is a private define JobClient.SPLIT_FILE_HEADER
                splitStream.write("SPL".getBytes());
                // This is a private define JobClient.CURRENT_SPLIT_FILE_VERSION
                WritableUtils.writeVInt(splitStream, 0);
                InputSplit[] splits = new PigInputFormat().getSplits(conf, mapTasks);
				WritableUtils.writeVInt(splitStream, splits.length);
				conf.setNumMapTasks(splits.length);
	            	for(InputSplit s: splits) {
            		Text.writeString(splitStream, s.getClass().getName());
            		ByteArrayOutputStream baos = new ByteArrayOutputStream();
            		s.write(new DataOutputStream(baos));
            		byte splitBytes[] = baos.toByteArray();
            		splitStream.writeInt(splitBytes.length);
            		splitStream.write(splitBytes);
            		WritableUtils.writeVInt(splitStream, s.getLocations().length);
            		for(String loc: s.getLocations()) {
            			Text.writeString(splitStream, loc);
            		}
            	}
            } finally {
            	splitStream.close();
            }
            conf.set("mapred.job.split.file", submitSplitFile.toString());
            
            // Write job file to JobTracker's fs
            FSDataOutputStream out = jobfs.create(submitJobFile, (short) replication);
            try {
                conf.write(out);
            } finally {
                out.close();
            }
            //
            // Now, actually submit the job (using the submit name)
            //
            JobStatus status = jobTracker.submitJob(submitJobFile.toString());
            if (MapReduceLauncher.debug) System.err.println("submitted job: " + status.getJobId());
            
            long sleepTime = 1000;
            while (true) {
                try {
                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                }
                status = getJobTracker().getJobStatus(status.getJobId());
                if (status == null || status.getRunState() == JobStatus.SUCCEEDED || status.getRunState() == JobStatus.FAILED) {
                    boolean successful = false;
                    if (status != null && status.getRunState() == JobStatus.SUCCEEDED) successful = true;
                    if (MapReduceLauncher.debug) System.err.print("\rJob finished " + (successful ? "" : "un") + "successfully");
                    try {
                        Thread.sleep(sleepTime);
                    } catch (Exception e) {
                    }
                    if (successful) mrJobNumber++;
                    if (MapReduceLauncher.debug) {
                        System.err.print("\r                                                              \r");
                    }
                    else {
                        double queryProgress = ((double) mrJobNumber) / ((double) numMRJobs);
                        System.err.print("\rPig progress = " + ((int) (queryProgress * 100)) + "%");
                        if (queryProgress >= 1) System.err.print("\n");
                    }
                    break;
                } else {
                    if (status.getRunState() != JobStatus.RUNNING) {
                        JobStatus jobs[] = jobTracker.jobsToComplete();
                        if (MapReduceLauncher.debug) {
                            System.err.print("\rJobs queued: " + jobs.length);
                        } else {
                            double queryProgress = ((double) mrJobNumber) / ((double) numMRJobs);
                            System.err.print("\rPig progress = " + ((int) (queryProgress * 100)) + "%");
                        }
                    } else {
                        double mapProgress = status.mapProgress();
                        double reduceProgress = status.reduceProgress();
                        if (MapReduceLauncher.debug) { 
                            System.err.print("\rHadoop job progress: Map=" + (int) (mapProgress * 100) + "% Reduce="
                                    + (int) (reduceProgress * 100) + "%");
                        } else {
                            double numJobsCompleted = mrJobNumber;
                            double thisJobProgress = (mapProgress + reduceProgress) / 2.0;
                            double queryProgress = (numJobsCompleted + thisJobProgress) / ((double) numMRJobs);
                            System.err.print("\rPig progress = " + ((int) (queryProgress * 100)) + "%");
                        }
                    }
                }
            }
            success = (status == null)? false : (status.getRunState() == JobStatus.SUCCEEDED);
            
            // bug 1030028: if the input file is empty; hadoop doesn't create the output file!
            Path outputFile = conf.getOutputPath();
            String outputName = outputFile.getName();
            int colon = outputName.indexOf(':');
            if (colon != -1) {
                outputFile = new Path(outputFile.getParent(), outputName.substring(0, colon));
            }
            if (success && !dfs.exists(outputFile)) {
                // create an empty output file
                PigFile f = new PigFile(FileLocalizer.HADOOP_PREFIX + outputFile.toString(), false);
                f.store(ExecType.MAPREDUCE, new DataBag(), new PigStorage());
            }
            
            if (!success && MapReduceLauncher.debug) {
                String host = conf.get("mapred.job.tracker").split(":")[0];
                int port = conf.getInt("mapred.job.tracker.info.port", 50030);
                URL logURL = new URL("http", host, port, "/logs/hadoop-crawler-jobtracker-" + host + ".log");
                BufferedReader br = new BufferedReader(new InputStreamReader(logURL.openStream()));
                String line;
                boolean jobLine = false;
                String jid = status.getJobId();
                int underScore = jid.indexOf('_');
                String tid = "task" + jid.substring(underScore);
                while ((line = br.readLine()) != null) {
                    // This whole log processing logic is embarrassing, but this
                    // part is the most
                    // embarrassing! New log entries start with the date, so we
                    // are looking for
                    // the year 200x. (Hopefully by 2010 this wretched code will
                    // be gone!)
                    if (line.startsWith("200")) {
                        jobLine = line.indexOf(jid) != -1 || line.indexOf(tid) != -1;
                    }
                    if (jobLine)
                        System.out.println(line);
                }
            }
            
            // measure running time
            if (success) { 
                long timeSpent = 0;
              
                // NOTE: this call is crashing due to a bug in Hadoop; the bug is known and the patch has not been applied yet.
                TaskReport[] mapReports = jobTracker.getMapTaskReports(status.getJobId());
                TaskReport[] reduceReports = jobTracker.getReduceTaskReports(status.getJobId());
                for (TaskReport r : mapReports) {
                    timeSpent += (r.getFinishTime() - r.getStartTime());
                }
                for (TaskReport r : reduceReports) {
                    timeSpent += (r.getFinishTime() - r.getStartTime());
                }
                
                totalHadoopTimeSpent += timeSpent;
            }
            
        } finally {
            jobfs.delete(submitJobDir);
            jobfs.delete(submitJobFile);
            jobfs.delete(submitJarFile);
        }
        
        return success;
    }

    /**
     * The Main-Class for the Pig Jar that will provide a shell and setup a classpath appropriate
     * for executing Jar files.
     * 
     * @param args
     *            -jar can be used to add additional jar files (colon separated). - will start a
     *            shell. -e will execute the rest of the command line as if it was input to the
     *            shell.
     * @throws IOException
     */
    public static void main(String args[]) throws IOException {
        InputStream pis = MapReduceLauncher.class.getClassLoader().getResourceAsStream("properties");
        if (pis != null) {
            Properties p = new Properties(System.getProperties());
            p.load(pis);
            System.setProperties(p);
        }
        int jarIndex = 0;
        Vector<String> extraJars = new Vector<String>();
        while (jarIndex + 1 < args.length) {
            if (!args[jarIndex].equals("-jar")) {
                break;
            }
            String splits[] = args[jarIndex + 1].split(":", -1);
            for (int i = 0; i < splits.length; i++) {
                if (splits[i].length() > 0) {
                    extraJars.add(splits[i]);
                }
            }
            jarIndex += 2;
        }
        for (Iterator<String> it = extraJars.iterator(); it.hasNext(); ) {
            addJar(it.next());
        }
        if (jarIndex == args.length || args[jarIndex].equals("-h")) {
            System.err.println("USAGE: MapReduceLauncher [-jar needed_jar:needed_jar] [-cluster jobtrackerHost:Port] -|-e commands|script.pig|app.jar|-H hadoopCommand");
            System.err.println("\t- specifies that grunt commands should be read from stdin (use the help command for a list of commands");
            System.err.println("\t-e execute grunt commands following -e");
            System.err.println("\tscript.pig if the name ends with a .pig extension it will be run as a script of grunt commands");
            System.err.println("\tapp.jar runs the Main-Class of the app.jar in the pig environment");
            return;
        }

        /**
         * Needed away to specify the cluster to run the MR job on
         * Bug 831708 - fixed
         */
        if(args[jarIndex].equals("-cluster")) {
            String cluster = args[jarIndex+1];
            System.out.println("Changing MR cluster to " + cluster);
            if(cluster.indexOf(':') < 0) {
                cluster += ":50020";
            }
            conf.set("mapred.job.tracker", cluster);
            jarIndex+=2;
        }
                
        try {
            if (args[jarIndex].equals("-") || args[jarIndex].equals("-e") || args[jarIndex].endsWith(".pig")) {
                LogicalPlanBuilder.classloader = MapReduceLauncher.createCl(null);
                boolean interactive = true;
                BufferedReader in;
                if (args[jarIndex].equals("-")) {
                    interactive = true;
                    in = new BufferedReader(new InputStreamReader(System.in));
                } else if (args[jarIndex].endsWith(".pig")) {
                    interactive = false;
                    in = new BufferedReader(new FileReader(args[jarIndex]));
                } else {
                    interactive = false;
                    StringBuffer sb = new StringBuffer();
                    for (int i = jarIndex + 1; i < args.length; i++) {
                        if (i != jarIndex + 1)
                            sb.append(' ');
                        sb.append(args[i]);
                    }
                    in = new BufferedReader(new StringReader(sb.toString()));
                }
                init();
                Grunt grunt = new Grunt(in, dfs, lfs, conf, jobTracker);
                if (interactive) {
                    grunt.run();
                } else {
                    grunt.exec();
                }
            } else {
                JarFile jar = new JarFile(args[jarIndex]);
                Manifest m;
                String v;
                if (jar != null && (m = jar.getManifest()) != null
                        && (v = m.getMainAttributes().getValue("Main-Class")) != null) {
                    v = v.replaceAll("/", ".");
                    LogicalPlanBuilder.classloader = MapReduceLauncher.createCl(args[jarIndex]);
                    Class clazz = LogicalPlanBuilder.classloader.loadClass(v);
                    String newArgs[] = new String[args.length - jarIndex - 1];
                    System.arraycopy(args, jarIndex + 1, newArgs, 0, newArgs.length);
                    invokeMain(clazz, args);
                } else {
                    System.err.println("Invalid JAR file : " + args[0]);
                }
            }
        } catch (Exception e) {
            recursivePrintStackTrace(e);
        }
    }
    
    public static void addJar(String path) {
        extraJars.add(path);
    }

    /**
     * Merge one Jar file into another.
     * 
     * @param jarFile
     *            the stream of the target jar file.
     * @param jar
     *            the name of the jar file to be merged.
     * @param prefix
     *            if not null, only entries in jar that start with this prefix will be merged.
     * @param contents
     *            the current contents of jarFile. (Use to prevent duplicate entries.)
     * @throws FileNotFoundException
     * @throws IOException
     */
    private static void mergeJar(JarOutputStream jarFile, String jar, String prefix, Map<String, String> contents)
            throws FileNotFoundException, IOException {
        JarInputStream jis = new JarInputStream(new FileInputStream(jar));
        JarEntry entry;
        while ((entry = jis.getNextJarEntry()) != null) {
            if (prefix != null && !entry.getName().startsWith(prefix)) {
                continue;
            }
            addStream(jarFile, entry.getName(), jis, contents);
        }
    }

    /**
     * Prints a stack trace of an exception followed by any exceptions that are the causes of that
     * exception.
     */
    private static void recursivePrintStackTrace(Throwable e) {
        while (e != null) {
            e.printStackTrace();
            e = e.getCause();
        }
    }

    /**
     * Extract the Class that corresponds to a function.
     * 
     * @param spec
     *            a function name.
     * @return the Class of the corresponding function.
     * @throws ClassNotFoundException
     */
    private static Class extractClass(String spec) throws ClassNotFoundException {
        if (spec == null || spec.equals(""))
            return null;
        spec = spec.trim();
        return LogicalPlanBuilder.classloader.loadClass(spec);
    }

    /**
     * The jars that should not be merged in. (Some functions may come from pig.jar and we don't
     * want the whole jar file.)
     */
    static Vector<String> skipJars = new Vector<String>(2);
    static {
        String pigJar = findContainingJar(MapReduceLauncher.class);
        String hadoopJar = findContainingJar(FileSystem.class);
        if (pigJar != null) {
            skipJars.add(pigJar);
            if (!pigJar.equals(hadoopJar))
                skipJars.add(hadoopJar);
        }
    }

    /**
     * Adds the Jar file containing the given class to the list of jar files to be merged.
     * 
     * @param jarList
     *            the list of jar files to be merged.
     * @param clazz
     *            the class in a jar file to be merged.
     * @param prefix
     *            if not null, only resources from the jar file that start with this prefix will be
     *            merged.
     */
    private static void addContainingJar(Vector<MapReduceLauncher.JarListEntry> jarList, Class clazz, String prefix) {
        String jar = findContainingJar(clazz);
        if (skipJars.contains(jar) && prefix == null)
            return;
        if (jar == null)
            throw new RuntimeException("Couldn't find the jar for " + clazz.getName());
        MapReduceLauncher.JarListEntry jarListEntry = new MapReduceLauncher.JarListEntry(jar, prefix);
        if (!jarList.contains(jarListEntry))
            jarList.add(jarListEntry);
    }

    /**
     * Create a jarfile in a temporary path, that is a merge of all the jarfiles containing the
     * functions and the core pig classes.
     * 
     * @param funcs
     *            the functions that will be used in a job and whose jar files need to be included
     *            in the final merged jar file.
     * @return the temporary path to the merged jar file.
     * @throws ClassNotFoundException
     * @throws IOException
     */
    public static void createJar(OutputStream os, List<String> funcs, PigContext pigContext) throws ClassNotFoundException, IOException {
        Vector<MapReduceLauncher.JarListEntry> jarList = new Vector<MapReduceLauncher.JarListEntry>();
        addContainingJar(jarList, PigMapReduce.class, "com/yahoo/pig");
        for (String func: funcs) {
            String alias = pigContext.definedFunctions.get(func);
            if (alias != null) {
                int paren = alias.indexOf('(');
                if (paren == -1) {
                    func = alias;
                } else {
                    func = alias.substring(0, paren);
                }
            }
            Class clazz = extractClass(func);
            if (clazz != null)
                addContainingJar(jarList, clazz, null);
        }
        HashMap<String, String> contents = new HashMap<String, String>();
        JarOutputStream jarFile = new JarOutputStream(os);
        Iterator<MapReduceLauncher.JarListEntry> it = jarList.iterator();
        while (it.hasNext()) {
            MapReduceLauncher.JarListEntry jarEntry = it.next();
            mergeJar(jarFile, jarEntry.jar, jarEntry.prefix, contents);
        }
        for (int i = 0; i < extraJars.size(); i++) {
            mergeJar(jarFile, extraJars.get(i), null, contents);
        }
        if (pigContext != null) {
            jarFile.putNextEntry(new ZipEntry("pigContext"));
            new ObjectOutputStream(jarFile).writeObject(pigContext);
        }
        jarFile.close();
    }

    /**
     * Creates a Classloader based on the passed jarFile and any extra jar files.
     * 
     * @param jarFile
     *            the jar file to be part of the newly created Classloader. This jar file plus any
     *            jars in the extraJars list will constitute the classpath.
     * @return the new Classloader.
     * @throws MalformedURLException
     */
    static ClassLoader createCl(String jarFile) throws MalformedURLException {
        int len = MapReduceLauncher.extraJars.size();
        int passedJar = jarFile == null ? 0 : 1;
        URL urls[] = new URL[len + passedJar];
        if (jarFile != null) {
            urls[0] = new URL("file:" + jarFile);
        }
        for (int i = 0; i < MapReduceLauncher.extraJars.size(); i++) {
            urls[i + passedJar] = new URL("file:" + MapReduceLauncher.extraJars.get(i));
        }
        return new URLClassLoader(urls, PigMapReduce.class.getClassLoader());
    }

    /**
     * Extra jar files that are needed to run a Job.
     */
    static List<String> extraJars = new LinkedList<String>();
    
    /**
     * The DFS shell to use for DFS operation in the interactive shell.
     */
    // static private DFSShell dfsShell;
    static private FileSystem            dfs;
    static private FileSystem            lfs;
    static private Configuration         conf        = new Configuration();
    static private JobSubmissionProtocol jobTracker;
    static private boolean               initialized = false;

    /**
     * The client used to submit hadoop jobs.
     */
    public static boolean                       debug       = false;
    
    static void init() {
        if (!initialized) {
            String g = System.getProperty("ssh.gateway");
            if (g != null && g.length() > 0) {
        	try {
                    Class clazz = Class.forName("com.yahoo.shock.SSHSocketImplFactory");
		    Socket.setSocketImplFactory((SocketImplFactory)clazz.newInstance());
		} catch (Exception e) {
		    e.printStackTrace();
		}
            }
            try {
                lfs = FileSystem.getNamed("local", conf);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                dfs = FileSystem.get(conf);
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                jobTracker = (JobSubmissionProtocol) RPC.getProxy(JobSubmissionProtocol.class,
                        JobSubmissionProtocol.versionID, JobTracker.getAddress(conf), conf);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void rename(String oldName, String newName) throws IOException {
        System.out.println("Renaming " + oldName + " to " + newName);
        Path dst = new Path(newName);
        if (dfs.exists(dst))
            dfs.delete(dst);
        dfs.rename(new Path(oldName), dst);
    }

    public static void copy(String src, String dst, boolean localDst) throws IOException {
        FileUtil.copy(dfs, new Path(src), (localDst ? lfs : dfs), new Path(dst), false, conf);
    }

    public static FileSystem getDfs() {
        if (dfs == null)
            init();
        return dfs;
    }

    public static FileSystem getLfs() {
        if (lfs == null)
            init();
        return lfs;
    }

    public static JobSubmissionProtocol getJobTracker() {
        if (jobTracker == null) {
            init();
        }
        return jobTracker;
    }

    public static Configuration getConf() {
        return conf;
    }

    public static void setJobtrackerLocation(String newLocation) {
        conf.set("mapred.job.tracker", newLocation);
    }

    public static void setFilesystemLocation(String newLocation) {
        conf.set("fs.default.name", newLocation);
    }

}
