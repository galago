/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;


public class DataBagFileWriter {
	File store;
	DataOutputStream out;

	public DataBagFileWriter(File store) throws IOException{
		this.store = store;
		out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(store)));
	}
	
	public void write(Tuple t) throws IOException{
		t.write(out);
	}
	
	public void write(Iterator<Tuple> iter) throws IOException{
		while (iter.hasNext())
			iter.next().write(out);
	}
	
	public void close() throws IOException{
		flush();
		out.close();
	}
	
	public void flush() throws IOException{
		out.flush();
	}
	
}
