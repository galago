/*
 * PositionPostingsToCounts
 *
 * August 21, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentWordPosition;
import galago.types.WordCount;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.types.DocumentWordPosition")
@OutputClass(className="galago.types.WordCount")
public class PositionPostingsToCounts extends StandardStep<DocumentWordPosition, WordCount> {
    private HashMap<String, Integer> counts = new HashMap();
    
    public void process( DocumentWordPosition object ) throws IOException {
        String word = Utility.makeString(object.word);
        
        if( counts.containsKey(word) ) {
            counts.put( word, counts.get(word)+1 );
        } else {
            counts.put( word, 1 );
        }

        if( counts.size() > 10000 )
            flush();
    }
    
    private void flush() throws IOException {
        for( String key : counts.keySet() ) {
            processor.process( new WordCount( key, counts.get(key), 1 ) );
        }
        counts = new HashMap();
    }
    
    public void close() throws IOException {
        flush();
    }
}