/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;
import java.util.Iterator;

import com.yahoo.pig.AlgebraicAtomEvalFunc;
import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.TupleEvalFunc;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

/**
 * Generates the sum of the values of the first field of a tuple.
 */
public class SUM extends AlgebraicAtomEvalFunc {

    @Override
    public void exec(Tuple input, DataAtom output) throws IOException {
        output.setValue(sum(input));
    }

    public TupleEvalFunc getInitial() {
        return new Initial();
    }

    public TupleEvalFunc getIntermed() {
        return new Initial();
    }

    public AtomEvalFunc getFinal() {
        return new Final();
    }

    static public class Initial extends TupleEvalFunc {
        public void exec(Tuple input, Tuple output) throws IOException {
            output.appendField(new DataAtom(sum(input)));
        }
    }
    static public class Final extends AtomEvalFunc {
        public void exec(Tuple input, DataAtom output) throws IOException {
            output.setValue(sum(input));
        }
    }

    static protected double sum(Tuple input) throws IOException {
        DataBag values = input.getBagField(0);

        double sum = 0;
        for (Iterator it = values.content(); it.hasNext();) {
            Tuple t = (Tuple) it.next();
            try {
            sum += t.getAtomField(0).numval();
            }catch(RuntimeException exp) {
                throw new RuntimeException(exp.getMessage() + " error processing: " + t.toString());
            }
        }

        return sum;
    }
    @Override
    public SchemaItem outputSchema() {
        return new SchemaField("sum");
    }
}
