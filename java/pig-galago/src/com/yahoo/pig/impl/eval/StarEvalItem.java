/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

final public class StarEvalItem extends EvalItem {

	public StarEvalItem(PigContext pigContext) {
		super(pigContext);
	}
    public String toString() {
        return "*";
    }

    public List<String> getFuncs() {
        return new ArrayList<String>();
    }
    
    public boolean isSimple() {
        return true;
    }

    public String stringVal(Tuple tup) throws IOException {
        throw new IOException("Internal error: illegal to call stringVal() on StarEvalItem.");
    }
    
    public Double doubleVal(Tuple tup) throws IOException {
        throw new IOException("Internal error: illegal to call doubleVal() on StarEvalItem.");
    }

    public void eval(Tuple input, DataCollector output) throws IOException {
        output.add(input);
        output.add(null);  // EOF
    }

    @Override
    public Datum simpleEval(Tuple input) throws IOException {
        return input;
    }
    
    @Override
    public SchemaItem mapInputSchema(SchemaItem input){
    	if (schema!=null)
    		return schema;
    	return input;
    }


}
