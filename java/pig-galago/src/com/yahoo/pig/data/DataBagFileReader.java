/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

public class DataBagFileReader {
	File store;
	
	public DataBagFileReader(File f) throws IOException{
		store = f;
	}
	
	private class myIterator implements Iterator<Tuple>{
		DataInputStream in;
		Tuple nextTuple;
		
		public myIterator() throws IOException{
			in = new DataInputStream(new BufferedInputStream(new FileInputStream(store)));
			getNextTuple();
		}
		
		private void getNextTuple() throws IOException{
			try{
				nextTuple = new Tuple();
		        nextTuple.readFields(in);
			} catch (EOFException e) {
				in.close();
				nextTuple = null;
			}
		}
		
		public boolean hasNext(){
			return nextTuple != null;
		}
		
		public Tuple next(){
			Tuple returnValue = nextTuple;
			if (returnValue!=null){
				try{
					getNextTuple();
				}catch (IOException e){
					throw new RuntimeException(e.getMessage());
				}
			}
			return returnValue;
		}
		
		public void remove(){
			throw new RuntimeException("Read only cursor");
		}
	}

	public Iterator<Tuple> content() throws IOException{
		return new myIterator();		
	}
	
	public void clear() throws IOException{
		store.delete();
	}
}
