/*
 * DocumentURLExtractor
 *
 * May 2, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentURL;
import java.io.IOException;

/**
 * Extracts just the URL part of a document.  The URL is assumed to be in the
 * metadata of the document, and is retrieved by document.metadata.get("url").
 * The identifier of the document is retrieved too.
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.types.DocumentURL")
public class DocumentURLExtractor extends StandardStep<Document, DocumentURL> {
    public void process( Document document ) throws IOException {
        String url = document.metadata.get("url");
        
        if( url == null )
            return;
        
        processor.process( new DocumentURL( document.identifier, url ) );
    }
}
