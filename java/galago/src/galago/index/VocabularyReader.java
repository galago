//
// VocabularyReader.java
//
// 28 November 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.index;

import galago.Utility;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class VocabularyReader {
    public static class TermSlot {
        public byte[] termData;
        public long begin;
        public long length;
    }

    ArrayList<TermSlot> slots;
    
    /** Creates a new instance of DocumentNameReader */
    public VocabularyReader( String filename, long invertedFileLength ) throws IOException {
        FileInputStream f = new FileInputStream( filename );
        DataInputStream input = new DataInputStream( new BufferedInputStream( f ) );
        slots = new ArrayList<TermSlot>();
        read( invertedFileLength, input );
    }
    
    public ArrayList<TermSlot> getSlots() {
        return slots;
    }
    
    public void read( long invertedFileLength, DataInputStream input ) throws IOException {
        long last = 0;
        
        while( input.available() > 0 ) {
            short length = input.readShort();
            byte[] data = new byte[length];
            input.read(data);
            long offset = input.readLong();
            TermSlot slot = new TermSlot();
            
            if( slots.size() > 0 )
                slots.get(slots.size()-1).length = offset - last;
            
            slot.begin = offset;
            slot.termData = data;
            slots.add(slot);
            
            last = offset;
        }
        
        if( slots.size() > 0 )
            slots.get(slots.size()-1).length = invertedFileLength - last;
        
        input.close();
        assert invertedFileLength >= last;
    }

    public TermSlot get( String object ) {
        if( slots.size() == 0 )
            return null;
        
        int big = slots.size()-1;
        int small = 0;
        
        while( big-small > 1 ) {
            int middle = small + (big-small)/2;
            String middleString = new String(slots.get(middle).termData);
            
            if (middleString.compareTo(object) <= 0)
                small = middle;
            else
                big = middle;
        }

        TermSlot one = slots.get(small);
        TermSlot two = slots.get(big);
        String twoString = new String(two.termData);
        
        if (twoString.compareTo(object) <= 0)
            return two;
        else
            return one;
    }
}
