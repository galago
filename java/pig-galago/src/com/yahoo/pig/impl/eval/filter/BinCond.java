/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.List;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalItem;
import com.yahoo.pig.impl.eval.NestableEvalItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class BinCond extends NestableEvalItem {
    public Cond cond;
    public EvalItem ifTrue, ifFalse;
    
    public BinCond(PigContext pigContext, Cond cond, EvalItem ifTrue, EvalItem ifFalse) {
    	super(pigContext);
        this.cond = cond;
        this.ifTrue = ifTrue;
        this.ifFalse = ifFalse;
    }

    public List<String> getFuncs() {
        List<String> funcs = cond.getFuncs();
        funcs.addAll(ifTrue.getFuncs());
        funcs.addAll(ifFalse.getFuncs());
        return funcs;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        sb.append(cond);
        sb.append(" ? ");
        sb.append(ifTrue);
        sb.append(" : ");
        sb.append(ifFalse);
        sb.append(")");
        return sb.toString();        
    }
    
    public Double doubleVal(Tuple t) throws IOException{
    	if (cond.eval(t)){
    		return ifTrue.doubleVal(t);
    	}else{
    		return ifFalse.doubleVal(t);
    	}
    }
    
    public boolean isSimple(){
    	return ifTrue.isSimple() && ifFalse.isSimple();
    }
    
    public void eval(Tuple input, DataCollector output) throws IOException{
    	if (cond.eval(input)){
    		ifTrue.eval(input, output);
    	}else{
    		ifFalse.eval(input, output);
    	}
    }
    
    public Datum simpleEval(Tuple input) throws IOException{
    	if (cond.eval(input)){
    		return ifTrue.simpleEval(input);
    	}else{
    		return ifFalse.simpleEval(input);
    	}
    }
    
    public String stringVal(Tuple input) throws IOException{
    	if (cond.eval(input))
    		return ifTrue.stringVal(input);
    	else
    		return ifFalse.stringVal(input);
    }
    
    @Override
    public SchemaItem mapInputSchemaInitial(SchemaItem input){
    	return new SchemaItemList();
    }

    
    
    

    
}
