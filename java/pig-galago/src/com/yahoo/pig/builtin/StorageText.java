/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.PigServer;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.io.InputStreamPosition;

/**
 * This load function simply creates a tuple for each line of text that has a single field that
 * contains the line of text.
 */
public class StorageText extends StorageFunc {
    BufferedReader      fr;
    InputStream         fsis;
    InputStreamPosition posInputStream;
    long                end;

    public void bindTo(InputStream is, long offset, long end) throws IOException {
        posInputStream = new InputStreamPosition(is, offset);
        fr = new BufferedReader(new InputStreamReader(posInputStream));
        // Since we are not block aligned we throw away the first
        // record and cound on a different instance to read it
        if (offset != 0)
            getNext();
        this.end = end;
        this.fsis = is;
    }

    public Tuple getNext() throws IOException {
        if (fsis != null && posInputStream.getPosition() > end)
            return null;
        String line;
        if ((line = fr.readLine()) != null) {
            Tuple t = new Tuple(1);
            t.setField(0, new DataAtom(line));
            return t;
        }

        return null;
    }

    OutputStream os;
	@Override
	public void bindTo(OutputStream os) throws IOException {
		this.os = os;
	}

	@Override
	public void done() throws IOException {
	}

	@Override
	public void putNext(Tuple f) throws IOException {
		os.write(f.toDelimitedString(", ").getBytes());
	}
}
