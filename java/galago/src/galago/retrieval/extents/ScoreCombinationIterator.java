/*
 * ScoreCombinationIterator
 *
 * August 17, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import galago.Utility;
import galago.tupleflow.Parameters;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class ScoreCombinationIterator implements ScoreIterator {
    ArrayList<ScoreIterator> iterators;
    boolean done;
    boolean requireAll;

    public ScoreCombinationIterator( Parameters parameters, ArrayList<DocumentDataIterator> childIterators ) {
        this.iterators = Utility.checkedCast( childIterators, ScoreIterator.class );
        this.requireAll = parameters.get( "requireAll", false );
    }
    
    int nextCandidateAny() {
        int candidate = Integer.MAX_VALUE;
        
        for( ScoreIterator iterator : iterators ) {
            if( iterator.isDone() )
                continue;
            
            candidate = Math.min( candidate, iterator.nextCandidate() );
        }
        
        return candidate;
    }
    
    int nextCandidateFilter() {
        int candidate = 0;
        
        for( ScoreIterator iterator : iterators ) {
            if( iterator.isDone() )
                return Integer.MAX_VALUE;
            
            candidate = Math.max( candidate, iterator.nextCandidate() );
        }
        
        return candidate;
    }
    
    public int nextCandidate() {
        if( requireAll )
            return nextCandidateFilter();
        else
            return nextCandidateAny();
    }
    
    boolean hasMatchFilter( int document ) {
        for( ScoreIterator iterator : iterators ) {
            if( iterator.isDone() || !iterator.hasMatch( document ) )
                return false;
        }
        
        return true;
    }
    
    boolean hasMatchAny( int document ) {
        for( ScoreIterator iterator : iterators ) {
            if( !iterator.isDone() && iterator.hasMatch( document ) )
                return true;
        }
        
        return false;
    }
    
    public boolean hasMatch( int document ) {
        if( requireAll )
            return hasMatchFilter( document );
        else
            return hasMatchAny( document );
    }
    
    public double score( int document, int length ) {
        float total = 0;
        
        for( ScoreIterator iterator : iterators )
            total += iterator.score( document, length );
        
        return total;
    }
    
    public void movePast( int document ) throws IOException {
        for( ScoreIterator iterator : iterators )
            iterator.movePast( document );
    }
    
    public void moveTo( int document ) throws IOException {
        for( ScoreIterator iterator : iterators )
            iterator.moveTo( document );
    }
    
    public boolean isDone() {
        for( ScoreIterator iterator : iterators ) {
            if( !iterator.isDone() )
                return false;
        }
        
        return true;
    }
    
    public void reset() throws IOException {
        for( ScoreIterator iterator : iterators ) {
            iterator.reset();
        }
    }
}
