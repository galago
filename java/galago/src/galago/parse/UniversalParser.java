/*
 * UniversalParser
 *
 * March 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.FileName;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author trevor
 */
@Verified
@InputClass(className="galago.types.FileName")
@OutputClass(className="galago.parse.Document")
public class UniversalParser extends StandardStep<FileName, Document> {
    public void process( FileName fileName ) throws IOException {
        String collectionFilename = fileName.filename;
        DocumentStreamParser parser;
        
        if( collectionFilename.contains(".arc.") || collectionFilename.endsWith(".arc") ) {
            parser = new ArcParser( collectionFilename );
        } else if( collectionFilename.contains(".dat.") || collectionFilename.endsWith(".dat") ) {
            parser = new TrecTextParser( collectionFilename );
        } else {
            parser = new TrecWebParser( collectionFilename );
        }
        
        Logger logger = Logger.getLogger(UniversalParser.class.toString());
        Document document;
        int count = 0;
        
        logger.info( "Starting " + collectionFilename );
        
        while( (document = parser.nextDocument()) != null ) {
            processor.process( document );
            
            if( (++count % 5000) == 0 ) {
                logger.info( "Documents: " + count );
            }
        }
        
        logger.info( "Finished (" + count + ") " + collectionFilename );
    }
}
