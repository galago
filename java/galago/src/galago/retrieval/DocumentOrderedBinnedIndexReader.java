/*
 * DocumentOrderedFeatureIndex
 *
 * August 8, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.index.DocumentTransformationFactory;
import galago.tupleflow.Processor;
import galago.tupleflow.VByteInput;
import galago.parse.Document;
import galago.tupleflow.BufferedDataStream;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

/**
 *
 * @author trevor
 */
public class DocumentOrderedBinnedIndexReader implements DocumentOrderedBinnedListReader {
    IndexReader reader;

    public class DocumentOrderedBinnedIndexIterator implements DocumentOrderedIterator {
        IndexReader.Iterator iterator;
        DataInputStream stream;
        VByteInput dataStream;

        VByteInput skips;
        VByteInput postings;
        
        BufferedDataStream skipsStream;
        BufferedDataStream postingsStream;
        
        long documentCount;
        long options;
        long skipLength;
        long dataLength;
        boolean done;
        
        /// Current score.
        int score;
        
        /// Current document.
        int document;
        
        /// Last skip document.
        int lastSkipDocument;
        
        /// Last skip byte position.
        long lastSkipByteOffset;
        
        /// Current skip document
        int currentSkipDocument;
        
        /// Current skip byte position
        int currentSkipByteOffset;
                
        /// No documents in this skip region have a score larger than this bound.
        int lastScoreBound;
        int currentScoreBound;
        int listBound;
        
        boolean hasSkips;
                
        public DocumentOrderedBinnedIndexIterator( IndexReader.Iterator iterator ) throws IOException {
            this.iterator = iterator;
            
            score = 0;
            document = 0;
            lastSkipDocument = 0;
            lastSkipByteOffset = 0;
            
            currentSkipDocument = 0;
            currentSkipByteOffset = 0;
            done = false;
            
            load();

            readSkip();
            readDocument();
        }
        
        void load() throws IOException {
            // how long might the header be?  allocate 10 bytes per possible read = 40 bytes
            long listStart = iterator.getListStart();
            long listEnd = iterator.getListEnd();
            long maxHeaderLength = Math.min( 40, listEnd - listStart );

            BufferedDataStream headerStream = reader.blockStream( listStart, maxHeaderLength );
            VByteInput header = new VByteInput( headerStream );

            options = header.readLong();
            documentCount = header.readLong();
            listBound = header.readInt();
            
            if( (options & 1) > 0 ) {
                skipLength = header.readLong();
                hasSkips = true;
            } else {
                skipLength = 0;
                hasSkips = false;
            }
            
            dataLength = header.readLong();
            
            long currentLocation = headerStream.getRelativePosition() + listStart;
            
            skipsStream = reader.blockStream( currentLocation, skipLength );
            postingsStream = reader.blockStream( currentLocation + skipLength, dataLength );

            skips = new VByteInput( skipsStream );
            postings = new VByteInput( postingsStream );
            done = false;
        }

        public void nextList() throws IOException {
            iterator.nextTerm();
            load();
            readSkip();
            readDocument();
        }

        public void printList( PrintStream stream ) throws IOException {
            load();

            stream.println( "# 'term', term, options, documentCount, listBound, skipLength, dataLength" );
            stream.format( "term,%s,%d,%d,%d,%d,%d\n", iterator.currentTerm(), options,
                                                           documentCount, listBound, skipLength, dataLength );

            while( !skipsStream.isDone() ) {
                readSkip();
                stream.format( "skip,%d,%d,%d\n", lastSkipDocument, lastSkipByteOffset, lastScoreBound );
            }                                               
            
            if( skipLength > 0 ) {
                stream.format( "skip,%d,%d,%d\n", currentSkipDocument, currentSkipByteOffset, currentScoreBound );
            }
            
            while( !postingsStream.isDone() ) {
                readDocument();
                stream.format( "%d,%d\n", document, score );
            }
        }
        
        public long listByteLength() {
            return iterator.getListEnd() - iterator.getListStart();
        }
        
        public int listBound() {
            return listBound;
        }
        
        public int currentScore() {
            return score;
        }
        
        public int currentDocument() {
            return document;
        }
        
        public boolean skipToBound( int bound ) throws IOException {
            while( hasSkips && !done && currentScoreBound < bound && currentSkipDocument != Integer.MAX_VALUE )
                readSkip();

            if( document < lastSkipDocument )
                moveToSkip();
            
            while( !done && score < bound )
                readDocument();
            
            return !done && score < bound;
        }
        
        public boolean skipToDocument( int skipTo ) throws IOException {
            while( hasSkips && !done && skipTo > currentSkipDocument )
                readSkip();

            if( skipTo > lastSkipDocument && document < lastSkipDocument )
                moveToSkip();
            
            while( !done && skipTo > document )
                readDocument();
            
            return !done && document == skipTo;
        }
        
        public boolean skipToDocument( int skipTo, int bound ) throws IOException {
            while( hasSkips && !done && skipTo > currentSkipDocument )
                readSkip();

            if( hasSkips && currentScoreBound < bound )
                return false;
            
            while( !done && skipTo > document )
                readDocument();
            
            return !done && document == skipTo && score >= bound;
        }
        
        public void nextDocument() throws IOException {
            readDocument();
        }

        public boolean isDone() {
            return done;
        }
        
        /**
         * Reads the next skip data, seeks postings stream to 
         * the previous seek point.
         */
        
        private void readSkip() throws IOException {
            if( skipLength == 0 ) {
                currentSkipDocument = Integer.MAX_VALUE;
                return;
            }
            
            lastScoreBound = currentScoreBound;
            lastSkipDocument = currentSkipDocument;
            lastSkipByteOffset = currentSkipByteOffset;
            
            if( skipsStream.isDone() ) {
                currentScoreBound = 0;
                currentSkipDocument = Integer.MAX_VALUE;
                currentSkipByteOffset = (int) dataLength;
            } else {
                lastScoreBound = currentScoreBound;
                lastSkipDocument = currentSkipDocument;
                lastSkipByteOffset = currentSkipByteOffset;

                currentScoreBound = skips.readInt();
                currentSkipDocument += skips.readInt();
                currentSkipByteOffset += skips.readLong();
            }
        }
        
        private void moveToSkip() throws IOException {
            if( lastSkipDocument == Integer.MAX_VALUE ) {
                done = true;
                return;
            }

            if( lastSkipByteOffset > postingsStream.getRelativePosition() )
                postingsStream.seekRelative( lastSkipByteOffset );

            document = lastSkipDocument;
            score = 0;
        }

        private void readDocument() throws IOException {
            if( postingsStream.isDone() ) {
                done = true;
            } else {
                document += postings.readInt();
                score = postings.readInt();
            }
        }
    }
    
    public DocumentOrderedBinnedIndexReader( String pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }

    public DocumentOrderedIterator getIterator() throws IOException {
        return new DocumentOrderedBinnedIndexIterator( reader.getIterator() );
    }

    public DocumentOrderedIterator getTerm( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new DocumentOrderedBinnedIndexIterator( iterator );
        
        return null;
    }
    
    public long maximumDocumentCount() {
        return reader.getManifest().get( "maximumDocumentCount", 1 );
    }
    
    public boolean needsQueryWeights() {
        return reader.getManifest().get("needsWeights", false);
    }
    
    List< Processor<Document> > transformations() {
        return DocumentTransformationFactory.instance( reader.getManifest() );
    }

    public void close() throws IOException {
        reader.close();
    }    
}
