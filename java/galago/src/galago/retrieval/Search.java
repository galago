/*
 * Search
 *
 * June 9, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.parse.Document;
import galago.retrieval.query.Node;
import galago.retrieval.query.SimpleQuery;
import galago.retrieval.query.StructuredQuery;
import galago.store.DocumentStore;
import galago.store.SnippetGenerator;
import galago.tupleflow.Parameters;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author trevor
 */
public class Search {
    SnippetGenerator generator;
    DocumentStore store;
    Retrieval retrieval;

    public Search( String index, String databaseUrl, String driverName ) throws SQLException, IOException, ClassNotFoundException {
        store = new DocumentStore( driverName, databaseUrl );
        generator = new SnippetGenerator();
        retrieval = Retrieval.instance( index );
    }

    public void close() throws IOException {
        store.close();
        retrieval.close();
    }
    
    public static class SearchResult {
        public List<SearchResultItem> items;
    }
    
    public static class SearchResultItem {
        public int rank;
        public String identifier;
        public String displayTitle;
        public String url;
        public Map<String, String> metadata;
        public String summary;
    }
    
    public String getSummary( Document document, String query ) throws IOException {
        if( document.metadata.containsKey( "description" ) ) {
            String description = document.metadata.get( "description" );
            
            if( description.length() > 10 ) {
                return generator.highlight( description, query );
            }
        }
        
        return generator.getSnippet( document.text, query );
    }
    
    public static Node parseQuery( String query, Parameters parameters ) {
        String queryType = parameters.get( "queryType", "complex" );
        
        if( queryType.equals( "simple" ) ) {
            return SimpleQuery.parseTree( query );
        }
        
        return StructuredQuery.parse( query );
    }
    
    public SearchResult runQuery( String query, int startAt, int count, boolean summarize ) throws Exception {
        Node tree = parseQuery( query, new Parameters() );
        ScoredDocument[] results = retrieval.runQuery( tree, startAt + count );
        SearchResult result = new SearchResult();
        result.items = new ArrayList();
        
        for( int i=startAt; i < Math.min(startAt + count, results.length); i++ ) {
            String identifier = retrieval.getDocument( results[i].document );
            Document document = store.get( "identifier", identifier );
            int rank = i+1;
            
            SearchResultItem item = new SearchResultItem();
            
            item.rank = i+1;
            item.identifier = identifier;
            item.displayTitle = identifier;
            
            if( document.metadata.containsKey( "title" ) ) {
                item.displayTitle = generator.highlight( document.metadata.get( "title" ), query );
            }
            
            if( document.metadata.containsKey( "url" ) ) {
                item.url = document.metadata.get( "url" );
            }
            
            if( summarize ) {
                item.summary = getSummary( document, query );
            }

            item.metadata = document.metadata;
            result.items.add(item);
        }
        
        return result;
    }
}
