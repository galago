/*
 * TemplateTypeBuilder
 *
 * May 23, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow;

import galago.Utility;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import org.antlr.stringtemplate.CommonGroupLoader;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;
import org.antlr.stringtemplate.language.AngleBracketTemplateLexer;

/**
 *
 * @author trevor
 */
public class TemplateTypeBuilder {
    StringTemplateGroup template;
    
    String typeName;
    String typePackage;
    ArrayList<Field> typeFields;
    ArrayList<OrderSpec> typeOrders;
    
    public static class Field {
        public Field( String type, String name ) {
            this.type = type;
            this.name = name;
            this.capsName = Utility.caps(name);
            this.inputType = Utility.caps(type);
            
            if( inputType.endsWith( "[]" ) ) {
                inputType = inputType.substring( 0, inputType.length()-2 );
                inputType = Utility.plural( inputType );
            }
            
            isInteger = (type.equals("int") || type.equals("long"));
            isObject = (type.equals("String"));
            isString = (type.equals("String"));
            this.pluralName = Utility.plural(name);
            this.baseType = type;
            
            if( baseType.endsWith( "[]" ) ) {
                baseType = baseType.substring( 0, baseType.length()-2 );
                isArray = true;
            }
            
            if( type.equals("long") || type.equals("double") || type.equals("float") )
                classTypeName = Utility.caps(type);
            else if( type.equals("int") )
                classTypeName = "Integer";
            else
                classTypeName = type;
        }
                
        public String type;
        public String name;
        public String baseType;
        
        public boolean isInteger;
        public boolean isString;
        public boolean isObject;
        public boolean isArray;
        
        public String classTypeName;
        public String capsName;
        public String inputType;
        public String pluralName;
    }
    
    public static class OrderedField extends Field {
        public OrderedField( Field field, boolean ascending, boolean delta, boolean runLengthEncoded ) {
            this(field.type, field.name, ascending, delta, runLengthEncoded);
        }
        
        public OrderedField( String type, String name, boolean ascending, boolean delta, boolean runLengthEncoded ) {
            super(type, name);
            this.ascending = ascending;
            this.direction = ascending ? "+" : "-";
            this.directionName = ascending ? "Ascending" : "Descending";
            this.runLengthEncoded = runLengthEncoded;
            this.delta = delta;
        }
        
        public boolean ascending;
        public String direction;
        public String directionName;
        public boolean delta;
        public boolean runLengthEncoded;
        public ArrayList<OrderedField> remaining = new ArrayList();
    }

    public static class OrderSpec {
        public OrderSpec( String spec, ArrayList<Field> allFields ) {
            this.allFields = allFields;
            
            HashMap<String, Field> fieldMap = new HashMap();
            HashSet<String> orderedNames = new HashSet();

            for( Field f : allFields ) {
                fieldMap.put( f.name, f );
            }
            
            if( spec.length() != 0 ) {
                // this is a messy hack of parsing, unfortunately
                // should be cleaned up later
                String[] fieldNames = spec.substring(1).split( "[+-]", 0 );
                
                boolean[] ascending = new boolean[fieldNames.length];
                int j = 0;        

                for( int i=0; i<spec.length(); i++ ) {
                    char c = spec.charAt(i);
                    if (c == '+') {
                        ascending[j] = true;
                        j++;
                    } else if(c == '-') {
                        ascending[j] = false;
                        j++;
                    }
                }
                
                for( int i=0; i<fieldNames.length; i++ ) {
                    Field field = fieldMap.get(fieldNames[i]);
                    boolean isLastField = ((fieldNames.length - i) == 1);
                    // BUGBUG: this is where delta encoding should go
                    boolean useDelta = false; //isLastField && (field.isInteger || field.isString); 
                    boolean useRLE = !useDelta;
                    
                    OrderedField ordered = new OrderedField( fieldMap.get(fieldNames[i]), ascending[i], useDelta, useRLE );
                    orderedFields.add(ordered);
                    orderedNames.add(fieldNames[i]);
                }
                
                for( int i=0; i<fieldNames.length; i++ ) {
                    orderedFields.get(i).remaining.addAll( orderedFields.subList(i+1, orderedFields.size()) );
                }
                
                backwardOrderedFields.addAll(orderedFields);
                Collections.reverse(backwardOrderedFields);
                
                for( int i=0; i<orderedFields.size(); i++ ) {
                    if( orderedFields.get(i).runLengthEncoded )
                        rleFields.add( orderedFields.get(i) );
                    if( orderedFields.get(i).delta )
                        deltaFields.add( orderedFields.get(i) );
                }
                
                for( int i=0; i<orderedFields.size(); i++ ) {
                    OrderedField current = orderedFields.get(i);
                    OrderedField next = ((i+1) < orderedFields.size()) ? orderedFields.get(i+1) : null;
                    OrderedField previous = (i != 0) ? orderedFields.get(i-1) : null;
                    
                    fieldPairs.add( new FieldPair( current, next, previous ) );
                }
            }
        
            for( int i=0; i<allFields.size(); i++ ) {
                if( orderedNames.contains(allFields.get(i).name) )
                    continue;
                unorderedFields.add( allFields.get(i) );
            }
        }
        
        public String getClassName() {
            StringBuilder builder = new StringBuilder();
            
            if( orderedFields.size() > 0 ) {
                for( OrderedField orderedField : orderedFields ) {
                    if( !orderedField.ascending )
                        builder.append( "Desc" );
                    builder.append( Utility.caps(orderedField.name) );
                }

                builder.append( "Order" );
            } else {
                builder.append( "Unordered" );
            }
            
            return builder.toString();
        }
        
        public static class FieldPair {
            public FieldPair( OrderedField c, OrderedField n, OrderedField p ) {
                current = c;
                next = n;
                previous = p;
            }
            
            public OrderedField current;
            public OrderedField next;
            public OrderedField previous;
        }
        
        public ArrayList<OrderedField> orderedFields = new ArrayList();
        public ArrayList<OrderedField> backwardOrderedFields = new ArrayList();
        public ArrayList<FieldPair> fieldPairs = new ArrayList();
        public ArrayList<OrderedField> rleFields = new ArrayList();
        public ArrayList<Field> unorderedFields = new ArrayList();
        public ArrayList<OrderedField> deltaFields = new ArrayList();
        public ArrayList<Field> allFields;
    }
    
    /**
     * Returns the text of a Type<T> object for the T class.
     */
    
    public String toString() {
        StringTemplate t = template.getInstanceOf( "type" );
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put( "typeName", this.typeName );
        map.put( "package", this.typePackage );
        map.put( "orders", this.typeOrders );
        map.put( "fields", this.typeFields );
        
        boolean containsArray = false;
        for( Field f : typeFields ) {
            containsArray = containsArray || f.isArray;
        }
        map.put( "containsArray", containsArray );
        t.setAttributes( map );
        return t.toString();
    }
    
    /** 
     * Prints usage information for the main method.
     */
    
    public static void usage() {
        System.err.println( "Usage: tupleflow.TypeBuilder <OutputFile> <TypeName> [ <TypeSpec> ]+" );
        System.err.println( "      OutputFile: path to the file where output should be written" );
        System.err.println( "      TypeName: name of the base class we're building a type for" );
        System.err.println( "      TypeSpec: the name and type of one of the fields of the base class, ");
        System.err.println( "                separated by a colon, like this:  String:name" );
        System.err.println();
        System.err.println( "      Example: java galago.tupleflow.TemplateTypeBuilder /tmp/WordCountType.java WordCount galago.types String:term int:count" );
        System.err.println( "          This builds a type for the following Java class:" );
        System.err.println( "          public class WordCount {" );
        System.err.println( "              String term;" );
        System.err.println( "              int count;" );
        System.err.println( "          }");
    }
    
    /** 
     * Prints usage information for the main method, then exits with an error code.
     */
    
    public static void usageAndExit() {
        usage();
        System.exit(-1);
    }
    
    public TemplateTypeBuilder( String name, String typePackage, ArrayList<Field> fields, ArrayList<OrderSpec> orders ) throws FileNotFoundException {
        CommonGroupLoader loader = new CommonGroupLoader( "galago/tupleflow/templates", null );
        StringTemplateGroup.registerGroupLoader(loader);    
        StringTemplateGroup.registerDefaultLexer(AngleBracketTemplateLexer.class);
        this.template = StringTemplateGroup.loadGroup( "GalagoType" );
        
        this.typeName = name;
        this.typeFields = fields;
        this.typeOrders = orders;
        this.typePackage = typePackage;
    }
    
    public static void main( String[] args ) throws java.lang.Exception {
        String outputFile = args[0];
        String typeName = args[1];
        String typePackage = args[2];
        ArrayList<Field> fields = new ArrayList();
        ArrayList<OrderSpec> orders = new ArrayList();
        
        if( args.length < 3 )
            usageAndExit();
        
        String[] spec = Utility.subarray( args, 3 );
        
        for( int i=0; i<spec.length; i++ ) {
            String[] typeAndName = spec[i].split(":");
            
            if( typeAndName.length != 2 && !(typeAndName.length==1 && typeAndName[0].equals("order")) )
                usageAndExit();
            
            String type = typeAndName[0];
            String name = "";
            
            if( typeAndName.length > 1 )
                name = typeAndName[1];
            
            if( type.equals( "order" ) ) {
                orders.add( new OrderSpec(name, fields) );
            } else if( orders.size() > 0 ) {
                throw new Exception( "Only orders are allowed at the end of the command line (found type = '" + type + "')" );
            } else {
                if( type.equals( "bytes" ) ) {
                    fields.add( new Field( "byte[]", name ) );
                } else if( type.equals( "String" ) ) {
                    fields.add( new Field( type, name ) );
                } else if( type.equals( "boolean" ) || type.equals( "int" ) || type.equals( "short" ) || type.equals( "byte" ) || type.equals( "long" ) ) {
                    fields.add( new Field( type, name ) );
                } else if( type.equals( "float" ) || type.equals( "double" ) ) {
                    fields.add( new Field( type, name ) );
                } else {
                    throw new Exception( "Unknown field type: " + type );
                } 
            }
        }
                
        TemplateTypeBuilder builder = new TemplateTypeBuilder( typeName, typePackage, fields, orders );
        java.io.FileWriter writer = new java.io.FileWriter( outputFile );
        
        String comment = "// This file was automatically generated with the command: \n" +
                         "//     java galago.tupleflow.TemplateTypeBuilder %s\n";
        
        writer.write( String.format(comment, Utility.join(args)) ); 
        writer.write( builder.toString() );
        writer.close();
    }
}
