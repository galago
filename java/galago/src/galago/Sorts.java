/*
 * Sorts
 *
 * December 11, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * These are some sort algorithms that can be used as replacements for
 * Collections.sort.
 * 
 * These methods are kept in case someone wants to use them.  The
 * quicksort implementation doesn't seem to be any faster than the
 * default sort, but the multiWaySort can be 35% faster for very large
 * lists if you use the right number of pivots (usually about 100).
 * 
 * @see Collections#sort( List )
 * @author trevor
 */
public class Sorts {
    
    /** 
     * A Bucketer is an extension of a Comparator used with the bucket sort algorithm.
     *
     * The Comparator interface allows only comparison between pairs of elements, which 
     * restricts the speed of our sort algorithm, typically to O(n log_2 n).  
     * The bucketer interface partitions incoming objects into a series of buckets, such
     * that all objects in bucket <i>n</i> come before all objects in bucket <i>n+1</i>
     * in sorted order.  Once objects have been placed in buckets, we can sort the objects
     * in each bucket using the comparator, then place the buckets in order to find
     * the final order.
     */

    public interface Bucketer<T> {
        int bucket( T object );
        int maxBuckets();
    }
    
    public static <T> void bucketSort( ArrayList<T> items, Bucketer<T> bucketer, Comparator<T> comparator ) {
        ArrayList[] buckets = new ArrayList[bucketer.maxBuckets()];
        
        for( int i=0; i<bucketer.maxBuckets(); i++ ) {
            buckets[i] = new ArrayList();
        }

        for( T object : items ) {
            int bucket = bucketer.bucket(object);
            buckets[bucket].add(object);
        }
        
        items.clear();
        
        for( ArrayList bucket : buckets ) {
            quicksort( bucket, comparator );
            items.addAll( bucket );
        }
    }
    
    public static <T extends Comparable<T>> void multiWaySort( List<T> items, int ways, Comparator<T> comparator ) {
        if( items.size() < ways*100 ) {
            Collections.sort(items, comparator);
        } else {
            ArrayList[] outputs = new ArrayList[ways];
            Object[] splits = new Object[ways-1];
            
            // find random objects
            HashSet<T> possibleSplitSet = new HashSet<T>();
            while( possibleSplitSet.size() < 2*ways-1 ) {
                int index = (int) (Math.random() * (items.size()-1));
                possibleSplitSet.add( items.get(index) );
            }
            ArrayList<T> possibleSplits = new ArrayList<T>();
            possibleSplits.addAll(possibleSplitSet);
            Collections.sort(possibleSplits);
            
            // prune the possible splits down by half
            for( int i=0; i<splits.length; i++ ) {
                splits[i] = possibleSplits.get(2*i);
            }

            for( int i=0; i<outputs.length; i++ ) {
                outputs[i] = new ArrayList<T>( 2*items.size()/ways );
            }
            
            // now we have a list of splits, and they're in order,
            // so we assign items to bins.
            
            itemsLoop:
            for( T item : items ) {
                int small = 0; 
                int big = ways-2;
                
                while( big-small > 1 ) {
                    int middleIndex = (big + small) / 2;
                    T middle = (T) splits[ middleIndex ];
                    int result = comparator.compare( middle, item );
                    
                    if ( result < 0 ) {
                        small = middleIndex;
                    } else if( result > 0 ) {
                        big = middleIndex;
                    } else {
                        outputs[big].add(item);
                        continue itemsLoop;
                    }
                }
                
                if( small > 0 && big < ways-2 ) {
                    // we know that small is less and big is more
                    outputs[big].add(item);
                } else if( small > 0 ) {
                    // we know that the item is bigger than the small item
                    T bigItem = (T) splits[big];
                    int result = comparator.compare( bigItem, item );
                    
                    if( result < 0 ) {
                        outputs[big].add(item);
                    } else {
                        outputs[big+1].add(item);
                    }
                } else if( big < ways-2 ) {
                    // we know item is smaller than the big item
                    T smallItem = (T) splits[small];
                    int result = comparator.compare( smallItem, item );
                    
                    if( result < 0 ) {
                        outputs[small].add(item);
                    } else {
                        outputs[small+1].add(item);
                    }
                }
            }

            // now we sort and combine
            items.clear();
            
            for( int i=0; i<outputs.length; i++ ) {
                Collections.sort( outputs[i], comparator );
                items.addAll( outputs[i] );
            }
        }
    }
    
    /**
     * Picks three objects from the items list, and determines which item falls in the middle of the three
     * in sorted order.  That middle object is then swapped to the front of the list.
     *
     * This is used in the middle-of-3 heuristic for quicksort, which offers some protection against
     * the possibility of O(n^2) from quicksort.
     */
    private static <T> void pickMiddleElement( ArrayList<T> items, Comparator<T> comparator, int start, int end ) {
        T one = items.get(start);
        T two = items.get(end-1);
        T three = items.get( start/2 + end/2 );
        
        int oneTwo = comparator.compare(one, two);
        int twoThree = comparator.compare(two, three);
        
        if( Math.signum(oneTwo) == Math.signum(twoThree) ) {
            // one < two < three || one > two > three
            // swap one and two
            items.set(start, two);
            items.set(end-1, one);
        } else {
            // one > two < three || one < two > three
            int oneThree = comparator.compare(one, three);

            if( Math.signum(oneThree) == Math.signum(twoThree) ) {
                // two < one < three || two > one > three
                // one is in the middle, do nothing
            } else {
                // two < three < one || two > three > one
                // swap one and three
                items.set( start, three );
                items.set( start/2 + end/2, one );
            }
        }
    }
    
    public static <T> void selectionSort( ArrayList<T> items, Comparator<T> comparator, int start, int end ) {
        // at each position, we choose the smallest element and swap it to the start of the list
        // this algorithm is O(n^2), but should have a smaller constant than other methods;
        // therefore we use it for very short lists.
        for( int i=start; i<end; i++ ) {
            T smallest = items.get(i);
            int smallIndex = i;
            
            for( int j=i+1; j<end; j++ ) {
                T item = items.get(j);
                
                if( comparator.compare(smallest, item) > 0 ) {
                    smallest = item;
                    smallIndex = j;
                }
            }
        
            assert comparator.compare( smallest, items.get(i) ) < 0 || i == smallIndex;
            items.set( smallIndex, items.get(i) );
            items.set( i, smallest );
        }
    }
    
    public static <T> void quicksort( ArrayList<T> items, Comparator<T> comparator ) {
        quicksort( items, comparator, 0, items.size() );
    }
    
    public static <T> void quicksort( ArrayList<T> items, Comparator<T> comparator, int start, int end ) {
        if( end - start < 8 ) {
            selectionSort( items, comparator, start, end );
        } else {
            pickMiddleElement( items, comparator, start, end );

            T center = items.get(start);
            int leftIndex = start + 1;
            int rightIndex = end - 1;

            boolean leftWrong = false;
            boolean rightWrong = false;
            
            while( leftIndex < rightIndex ) {
                T left = items.get(leftIndex);
                T right = items.get(rightIndex);

                int leftCompare = comparator.compare( left, center );
                int rightCompare = comparator.compare( right, center );

                leftWrong = leftCompare > 0;
                rightWrong = rightCompare <= 0;

                if( leftWrong && rightWrong ) {
                    // swap items
                    items.set( leftIndex, right );
                    items.set( rightIndex, left );

                    leftIndex++;
                    rightIndex--;
                } else {
                    if( !leftWrong )
                        leftIndex++;
                    if( !rightWrong )
                        rightIndex--;
                }
            }

            assert leftIndex >= 1;

            // leftLast should point to the last left element
            int leftLast;
            int rightStart;

            if( leftWrong && rightWrong ) {
                // the last action was a swap
                leftLast = leftIndex - 1;
                rightStart = rightIndex + 1;
            } else if( leftWrong && !rightWrong ) {
                // the final action was a subtract on the right pointer,
                // and leftIndex is pointing to something that should be on the right side
                leftLast = leftIndex - 1;
                rightStart = rightIndex;
            } else if( !leftWrong && rightWrong ) {
                // the final action was an add on the left pointer
                // and rightIndex is pointing to something that should be on the left side
                leftLast = leftIndex - 1;
                rightStart = rightIndex + 1;
            } else {
                // the final action was an add on the left pointer
                // and a subtract on the right pointer
                leftLast = leftIndex - 1;
                rightStart = rightIndex + 1;
            }
            
            if( rightStart - leftLast == 2 ) {
                int result = comparator.compare( center, items.get(leftLast+1) );
                
                if( result < 0 ) {
                    rightStart--;
                } else {
                    leftLast++;
                }
            }

            // shift center into the middle
            items.set( start, items.get(leftLast) );
            items.set( leftLast, center );
            
            // assert that we're in order
            for( int i=start; i<end; i++ ) {
                int cResult = comparator.compare( center, items.get(i) );
                if( i <= leftLast )
                    assert cResult >= 0 : "" + i + " " + leftLast + " " + rightStart + " " + leftWrong + " " + rightWrong + " " + cResult + " " + center + " " + items.get(i);
                if( i >= rightStart )
                    assert cResult <= 0 : "" + i + " " + leftLast + " " + rightStart + " " + leftWrong + " " + rightWrong + " " + cResult + " " + center + " " + items.get(i);
            }

            // now we recurse
            quicksort( items, comparator, start, leftLast );
            quicksort( items, comparator, rightStart, end );
        }
    }
    
    public static String randomString() {
        byte[] tenBytes = new byte[10];
        
        for( int i=0; i<10; i++ ) {
            byte c = (byte)('a' + (char) (Math.random() * 26));
            tenBytes[i] = c;
        }
        
        return new String(tenBytes);
    }
    
    public static ArrayList<String> randomStrings( int count ) {
        ArrayList<String> lots = new ArrayList<String>();
        for( int i=0; i<count; i++ ) {
            lots.add(randomString());
        }
        return lots;
    }
    
    public static void test( int count ) {
        ArrayList<String> lots;
        lots = randomStrings(count);
        System.out.println( "copying: " );
        long start = new Date().getTime();
        ArrayList<String> o = new ArrayList<String>();
        o.addAll(lots);
        System.out.println( "done copying: " + (new Date().getTime() - start) );
        
        System.out.println( "sorting by collections: " );
        System.gc();
        start = new Date().getTime();
        Collections.sort( lots );
        System.out.println( "done: " + (new Date().getTime() - start) );
        
        for( int i=16; i <= 4096; i *= 2 ) {
            lots = randomStrings(count);
            System.out.println( "sorting by mine: " + i );
            System.gc();
            start = new Date().getTime();
            // BUGBUG: added comparator, broke the test: multiWaySort( lots, i );
            System.out.println( "done: " + (new Date().getTime() - start) );
        }    
    }
    
    public static void main( String[] args ) {
        System.out.println( "10000" );
        test( 100*1000 );
        test( 100*1000 );
        test( 100*1000 );
        System.out.println( "500 000" );
        //test( 500*1000 );
        System.out.println( "3 000 000" );
        test( 3000*1000 );
    }
}
