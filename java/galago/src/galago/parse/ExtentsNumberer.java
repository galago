/*
 * ExtentsNumberer
 *
 * September 14, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentExtent;
import galago.types.DocumentIdentifierExtent;
import galago.types.DocumentIdentifierNumber;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author trevor
 */
@InputClass(className="galago.types.DocumentIdentifierExtent")
@OutputClass(className="galago.types.DocumentExtent")
@Verified
public class ExtentsNumberer extends StandardStep<DocumentIdentifierExtent, DocumentExtent> {
    HashMap<String, Integer> documentNumbers = new HashMap();
    
    public void process( DocumentIdentifierExtent object ) throws IOException {
        int documentNumber = documentNumbers.get( object.document );
        processor.process( new DocumentExtent( Utility.makeBytes(object.extentName),
                                               documentNumber, object.begin, object.end ) );
    }
    
    public ExtentsNumberer( TupleFlowParameters parameters ) throws IOException {
        TypeReader<DocumentIdentifierNumber> reader = parameters.getTypeReader( "documentNumbers" );
        DocumentIdentifierNumber dn;
        
        while( (dn = reader.read()) != null ) {
            documentNumbers.put( dn.identifier, dn.document );
        }
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "documentNumbers", DocumentIdentifierNumber.class, parameters, handler );
    }
}