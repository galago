/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import com.yahoo.pig.impl.galago.GalagoPlanCompiler;
import java.io.IOException;
import java.util.Map;

import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.LogicalPlan;

public class PhysicalPlan {
    public PhysicalOperator root;

    public PhysicalPlan(PhysicalOperator rootIn) {
        root = rootIn;
    }

    public DataBag exec(boolean continueFromLast) throws IOException {
        DataBag results = new DataBag();

        root.open(continueFromLast);
        Tuple t;
        while ((t = (Tuple) root.getNext()) != null)
            results.add(t);
        root.close();

        return results;
    }

    // this constructor compiles a logical plan into a (local or mapreduce or hadoop) physical plan:
    public PhysicalPlan(LogicalPlan lp, Map queryResults) throws IOException {
    	switch(lp.getPigContext().getExecType()) {
    	case LOCAL:
            // always append a splitmaster on the front of each newly-compiled plan
            // (allows future plans to read from it)
            root = new POSplitMaster(new LocalPlanCompiler(lp.getPigContext()).compile(lp.root(), queryResults),lp.getOutputType());
            break;
    	case MAPREDUCE:
            root = (new MapreducePlanCompiler(lp.getPigContext())).compile(lp.root(), queryResults);
            break;
        case GALAGO:
            root = (new GalagoPlanCompiler(lp.getPigContext())).compile(lp.root(), queryResults);
            break;
        case PIG:
            root = (new PigbodyPlanCompiler(lp.getPigContext())).compile(lp.root(), queryResults);
        default:
            throw new IOException("Unknown execType.");
        }
    }
}
