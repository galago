
//
// galagod
//
// 27 February 2007 -- tds
// 
// BSD License (http://galagosearch.org/license)
//

#define GALAGOD_DEFAULT_QUERY_RESULTS (20)
#define GALAGOD_MAXIMUM_QUERY_RESULTS (1<<20)

//
// print_xml_results
//

static void print_xml_results( const std::vector<indri::api::ScoredExtentResult>& results,
                               Retrieval& retrieval,
                               FCGX_Request& request )
{                                    
    FCGX_FPrintF( request.out, "[\n" );

    for( int i=0; i<results.size(); i++ ) {
        std::string document = retrieval.getDocument(results[i].document);                                          
        bool more = (results.size() - i > 1);
        
        FCGX_FPrintF( request.out, "\t{ \"score\" : %f, \"document\" : %s }%s\n", results[i].score,
                                                                                  document.c_str(),
                                                                                  more ? "," : "" );
    }
       
    FCGX_FPrintF( request.out, "]\n" );
}                  

//
// print_json_results
//                   

static void print_json_results( const std::vector<indri::api::ScoredExtentResult>& results,
                               Retrieval& retrieval,
                               FCGX_Request& request )
{
    FCGX_FPrintF( request.out, "<results>\n" );

    for( int i=0; i<results.size(); i++ ) {
        std::string document = retrieval.getDocument(results[i].document);
        
        FCGX_FPrintF( request.out, "\t<result>\n" );
        FCGX_FPrintF( request.out, "\t\t<score>%f</score>\n", results[i].score );
        FCGX_FPrintF( request.out, "\t\t<document>%s</document>\n", document.c_str() );
        FCGX_FPrintF( request.out, "\t</result>\n" );
    }
       
    FCGX_FPrintF( request.out, "</results>\n" );
}                  

//
// unescape_string
//

static std::string unescape_string( const std::string& input ) {
    std::stringstream output;
    
    for( int i=0; i<input.size(); i++ ) {
        if( input[i] == '+' ) {
            output << ' ';
        } else if( input[i] == '%' ) {
            output << unescape_hex( input, i+1 );
            i += 2;
        } else {
            output << input[i];
        }
    }

    return output.str();
}

//
// parse_query_string
//

static std::map<std::string, std::string> parse_query_string( const std::string& input ) {
    int location = 0;
    std::map<std::string, std::string> result;

    while (true) {
        std::string::size_type equals = input.find( '=', location );
        std::string::size_type ampersand = input.find( '&', equals );
        
        if( equals == std::string::npos )
            break;
            
        std::string key = input.substr( location, equals-location-1 );
        std::string value;
        
        if( ampersand != std::string::npos ) {
            value = input.substr( equals+1 );
        } else {
            value = input.substr( equals+1, ampersand-equals-1 );
        }
        
        value = unescape_string( value );
        result[key] = value;
        
        if( ampersand == std::string::npos )
            break;
    }
    
    return result;
}

//
// fcgi_daemon 
//

static void fcgi_daemon( Retrieval& retrieval, indri::api::Parameters& parameters ) {
    FCGX_Request request;
    FCGX_InitRequest(&request, 0, 0);
                                  
    while (true) {
        int rc = FCGX_Accept_r( &request );

        if( rc < 0 )
            break;
            
        const char* requestString = FCGX_GetParam( "QUERY_STRING", request.envp );
        
        // parse the query string
        std::map<std::string, std::string> parsed = parse_query_string( requestString );

        // get the count parameter
        int count = GALAGOD_DEFAULT_QUERY_RESULTS;
        
        if( parsed.find( "count" ) != parsed.end() ) {
            count = string_to_i64( parsed["count"] );
            count = std::min( count, GALAGOD_MAXIMUM_QUERY_RESULTS;
        }
        
        // get the query
        std::string queryText;
        
        if( parsed.find( "query" ) != parsed.end() ) {
            queryText = parsed["query"];
        }

        // run the query
        std::vector<indri::api::ScoredExtentResult> results;
        results = retrieval.runQuery( queryText, requested );

        // print the results
	if( parsed.find( "json" ) != parsed.end() ) {
        	print_json_results( results, retrieval, request );
	} else {
        	print_xml_results( results, retrieval, request );
	}
        FCGX_Finish_r(&request);                 
    }
}

//
// main
//
        
int main( int argc, char** argv ) {
	try {
		// read in parameters
		indri::api::Parameters::instance().loadCommandLine( argc, argv );
		indri::api::Parameters& parameters = indri::api::Parameters::instance();     

		// open index
		Retrieval retrieval;
		std::string indexPath = parameters["index"];
		retrieval.openRead( indexPath );
        
        	fcgi_daemon( retrieval, parameters );
	} catch( lemur::api::Exception& e ) {
		LEMUR_ABORT(e);
	}
    
    return 0;
}

