/*
 * StageInstanceDescription
 *
 * August 24, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.Utility;
import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.FileOrderedReader;
import galago.tupleflow.FileOrderedWriter;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Order;
import galago.tupleflow.OrderedCombiner;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Source;
import galago.tupleflow.Splitter;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 *
 * @author trevor
 */
public class StageInstanceDescription implements Serializable {

    /// A stage object (probably genereated by the JobConstructor parser)
    Stage stage;
    
    /// The index of this job (between 0 and n-1 if there are n instances)
    int index;

    /// A map from stage input names to data pipe structures (note that the output from a pipe is a stage input)
    Map<String, PipeOutput> readers;

    /// A map from stage input names to data pipe structures (note that the input to a pipe connects to a stage output)
    Map<String, PipeInput> writers;

    public StageInstanceDescription(Stage stage, int index, Map<String, PipeInput> pipeInputs, Map<String, PipeOutput> pipeOutputs) {
        this.stage = stage;
        this.index = index;
        this.writers = pipeInputs;
        this.readers = pipeOutputs;
    }

    public static class PipeInput implements Serializable {
        private int index;
        private DataPipe pipe;

        public PipeInput(DataPipe pipe, int index) {
            this.index = index;
            this.pipe = pipe;
        }

        public String[] getFileNames() {
            return pipe.getInputFileNames(index);
        }
    }

    public static class PipeOutput implements Serializable {
        private int start;
        private int stop;
        private DataPipe pipe;

        public PipeOutput(DataPipe pipe, int index) {
            this(pipe, index, index + 1);
        }

        public PipeOutput(DataPipe pipe, int start, int stop) {
            this.start = start;
            this.stop = stop;
            this.pipe = pipe;
        }

        public String[] getFileNames() {
            ArrayList<String[]> allFilenames = new ArrayList();
            int totalNames = 0;

            for (int i = start; i < stop; i++) {
                String[] batch = pipe.getOutputFileNames(i);
                totalNames += batch.length;
                allFilenames.add(batch);
            }

            String[] result = new String[totalNames];
            int spot = 0;

            for (String[] batch : allFilenames) {
                System.arraycopy(batch, 0, result, spot, batch.length);
                spot += batch.length;
            }

            return result;
        }
    }

    public class StepParameters implements TupleFlowParameters {
        Parameters xml;

        public StepParameters(Step o) {
            this.xml = o.getParameters();
        }

        public TypeReader getTypeReader(String specification) throws IOException {
            return StageInstanceDescription.this.getTypeReader(specification);
        }

        public Processor getTypeWriter(String specification) throws IOException {
            return StageInstanceDescription.this.getTypeWriter(specification);
        }

        public boolean readerExists(String specification, String className, String[] order) {
            return StageInstanceDescription.this.readerExists(specification, className, order);
        }

        public boolean writerExists(String specification, String className, String[] order) {
            return StageInstanceDescription.this.writerExists(specification, className, order);
        }

        public Parameters getXML() {
            return xml;
        }
    }

    public galago.tupleflow.Step instantiate(ArrayList<Step> steps) throws IncompatibleProcessorException, IOException {
        galago.tupleflow.Step previous = null;
        galago.tupleflow.Step first = null;

        for (Step step : steps) {
            galago.tupleflow.Step current;

            if (step instanceof MultiStep) {
                current = instantiateMulti(step);
            } else if (step instanceof InputStep) {
                current = instantiateInput((InputStep) step);
            } else if (step instanceof OutputStep) {
                current = instantiateOutput((OutputStep) step);
            } else {
                current = instantiateStep(step);
            }

            if (first == null) {
                first = current;
            }
            if (previous != null) {
                ((Source) previous).setProcessor(current);
            }

            previous = current;
        }

        return first;
    }

    public galago.tupleflow.Step instantiateStep(final Step step) throws IOException {
        galago.tupleflow.Step object;

        try {
            Class objectClass = Class.forName(step.getClassName());
            Constructor parameterArgumentConstructor = null;
            Constructor noArgumentConstructor = null;

            for (Constructor c : objectClass.getConstructors()) {
                java.lang.reflect.Type[] parameters = c.getGenericParameterTypes();

                if (parameters.length == 0) {
                    noArgumentConstructor = c;
                } else if (parameters.length == 1 && parameters[0] == TupleFlowParameters.class) {
                    parameterArgumentConstructor = c;
                }
            }

            if (parameterArgumentConstructor != null) {
                object = (galago.tupleflow.Step) parameterArgumentConstructor.newInstance( new StepParameters( step ) );
            } else if (noArgumentConstructor != null) {
                object = (galago.tupleflow.Step) noArgumentConstructor.newInstance();
            } else {
                throw new IncompatibleProcessorException("Couldn't instantiate this class because " + "no compatible constructor was found: " + step.getClassName());
            }
        } catch (Exception e) {
            throw (IOException) new IOException( "Couldn't instantiate a step object: " + step.getClassName() ).initCause(e);
        }

        return object;
    }

    public galago.tupleflow.Step instantiateInput(final InputStep step) throws IOException {
        return getTypeReaderSource(step.getId());
    }

    public galago.tupleflow.Step instantiateOutput(final OutputStep step) throws IOException {
        return getTypeWriter(step.getId());
    }

    private galago.tupleflow.Step instantiateMulti(final Step step) throws IncompatibleProcessorException, IOException {
        MultiStep multiStep = (MultiStep) step;
        Processor[] processors = new Processor[multiStep.groups.size()];

        for (int i = 0; i < multiStep.groups.size(); i++) {
            ArrayList<Step> group = multiStep.groups.get(i);
            processors[i] = (galago.tupleflow.Processor) instantiate( group );
        }

        return new galago.tupleflow.Multi(processors);
    }

    public String getName() {
        return stage.name;
    }
    
    public int getIndex() {
        return index;
    }
    
    public String getPath() {
        return getName() + File.separator + getIndex();
    }
    
    public ExNihiloSource instantiate() throws IncompatibleProcessorException, IOException {
        return (ExNihiloSource) instantiate( stage.steps );
    }

    protected static Order createOrder(final DataPipe pipe) throws IOException {
        return createOrder(pipe.className, pipe.order);
    }

    public static Order createOrder(String className, String[] orderSpec) throws IOException {
        Order order;

        try {
            Class typeClass = Class.forName(className);
            galago.tupleflow.Type type = (galago.tupleflow.Type) typeClass.getConstructor().newInstance();
            order = type.getOrder(orderSpec);
        } catch (Exception e) {
            throw (IOException) new IOException( "Couldn't create an order object for type: " + className ).initCause(e);
        }

        return order;
    }

    public ReaderSource getTypeReaderSource(String specification) throws IOException {
        PipeOutput pipeOutput = readers.get(specification);
        ReaderSource reader;

        if (pipeOutput == null) {
            return null;
        }

        Order order = createOrder(pipeOutput.pipe);
        String[] fileNames = pipeOutput.getFileNames();

        if (fileNames.length > 1) {
            reader = OrderedCombiner.combineFromFiles(Arrays.asList(fileNames), order);
        } else {
            reader = new FileOrderedReader(fileNames[0], order);
        }
        return reader;
    }

    public TypeReader getTypeReader(String specification) throws IOException {
        PipeOutput pipeOutput = readers.get(specification);
        return getTypeReader(pipeOutput);
    }

    @SuppressWarnings(value = "unchecked")
    public static <T> ReaderSource<T> getTypeReader(final PipeOutput pipeOutput) throws IOException {
        ReaderSource<T> reader;

        if (pipeOutput == null) {
            return null;
        }

        Order order = createOrder(pipeOutput.pipe);
        String[] fileNames = pipeOutput.getFileNames();

        if (fileNames.length > 100) {
            List<String> names = Arrays.asList(fileNames);
            ArrayList<String> reduced = new ArrayList<String>();
            
            // combine 20 files at a time
            for( int i=0; i<names.size(); i += 20 ) {
                int start = i;
                int end = Math.min( names.size(), i+20 );
                List<String> toCombine = names.subList(start, end);
                
                reader = OrderedCombiner.combineFromFiles( toCombine, order );
                File temporary = Utility.createTemporary();
                FileOrderedWriter<T> writer = new FileOrderedWriter<T>( temporary, order );
                
                try {
                    reader.setProcessor( writer );
                } catch( IncompatibleProcessorException e ) {
                    throw (IOException) new IOException( "Incompatible processor for reader tuples" ).initCause(e);
                }
                
                reader.run();
                
                reduced.add( temporary.toString() );
                temporary.deleteOnExit();
            }
            
            reader = OrderedCombiner.combineFromFiles( reduced, order );
        } else if (fileNames.length > 1) {
            reader = OrderedCombiner.combineFromFiles(Arrays.asList(fileNames), order);
        } else {
            reader = new FileOrderedReader(fileNames[0], order);
        }
        return reader;
    }

    public Processor getTypeWriter(String specification) throws IOException {
        PipeInput pipeInput = writers.get(specification);
        return getTypeWriter(pipeInput);
    }

    public static Processor getTypeWriter(final PipeInput pipeInput) throws IOException, IOException {
        Processor writer;

        if (pipeInput == null) {
            return null;
        }
        String[] fileNames = pipeInput.getFileNames();
        Order order = createOrder(pipeInput.pipe);
        Order hashOrder = createOrder(pipeInput.pipe.className, pipeInput.pipe.hash);
        
        assert order != null : "Order not found: " + Arrays.toString(pipeInput.pipe.order);

        try {
            if (fileNames.length == 1) {
                writer = new FileOrderedWriter(fileNames[0], order);
            } else {
                assert hashOrder != null : "Hash order not found: " + pipeInput.pipe.pipeName + " " + pipeInput.pipe.hash;
                writer = Splitter.splitToFiles(fileNames, order, hashOrder);
            }
        } catch (IncompatibleProcessorException e) {
            throw (IOException) new IOException( "Failed to create a typeWriter" ).initCause(e);
        }

        return writer;
    }

    private boolean writerExists(String specification, String className, String[] order) {
        assert false : "Incomplete specification";
        return writers.get(specification) != null;
    }

    private boolean readerExists(String specification, String className, String[] order) {
        assert false : "Incomplete specification";
        return readers.get(specification) != null;
    }
}
