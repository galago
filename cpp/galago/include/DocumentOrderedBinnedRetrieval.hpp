
//
// DocumentOrderedBinnedRetrieval
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_DOCUMENTORDEREDBINNEDRETRIEVAL_HPP
#define GALAGO_DOCUMENTORDEREDBINNEDRETRIEVAL_HPP

#include "DocumentOrderedBinnedIndex.hpp"
#include "DocumentOrderedBinnedIterator.hpp"
#include "indri/ScoredExtentResult.hpp"
#include "Query.hpp"
#include "Retrieval.hpp"
#include <vector>
#include <queue>

class DocumentOrderedBinnedRetrieval : public Retrieval {
private:
    DocumentOrderedBinnedIndex _index;
    std::vector<DocumentOrderedBinnedIterator*> getIterators( const std::vector<QueryTerm>& terms );

public:
    std::vector<indri::api::ScoredExtentResult> runQuery( const std::vector<QueryTerm>& query,
                                                          int requested,
                                                          int threshold );
    std::string getDocument( int document );
    
    void openRead( const std::string& path );
    void close();
};

#endif // GALAGO_DOCUMENTORDEREDBINNEDRETRIEVAL_HPP
