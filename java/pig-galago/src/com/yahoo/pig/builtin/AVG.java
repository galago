/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;
import java.util.Iterator;

import com.yahoo.pig.AlgebraicAtomEvalFunc;
import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.TupleEvalFunc;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

/**
 * Generates the average of the values of the first field of a tuple. This class is Algebraic in
 * implemenation, so if possible the execution will be split into a local and global application
 */
public class AVG extends AlgebraicAtomEvalFunc {

    public void exec(Tuple input, DataAtom output) throws IOException {
        double sum = sum(input);
        double count = count(input);

        double avg = 0;
        if (count > 0)
            avg = sum / count;

        output.setValue(avg);
    }

    public TupleEvalFunc getInitial() {
        return new Initial();
    }

    public TupleEvalFunc getIntermed() {
        return new Intermed();
    }

    public AtomEvalFunc getFinal() {
        return new Final();
    }

    static public class Initial extends TupleEvalFunc {
        public void exec(Tuple input, Tuple output) throws IOException {
        	try {
            output.appendField(new DataAtom(sum(input)));
            output.appendField(new DataAtom(count(input)));
            output.appendField(new DataAtom("processed by initial"));
        	} catch(RuntimeException t) {
        		throw new RuntimeException(t.getMessage() + ": " + input);
        	}
        }
    }

    static public class Intermed extends TupleEvalFunc {
        public void exec(Tuple input, Tuple output) throws IOException {
            combine(input.getBagField(0), output);
        }
    }

    static public class Final extends AtomEvalFunc {
        public void exec(Tuple input, DataAtom output) throws IOException {
            Tuple combined = new Tuple();
            if(input.getField(0) instanceof DataBag) {
                combine(input.getBagField(0), combined);    
            } else {
                throw new RuntimeException("Bag not found in: " + input);
                
                
                //combined = input.getTupleField(0);
            }
            double sum = combined.getAtomField(0).numval();
            double count = combined.getAtomField(1).numval();

            double avg = 0;
            if (count > 0) {
                avg = sum / count;
            }
            output.setValue(avg);
        }
    }

    static protected void combine(DataBag values, Tuple output) throws IOException {
        double sum = 0;
        double count = 0;

        for (Iterator it = values.content(); it.hasNext();) {
            Tuple t = (Tuple) it.next();
//            if(!(t.getField(0) instanceof DataAtom)) {
//                throw new RuntimeException("Unexpected Type: " + t.getField(0).getClass().getName() + " in " + t);
//            }
            
            sum += t.getAtomField(0).numval();
            count += t.getAtomField(1).numval();
        }

        output.appendField(new DataAtom(sum));
        output.appendField(new DataAtom(count));
    }

    static protected long count(Tuple input) throws IOException {
        DataBag values = input.getBagField(0);

        
        return values.cardinality();
    }

    static protected double sum(Tuple input) throws IOException {
        DataBag values = input.getBagField(0);

        double sum = 0;
        for (Iterator it = values.content(); it.hasNext();) {
            Tuple t = (Tuple) it.next();
            sum += t.getAtomField(0).numval();
        }

        return sum;
    }
    
    @Override
    public SchemaItem outputSchema() {
        SchemaItemList schema = new SchemaItemList();
        schema.add(new SchemaField("average"));
        return schema;
    }

}
