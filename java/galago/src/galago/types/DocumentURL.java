// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/DocumentURL.java DocumentURL galago.types String:identifier String:url order: order:+url order:+identifier
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class DocumentURL implements Type<DocumentURL> {
    public String identifier;
    public String url; 
    
    public DocumentURL() {}
    public DocumentURL( String identifier, String url ) {
        this.identifier = identifier;
        this.url = url;
    }  
    
    public String toString() {
            return String.format( "%s,%s",
                                   identifier, url );
    } 

    public Order<DocumentURL> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] {  } ) ) {
            return new Unordered();
        }
        if ( Arrays.equals( spec, new String[] { "+url" } ) ) {
            return new UrlOrder();
        }
        if ( Arrays.equals( spec, new String[] { "+identifier" } ) ) {
            return new IdentifierOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<DocumentURL> {
        public void process( DocumentURL object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class Unordered implements Order<DocumentURL> {
        public int hash( DocumentURL object ) {
            int h = 0;
            return h;
        } 
        public Comparator<DocumentURL> greaterThan() {
            return new Comparator<DocumentURL>() {
                public int compare( DocumentURL one, DocumentURL two ) {
                    int result = 0;
                    do {
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentURL> lessThan() {
            return new Comparator<DocumentURL>() {
                public int compare( DocumentURL one, DocumentURL two ) {
                    int result = 0;
                    do {
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentURL> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentURL> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentURL> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentURL > {
            DocumentURL last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentURL object ) throws IOException {
               boolean processAll = false;
               shreddedWriter.processTuple( object.identifier, object.url );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentURL> getInputClass() {
                return DocumentURL.class;
            }
        } 
        public ReaderSource<DocumentURL> orderedCombiner( Collection<TypeReader<DocumentURL>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentURL> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentURL clone( DocumentURL object ) {
            DocumentURL result = new DocumentURL();
            if ( object == null ) return result;
            result.identifier = object.identifier; 
            result.url = object.url; 
            return result;
        }                 
        public Class<DocumentURL> getOrderedClass() {
            return DocumentURL.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processTuple( String identifier, String url ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public final void processTuple( String identifier, String url ) throws IOException {
                if( lastFlush ) {
                    lastFlush = false;
                }
                buffer.processTuple( identifier, url );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeString( buffer.getIdentifier() );
                    output.writeString( buffer.getUrl() );
                    buffer.incrementTuple();
                }
            }  
            public void flush() throws IOException { 
                flushTuples( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
                            
            String[] identifiers;
            String[] urls;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                identifiers = new String[batchSize];
                urls = new String[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processTuple( String identifier, String url ) {
                identifiers[writeTupleIndex] = identifier;
                urls[writeTupleIndex] = url;
                writeTupleIndex++;
            }
            public void resetData() {
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getIdentifier() {
                assert readTupleIndex < writeTupleIndex;
                return identifiers[readTupleIndex];
            }                                         
            public String getUrl() {
                assert readTupleIndex < writeTupleIndex;
                return urls[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getIdentifier(), getUrl() );
                   incrementTuple();
                }
            }                                                                           
             
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentURL>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentURL.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentURL.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentURL>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentURL> getOutputClass() {
                return DocumentURL.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentURL read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentURL result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentURL>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentURL last = new DocumentURL();         
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentURL read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentURL result = new DocumentURL();
                
                result.identifier = buffer.getIdentifier();
                result.url = buffer.getUrl();
                
                buffer.incrementTuple();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        buffer.processTuple( input.readString(), input.readString() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }


            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentURL.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentURL.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentURL>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentURL> getOutputClass() {
                return DocumentURL.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentURL last = new DocumentURL();
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

          
            
                               
            public void processTuple( String identifier, String url ) throws IOException {
                processor.processTuple( identifier, url );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentURL last = new DocumentURL();
            public galago.tupleflow.Processor<DocumentURL> processor;                               
            
            public TupleUnshredder( DocumentURL.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentURL> processor ) {
                this.processor = processor;
            }
            
            public DocumentURL clone( DocumentURL object ) {
                DocumentURL result = new DocumentURL();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.url = object.url; 
                return result;
            }                 
            
            
            public void processTuple( String identifier, String url ) throws IOException {
                last.identifier = identifier;
                last.url = url;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentURL last = new DocumentURL();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentURL clone( DocumentURL object ) {
                DocumentURL result = new DocumentURL();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.url = object.url; 
                return result;
            }                 
            
            public void process( DocumentURL object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                processor.processTuple( object.identifier, object.url );                                         
            }
                          
            public Class<DocumentURL> getInputClass() {
                return DocumentURL.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
    public static class UrlOrder implements Order<DocumentURL> {
        public int hash( DocumentURL object ) {
            int h = 0;
            h += Utility.hash(object.url);
            return h;
        } 
        public Comparator<DocumentURL> greaterThan() {
            return new Comparator<DocumentURL>() {
                public int compare( DocumentURL one, DocumentURL two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.url, two.url );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentURL> lessThan() {
            return new Comparator<DocumentURL>() {
                public int compare( DocumentURL one, DocumentURL two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.url, two.url );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentURL> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentURL> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentURL> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentURL > {
            DocumentURL last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentURL object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.url, last.url) ) { processAll = true; shreddedWriter.processUrl( object.url ); }
               shreddedWriter.processTuple( object.identifier );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentURL> getInputClass() {
                return DocumentURL.class;
            }
        } 
        public ReaderSource<DocumentURL> orderedCombiner( Collection<TypeReader<DocumentURL>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentURL> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentURL clone( DocumentURL object ) {
            DocumentURL result = new DocumentURL();
            if ( object == null ) return result;
            result.identifier = object.identifier; 
            result.url = object.url; 
            return result;
        }                 
        public Class<DocumentURL> getOrderedClass() {
            return DocumentURL.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+url"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processUrl( String url ) throws IOException;
            public void processTuple( String identifier ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastUrl;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processUrl( String url ) {
                lastUrl = url;
                buffer.processUrl( url );
            }
            public final void processTuple( String identifier ) throws IOException {
                if( lastFlush ) {
                    if( buffer.urls.size() == 0 ) buffer.processUrl( lastUrl );
                    lastFlush = false;
                }
                buffer.processTuple( identifier );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeString( buffer.getIdentifier() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushUrl( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getUrlEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getUrl() );
                    output.writeInt( count );
                    buffer.incrementUrl();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushUrl( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> urls = new ArrayList();
            ArrayList<Integer> urlTupleIdx = new ArrayList();
            int urlReadIdx = 0;
                            
            String[] identifiers;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                identifiers = new String[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processUrl( String url ) {
                urls.add( url );
                urlTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( String identifier ) {
                assert urls.size() > 0;
                identifiers[writeTupleIndex] = identifier;
                writeTupleIndex++;
            }
            public void resetData() {
                urls.clear();
                urlTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                urlReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementUrl() {
                urlReadIdx++;  
            }                                                                                              

            public void autoIncrementUrl() {
                while( readTupleIndex >= getUrlEndIndex() && readTupleIndex < writeTupleIndex )
                    urlReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getUrlEndIndex() {
                if( (urlReadIdx+1) >= urlTupleIdx.size() )
                    return writeTupleIndex;
                return urlTupleIdx.get(urlReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getUrl() {
                assert readTupleIndex < writeTupleIndex;
                assert urlReadIdx < urls.size();
                
                return urls.get( urlReadIdx );
            }
            public String getIdentifier() {
                assert readTupleIndex < writeTupleIndex;
                return identifiers[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getIdentifier() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexUrl( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processUrl( getUrl() );
                    assert getUrlEndIndex() <= endIndex;
                    copyTuples( getUrlEndIndex(), output );
                    incrementUrl();
                }
            }  
            public void copyUntilUrl( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getUrl(), other.getUrl() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processUrl( getUrl() );
                                      
                        copyTuples( getUrlEndIndex(), output );
                    } else {
                        output.processUrl( getUrl() );
                        copyTuples( getUrlEndIndex(), output );
                    }
                    incrementUrl();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilUrl( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentURL>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentURL.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentURL.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentURL>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentURL> getOutputClass() {
                return DocumentURL.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentURL read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentURL result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentURL>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentURL last = new DocumentURL();         
            long updateUrlCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getUrl(), otherBuffer.getUrl() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentURL read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentURL result = new DocumentURL();
                
                result.url = buffer.getUrl();
                result.identifier = buffer.getIdentifier();
                
                buffer.incrementTuple();
                buffer.autoIncrementUrl();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateUrlCount - tupleCount > 0 ) {
                            buffer.urls.add(last.url);
                            buffer.urlTupleIdx.add( (int) (updateUrlCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateUrl();
                        buffer.processTuple( input.readString() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateUrl() throws IOException {
                if( updateUrlCount > tupleCount )
                    return;
                     
                last.url = input.readString();
                updateUrlCount = tupleCount + input.readInt();
                                      
                buffer.processUrl( last.url );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentURL.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentURL.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentURL>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentURL> getOutputClass() {
                return DocumentURL.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentURL last = new DocumentURL();
            boolean urlProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processUrl( String url ) throws IOException {  
                if( urlProcess || Utility.compare( url, last.url ) != 0 ) {
                    last.url = url;
                    processor.processUrl( url );
                    urlProcess = false;
                }
            }  
            
            public void resetUrl() {
                 urlProcess = true;
            }                                                
                               
            public void processTuple( String identifier ) throws IOException {
                processor.processTuple( identifier );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentURL last = new DocumentURL();
            public galago.tupleflow.Processor<DocumentURL> processor;                               
            
            public TupleUnshredder( DocumentURL.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentURL> processor ) {
                this.processor = processor;
            }
            
            public DocumentURL clone( DocumentURL object ) {
                DocumentURL result = new DocumentURL();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.url = object.url; 
                return result;
            }                 
            
            public void processUrl( String url ) throws IOException {
                last.url = url;
            }   
                
            
            public void processTuple( String identifier ) throws IOException {
                last.identifier = identifier;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentURL last = new DocumentURL();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentURL clone( DocumentURL object ) {
                DocumentURL result = new DocumentURL();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.url = object.url; 
                return result;
            }                 
            
            public void process( DocumentURL object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.url, object.url ) != 0 || processAll ) { processor.processUrl( object.url ); processAll = true; }
                processor.processTuple( object.identifier );                                         
            }
                          
            public Class<DocumentURL> getInputClass() {
                return DocumentURL.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
    public static class IdentifierOrder implements Order<DocumentURL> {
        public int hash( DocumentURL object ) {
            int h = 0;
            h += Utility.hash(object.identifier);
            return h;
        } 
        public Comparator<DocumentURL> greaterThan() {
            return new Comparator<DocumentURL>() {
                public int compare( DocumentURL one, DocumentURL two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.identifier, two.identifier );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentURL> lessThan() {
            return new Comparator<DocumentURL>() {
                public int compare( DocumentURL one, DocumentURL two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.identifier, two.identifier );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentURL> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentURL> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentURL> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentURL > {
            DocumentURL last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentURL object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.identifier, last.identifier) ) { processAll = true; shreddedWriter.processIdentifier( object.identifier ); }
               shreddedWriter.processTuple( object.url );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentURL> getInputClass() {
                return DocumentURL.class;
            }
        } 
        public ReaderSource<DocumentURL> orderedCombiner( Collection<TypeReader<DocumentURL>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentURL> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentURL clone( DocumentURL object ) {
            DocumentURL result = new DocumentURL();
            if ( object == null ) return result;
            result.identifier = object.identifier; 
            result.url = object.url; 
            return result;
        }                 
        public Class<DocumentURL> getOrderedClass() {
            return DocumentURL.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+identifier"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processIdentifier( String identifier ) throws IOException;
            public void processTuple( String url ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastIdentifier;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processIdentifier( String identifier ) {
                lastIdentifier = identifier;
                buffer.processIdentifier( identifier );
            }
            public final void processTuple( String url ) throws IOException {
                if( lastFlush ) {
                    if( buffer.identifiers.size() == 0 ) buffer.processIdentifier( lastIdentifier );
                    lastFlush = false;
                }
                buffer.processTuple( url );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeString( buffer.getUrl() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushIdentifier( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getIdentifierEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getIdentifier() );
                    output.writeInt( count );
                    buffer.incrementIdentifier();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushIdentifier( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> identifiers = new ArrayList();
            ArrayList<Integer> identifierTupleIdx = new ArrayList();
            int identifierReadIdx = 0;
                            
            String[] urls;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                urls = new String[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processIdentifier( String identifier ) {
                identifiers.add( identifier );
                identifierTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( String url ) {
                assert identifiers.size() > 0;
                urls[writeTupleIndex] = url;
                writeTupleIndex++;
            }
            public void resetData() {
                identifiers.clear();
                identifierTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                identifierReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementIdentifier() {
                identifierReadIdx++;  
            }                                                                                              

            public void autoIncrementIdentifier() {
                while( readTupleIndex >= getIdentifierEndIndex() && readTupleIndex < writeTupleIndex )
                    identifierReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getIdentifierEndIndex() {
                if( (identifierReadIdx+1) >= identifierTupleIdx.size() )
                    return writeTupleIndex;
                return identifierTupleIdx.get(identifierReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getIdentifier() {
                assert readTupleIndex < writeTupleIndex;
                assert identifierReadIdx < identifiers.size();
                
                return identifiers.get( identifierReadIdx );
            }
            public String getUrl() {
                assert readTupleIndex < writeTupleIndex;
                return urls[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getUrl() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexIdentifier( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processIdentifier( getIdentifier() );
                    assert getIdentifierEndIndex() <= endIndex;
                    copyTuples( getIdentifierEndIndex(), output );
                    incrementIdentifier();
                }
            }  
            public void copyUntilIdentifier( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getIdentifier(), other.getIdentifier() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processIdentifier( getIdentifier() );
                                      
                        copyTuples( getIdentifierEndIndex(), output );
                    } else {
                        output.processIdentifier( getIdentifier() );
                        copyTuples( getIdentifierEndIndex(), output );
                    }
                    incrementIdentifier();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilIdentifier( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentURL>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentURL.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentURL.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentURL>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentURL> getOutputClass() {
                return DocumentURL.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentURL read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentURL result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentURL>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentURL last = new DocumentURL();         
            long updateIdentifierCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getIdentifier(), otherBuffer.getIdentifier() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentURL read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentURL result = new DocumentURL();
                
                result.identifier = buffer.getIdentifier();
                result.url = buffer.getUrl();
                
                buffer.incrementTuple();
                buffer.autoIncrementIdentifier();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateIdentifierCount - tupleCount > 0 ) {
                            buffer.identifiers.add(last.identifier);
                            buffer.identifierTupleIdx.add( (int) (updateIdentifierCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateIdentifier();
                        buffer.processTuple( input.readString() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateIdentifier() throws IOException {
                if( updateIdentifierCount > tupleCount )
                    return;
                     
                last.identifier = input.readString();
                updateIdentifierCount = tupleCount + input.readInt();
                                      
                buffer.processIdentifier( last.identifier );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentURL.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentURL.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentURL>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentURL> getOutputClass() {
                return DocumentURL.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentURL last = new DocumentURL();
            boolean identifierProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processIdentifier( String identifier ) throws IOException {  
                if( identifierProcess || Utility.compare( identifier, last.identifier ) != 0 ) {
                    last.identifier = identifier;
                    processor.processIdentifier( identifier );
                    identifierProcess = false;
                }
            }  
            
            public void resetIdentifier() {
                 identifierProcess = true;
            }                                                
                               
            public void processTuple( String url ) throws IOException {
                processor.processTuple( url );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentURL last = new DocumentURL();
            public galago.tupleflow.Processor<DocumentURL> processor;                               
            
            public TupleUnshredder( DocumentURL.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentURL> processor ) {
                this.processor = processor;
            }
            
            public DocumentURL clone( DocumentURL object ) {
                DocumentURL result = new DocumentURL();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.url = object.url; 
                return result;
            }                 
            
            public void processIdentifier( String identifier ) throws IOException {
                last.identifier = identifier;
            }   
                
            
            public void processTuple( String url ) throws IOException {
                last.url = url;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentURL last = new DocumentURL();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentURL clone( DocumentURL object ) {
                DocumentURL result = new DocumentURL();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.url = object.url; 
                return result;
            }                 
            
            public void process( DocumentURL object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.identifier, object.identifier ) != 0 || processAll ) { processor.processIdentifier( object.identifier ); processAll = true; }
                processor.processTuple( object.url );                                         
            }
                          
            public Class<DocumentURL> getInputClass() {
                return DocumentURL.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    