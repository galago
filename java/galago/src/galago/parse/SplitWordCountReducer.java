/*
 * SplitWordCountReducer
 *
 * May 8, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Utility;
import galago.tupleflow.Reducer;
import galago.types.DocumentLengthWordCount;
import galago.types.WordCount;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This object overrides the typical use of a DocumentLengthWordCount
 * object here.  All we're interested in is the WordCount part.  We
 * combine all objects with the same word by adding the count part.
 *
 * The point of using this is to come up with a word count estimate
 * for a particular split of documents.  We assume that this reducer is 
 * run after the split, so that we're only adding up word counts for
 * a certain set of documents.
 *
 * @author trevor
 */
public class SplitWordCountReducer implements Reducer<DocumentLengthWordCount> {
    public ArrayList<DocumentLengthWordCount> reduce( List<DocumentLengthWordCount> input ) {
        HashMap<String, DocumentLengthWordCount> countObjects = new HashMap<String, DocumentLengthWordCount>();
        
        for( DocumentLengthWordCount wordCount : input ) {
            DocumentLengthWordCount original = countObjects.get( wordCount.word );
            
            if( original == null ) {
                countObjects.put( wordCount.word, wordCount );
            } else {
                original.count += wordCount.count;
            }
        }

        return new ArrayList<DocumentLengthWordCount>( countObjects.values() );
    }
}
