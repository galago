/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.window;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.TimestampedTuple;
import com.yahoo.pig.data.Tuple;

public class TimeWindowSpec extends WindowSpec {
	double duration;  // duration in seconds
	List<TimestampedTuple> window;
        
	public TimeWindowSpec(windowType type, double duration){
		super(type);
		this.duration = duration;
        window = new LinkedList<TimestampedTuple>();
    }
    
    @Override
    public DataCollector collector(final DataCollector output) {
        return new DataCollector() {

            public void add(Tuple t) throws IOException {
                
                boolean changed = false;
                
                if (!(((TimestampedTuple) t).isHeartbeat)) {
                    window.add((TimestampedTuple) t);
                    changed = true;
                }

                double expireTime = ((TimestampedTuple) t).timestamp - duration;
                while (true) {
                    TimestampedTuple tail = window.get(0);
                    
                    if (tail != null & tail.timestamp <= expireTime) {
                        window.remove(0);
                        changed = true;
                    } else {
                        break;
                    }
                }
                
                if (changed) {
                    // emit entire window content to output collector
                    for (Iterator<TimestampedTuple> it = window.iterator(); it.hasNext(); ) {
                        output.add(it.next());
                    }
                }
            }
        };
    }
}
