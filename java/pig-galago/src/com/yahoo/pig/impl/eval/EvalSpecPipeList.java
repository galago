/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.yahoo.pig.impl.PigContext;

public class EvalSpecPipeList {
    public List<EvalSpecPipe> list;
    PigContext pigContext;

    public EvalSpecPipeList(PigContext pigContext) {
    	this.pigContext = pigContext;
        list = new ArrayList<EvalSpecPipe>();
    }
    
    public void add(EvalSpecPipe addMe) {
        list.add(addMe);
    }

    public EvalSpecPipeList(EvalSpecPipe[] specs) {
        list = new ArrayList<EvalSpecPipe>(specs.length);

        for (int i = 0; i < specs.length; i++) {
            list.add(specs[i]);
        }
    }

    public EvalSpecPipeList(String str) throws IOException {
        String splitStr[] = str.split(";");
        list = new ArrayList<EvalSpecPipe>(splitStr.length);

        for (int i = 0; i < splitStr.length; i++) {
            list.add(new EvalSpecPipe(pigContext, splitStr[i]));
        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (Iterator<EvalSpecPipe> it = list.iterator(); it.hasNext();) {
            sb.append(it.next());
            if (it.hasNext()) sb.append(";");
        }

        return sb.toString();
    }

    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        for (Iterator<EvalSpecPipe> it = list.iterator(); it.hasNext(); ) {
            funcs.addAll(it.next().getFuncs());
        }
        return funcs;
    }
    
}
