/*
 * InputIterator
 * 
 * September 20, 2007 -- Trevor Strohman
 * 
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This annotation describes the type of iterators that will be accepted
 * by a DocumentDataIterator.
 * @author trevor
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface InputIterator {
    /** 
     * The type of iterators that are acceptable to this iterator.
     */
    String className() default "galago.retrieval.extents.CountIterator";
    
    /** 
     * The number of iterators this operator accepts.
     * The default value of -1 means that any number are acceptable.
     */
    int cardinality() default -1;
}
