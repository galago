// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/NumberWordCount.java NumberWordCount galago.types int:document bytes:word int:count order:+word order:+word+document
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class NumberWordCount implements Type<NumberWordCount> {
    public int document;
    public byte[] word;
    public int count; 
    
    public NumberWordCount() {}
    public NumberWordCount( int document, byte[] word, int count ) {
        this.document = document;
        this.word = word;
        this.count = count;
    }  
    
    public String toString() {
        try {
            return String.format( "%d,%s,%d",
                                   document, new String(word, "UTF-8"), count );
        } catch( UnsupportedEncodingException e ) {
            throw new RuntimeException( "Couldn't convert string to UTF-8." );
        }
    } 

    public Order<NumberWordCount> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+word" } ) ) {
            return new WordOrder();
        }
        if ( Arrays.equals( spec, new String[] { "+word", "+document" } ) ) {
            return new WordDocumentOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<NumberWordCount> {
        public void process( NumberWordCount object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class WordOrder implements Order<NumberWordCount> {
        public int hash( NumberWordCount object ) {
            int h = 0;
            h += Utility.hash(object.word);
            return h;
        } 
        public Comparator<NumberWordCount> greaterThan() {
            return new Comparator<NumberWordCount>() {
                public int compare( NumberWordCount one, NumberWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<NumberWordCount> lessThan() {
            return new Comparator<NumberWordCount>() {
                public int compare( NumberWordCount one, NumberWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<NumberWordCount> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<NumberWordCount> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<NumberWordCount> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< NumberWordCount > {
            NumberWordCount last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( NumberWordCount object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.word, last.word) ) { processAll = true; shreddedWriter.processWord( object.word ); }
               shreddedWriter.processTuple( object.document, object.count );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<NumberWordCount> getInputClass() {
                return NumberWordCount.class;
            }
        } 
        public ReaderSource<NumberWordCount> orderedCombiner( Collection<TypeReader<NumberWordCount>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<NumberWordCount> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public NumberWordCount clone( NumberWordCount object ) {
            NumberWordCount result = new NumberWordCount();
            if ( object == null ) return result;
            result.document = object.document; 
            result.word = object.word; 
            result.count = object.count; 
            return result;
        }                 
        public Class<NumberWordCount> getOrderedClass() {
            return NumberWordCount.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+word"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processWord( byte[] word ) throws IOException;
            public void processTuple( int document, int count ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            byte[] lastWord;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processWord( byte[] word ) {
                lastWord = word;
                buffer.processWord( word );
            }
            public final void processTuple( int document, int count ) throws IOException {
                if( lastFlush ) {
                    if( buffer.words.size() == 0 ) buffer.processWord( lastWord );
                    lastFlush = false;
                }
                buffer.processTuple( document, count );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getDocument() );
                    output.writeInt( buffer.getCount() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushWord( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getWordEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeBytes( buffer.getWord() );
                    output.writeInt( count );
                    buffer.incrementWord();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushWord( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<byte[]> words = new ArrayList();
            ArrayList<Integer> wordTupleIdx = new ArrayList();
            int wordReadIdx = 0;
                            
            int[] documents;
            int[] counts;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                documents = new int[batchSize];
                counts = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processWord( byte[] word ) {
                words.add( word );
                wordTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int document, int count ) {
                assert words.size() > 0;
                documents[writeTupleIndex] = document;
                counts[writeTupleIndex] = count;
                writeTupleIndex++;
            }
            public void resetData() {
                words.clear();
                wordTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                wordReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementWord() {
                wordReadIdx++;  
            }                                                                                              

            public void autoIncrementWord() {
                while( readTupleIndex >= getWordEndIndex() && readTupleIndex < writeTupleIndex )
                    wordReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getWordEndIndex() {
                if( (wordReadIdx+1) >= wordTupleIdx.size() )
                    return writeTupleIndex;
                return wordTupleIdx.get(wordReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public byte[] getWord() {
                assert readTupleIndex < writeTupleIndex;
                assert wordReadIdx < words.size();
                
                return words.get( wordReadIdx );
            }
            public int getDocument() {
                assert readTupleIndex < writeTupleIndex;
                return documents[readTupleIndex];
            }                                         
            public int getCount() {
                assert readTupleIndex < writeTupleIndex;
                return counts[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getDocument(), getCount() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexWord( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processWord( getWord() );
                    assert getWordEndIndex() <= endIndex;
                    copyTuples( getWordEndIndex(), output );
                    incrementWord();
                }
            }  
            public void copyUntilWord( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getWord(), other.getWord() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processWord( getWord() );
                                      
                        copyTuples( getWordEndIndex(), output );
                    } else {
                        output.processWord( getWord() );
                        copyTuples( getWordEndIndex(), output );
                    }
                    incrementWord();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilWord( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<NumberWordCount>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof NumberWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (NumberWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<NumberWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<NumberWordCount> getOutputClass() {
                return NumberWordCount.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public NumberWordCount read() throws IOException {
                if( uninitialized )
                    initialize();

                NumberWordCount result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<NumberWordCount>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            NumberWordCount last = new NumberWordCount();         
            long updateWordCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getWord(), otherBuffer.getWord() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final NumberWordCount read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                NumberWordCount result = new NumberWordCount();
                
                result.word = buffer.getWord();
                result.document = buffer.getDocument();
                result.count = buffer.getCount();
                
                buffer.incrementTuple();
                buffer.autoIncrementWord();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateWordCount - tupleCount > 0 ) {
                            buffer.words.add(last.word);
                            buffer.wordTupleIdx.add( (int) (updateWordCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateWord();
                        buffer.processTuple( input.readInt(), input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateWord() throws IOException {
                if( updateWordCount > tupleCount )
                    return;
                     
                last.word = input.readBytes();
                updateWordCount = tupleCount + input.readInt();
                                      
                buffer.processWord( last.word );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof NumberWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (NumberWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<NumberWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<NumberWordCount> getOutputClass() {
                return NumberWordCount.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            NumberWordCount last = new NumberWordCount();
            boolean wordProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processWord( byte[] word ) throws IOException {  
                if( wordProcess || Utility.compare( word, last.word ) != 0 ) {
                    last.word = word;
                    processor.processWord( word );
                    wordProcess = false;
                }
            }  
            
            public void resetWord() {
                 wordProcess = true;
            }                                                
                               
            public void processTuple( int document, int count ) throws IOException {
                processor.processTuple( document, count );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            NumberWordCount last = new NumberWordCount();
            public galago.tupleflow.Processor<NumberWordCount> processor;                               
            
            public TupleUnshredder( NumberWordCount.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<NumberWordCount> processor ) {
                this.processor = processor;
            }
            
            public NumberWordCount clone( NumberWordCount object ) {
                NumberWordCount result = new NumberWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void processWord( byte[] word ) throws IOException {
                last.word = word;
            }   
                
            
            public void processTuple( int document, int count ) throws IOException {
                last.document = document;
                last.count = count;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            NumberWordCount last = new NumberWordCount();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public NumberWordCount clone( NumberWordCount object ) {
                NumberWordCount result = new NumberWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void process( NumberWordCount object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.word, object.word ) != 0 || processAll ) { processor.processWord( object.word ); processAll = true; }
                processor.processTuple( object.document, object.count );                                         
            }
                          
            public Class<NumberWordCount> getInputClass() {
                return NumberWordCount.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
    public static class WordDocumentOrder implements Order<NumberWordCount> {
        public int hash( NumberWordCount object ) {
            int h = 0;
            h += Utility.hash(object.word);
            h += Utility.hash(object.document);
            return h;
        } 
        public Comparator<NumberWordCount> greaterThan() {
            return new Comparator<NumberWordCount>() {
                public int compare( NumberWordCount one, NumberWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<NumberWordCount> lessThan() {
            return new Comparator<NumberWordCount>() {
                public int compare( NumberWordCount one, NumberWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<NumberWordCount> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<NumberWordCount> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<NumberWordCount> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< NumberWordCount > {
            NumberWordCount last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( NumberWordCount object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.word, last.word) ) { processAll = true; shreddedWriter.processWord( object.word ); }
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               shreddedWriter.processTuple( object.count );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<NumberWordCount> getInputClass() {
                return NumberWordCount.class;
            }
        } 
        public ReaderSource<NumberWordCount> orderedCombiner( Collection<TypeReader<NumberWordCount>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<NumberWordCount> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public NumberWordCount clone( NumberWordCount object ) {
            NumberWordCount result = new NumberWordCount();
            if ( object == null ) return result;
            result.document = object.document; 
            result.word = object.word; 
            result.count = object.count; 
            return result;
        }                 
        public Class<NumberWordCount> getOrderedClass() {
            return NumberWordCount.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+word", "+document"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processWord( byte[] word ) throws IOException;
            public void processDocument( int document ) throws IOException;
            public void processTuple( int count ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            byte[] lastWord;
            int lastDocument;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processWord( byte[] word ) {
                lastWord = word;
                buffer.processWord( word );
            }
            public void processDocument( int document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public final void processTuple( int count ) throws IOException {
                if( lastFlush ) {
                    if( buffer.words.size() == 0 ) buffer.processWord( lastWord );
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    lastFlush = false;
                }
                buffer.processTuple( count );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getCount() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushWord( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getWordEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeBytes( buffer.getWord() );
                    output.writeInt( count );
                    buffer.incrementWord();
                      
                    flushDocument( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeInt( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushWord( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<byte[]> words = new ArrayList();
            ArrayList<Integer> documents = new ArrayList();
            ArrayList<Integer> wordTupleIdx = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            int wordReadIdx = 0;
            int documentReadIdx = 0;
                            
            int[] counts;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                counts = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processWord( byte[] word ) {
                words.add( word );
                wordTupleIdx.add( writeTupleIndex );
            }                                      
            public void processDocument( int document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int count ) {
                assert words.size() > 0;
                assert documents.size() > 0;
                counts[writeTupleIndex] = count;
                writeTupleIndex++;
            }
            public void resetData() {
                words.clear();
                documents.clear();
                wordTupleIdx.clear();
                documentTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                wordReadIdx = 0;
                documentReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementWord() {
                wordReadIdx++;  
            }                                                                                              

            public void autoIncrementWord() {
                while( readTupleIndex >= getWordEndIndex() && readTupleIndex < writeTupleIndex )
                    wordReadIdx++;
            }                 
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getWordEndIndex() {
                if( (wordReadIdx+1) >= wordTupleIdx.size() )
                    return writeTupleIndex;
                return wordTupleIdx.get(wordReadIdx+1);
            }

            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public byte[] getWord() {
                assert readTupleIndex < writeTupleIndex;
                assert wordReadIdx < words.size();
                
                return words.get( wordReadIdx );
            }
            public int getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }
            public int getCount() {
                assert readTupleIndex < writeTupleIndex;
                return counts[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getCount() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexWord( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processWord( getWord() );
                    assert getWordEndIndex() <= endIndex;
                    copyUntilIndexDocument( getWordEndIndex(), output );
                    incrementWord();
                }
            } 
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyTuples( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            }  
            public void copyUntilWord( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getWord(), other.getWord() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processWord( getWord() );
                                      
                        if( c < 0 ) {
                            copyUntilIndexDocument( getWordEndIndex(), output );
                        } else if( c == 0 ) {
                            copyUntilDocument( other, output );
                            autoIncrementWord();
                            break;
                        }
                    } else {
                        output.processWord( getWord() );
                        copyUntilIndexDocument( getWordEndIndex(), output );
                    }
                    incrementWord();  
                    
               
                }
            }
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        copyTuples( getDocumentEndIndex(), output );
                    } else {
                        output.processDocument( getDocument() );
                        copyTuples( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
                    if( getWordEndIndex() <= readTupleIndex )
                        break;   
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilWord( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<NumberWordCount>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof NumberWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (NumberWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<NumberWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<NumberWordCount> getOutputClass() {
                return NumberWordCount.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public NumberWordCount read() throws IOException {
                if( uninitialized )
                    initialize();

                NumberWordCount result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<NumberWordCount>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            NumberWordCount last = new NumberWordCount();         
            long updateWordCount = -1;
            long updateDocumentCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getWord(), otherBuffer.getWord() );
                    if( result != 0 ) break;
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final NumberWordCount read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                NumberWordCount result = new NumberWordCount();
                
                result.word = buffer.getWord();
                result.document = buffer.getDocument();
                result.count = buffer.getCount();
                
                buffer.incrementTuple();
                buffer.autoIncrementWord();
                buffer.autoIncrementDocument();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateWordCount - tupleCount > 0 ) {
                            buffer.words.add(last.word);
                            buffer.wordTupleIdx.add( (int) (updateWordCount - tupleCount) );
                        }                              
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateDocument();
                        buffer.processTuple( input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateWord() throws IOException {
                if( updateWordCount > tupleCount )
                    return;
                     
                last.word = input.readBytes();
                updateWordCount = tupleCount + input.readInt();
                                      
                buffer.processWord( last.word );
            }
            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                updateWord();
                last.document = input.readInt();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof NumberWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (NumberWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<NumberWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<NumberWordCount> getOutputClass() {
                return NumberWordCount.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            NumberWordCount last = new NumberWordCount();
            boolean wordProcess = true;
            boolean documentProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processWord( byte[] word ) throws IOException {  
                if( wordProcess || Utility.compare( word, last.word ) != 0 ) {
                    last.word = word;
                    processor.processWord( word );
            resetDocument();
                    wordProcess = false;
                }
            }
            public void processDocument( int document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
                    documentProcess = false;
                }
            }  
            
            public void resetWord() {
                 wordProcess = true;
            resetDocument();
            }                                                
            public void resetDocument() {
                 documentProcess = true;
            }                                                
                               
            public void processTuple( int count ) throws IOException {
                processor.processTuple( count );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            NumberWordCount last = new NumberWordCount();
            public galago.tupleflow.Processor<NumberWordCount> processor;                               
            
            public TupleUnshredder( NumberWordCount.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<NumberWordCount> processor ) {
                this.processor = processor;
            }
            
            public NumberWordCount clone( NumberWordCount object ) {
                NumberWordCount result = new NumberWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void processWord( byte[] word ) throws IOException {
                last.word = word;
            }   
                
            public void processDocument( int document ) throws IOException {
                last.document = document;
            }   
                
            
            public void processTuple( int count ) throws IOException {
                last.count = count;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            NumberWordCount last = new NumberWordCount();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public NumberWordCount clone( NumberWordCount object ) {
                NumberWordCount result = new NumberWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void process( NumberWordCount object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.word, object.word ) != 0 || processAll ) { processor.processWord( object.word ); processAll = true; }
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                processor.processTuple( object.count );                                         
            }
                          
            public Class<NumberWordCount> getInputClass() {
                return NumberWordCount.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    