/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.BigDataBag;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.IndexedTuple;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;

public class PigCombine implements Reducer {

    private JobConf         job;
    private CombineDataOutputCollector finalout;
    private DataCollector   evalPipe;
    private OutputCollector oc;
    private int             index;
    private int             inputCount;
    private BigDataBag      bags[];
    private File            tmpdir;

    public void reduce(WritableComparable key, Iterator values, OutputCollector output, Reporter reporter)
            throws IOException {

        try {
            tmpdir = new File(job.get("mapred.task.id"));
            tmpdir.mkdirs();

            BagFactory.init(tmpdir);
            PigContext pigContext = PigMapReduce.getPigContext();
            if (evalPipe == null) {
                inputCount = job.get("pig.inputs").split(";", -1).length;
                oc = output;
                finalout = new CombineDataOutputCollector(oc);
                String evalSpec = job.get("pig.combineFunc", "");
                EvalSpecPipe esp = new EvalSpecPipe(pigContext, evalSpec);
                evalPipe = esp.collector(finalout);
                //throw new RuntimeException("combine spec: " + evalSpec + " combine pipe: " + esp.toString());
                
                inputCount = job.get("pig.inputs").split(";", -1).length;

                bags = new BigDataBag[inputCount];
                for (int i = 0; i < inputCount; i++) {
                    bags[i] = BagFactory.getInstance().getNewBigBag();
                }
            }

            PigSplit split = PigInputFormat.PigRecordReader.getPigRecordReader().getPigFileSplit();
            index = split.getIndex();

            Datum groupName = ((Tuple) key).getField(0);
            finalout.group = ((Tuple) key);
            finalout.index = index;
            
            Tuple t = new Tuple(1 + inputCount);
            t.setField(0, groupName);
            for (int i = 1; i < 1 + inputCount; i++) {
                bags[i - 1].clear();
                t.setField(i, bags[i - 1]);
            }

            while (values.hasNext()) {
                IndexedTuple it = (IndexedTuple) values.next();
                t.getBagField(it.index + 1).add(it);
            }
            for (int i = 0; i < inputCount; i++) {  // XXX: shouldn't we only do this if INNER flag is set?
                if (t.getBagField(1 + i).isEmpty())
                    return;
            }
//          throw new RuntimeException("combine input: " + t.toString());
            evalPipe.add(t);
            evalPipe.add(null); // EOF marker
        } catch (Throwable tr) {
            tr.printStackTrace();
            RuntimeException exp = new RuntimeException(tr.getMessage());
            exp.setStackTrace(tr.getStackTrace());
            throw exp;
        }
    }

    /**
     * Just save off the JobConf for later use.
     */
    public void configure(JobConf job) {
        this.job = job;
    }

    /**
     * Nothing happens here.
     */
    public void close() throws IOException {
    }

    private static class CombineDataOutputCollector extends DataCollector {
        OutputCollector oc    = null;
        Tuple           group = null;
        int             index = -1;

        public CombineDataOutputCollector(OutputCollector oc) {
            this.oc = oc;
        }

        public void add(Tuple t) throws IOException {
            if (t == null) return;  // EOF marker from eval pipeline; ignore
//          throw new RuntimeException("combine collector input: " + t.toString());
            oc.collect(group, new IndexedTuple(t.getTupleField(0),index));
        }
    }

}
