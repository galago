
//
// BinOrderedBinnedIterator
//
// 6 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_BINORDEREDBINNEDTITERATOR_HPP
#define GALAGO_BINORDEREDBINNEDTITERATOR_HPP
                              
#include "indri/indri-platform.h"
#include "indri/greedy_vector"

class BinOrderedBinnedIterator {
private:
    struct BinInfo {
        UINT32 bin;
		
        UINT64 binStart;
        UINT64 binLength;
        UINT64 dataStart;
        UINT64 dataLength;
        UINT64 skipStart;
        UINT64 skipLength;
    };

    const char* invertedData;
    
    indri::utility::greedy_vector<BinInfo> _termBins;
    int _termBinIndex;
    UINT64 _postingsLength;
    UINT64 _documentCount;
   
    const BinInfo& _currentBinInfo() const;

    void _readTermHeader( UINT64 startPosition, UINT64 endPosition );
       
public:    
    BinOrderedBinnedIterator( const char* data, UINT64 start, UINT64 end );
    
    int bin();
    bool next();
    void reset();
    
    const char* binData();
    UINT64 binDataLength();
    const char* skipData();
    UINT64 skipDataLength();
    UINT64 postingsDataLength() const;
    UINT64 documentCount() const;                               
};

#endif // GALAGO_BINORDEREDBINNEDTITERATOR_HPP
