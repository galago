<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@page import="galago.retrieval.Search" %>
<%@page import="galago.retrieval.Search.SearchResult" %>
<%@page import="galago.retrieval.Search.SearchResultItem" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= request.getParameter("query") %> - Galago Search</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        
    <% String query = request.getParameter( "query" ); %>
        
    <div id="header">
        <table>
            <tr><td><a href="http://galagosearch.org"><img src="galago-small.png"/></a></td>
            <td><br/><form action="search"><input name="query" size="40" value="<%= query %>" /> <input value="Search" type="submit" /></form></td></tr>
        </table>
    </div>

    <% 
    SearchResult results = (SearchResult) request.getAttribute( "results" );
    for( SearchResultItem result : results.items ) {
    %>
        <div id="result">
            <a href="document?identifier=<%= result.identifier %>"><%= result.displayTitle %></a><br/>
            <div id="summary"><%= result.summary %></div>
            <div id="meta"><%= result.identifier %> - <%= result.url %></div>
        </div>
    <% } %>
    
    <center>
        <% 
        int startAt = (request.getParameter("start") != null) ? Integer.parseInt(request.getParameter("start")) : 0;
        int count = (request.getParameter("count") != null) ? Integer.parseInt(request.getParameter("count")) : 10;     
        
        if( startAt != 0 ) {
            %><a href="search?query=<%= request.getParameter("query") %>&start=<%= Math.max(startAt-10,0) %>&=count=10">Previous</a><%
        }
        %>
        <%
        if( results.items.size() >= count ) {
            %><a href="search?query=<%= request.getParameter("query") %>&start=<%= startAt+10 %>&=count=<%= count %>">Next</a><%
        }
        %>
    </center>
    </body>
</html>
