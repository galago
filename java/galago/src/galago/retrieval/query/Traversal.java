
//
// Traversal.java
//
// 10 August 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.retrieval.query;

import java.util.ArrayList;

/**
 * 
 * @author trevor
 */
public interface Traversal {
    public void beforeNode( Node object ) throws Exception;
    public Node afterNode( Node object, ArrayList<Node> children ) throws Exception;
}
    
