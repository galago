//
// ExtractQueryTerms.java
//
// Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.experimental;

import galago.parse.Document;
import galago.parse.Porter2Stemmer;
import galago.parse.TagTokenizer;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author trevor
 */
public class ExtractQueryTerms {
    public static class Query {
        Query( String number, String text ) {
            this.number =number;
            this.text= text;
        }
        String number;
        String text;
    }
    
    Porter2Stemmer stemmer = new Porter2Stemmer();
    TagTokenizer tokenizer = new TagTokenizer();
    HashSet<String> stopwords = new HashSet<String>();
    
    public void writeQueries( BufferedWriter writer, ArrayList<Query> queries ) throws IOException {
        for( Query q : queries ) {
            Document result = tokenizer.tokenize(q.text);
            stemmer.process(result);
            
            ArrayList<String> terms = new ArrayList();
            ArrayList<String> phrases = new ArrayList();
            
            for( int i=0; i<result.terms.size(); i++ ) {
                String term = result.terms.get(i);
                
                if( stopwords.contains(term) )
                    result.terms.set(i, null);
            }
            
            for( int i=0; i<result.terms.size()-1; i++ ) {
                String first = result.terms.get(i);
                String second = result.terms.get(i+1);
                
                if( first != null && second != null ) {
                    phrases.add( first + " " + second );
                }
            }

            for( String term : result.terms ) {
                if( term != null )
                    terms.add(term);
            }
            
            if( terms.size() == 0 )
                continue;
            
            writer.write( q.number );
            writer.write( ':' );
            
            for( String term : terms ) {
                writer.write( term );
                writer.write( ' ' );
            }
            
            for( String phrase : phrases ) {
                writer.write( '"' );
                writer.write( phrase );
                writer.write( '"' );
                writer.write( ' ' );
            }
            
            writer.newLine();
        }
    }
    
    public void processFile( String input, String output ) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader( new FileReader( input ) );
        String line;
        ArrayList<Query> queries = new ArrayList<Query>();

        while( (line = reader.readLine()) != null ) {
            String[] fields = line.split(":");
            String number = fields[0];
            String query = fields[1];

            Document result = tokenizer.tokenize(query);
            stemmer.process(result);

            Query q = new Query( number, query );
            queries.add(q);
        }

        reader.close();
        
        BufferedWriter writer = new BufferedWriter( new FileWriter( output ) );
        writeQueries( writer, queries );
        writer.close();
    }

    public static void main( String[] args ) throws FileNotFoundException, IOException {
        ExtractQueryTerms eqt = new ExtractQueryTerms();
        
        HashSet<String> stopwords = new HashSet<String>();
        BufferedReader reader = new BufferedReader( new FileReader(args[0]) );
        String line;
        while( (line = reader.readLine()) != null ) {
            stopwords.add(line);
        }
        eqt.stopwords = stopwords;
        
        for( int i=1; i<args.length; i++ ) {
            eqt.processFile( args[i], args[i] + "-output" );
        }
    }
}
