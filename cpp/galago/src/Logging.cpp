//
// Logging
//
// 10 January 2007
// 
// BSD License (http://galagosearch.org/license)
//

#include "Logging.hpp"
#include "indri/IndriTimer.hpp"

static indri::utility::IndriTimer timer;

void galago_log( galago_logging_docordered& log ) {
    std::cout << "qscored,";
    timer.printElapsedMicroseconds( std::cout );
    std::cout << "," << log.fullScored << "," << log.scoreSkip << "," << log.scoreBoundSkip << ",";

    for( int i=0; i<30; i++ ) {
        std::cout << log.docsScored[i] << ",";
    }
    std::cout << std::endl;
}

std::ostream& galago_log( const char* tag ) {
    std::cout << tag << ",";
    timer.printElapsedMicroseconds( std::cout );
    std::cout << ",";
    return std::cout;
}

#ifdef GALAGO_USE_LOGGING

void i_galago_log_start() {
    timer.start();
}

void i_galago_log_query_terms( int termCount ) {
    std::cout << "qterms,";
    timer.printElapsedMicroseconds( std::cout );
    std::cout << "," << termCount << std::endl;
}

void i_galago_log_skipping_term( int remaining ) {
    std::cout << "qskip,";
    timer.printElapsedMicroseconds( std::cout );
    std::cout << remaining << std::endl;
}

void i_galago_log( const galago_logging& log ) {
    std::cout << "qbin,";

    timer.printElapsedMicroseconds( std::cout );

    std::cout << "," <<
        log.mode << "," <<
        log.index << "," <<
        log.score << "," <<
        log.startAccumulators << "," <<
        log.endAccumulators << "," <<
        log.threshold << "," <<
        log.postingsProcessed << "," <<
        log.accumulatorHits << "," <<
        log.skipLength << "," << 
        log.byteLength << std::endl;
}

void i_galago_log_bin_start( galago_logging& log, int mode, int score, int index, int byteLength, int skipLength, int startAccumulators, int threshold ) {
    log.mode = mode;
    log.score = score;
    log.index = index;
    log.byteLength = byteLength;
    log.skipLength = skipLength;
    log.startAccumulators = startAccumulators;
    log.endAccumulators = -1;
    log.postingsProcessed = 0;
    log.accumulatorHits = 0;
    log.threshold = threshold;
}

void i_galago_log_bin_end( galago_logging& log, int endAccumulators ) {
    log.endAccumulators = endAccumulators;
    i_galago_log( log );
}

void i_galago_log_accumulator( galago_logging& log, int document, int score ) {
    log.accumulatorHits++;
    std::cout << "achg," << document << "," << score << std::endl;
}

class LoggingHackObject {
public:
    LoggingHackObject() {
        i_galago_log_start();
    }
};

LoggingHackObject o;

#endif
