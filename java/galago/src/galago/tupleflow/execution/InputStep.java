
//
// InputStep.java
//
// 19 Oct 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow.execution;

/**
 * Represents an input step in a TupleFlow stage.
 * 
 * @author trevor
 */

public class InputStep extends Step {
    String id;

    public InputStep( String id ) {
        this.id = id;
    }

    public InputStep( FileLocation location, String id ) {
        this.id = id;
        this.location = location;
    }

    public String getId() {
        return id;
    }
}