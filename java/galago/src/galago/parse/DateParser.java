/*
 * DateParser
 *
 * September 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 *
 * @author trevor
 */
public class DateParser {
    DateFormat format = DateFormat.getInstance();
    
    public double parse( String dateString ) {
        double result;

        try {
            Date d = format.parse(dateString);
            result = d.getTime() / 1000.0;
            assert false : "this returns milliseconds since 1970, which is kind of limiting.";
        } catch( ParseException e ) {
            return 0;
        }
        
        return result;
    }
}
