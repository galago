/*
 * GalagoPlanCompiler
 *
 * September 1, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;
import com.yahoo.pig.impl.logicalLayer.LOCogroup;
import com.yahoo.pig.impl.logicalLayer.LOEval;
import com.yahoo.pig.impl.logicalLayer.LOLoad;
import com.yahoo.pig.impl.logicalLayer.LORead;
import com.yahoo.pig.impl.logicalLayer.LOStore;
import com.yahoo.pig.impl.logicalLayer.LOUnion;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;
import com.yahoo.pig.impl.physicalLayer.IntermedResult;
import com.yahoo.pig.impl.physicalLayer.PhysicalOperator;
import com.yahoo.pig.impl.physicalLayer.PlanCompiler;
import galago.tupleflow.Parameters;
import galago.tupleflow.execution.Job;
import galago.tupleflow.execution.Connection;
import galago.tupleflow.execution.ConnectionAssignmentType;
import galago.tupleflow.execution.ConnectionEndPoint;
import galago.tupleflow.execution.ConnectionPointType;
import galago.tupleflow.execution.InputStep;
import galago.tupleflow.execution.OutputStep;
import galago.tupleflow.execution.Stage;
import galago.tupleflow.execution.StageConnectionPoint;
import galago.tupleflow.execution.Step;
import galago.tupleflow.Parameters.Value;
import galago.tupleflow.execution.Connection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author trevor
 */
public class GalagoPlanCompiler extends PlanCompiler  {
    PigContext pigContext;
    int stageCounter = 0;
    Job job = new Job();
    HashMap<String, String> outputToStage = new HashMap<String, String>();
    HashMap<String, Connection> connections = new HashMap<String, Connection>();
    
    public GalagoPlanCompiler( PigContext pigContext ) {
        super(pigContext);
        
        if( System.getProperty( "hashCount" ) != null )
            job.properties.put( "hashCount", System.getProperty("hashCount") );
    }
    
    private Stage buildStage( String stageType ) {
        Stage s = new Stage();
        s.name = stageType + stageCounter;
        stageCounter++;
        job.add(s);
        return s;
    }
    
    private StageConnectionPoint buildPoint( ConnectionPointType type, String name ) {
        return new StageConnectionPoint( type, 
                                         name,
                                         "com.yahoo.pig.impl.galago.Tuple",
                                         new String[0],
                                         null );
    }
    
    private StageConnectionPoint buildOutput( String name ) {
        return buildPoint( ConnectionPointType.Output, name );
    }
    
    private StageConnectionPoint buildInput( String name ) {
        return buildPoint( ConnectionPointType.Input, name );
    }    
    
    private void storeOutputLookup( String aliasName, String stageName ) {
        outputToStage.put( aliasName, stageName );
    }
    
    public void addLoadStage( LOLoad load ) {
        Stage splitStage = buildStage( "split" + load.alias );
        String splitAlias = load.alias + "$split";
        
        Parameters splitParameters = new Parameters();
        splitParameters.add( "filename", load.filename );
        
        Step splitGeneratorStep = new Step( null, "com.yahoo.pig.impl.galago.MakeReadSplits", splitParameters );
        splitStage.steps.add( splitGeneratorStep );
        splitStage.steps.add( new OutputStep( null, splitAlias ) );
        splitStage.connections.put( splitAlias, buildOutput( splitAlias ) );
        storeOutputLookup( splitAlias, splitStage.name );
        
        Stage loadStage = buildStage( "load" + load.alias );
        
        Parameters loadParameters = new Parameters();
        loadParameters.add( "function", load.lf.toString() );
        
        Step inputStep = new InputStep( null, splitAlias );
        Step loadStep = new Step( null, "com.yahoo.pig.impl.galago.ReadTuples", loadParameters );
        Step storeStep = new OutputStep( null, load.alias );
        
        loadStage.steps.add( inputStep );
        loadStage.steps.add(loadStep);
        loadStage.steps.add(storeStep);
        loadStage.connections.put( splitAlias, buildInput( splitAlias ) );
        loadStage.connections.put( load.alias, buildOutput( load.alias ) );
        
        storeOutputLookup( load.alias, loadStage.name );
        createHashedConnection( loadStage, splitAlias );
    }

    private void addEvalStage(LOEval eval) {
        assert eval.inputs.length == 1;
        LogicalOperator input = eval.inputs[0];
        Stage evalStage = buildStage( "eval" + input.alias );
        
        Parameters evalParameters = new Parameters();
        evalParameters.add( "spec", eval.spec.toString() );
        
        Step inputStep = new InputStep( null, input.alias );
        Step evalStep = new Step( null, "com.yahoo.pig.impl.galago.EvalTuples", evalParameters );
        Step outputStep = new OutputStep( null, eval.alias );

        evalStage.steps.add( inputStep );
        evalStage.steps.add( evalStep );
        evalStage.steps.add( outputStep );
        
        evalStage.connections.put( input.alias, buildInput( input.alias ) );
        evalStage.connections.put( eval.alias, buildOutput( eval.alias ) );

        createEachConnection( evalStage, input.alias );
        storeOutputLookup( eval.alias, evalStage.name );
        connectNowhere( evalStage, eval.alias );
    }
    
    private ConnectionEndPoint createConnectionInput( String source ) {
        assert outputToStage.containsKey( source );
        return new ConnectionEndPoint( null, outputToStage.get( source ), source, ConnectionPointType.Input );
    }
    
    private ConnectionEndPoint createConnectionOutput( Stage destination, String source, String[] hash ) {
        ConnectionAssignmentType type = (hash != null ? ConnectionAssignmentType.Each : ConnectionAssignmentType.Combined);
        return createConnectionOutput( destination, source, hash, type );
    }
    
    private ConnectionEndPoint createConnectionOutput( Stage destination, String source, String[] hash, ConnectionAssignmentType type ) {
        return new ConnectionEndPoint( null, destination.name, source, type, ConnectionPointType.Output );
    }
    
    private void createEachConnection( Stage destination, String source ) {
        createConnection( destination, source, new String[0], null, ConnectionAssignmentType.Each );
    }
    
    private void createCombinedConnection( Stage destination, String source ) {
        createConnection( destination, source, new String[0], null, ConnectionAssignmentType.Combined );
    }
    
    private void createHashedConnection( Stage destination, String source ) {
        createConnection( destination, source, new String[0], new String[] { "+0" }, ConnectionAssignmentType.Each );
    }
    
    private void createConnection( Stage destination, String source, String[] order, String[] hash, ConnectionAssignmentType type ) {
        assert outputToStage.containsKey( source ) : destination.name + " " + source;
        
        if( !connections.containsKey(source) ) {
            Connection c = new Connection( null, "com.yahoo.pig.impl.galago.Tuple", order, hash, -1 );
            c.inputs.add( createConnectionInput( source ) );
            connections.put( source, c );
        }
        
        Connection connection = connections.get( source );
        connection.outputs.add( createConnectionOutput( destination, source, hash, type ) );
        job.connections.add( connection );
    }
    
    private void connectNowhere( Stage outputStage, String outputPoint ) {
        // BUGBUG: should remove this method
        /*
        Connection connection = new Connection( null, "com.yahoo.pig.impl.galago.Tuple", new String[0], null, -1 );
        connection.inputs.add( new ConnectionEndPoint( null, outputStage.name, outputPoint, ConnectionPointType.Input ) );
        job.connections.add( connection );
        */
    }
    
    private void addStoreStage(LOStore store) {
        Stage s = buildStage( "store" + store.alias );
        String inputName = store.inputs[0].alias;
        Parameters parameters = new Parameters();
        
        parameters.add( "filename", store.filename );
        parameters.add( "function", store.sf );
        Step inputStep = new InputStep( inputName );
        Step storeStep = new Step( null, "com.yahoo.pig.impl.galago.StoreTuples", parameters );

        s.steps.add( inputStep );
        s.steps.add( storeStep );
        s.connections.put( inputName, buildInput( inputName ) );
        
        createCombinedConnection( s, inputName );
    }

    private void addGroupStage(LOCogroup group) {
        ArrayList<String> outputNames = new ArrayList();
        
        // Create one Map stage for each input to the COGROUP operator.
        // A COGROUP looks like:
        //  COGROUP a by x, b by y, c by z
        // This COGROUP ends up being 3 different map stages, one for each input,
        // which performs the hashing, etc.
        // Then, there's a final Reduce stage that aligns all the similar tuples
        // and outputs the final tuple set.
        
        Parameters reduceParameters = new Parameters();
        ArrayList<Value> inputParamList = new ArrayList<Value>();
        
        for( int i=0; i<group.inputs.length; i++ ) {
            LogicalOperator input = group.inputs[i];
            GroupBySpec spec = group.specs[i];
            
            Stage inputStage = buildStage( "input" + input.alias );
            String outputName = "map$" + stageCounter + "$" + input.alias + "$" + group.alias;
            outputNames.add( outputName );
            
            inputStage.connections.put( input.alias, buildInput( input.alias ) );
            inputStage.connections.put( outputName, buildOutput( outputName ) );
            storeOutputLookup( outputName, inputStage.name );
            
            Parameters inputParameters = new Parameters();
            inputParameters.add( "group", spec.toString() );
            
            Step inputStep = new InputStep( null, input.alias );
            Step mapStep = new Step( null, "com.yahoo.pig.impl.galago.MapTuples", inputParameters );
            Step outputStep = new OutputStep( null, outputName );
            
            inputStage.steps.add( inputStep );
            inputStage.steps.add( mapStep );
            inputStage.steps.add( outputStep );

            createEachConnection( inputStage, input.alias );
            
            Value root = new Value();
            root.add( "name", outputName );
            root.add( "group", spec.toString() );
            inputParamList.add( root );
        }
        
        Stage reduceStage = buildStage( "reduce" + group.alias );
        reduceParameters.add( "input", inputParamList );
        reduceParameters.add( "eval", "" );
        
        Step reduceStep = new Step( null, "com.yahoo.pig.impl.galago.JoinTuples", reduceParameters );
        Step outputStep = new OutputStep( null, group.alias );
        
        reduceStage.steps.add(reduceStep);
        reduceStage.steps.add(outputStep);

        // Connect the Map stages to the Reduce stage
        for( String outputName : outputNames ) {
            // This is the connection spec in the reduce stage
            reduceStage.connections.put( outputName, buildInput( outputName ) );
            // This builds the connection at the job level
            createHashedConnection( reduceStage, outputName );
        }
        
        reduceStage.connections.put( group.alias, buildOutput( group.alias ) );
        
        storeOutputLookup( group.alias, reduceStage.name );
        connectNowhere( reduceStage, group.alias );
    }

    private void addUnionStage(LOUnion lOUnion) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
    private void addReadStage( LORead read, Map queryResults ) throws IOException {
        IntermedResult intermediate = read.readFrom;
        
        if( !intermediate.compiled() ) {
            compile( intermediate.lp.root(), queryResults );
            intermediate.setCompiled( true );
        }
    }
    
    public PhysicalOperator compile(LogicalOperator lo, Map queryResults) throws IOException {
        // each compile stage just creates something in the job
        // that we can reference later.
        for( LogicalOperator input : lo.inputs )
            compile( input, queryResults );
        
        if (lo instanceof LOLoad) {
            addLoadStage( (LOLoad)lo );
        } else if(lo instanceof LORead) {
            LORead read = (LORead) lo;
            addReadStage( read, queryResults );
        } else if(lo instanceof LOStore) {
            LOStore store = (LOStore) lo;
            addStoreStage( store );
        } else if(lo instanceof LOEval) {
            LOEval eval = (LOEval)lo;
            addEvalStage( eval );
        } else if(lo instanceof LOCogroup) {
            addGroupStage( (LOCogroup)lo );
        } else if(lo instanceof LOUnion) {
            addUnionStage( (LOUnion)lo );
        } else {
            throw new IOException( "unknown logical operator type " + lo.getClass().getName() );
        }
        
        return new POGalago( job );
    }
}
