/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.yahoo.pig.data.AmendableTuple;
import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;

// n-ary, blocking operator. Output has schema: < group_label, { <1>, <2>, ... }, { <a>, <b>, ... } >
class POCogroup extends PhysicalOperator {
    ArrayList<Datum[]>[] sortedInputs;
    GroupBySpec[]       specs;
    
    public POCogroup(PhysicalOperator[] inputs, GroupBySpec[] specs, int outputType) {
    	super(outputType);
        this.inputs = inputs;
        this.specs = specs;
    }
    
    public POCogroup(GroupBySpec[] specs, int outputType) {
    	super(outputType);
        this.inputs = new PhysicalOperator[specs.length];
        this.specs = specs;
    }

    // drain all inputs, and sort each by group (remember, this is a blocking operator!)
    @SuppressWarnings("unchecked")
    public boolean open(boolean continueFromLast) throws IOException {
        if (!super.open(continueFromLast))
            return false;

        sortedInputs = new ArrayList[inputs.length];

        for (int i = 0; i < inputs.length; i++) {
            sortedInputs[i] = new ArrayList<Datum[]>();

            Tuple t;
            while ((t = (Tuple) inputs[i].getNext()) != null) {
                Datum[] multGroups = specs[i].eval(t);
                for (int j = 0; j < multGroups.length; j++) {
                    Datum[] groupAndTuple = new Datum[2];
                    groupAndTuple[0] = multGroups[j];
                    groupAndTuple[1] = t;
                    sortedInputs[i].add(groupAndTuple);
                }
            }

            Collections.sort(sortedInputs[i], new Comparator<Datum[]>() {
                public int compare(Datum[] a, Datum[] b) {
                    return a[0].compareTo(b[0]);
                }
            });
        }

        return true;
    }

    public Tuple getNext() throws IOException {
        
        while (true) { // loop until we find a tuple we're allowed to output (or we hit the end)

            // find the smallest group among all inputs (this is the group we should make a tuple
            // out of)
            Datum smallestGroup = null;
            for (int i = 0; i < inputs.length; i++) {
                if (sortedInputs[i].size() > 0) {
                    Datum g = (sortedInputs[i].get(0))[0];
                    if (smallestGroup == null || g.compareTo(smallestGroup)<0)
                        smallestGroup = g;
                }
            }

            if (smallestGroup == null)
                return null; // we hit the end of the groups, so we're done

            // find all tuples in each input pertaining to the group of interest, and combine the
            // data into a single tuple
            
            Tuple output;
            if (outputType == LogicalOperator.AMENDABLE) output = new AmendableTuple(1 + inputs.length, smallestGroup);
            else output = new Tuple(1 + inputs.length);

            // set first field to the group tuple
            output.setField(0, smallestGroup);
            
            boolean done = true;
            for (int i = 0; i < inputs.length; i++) {
                DataBag b = BagFactory.getInstance().getNewBag();

                while (sortedInputs[i].size() > 0) {
                    Datum g = sortedInputs[i].get(0)[0];

                    Tuple t = (Tuple) sortedInputs[i].get(0)[1];

                    if (g.compareTo(smallestGroup) < 0) {
                        sortedInputs[i].remove(0); // discard this tuple
                    } else if (g.equals(smallestGroup)) {
                        b.add(t);
                        sortedInputs[i].remove(0);
                    } else {
                        break;
                    }
                }

                if (specs[i].isInner && b.isEmpty())
                    done = false; // this input uses "inner" semantics, and it has no tuples for
                                    // this group, so suppress the tuple we're currently building

                output.setField(1 + i, b);
            }

            if (done)
                return output;
        }

    }

}
