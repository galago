
//
// ConnectionPointType.java
//
// 19 Oct 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow.execution;

/**
 * Represents the end point type of a TupleFlow connection: either input
 * or output.
 * 
 * @author trevor
 */

public enum ConnectionPointType {
    Input   { @Override public String toString() { return "input"; } },
    Output  { @Override public String toString() { return "output"; } };

    public static boolean connectable( ConnectionPointType one, ConnectionPointType two ) {
        return one != two;
    }
}

