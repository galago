/*
 * PositionIndex
 *
 * March 29, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.index.DocumentTransformationFactory;
import galago.util.ExtentArray;
import galago.tupleflow.Processor;
import galago.tupleflow.VByteInput;
import galago.parse.Document;
import galago.retrieval.extents.CountIterator;
import galago.retrieval.extents.ExtentIterator;
import galago.tupleflow.BufferedDataStream;
import java.io.DataInput;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * Reads a simple positions-based index, where each inverted list in the
 * index contains both term count information and term position information.
 * The term counts data is stored separately from term position information for
 * faster query processing when no positions are needed.
 * 
 * For now, the iterator loads everything into memory before starting query 
 * processing, which is not a workable solution for larger collections.
 * 
 * @author trevor
 */
public class PositionIndexReader {
    public class ExtentListIterator extends ExtentIterator {
        int documentCount;
        int totalPositionCount;
        
        VByteInput documents;
        VByteInput counts;
        VByteInput positions;

        int documentIndex;
        int currentDocument;
        int currentCount;
        ExtentArray extentArray;

        IndexReader.Iterator iterator;
        
        ExtentListIterator( IndexReader.Iterator iterator ) throws IOException {
            this.iterator = iterator;
            load();
        }
        
        private void load() throws IOException {
            long startPosition = iterator.getListStart();
            long endPosition = iterator.getListEnd();

            RandomAccessFile input = reader.getInput();
            input.seek( startPosition );
            DataInput stream = new VByteInput( reader.getInput() );

            int options = stream.readInt();
            documentCount = stream.readInt();
            totalPositionCount = stream.readInt();
            
            long documentByteLength = stream.readLong();
            long countsByteLength = stream.readLong();
            long positionsByteLength = stream.readLong();
            
            long documentStart = input.getFilePointer();
            long documentEnd = documentStart + documentByteLength;
            
            long countsStart = documentEnd;
            long countsEnd = countsStart + countsByteLength;
            
            long positionsStart = countsEnd;
            long positionsEnd = positionsStart + positionsByteLength;

            assert positionsEnd == endPosition;
            
            // create streams for each kind of data
            documents = new VByteInput( new BufferedDataStream( input, documentStart, documentEnd ) );
            counts = new VByteInput( new BufferedDataStream( input, countsStart, countsEnd ) );
            positions = new VByteInput( new BufferedDataStream( input, positionsStart, positionsEnd ) );

            extentArray = new ExtentArray();
            documentIndex = 0;
            loadExtents();
        }
        
        private void loadExtents() throws IOException {
            currentDocument += documents.readInt();
            currentCount = counts.readInt();
            extentArray.reset();
            
            int position = 0;
            for( int i=0; i<currentCount; i++ ) {
                position += positions.readInt();
                extentArray.add( currentDocument, position, position+1 );
            }
            
        }
        
        public void reset() throws IOException {
            currentDocument = 0;
            currentCount = 0;
            extentArray.reset();
            
            load();
        }

        public long getByteLength() {
            return iterator.getListEnd() - iterator.getListStart();
        }
        
        public String getCurrentTerm() throws IOException {
            return iterator.currentTerm();
        }

        public void nextDocument() throws IOException {
            documentIndex += 1;
            
            if( !isDone() ) {
                loadExtents();
            }
        }

        public void nextTerm() throws IOException {
            iterator.nextTerm();
            load();
        }

        public boolean isDone() {
            return documentIndex >= documentCount;
        }

        public ExtentArray extents() {
            return extentArray;
        }

        public int document() {
            return currentDocument;
        }

        public int count() {
            return currentCount;
        }
    }
    
    IndexReader reader;
    
    public PositionIndexReader( String pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }

    /**
     * Returns an iterator pointing at the first term in the index.
     */

    public ExtentListIterator getIterator() throws IOException {
        return new ExtentListIterator( reader.getIterator() );
    }

    /**
     * Returns an iterator pointing at the specified term, or 
     * null if the term doesn't exist in the inverted file.
     */

    public ExtentListIterator getTermExtents( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new ExtentListIterator( iterator );
        
        return null;
    }
    
    /**
     * Returns an iterator pointing at the specified term, or 
     * null if the term doesn't exist in the inverted file.
     */

    public CountIterator getTermCounts( String term ) throws IOException {
        return getTermExtents( term );
    }
    
    List< Processor<Document> > transformations() {
        return DocumentTransformationFactory.instance( reader.getManifest() );
    }
    
    List< Processor<Document> > transformations( String field ) {
        return transformations();
    }

    void close() throws IOException {
        reader.close();
    }
}
