/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;

import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.LogicalPlan;
import com.yahoo.pig.impl.pigbodyExec.PigBodyLauncher;

public class POPigbody extends PhysicalOperator {
    
    LogicalPlan logicalPlan = null;
    
    PigContext pigContext = null;
    
    static PigBodyLauncher pig = new PigBodyLauncher();
    
    public POPigbody(PigContext pigContext, LogicalPlan logicalPlan) {
        this.pigContext = pigContext;
        this.logicalPlan = logicalPlan;
    }

    public boolean open(boolean continueFromLast) throws IOException {
        // first, call open() on all inputs
        if (inputs != null) {
            for (int i = 0; i < inputs.length; i++) {
                if (!inputs[i].open(continueFromLast))
                    return false;
            }
        }

        // then, have hadoop run this Pig job:
        boolean success = pig.launchPigBodyJob(logicalPlan, logicalPlan.getFuncs(), pigContext);
        if (!success) {
            // XXX: If we throw an exception on failure, why do we return a boolean ?!? - ben
            throw new IOException("Job failed");
        }
        return success;
    }

    public Tuple getNext() throws IOException {
        // drain all inputs
        for (int i = 0; i < inputs.length; i++) {
            while (inputs[i].getNext() != null)
                ;
        }

        // indicate that we are done
        return null;
    }
    
}
