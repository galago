/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.LinkedList;
import java.util.List;

import com.yahoo.pig.impl.PigContext;


public class LogicalPlan {
    public String alias;
	LogicalOperator root;
	PigContext pigContext;
	
	public LogicalPlan(LogicalOperator rootIn, PigContext pigContext) {
		this.pigContext = pigContext;
		root = rootIn;
        alias = root.alias;
	}
	
	public LogicalOperator root() {
		return root;
	}

    public void setRoot(LogicalOperator newRoot) {
        root = newRoot;
    }
	
	public PigContext getPigContext() {
		return pigContext;
	}

    public List<String> getFuncs() {
        if (root == null) return new LinkedList<String>();
        else return root.getFuncs();
    }
	
	// indentation for root is 0
	public String toString() {		
		StringBuffer sb = new StringBuffer();
		sb.append(root.name() +"(" + root.arguments() +")\n");
		sb.append(appendChildren(root, 1));
		return sb.toString();
	}
	public String appendChildren(LogicalOperator parent, int indentation) {
		StringBuffer sb = new StringBuffer();
		LogicalOperator[] children = parent.children();
		for(int i=0; i != children.length; i++) {
			for(int j=0; j != indentation; j++) {
				sb.append("\t");
			}
			sb.append(children[i].name() + "(" + children[i].arguments()+ ")\n");
			sb.append(appendChildren(children[i], indentation+1));
		}
		return sb.toString();
	}
	
	public int getOutputType(){
		return root.getOutputType();
	}
	
}
