/*==========================================================================
 * Copyright (c) 2004 University of Massachusetts.  All Rights Reserved.
 *
 * Use of the Lemur Toolkit for Language Modeling and Information Retrieval
 * is subject to the terms of the software license set forth in the LICENSE
 * file included with this software, and also available at
 * http://www.lemurproject.org/license.html
 *
 *==========================================================================
*/

//
// FileMapping
//
// 5 January 2007 -- tds
//

#include <string>
#include "indri/indri-platform.h"
#include "indri/File.hpp"

#ifndef WIN32
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#endif
                                   
#include "indri/FileMapping.hpp"
                  
#ifdef WIN32
indri::file::FileMapping::FileMapping( HANDLE handle, const void* _region, const UINT64 _length ) 
  :
  _handle(handle),
  region(region),
  length(length)
{   
}      

indri::file::FileMapping::~FileMapping() {
    UnmapViewOfFile(region);
    CloseHandle(_handle);
}     
#else
indri::file::FileMapping::FileMapping( const void* _region, const UINT64 _length ) 
  :
  region(_region),
  length(_length)
{
}

indri::file::FileMapping::~FileMapping() {
  munmap( const_cast<void*>(region), length );
}                          
#endif // WIN32

