/*
 * StatisticsGathererTest.java
 * JUnit based test
 *
 * Created on September 14, 2007, 9:04 AM
 */

package galago.retrieval.extents;

import java.io.IOException;
import junit.framework.*;

/**
 *
 * @author trevor
 */
public class StatisticsGathererTest extends TestCase {
    public StatisticsGathererTest(String testName) {
        super(testName);
    }
    
    public void testGather() throws IOException {
        int[][] data = { { 1, 5 }, { 3, 7 } };
        ExtentIterator iterator = new FakeExtentIterator( data );
        StatisticsGatherer instance = new StatisticsGatherer( iterator );
        instance.run();
        
        assertEquals( instance.getTermCount(), 2 );
        assertEquals( instance.getDocumentCount(), 2 );
    }
}
