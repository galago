/*
 * TermFeatureExtractor
 *
 * December 17, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import galago.tupleflow.Processor;
import galago.types.DocumentWordCount;
import galago.types.WordCount;

/**
 *
 * @author trevor
 */
public class TermFeatureExtractor implements FeatureExtractor {
    TokenCounter counter;
    
    /** Creates a new instance of TermFeatureExtractor */
    public TermFeatureExtractor( Processor<DocumentWordCount> postings, Processor<WordCount> counts ) {
        counter = new TokenCounter( postings, counts );
    }
    
    public static ArrayList<String> removeNulls( List<String> tokens ) {
        ArrayList<String> result = new ArrayList<String>();
        for( String token : tokens ) {
            if( token == null )
                continue;
            result.add(token);
        }
        return result;
    }
    
    public void extract( String id, Document document ) throws IOException {
        ArrayList<String> tokens = removeNulls(document.terms);
        counter.process( id, tokens );
    }
    
    public void close() throws IOException {
        counter.close();
    }
}
