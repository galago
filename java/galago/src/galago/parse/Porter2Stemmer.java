/*
 * Porter2Stemmer
 *
 * December 5, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.InputClass;
import galago.tupleflow.Linkage;
import galago.tupleflow.NullProcessor;
import galago.tupleflow.OutputClass;
import galago.tupleflow.Processor;
import galago.tupleflow.Source;
import galago.tupleflow.Step;
import galago.tupleflow.execution.Verified;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import org.tartarus.snowball.ext.englishStemmer;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.parse.Document")
public class Porter2Stemmer implements Processor<Document>, Source<Document> {
    englishStemmer stemmer = new englishStemmer();
    HashMap<String, String> cache = new HashMap();
    public Processor<Document> processor = new NullProcessor( Document.class );
   
    public void process( Document document ) throws IOException {
        List<String> words = document.terms;
        
        for( int i=0; i<words.size(); i++ ) {
            String word = words.get(i);
            
            if( word != null ) {
                if( cache.containsKey(word) ) {
                    words.set(i, cache.get(word));
                } else {
                    stemmer.setCurrent(word);
                    if( stemmer.stem() ) {
                        String stem = stemmer.getCurrent();
                        words.set(i, stem);
                        cache.put(word, stem);
                    } else {
                        cache.put(word, word);
                    }
                }
                
                if( cache.size() > 50000 )
                    cache.clear();
            }
        }
        
        processor.process( document );
    }
    
    public void setProcessor( Step processor ) throws IncompatibleProcessorException {
        Linkage.link( this, processor );
    }

    public void close() throws IOException {
        processor.close();
    }
}
