//
// MergeTest.java
//
// Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.tupleflow.execution;

import galago.tupleflow.Order;
import galago.tupleflow.OrderedCombiner;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.types.DocumentLengthWordCount;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author trevor
 */
public class MergeTest {
    public static class Params implements TupleFlowParameters {
        Parameters p;
        public Params( Parameters p ) {
            this.p=p;
        }
        public Parameters getXML() {
            return p;
        }

        public TypeReader getTypeReader(String specification) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public Processor getTypeWriter(String specification) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean readerExists(String specification, String className, String[] order) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        public boolean writerExists(String specification, String className, String[] order) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
        
    }
    public static void main( String[] args ) throws Exception {
        Parameters pp = new Parameters();
        pp.add( "class", "galago.types.DocumentLengthWordCount" );
        Params p = new Params( pp );
        galago.tupleflow.NullProcessor processor = new galago.tupleflow.NullProcessor(p);
        
        Order o = new DocumentLengthWordCount().getOrder( "+word", "+document" );
        OrderedCombiner c= OrderedCombiner.combineFromFiles(Arrays.asList(args), o, processor);
        
        c.run();
    }

}
