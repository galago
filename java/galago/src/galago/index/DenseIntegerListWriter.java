/*
 * DenseIntegerListWriter
 * 
 * October 5, 2007 -- Trevor Strohman
 *
 * BSD License (http://galagosearch.org/licenses)
 */

package galago.index;

import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.types.DocumentNumberWordInteger;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This class creates dense lists of bytes.
 *
 * Most Galago list writers create sparse lists.  Typically we want to store some
 * kind of information about documents, like the number of times the word 'dog'
 * appears in each document.  Since 'dog' only appears in a few documents, the
 * efficient way to do this is to store pairs, like (document, count).  If we want
 * to know how many times the word 'dog' appears in document number 20, we fetch
 * the inverted list for 'dog', then scan down it looking for document 20.  If document
 * 20 never appears, it's implied that document 20 doesn't contain 'dog' at all.
 *
 * For a quantity like PageRank, we assume that every document has some non-zero
 * value, which means that storing the document number for each posting is silly.
 * In the rare case where a document has no PageRank value, we just store a 0.
 * This is what we mean by dense instead of sparse data.
 *
 * The other advantage of a dense repesentation is random access.  These lists
 * are just one byte per document, so you can treat them like a byte array and
 * jump right to the appropriate value for a particular document.
 *
 * @author trevor
 */
public class DenseIntegerListWriter implements DocumentNumberWordInteger.WordDocumentOrder.ShreddedProcessor {
    InvertedListWriter writer;
    DenseIntegerInvertedList list;

    class DenseIntegerInvertedList implements InvertedList {
        BackedCompressedByteBuffer buffer;
        CompressedByteBuffer header;
        int nextDocument = 0;
        byte[] word;
        int bound;
        
        DenseIntegerInvertedList( byte[] word ) {
            this.word = word;
            this.buffer = new BackedCompressedByteBuffer();
            this.bound = 0;
            this.header = new CompressedByteBuffer();
        }
        
        public void addDocument( int document ) throws IOException {
            for( ; nextDocument < document; nextDocument++ )
                buffer.addRaw(0);
            nextDocument++;
        }
        
        public void addValue( int value ) throws IOException {
            buffer.addRaw(value);
            bound = Math.max( value, bound );
        }
        
        public byte[] word() {
            return word;
        }

        public long dataLength() {
            return header.length() + buffer.length();
        }

        public void write(DataOutputStream stream) throws IOException {
            header.write(stream);
            buffer.write(stream);
        }
        
        public void close() {
            header.add( bound );
        }
    }
    
    public DenseIntegerListWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        writer = new InvertedListWriter( parameters );
    }
    
    public DenseIntegerListWriter( String filename ) throws FileNotFoundException, IOException {
        writer = new InvertedListWriter( filename );
    }
    
    public void close() throws IOException {
        flush();
        writer.close();
    }
    
    void flush() throws IOException {
        if( list != null ) {
            list.close();
            writer.add(list);
        }
        list = null;
    }

    public void processWord(byte[] word) throws IOException {
        flush();
        list = new DenseIntegerInvertedList(word);
    }

    public void processDocument(int document) throws IOException {
        list.addDocument( document );
    }

    public void processTuple(int value) throws IOException {
        list.addValue( value );
    }
}
