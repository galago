/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;


final public class ColEvalItem extends NestableEvalItem {
    public String alias = null;
    public int colNum = -1;
     
    public ColEvalItem(PigContext pigContext, int colNum) {
    	super(pigContext);
        this.colNum = colNum;
    }
    
    public ColEvalItem(PigContext pigContext, String alias) {
    	super(pigContext);
        this.alias = alias;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        if (colNum != -1) sb.append("$" + colNum);
        else sb.append(alias);
        if (nestedEval != null) sb.append(nestedEval);
        
        if (subColSpec != null) sb.append(".(" + subColSpec + ")");
        
        return sb.toString();
    }

    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        
        if (nestedEval != null) funcs.addAll(nestedEval.getFuncs());
        
        if (subColSpec != null) funcs.addAll(subColSpec.getFuncs());
        
        return funcs;
    }
    
    public String stringVal(Tuple tup) throws IOException {
        return tup.getAtomField(colNum).strval();
    }
    
    
    @Override
    public Double doubleVal(Tuple tup) throws IOException {
        return tup.getAtomField(colNum).numval();
    }
    
    
    @Override
    public SchemaItem mapInputSchemaInitial(SchemaItem input){
    	if (colNum == -1 || input == null)
    		return new SchemaItemList();
    	SchemaItem output = input.schemaFor(colNum);
    	if (output == null)
    		output = new SchemaItemList();
    	return output;
    }

    @Override
    public void eval(Tuple input, DataCollector output) throws IOException {
        
        Datum d = null;
        try {
            d = input.getField(colNum);
        } catch (IOException e) {
            System.out.println("Warning: tuple does not contain field #" + colNum + "; substituting empty string.");
            output.add(new Tuple(new DataAtom()));  // insert default value
            return;
        }
        
        Datum amendKey = null;
        if (input instanceof AmendableTuple) amendKey = ((AmendableTuple) input).getAmendKey();
        
        if (d instanceof DataAtom) atomEval((DataAtom) d, output);
        else if (d instanceof Tuple) tupleEval((Tuple) d, output);
        else bagEval((DataBag) d, amendKey, output);
    }

    @Override
    public Datum simpleEval(Tuple input) throws IOException {
        
        Datum d = null;
        try {
            d = input.getField(colNum);
        }catch(ArrayIndexOutOfBoundsException exp) {
            throw new RuntimeException("ArrayException: |" + input + "| --> |" + colNum + "| |"  + alias + "|");
        } catch (IOException e) {
            System.out.println("Warning: tuple does not contain field #" + colNum + "; substituting empty string.");
            return new DataAtom();  // insert default value
        }
        
        if (nestedEval == null) return d;
        else return nestedEval.simpleEval(d);
    }
}