// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/DocumentExtent.java DocumentExtent galago.types bytes:extentName long:document int:begin int:end order:+extentName+document
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class DocumentExtent implements Type<DocumentExtent> {
    public byte[] extentName;
    public long document;
    public int begin;
    public int end; 
    
    public DocumentExtent() {}
    public DocumentExtent( byte[] extentName, long document, int begin, int end ) {
        this.extentName = extentName;
        this.document = document;
        this.begin = begin;
        this.end = end;
    }  
    
    public String toString() {
        try {
            return String.format( "%s,%d,%d,%d",
                                   new String(extentName, "UTF-8"), document, begin, end );
        } catch( UnsupportedEncodingException e ) {
            throw new RuntimeException( "Couldn't convert string to UTF-8." );
        }
    } 

    public Order<DocumentExtent> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+extentName", "+document" } ) ) {
            return new ExtentNameDocumentOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<DocumentExtent> {
        public void process( DocumentExtent object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class ExtentNameDocumentOrder implements Order<DocumentExtent> {
        public int hash( DocumentExtent object ) {
            int h = 0;
            h += Utility.hash(object.extentName);
            h += Utility.hash(object.document);
            return h;
        } 
        public Comparator<DocumentExtent> greaterThan() {
            return new Comparator<DocumentExtent>() {
                public int compare( DocumentExtent one, DocumentExtent two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.extentName, two.extentName );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentExtent> lessThan() {
            return new Comparator<DocumentExtent>() {
                public int compare( DocumentExtent one, DocumentExtent two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.extentName, two.extentName );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentExtent> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentExtent> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentExtent> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentExtent > {
            DocumentExtent last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentExtent object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.extentName, last.extentName) ) { processAll = true; shreddedWriter.processExtentName( object.extentName ); }
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               shreddedWriter.processTuple( object.begin, object.end );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentExtent> getInputClass() {
                return DocumentExtent.class;
            }
        } 
        public ReaderSource<DocumentExtent> orderedCombiner( Collection<TypeReader<DocumentExtent>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentExtent> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentExtent clone( DocumentExtent object ) {
            DocumentExtent result = new DocumentExtent();
            if ( object == null ) return result;
            result.extentName = object.extentName; 
            result.document = object.document; 
            result.begin = object.begin; 
            result.end = object.end; 
            return result;
        }                 
        public Class<DocumentExtent> getOrderedClass() {
            return DocumentExtent.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+extentName", "+document"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processExtentName( byte[] extentName ) throws IOException;
            public void processDocument( long document ) throws IOException;
            public void processTuple( int begin, int end ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            byte[] lastExtentName;
            long lastDocument;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processExtentName( byte[] extentName ) {
                lastExtentName = extentName;
                buffer.processExtentName( extentName );
            }
            public void processDocument( long document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public final void processTuple( int begin, int end ) throws IOException {
                if( lastFlush ) {
                    if( buffer.extentNames.size() == 0 ) buffer.processExtentName( lastExtentName );
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    lastFlush = false;
                }
                buffer.processTuple( begin, end );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getBegin() );
                    output.writeInt( buffer.getEnd() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushExtentName( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getExtentNameEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeBytes( buffer.getExtentName() );
                    output.writeInt( count );
                    buffer.incrementExtentName();
                      
                    flushDocument( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeLong( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushExtentName( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<byte[]> extentNames = new ArrayList();
            ArrayList<Long> documents = new ArrayList();
            ArrayList<Integer> extentNameTupleIdx = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            int extentNameReadIdx = 0;
            int documentReadIdx = 0;
                            
            int[] begins;
            int[] ends;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                begins = new int[batchSize];
                ends = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processExtentName( byte[] extentName ) {
                extentNames.add( extentName );
                extentNameTupleIdx.add( writeTupleIndex );
            }                                      
            public void processDocument( long document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int begin, int end ) {
                assert extentNames.size() > 0;
                assert documents.size() > 0;
                begins[writeTupleIndex] = begin;
                ends[writeTupleIndex] = end;
                writeTupleIndex++;
            }
            public void resetData() {
                extentNames.clear();
                documents.clear();
                extentNameTupleIdx.clear();
                documentTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                extentNameReadIdx = 0;
                documentReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementExtentName() {
                extentNameReadIdx++;  
            }                                                                                              

            public void autoIncrementExtentName() {
                while( readTupleIndex >= getExtentNameEndIndex() && readTupleIndex < writeTupleIndex )
                    extentNameReadIdx++;
            }                 
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getExtentNameEndIndex() {
                if( (extentNameReadIdx+1) >= extentNameTupleIdx.size() )
                    return writeTupleIndex;
                return extentNameTupleIdx.get(extentNameReadIdx+1);
            }

            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public byte[] getExtentName() {
                assert readTupleIndex < writeTupleIndex;
                assert extentNameReadIdx < extentNames.size();
                
                return extentNames.get( extentNameReadIdx );
            }
            public long getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }
            public int getBegin() {
                assert readTupleIndex < writeTupleIndex;
                return begins[readTupleIndex];
            }                                         
            public int getEnd() {
                assert readTupleIndex < writeTupleIndex;
                return ends[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getBegin(), getEnd() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexExtentName( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processExtentName( getExtentName() );
                    assert getExtentNameEndIndex() <= endIndex;
                    copyUntilIndexDocument( getExtentNameEndIndex(), output );
                    incrementExtentName();
                }
            } 
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyTuples( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            }  
            public void copyUntilExtentName( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getExtentName(), other.getExtentName() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processExtentName( getExtentName() );
                                      
                        if( c < 0 ) {
                            copyUntilIndexDocument( getExtentNameEndIndex(), output );
                        } else if( c == 0 ) {
                            copyUntilDocument( other, output );
                            autoIncrementExtentName();
                            break;
                        }
                    } else {
                        output.processExtentName( getExtentName() );
                        copyUntilIndexDocument( getExtentNameEndIndex(), output );
                    }
                    incrementExtentName();  
                    
               
                }
            }
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        copyTuples( getDocumentEndIndex(), output );
                    } else {
                        output.processDocument( getDocument() );
                        copyTuples( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
                    if( getExtentNameEndIndex() <= readTupleIndex )
                        break;   
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilExtentName( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentExtent>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentExtent.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentExtent.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentExtent>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentExtent> getOutputClass() {
                return DocumentExtent.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentExtent read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentExtent result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentExtent>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentExtent last = new DocumentExtent();         
            long updateExtentNameCount = -1;
            long updateDocumentCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getExtentName(), otherBuffer.getExtentName() );
                    if( result != 0 ) break;
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentExtent read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentExtent result = new DocumentExtent();
                
                result.extentName = buffer.getExtentName();
                result.document = buffer.getDocument();
                result.begin = buffer.getBegin();
                result.end = buffer.getEnd();
                
                buffer.incrementTuple();
                buffer.autoIncrementExtentName();
                buffer.autoIncrementDocument();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateExtentNameCount - tupleCount > 0 ) {
                            buffer.extentNames.add(last.extentName);
                            buffer.extentNameTupleIdx.add( (int) (updateExtentNameCount - tupleCount) );
                        }                              
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateDocument();
                        buffer.processTuple( input.readInt(), input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateExtentName() throws IOException {
                if( updateExtentNameCount > tupleCount )
                    return;
                     
                last.extentName = input.readBytes();
                updateExtentNameCount = tupleCount + input.readInt();
                                      
                buffer.processExtentName( last.extentName );
            }
            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                updateExtentName();
                last.document = input.readLong();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentExtent.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentExtent.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentExtent>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentExtent> getOutputClass() {
                return DocumentExtent.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentExtent last = new DocumentExtent();
            boolean extentNameProcess = true;
            boolean documentProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processExtentName( byte[] extentName ) throws IOException {  
                if( extentNameProcess || Utility.compare( extentName, last.extentName ) != 0 ) {
                    last.extentName = extentName;
                    processor.processExtentName( extentName );
            resetDocument();
                    extentNameProcess = false;
                }
            }
            public void processDocument( long document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
                    documentProcess = false;
                }
            }  
            
            public void resetExtentName() {
                 extentNameProcess = true;
            resetDocument();
            }                                                
            public void resetDocument() {
                 documentProcess = true;
            }                                                
                               
            public void processTuple( int begin, int end ) throws IOException {
                processor.processTuple( begin, end );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentExtent last = new DocumentExtent();
            public galago.tupleflow.Processor<DocumentExtent> processor;                               
            
            public TupleUnshredder( DocumentExtent.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentExtent> processor ) {
                this.processor = processor;
            }
            
            public DocumentExtent clone( DocumentExtent object ) {
                DocumentExtent result = new DocumentExtent();
                if ( object == null ) return result;
                result.extentName = object.extentName; 
                result.document = object.document; 
                result.begin = object.begin; 
                result.end = object.end; 
                return result;
            }                 
            
            public void processExtentName( byte[] extentName ) throws IOException {
                last.extentName = extentName;
            }   
                
            public void processDocument( long document ) throws IOException {
                last.document = document;
            }   
                
            
            public void processTuple( int begin, int end ) throws IOException {
                last.begin = begin;
                last.end = end;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentExtent last = new DocumentExtent();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentExtent clone( DocumentExtent object ) {
                DocumentExtent result = new DocumentExtent();
                if ( object == null ) return result;
                result.extentName = object.extentName; 
                result.document = object.document; 
                result.begin = object.begin; 
                result.end = object.end; 
                return result;
            }                 
            
            public void process( DocumentExtent object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.extentName, object.extentName ) != 0 || processAll ) { processor.processExtentName( object.extentName ); processAll = true; }
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                processor.processTuple( object.begin, object.end );                                         
            }
                          
            public Class<DocumentExtent> getInputClass() {
                return DocumentExtent.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    