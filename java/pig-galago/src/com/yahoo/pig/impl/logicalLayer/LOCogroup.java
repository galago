/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.List;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class LOCogroup extends LogicalOperator {
	public GroupBySpec[] specs;
	
	public LOCogroup(PigContext pigContext, LogicalOperator[] inputs, GroupBySpec[] specs) {
		super(pigContext);
        this.inputs = inputs;
        this.specs = specs;
        getOutputType();
    }
	public String name() {
		return "CoGroup";
	}
	public String arguments() {
	    StringBuffer sb = new StringBuffer();
        
        for (int i = 0; i < specs.length; i++) {
            sb.append(specs[i]);
            if (i+1 < specs.length) sb.append(", ");
        }
        
        return sb.toString();
	}
    
    public SchemaItemList outputSchema() {
    	if (schema == null){
	        schema = new SchemaItemList();
	        
        	
	        SchemaItem group = specs[0].mapInputSchema(inputs[0].outputSchema());
	        if (group == null)
	        	group = new SchemaItemList();
	        group.alias = "group";

	        schema.add(group);
	        
	        for (LogicalOperator lo : inputs) {
	        	SchemaItemList inputSchema = lo.outputSchema();
	        	if (inputSchema == null)
	        		inputSchema = new SchemaItemList();  
	            schema.add(inputSchema);
	        }
    	}
    	
    	schema.alias = alias;
        return schema;
    }
    
    public int getOutputType(){
    	int outputType = FIXED;
    	for (int i=0; i<inputs.length; i++){
    		switch (inputs[i].getOutputType()){
    			case FIXED: 
    				continue;
    			case MONOTONE: 
    				outputType = AMENDABLE;
    				break;
    			case AMENDABLE:
    			default:	
    				throw new RuntimeException("Can't feed a cogroup into another in the streaming case");
    		} 
    	}
    	return outputType;
    }

    public List<String> getFuncs() {
        List<String> funcs = super.getFuncs();
        for (int i = 0; i < specs.length; i++) {
            funcs.addAll(specs[i].getFuncs());
        }
        return funcs;
    }
}