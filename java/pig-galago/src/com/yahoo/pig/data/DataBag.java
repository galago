/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import com.yahoo.pig.impl.eval.EvalItem;
import com.yahoo.pig.impl.eval.EvalItemList;
import com.yahoo.pig.impl.eval.StarEvalItem;

/**
 * A collection of Tuples
 */
public class DataBag extends DataCollector implements Datum{
    protected List<Tuple> content;

    public DataBag() {
        content = new ArrayList<Tuple>();
    }

    public DataBag(List<Tuple> c) {
        content = c;
    }

    public DataBag(Tuple t) {
        content = new ArrayList<Tuple>();
        content.add(t);
    }

    public int cardinality() {
        return content.size();
    }

    public boolean isEmpty() {
        return content.size() == 0;
    }
    
    public int compareTo(Object other) {
    	if (other instanceof DataAtom) return +1;
        if (other instanceof Tuple) return -1;
        if (other instanceof DataBag){
        	DataBag bOther = (DataBag) other;
	        if (this.cardinality() != bOther.cardinality()) {
	            return (this.cardinality() - bOther.cardinality());
	        }
	        
	        // same cardinality, so compare tuple by tuple ...
	        this.sort();
	        bOther.sort();
	        
	        Iterator<Tuple> thisIt = this.content();
	        Iterator<Tuple> otherIt = bOther.content();
	        while (thisIt.hasNext() && otherIt.hasNext()) {
	            Tuple thisT = thisIt.next();
	            Tuple otherT = otherIt.next();
	            
	            int c = thisT.compareTo(otherT);
	            if (c != 0) return c;
	        }
	        
	        return 0;   // if we got this far, they must be equal
        }else{
        	return -1;
        }
	        	
    }
    
    public boolean equals(Datum other) {
        return (compareTo(other) == 0);
    }
    
    public void sort() {
        EvalItem item = new StarEvalItem(null);
        EvalItemList itemList = new EvalItemList(null);
        itemList.add(item);
        
        sort(itemList);
    }
    
    public void sort(EvalItemList spec) {
        Collections.sort(content, spec.getComparator());
    }
    
    public void arrange(EvalItemList spec) {
        sort(spec);
    }
    
    public void distinct(EvalItemList spec) {
        Comparator<Tuple> comparator = spec.getComparator();
        
        Collections.sort(content, comparator);
        
        Tuple lastTup = null;
        for (Iterator<Tuple> it = content.iterator(); it.hasNext(); ) {
            Tuple thisTup = it.next();
            
            if (lastTup == null) {
                lastTup = thisTup;
                continue;
            }
            
            if (comparator.compare(thisTup, lastTup) == 0) {
                it.remove();
            } else {
                lastTup = thisTup;
            }
        }
    }

    public Iterator<Tuple> content() {
        return content.iterator();
    }
    

    public void add(Tuple t) {
        //if (t!=null)
        	content.add(t);
    }

    public void addAll(DataBag b) {
        if (b instanceof BigDataBag) {
            Iterator<Tuple> it = b.content();
            while (it.hasNext()) {
                content.add(it.next());
            }
        } else {
            content.addAll(b.content);
        }
    }

    public void remove(Tuple d) {
        content.remove(d);
    }

    /**
     * Returns the value of field i. Since there may be more than one tuple in the bag, this
     * function throws an exception if it is not the case that all tuples agree on this field
     */
    public DataAtom getField(int i) throws IOException {
        DataAtom val = null;

        for (Iterator<Tuple> it = content(); it.hasNext();) {
            DataAtom currentVal = it.next().getAtomField(i);

            if (val == null) {
                val = currentVal;
            } else {
                if (!val.strval().equals(currentVal.strval()))
                    throw new IOException("Cannot call getField on a databag unless all tuples agree.");
            }
        }

        if (val == null)
            throw new IOException("Cannot call getField on an empty databag.");

        return val;
    }

    public void clear(){
    	content = new ArrayList<Tuple>();
    	Runtime.getRuntime().gc();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append('{');
        for (Iterator it = content(); it.hasNext();) {
            sb.append(it.next().toString());
            if (it.hasNext())
                sb.append(", ");
        }
        sb.append('}');
        return sb.toString();
    }
}
