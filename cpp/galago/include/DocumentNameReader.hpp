
//
// DocumentNameReader
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#ifndef GALAGO_DOCUMENTNAMEREADER_HPP
#define GALAGO_DOCUMENTNAMEREADER_HPP

#include <string>
#include <vector>
#include "indri/indri-platform.h"
#include "lemur/RVLCompress.hpp"
#include "indri/RVLDecompressStream.hpp"
#include "indri/File.hpp"
#include "UncompressedRead.hpp"

class DocumentNameReader {  
private:
    struct NameSlot {
        std::string prefix;
        int offset;
        int footerWidth;
        std::vector<int> footers;
    };                           
    
    std::vector<NameSlot> _slots;
    int _documentCount;   
    
public:   
    void getInSlot( std::string& result, const NameSlot& slot, int footerIndex ) const;
    std::string operator[] ( int index ) const;
    std::string get( int index ) const;            
    void read( const std::string& filename );
};                           

#endif // GALAGO_DOCUMENTNAMEREADER_HPP
