/*
 * BinnedFeatureIndex
 *
 * December 15, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import java.io.DataInput;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import galago.index.DocumentTransformationFactory;
import galago.util.IntArray;
import galago.tupleflow.Processor;
import galago.tupleflow.VByteInput;
import galago.parse.Document;
import galago.tupleflow.BufferedDataStream;
import java.io.File;
import java.io.RandomAccessFile;

/**
 *
 * @author trevor
 */
public class BinOrderedBinnedIndexReader {      
    public class BinnedListIterator {
        IndexReader.Iterator iterator;
        long documentCount;
        
        long wordCount;
        
        int currentBin;
        int[] documents;
        
        int[] skipDocuments;
        int[] skipBytes;
        
        class BinInfo {
            int bin;
            
            long binStart;
            long binEnd;
            
            long skipStart;
            long skipLength;
            
            long dataStart;
            long dataLength;
        }
        
        ArrayList<BinInfo> termBins = new ArrayList<BinInfo>();
        int termBinIndex;
        
        long postingsLength;
        long currentPosition;
        
        int termIndex;
        
        public BinnedListIterator( IndexReader.Iterator iterator ) throws IOException {
            this.iterator = iterator;
            loadIndex();
        }
        
        String currentTerm() {
            return iterator.term;
        }
        
        void loadIndex() throws IOException {
            readTermHeader( iterator.getListStart(), iterator.getListEnd() );
        }
        
        void nextTerm() throws IOException {
            iterator.nextTerm();           
            loadIndex();
        }
        
        long postingsByteLength() {
            return postingsLength;
        }
        
        boolean foundTerm() {
            return termIndex >= 0;
        }
        
        private void readTermHeader( long startPosition, long endPosition ) throws IOException {
            assert startPosition <= endPosition;
            
            RandomAccessFile input = reader.getInput();
            input.seek( startPosition );
            DataInput stream = new VByteInput( reader.getInput() );
            int headerLength = stream.readInt();
            int options = stream.readInt();
            documentCount = stream.readInt();
            
            long endHeader = input.getFilePointer() + headerLength;
            boolean useSkips = ((options & 0x01) != 0);
            
            stream = new VByteInput( reader.blockStream( headerLength ) );
            long totalDataLength = 0;
            
            termBins.clear();
            termBinIndex = -1;
            postingsLength = 0;

            try {
                while(true) {
                    BinInfo info = new BinInfo();
                    info.bin = stream.readInt();
                    
                    if( useSkips ) {
                        info.skipLength = stream.readInt();
                    } else {
                        info.skipLength = 0;
                    }

                    info.dataLength = stream.readInt();
                    
                    info.binStart = endHeader + totalDataLength;
                    info.skipStart = info.binStart;
                    info.dataStart = info.skipStart + info.skipLength;
                    info.binEnd = info.dataStart + info.dataLength;
                    
                    termBins.add(info);
                    totalDataLength += info.binEnd - info.binStart;
                    postingsLength += info.dataLength;
                }
            } catch( EOFException e ) {}
            
            assert endPosition == endHeader + totalDataLength;
        }
        
        private void readSkipData( final BinInfo info ) throws IOException {
            if( info.skipLength > 0 ) {
                BufferedDataStream stream = reader.blockStream( info.skipStart, info.skipLength );
                VByteInput vinput = new VByteInput( stream );
                
                IntArray skipDocs = new IntArray( (int)info.skipLength );
                IntArray skipB = new IntArray( (int)info.skipLength );
                int doc = 0;
                int bytes = 0;
                
                try {
                    while(true) {
                        int deltaDoc = vinput.readInt();
                        doc += deltaDoc;
                        int deltaByte = vinput.readInt();
                        bytes += deltaByte;

                        skipDocs.add(doc);
                        skipB.add(bytes);
                    }
                } catch( EOFException e ) {}
                
                skipDocuments = skipDocs.toArray();
                skipBytes = skipB.toArray();
            } else {
                skipDocuments = new int[0];
                skipBytes = new int[0];
            }
        }

        private void readDocumentData(final BinInfo info) throws IOException {
            BufferedDataStream stream = reader.blockStream( info.dataStart, info.dataLength );
            VByteInput vinput = new VByteInput( stream );
            int document = 0;
            IntArray docs = new IntArray( (int)info.dataLength );
            
            try {
                while( true ) {
                    int delta = vinput.readInt();
                    document += delta;
                    docs.add(document);
                }
            } catch( EOFException e ) {}
                
            documents = docs.toArray();
        }
        
        boolean next() throws IOException {
            termBinIndex++;
            
            if (termBinIndex >= termBins.size()) {
                termBinIndex--;
                return false;
            }
            
            BinInfo info = termBins.get(termBinIndex);
            currentBin = info.bin;
            
            readSkipData(info);
            readDocumentData(info);
            return true;
        }

        
        int[] documents() {
            return documents;
        }
        
        int bin() {
            return currentBin;
        }
        
        long documentCount() {
            return documentCount;
        }
    }
    
    IndexReader reader;
    
    public BinOrderedBinnedIndexReader( String pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }
    
    public BinOrderedBinnedIndexReader( File pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }

    public BinnedListIterator getIterator() throws IOException {
        return new BinnedListIterator( reader.getIterator() );
    }

    public BinnedListIterator getTerm( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new BinnedListIterator( iterator );
        
        return null;
    }
    
    public long maximumDocumentCount() {
        return reader.getManifest().get( "maximumDocumentCount", 1 );
    }
    
    public boolean needsQueryWeights() {
        return reader.getManifest().get("needsWeights", false);
    }
    
    List< Processor<Document> > transformations() {
        return DocumentTransformationFactory.instance( reader.getManifest() );
    }

    void close() throws IOException {
        reader.close();
    }
}
