/*
 * DocumentNameNumberer
 *
 * April 19, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentIdentifierNumber;
import galago.types.DocumentIdentifier;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.types.DocumentIdentifier")
@OutputClass(className="galago.types.DocumentIdentifierNumber", order={"+document"})
public class DocumentNameNumberer extends StandardStep<DocumentIdentifier, DocumentIdentifierNumber> {
    int number = 0;
    
    public void process( DocumentIdentifier object ) throws IOException {
        processor.process( new DocumentIdentifierNumber( number, object.identifier, object.length ) );
        number++;
    }
}
