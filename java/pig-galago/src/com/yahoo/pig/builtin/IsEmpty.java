/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;

import com.yahoo.pig.FilterFunc;
import com.yahoo.pig.data.Tuple;

public class IsEmpty extends FilterFunc {

    @Override
    public boolean exec(Tuple input) throws IOException {
        return (input.getBagField(0).cardinality() == 0);
    }

}
