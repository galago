package galago.tupleflow;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public class Max<T> implements Processor<T> {
    TopK<T> topk;
    Order<T> order;
    
    /** Creates a new instance of Max */
    public Max( Processor<T> processor, Order<T> key, Order<T> combined, int limit, int batch ) {
        this.order = key;
        topk = new TopK( processor, key.greaterThan(), combined.greaterThan(), limit, batch );
    }
    
    public Class<T> getInputClass() {
        return order.getOrderedClass();
    }
    
    public void process( T object ) throws IOException {
        topk.process(object);
    }
    
    public void close() throws IOException {
        topk.close();
    }
}
