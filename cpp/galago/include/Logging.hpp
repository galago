
//
// Logging
//
// 10 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_LOGGING_HPP
#define GALAGO_LOGGING_HPP

#include "indri/indri-platform.h"
#include <string>

#define GALAGO_MODE_OR		(1)
#define GALAGO_MODE_AND		(2)

struct galago_logging {
	int mode;
	int score;
	int index;
	
	UINT64 byteLength;
	UINT64 skipLength;
	int startAccumulators;
	int endAccumulators;

	int postingsProcessed;
	int accumulatorHits;
	int threshold;
};

struct galago_logging_docordered {
    int docsScored[256];
    int docsMoved[256];
    int fullScored;
    int scoreSkip;
    int scoreBoundSkip;

    galago_logging_docordered() {
        for( int i=0; i<256; i++ ) {
            docsScored[i] = docsMoved[i] = 0;
        }
        fullScored = 0;
        scoreSkip= 0;
        scoreBoundSkip = 0;
    }
};

std::ostream& galago_log( const char* tag );

void galago_log( galago_logging_docordered& log );

void i_galago_log_query_terms( int termCount );
void i_galago_log_skipping_term( int remaining );
void i_galago_log_bin_start( galago_logging& log, int mode, int score, int index, int byteLength, int skipLength, int startAccumulators, int threshold );
void i_galago_log_bin_end( galago_logging& log, int endAccumulators );
void i_galago_log_accumulator( galago_logging& log, int document, int score );

#if defined(GALAGO_LOG_EVERYTHING) && !defined(GALAGO_USE_LOGGING)
    #define GALAGO_USE_LOGGING
#endif

#ifndef GALAGO_USE_LOGGING

    inline void galago_log_query_terms( int termCount ) {}
    inline void galago_log_skipping_term( int remaining ) {}
    inline void galago_log_bin_start( galago_logging& log, int mode, int score, int index, int byteLength, int skipLength, int startAccumulators, int threshold ) {}
    inline void galago_log_bin_end( galago_logging& log, int endAccumulators ) {}
    inline void galago_log_posting( galago_logging& log ) {}
    inline void galago_log_accumulator( galago_logging& log, int document, int score ) {}

#else

    inline void galago_log_skipping_term( int remaining ) {
        i_galago_log_skipping_term(remaining);
    }

    inline void galago_log_query_terms( int termCount ) {
        i_galago_log_query_terms( termCount );
    }

    inline void galago_log_bin_start( galago_logging& log, int mode, int score, int index, int byteLength, int skipLength, int startAccumulators, int threshold ) {
        i_galago_log_bin_start( log, mode, score, index, byteLength, skipLength, startAccumulators, threshold );
    }
    
    inline void galago_log_bin_end( galago_logging& log, int endAccumulators ) {
        i_galago_log_bin_end( log, endAccumulators );
    }
    
    inline void galago_log_posting( galago_logging& log ) {
        log.postingsProcessed++;
    }
    
    inline void galago_log_accumulator( galago_logging& log, int document, int score ) {
    #ifdef GALAGO_LOG_EVERYTHING
        i_galago_log_accumulator( log, document, score );
    #endif
    }

#endif

#endif // GALAGO_LOGGING_HPP

