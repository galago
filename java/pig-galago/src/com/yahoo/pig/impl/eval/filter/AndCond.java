/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.Tuple;

public class AndCond extends Cond {
    public List<Cond> cList;
    
    public AndCond(List<Cond> cList) {
        this.cList = cList;
    }

    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        for (Iterator<Cond> it = cList.iterator(); it.hasNext(); ) {
            funcs.addAll(it.next().getFuncs());
        }
        return funcs;
    }

    public boolean eval(Tuple input) throws IOException {
        for (Iterator<Cond> it = cList.iterator(); it.hasNext(); ) {
            if (it.next().eval(input) == false) return false;
        }
        return true;
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        for (Iterator<Cond> it = cList.iterator(); it.hasNext(); ) {
            sb.append(it.next());
            if (it.hasNext()) sb.append(" AND ");
        }
        sb.append(")");
        return sb.toString();
    }
}
