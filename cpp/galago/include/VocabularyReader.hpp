
//
// VocabularyReader
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#ifndef GALAGO_VOCABULARYREADER_HPP
#define GALAGO_VOCABULARYREADER_HPP

#include <vector>
#include <string>
#include "indri/indri-platform.h"
#include "UncompressedRead.hpp"

class VocabularyReader {
public:
	struct TermSlot {
        const char* term;
        UINT64 begin;
        UINT64 length;
    };

private:
    std::vector<TermSlot> slots;  

public:
    ~VocabularyReader();    
	void read( const std::string& fileName, UINT64 invertedFileLength );
    const TermSlot& operator[] ( const std::string& term ) const;
	const TermSlot& get( const std::string& term ) const;
};

#endif

