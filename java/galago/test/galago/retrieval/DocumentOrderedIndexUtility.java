/*
 * DocumentOrderedIndexUtility.java
 * 
 * Created on Oct 11, 2007, 5:47:19 PM
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package galago.retrieval;

import galago.Utility;
import galago.index.DenseIntegerListWriter;
import galago.index.DocumentOrderedBinnedListWriter;
import galago.retrieval.query.Node;
import galago.tupleflow.Parameters;
import galago.tupleflow.TupleFlowParameters;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class DocumentOrderedIndexUtility {
    static int maxABits = 20;
    static int maxBBits = 20;
    static int maxCBits = 20;
    
    static int maxAScore = 20;
    static int maxBScore = 10;
    static int maxCScore = 40;
    
    static int aBitMask = 0x00000379c;
    static int bBitMask = 0x00000ce9f;
    static int cBitMask = 0x00000f379;

    static Node getThreeTermQuery() {
        // build the query "#combine( a b c )"
        Node one = new Node("scores", "a");
        Node two = new Node("scores", "b");
        Node three = new Node("scores", "c");
        ArrayList<Node> children = new ArrayList<Node>();

        children.add(one);
        children.add(two);
        children.add(three);
        Node combine = new Node("combine", children);

        return combine;
    }

    static Node getTwoTermQuery() {
        // build the query "#combine( a b )"
        Node one = new Node("scores", "a");
        Node two = new Node("scores", "b");
        ArrayList<Node> children = new ArrayList<Node>();

        children.add(one);
        children.add(two);
        Node combine = new Node("combine", children);

        return combine;
    }

    static Node getTwoTermPriorQuery() {
        // build the query "#combine( a b )"
        Node one = new Node("scores", "a");
        Node two = new Node("scores", "b");
        
        Parameters p = new Parameters();
        p.add( "index", "priors" );
        p.add( "default", "prior" );
        Node prior = new Node("scores", p, new ArrayList<Node>(), 0 );
        
        ArrayList<Node> children = new ArrayList<Node>();

        children.add(one);
        children.add(two);
        children.add(prior);
        Node combine = new Node("combine", children);

        return combine;
    }

    static void buildSimpleIndex( File path ) throws FileNotFoundException, IOException {
        File defaultIndexPath = new File( path, "default" );
        RetrievalTestUtility.createSubIndexPath( defaultIndexPath );
        TupleFlowParameters p = RetrievalTestUtility.createFakeParameters( defaultIndexPath );
        
        // build a score index
        DocumentOrderedBinnedListWriter writer = new DocumentOrderedBinnedListWriter( p );
        
        writer.processWord( Utility.makeBytes("a") );
        
        for( int i=0; i<5000; i++ ) {
            if( i < 500 && (i % 5) == 0 )
                continue;
            writer.processDocument(i);
            if( i < 501 || i > 550 )
                writer.processTuple(1);
            else
                writer.processTuple(i-500);
        }

        writer.processWord( Utility.makeBytes("b") );
        
        for( int i=0; i<5000; i++ ) {
            if( i < 200 )
                continue;
            writer.processDocument(i);
            if( i < 501 || i > 550 )
                writer.processTuple(1);
            else
                writer.processTuple((i-500)*2);
        }

        writer.close();
        
        // build a prior index
        File densePath = new File( path, "priors" );
        RetrievalTestUtility.createSubIndexPath( densePath );
        p = RetrievalTestUtility.createFakeParameters( densePath );
        DenseIntegerListWriter priors = new DenseIntegerListWriter( p );
        
        priors.processWord( Utility.makeBytes( "null") );

        for( int i=0; i<1000; i++ ) {
            int document = i;
            int score = 0;

            priors.processDocument( document );
            priors.processTuple( score );
        }
        
        priors.processWord( Utility.makeBytes( "prior") );
        
        for( int i=0; i<1000; i++ ) {
            int document = i;
            int score = i % 50;

            priors.processDocument( document );
            priors.processTuple( score );
        }
        
        priors.close();
    }
    
    static void buildComplexIndex( TupleFlowParameters parameters ) throws IOException {
        DocumentOrderedBinnedListWriter writer = new DocumentOrderedBinnedListWriter( parameters );
        
        // goals:
        //      - make inverted lists with a standard distribution, few
        //        high scores, some mid scores, lots of low scores
        //      - make them algorithmically, so that we can easily determine
        //        the score from the document
        //      - otherwise, make the scores relatively random.
        
        writer.processWord( Utility.makeBytes("a") );
        for( int i=1; i<(1<<maxABits); i++ ) {
            int score = RetrievalTestUtility.documentToScore( i, maxAScore, maxABits, aBitMask );
            
            if( score > 0 ) {
                writer.processDocument(i);
                writer.processTuple(score);
            }
        }
        
        writer.processWord( Utility.makeBytes("b") );
        for( int i=1; i<(1<<maxBBits); i++ ) {
            int score = RetrievalTestUtility.documentToScore( i, maxBScore, maxBBits, bBitMask );
            
            if( score > 0 ) {
                writer.processDocument(i);
                writer.processTuple(score);
            }
        }
        
        writer.processWord( Utility.makeBytes("c") );
        for( int i=1; i<(1<<maxCBits); i++ ) {
            int score = RetrievalTestUtility.documentToScore( i, maxCScore, maxCBits, cBitMask );
            
            if( score > 0 ) {
                writer.processDocument(i);
                writer.processTuple(score);
            }
        }
        
        writer.close();
    }
}
