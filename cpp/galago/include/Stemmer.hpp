
//
// Stemmer
//
// 9 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_STEMMER_HPP
#define GALAGO_STEMMER_HPP

#include <string>
#include "../contrib/libstemmer_c/include/libstemmer.h"

class Stemmer {
private:
    struct sb_stemmer* _stemmer;
    
public:
    Stemmer() {
        _stemmer = sb_stemmer_new( "en", NULL );
    }

    ~Stemmer() {
        sb_stemmer_delete( _stemmer );
    }
    
    void process( std::string& term ) {
        std::string text = term;
        int start = 0;
        bool wasSpace = true;
        term = "";
        
        for( int i=0; i < text.size(); i++ ) {
            char c = text[i];
            
            if( c == ' ' || c == '\t' ) {
                if( !wasSpace ) {
                    const sb_symbol* result = sb_stemmer_stem( _stemmer, (const unsigned char*)text.c_str() + start, i - start );
                    if( term.length() > 0 ) {
                        term += " ";
                    }
                    term += (const char*)result;
                }
                start = i+1;
                wasSpace = true;
            } else {
                wasSpace = false;
            }
        }
        
        if( !wasSpace ) {
            if( term.length() > 0 ) {
                term += " ";
            }

            const sb_symbol* result = sb_stemmer_stem( _stemmer, (const unsigned char*)text.c_str() + start, text.length() - start );
            term += (const char*) result;
        }
    }
};

#endif // GALAGO_STEMMER_HPP

