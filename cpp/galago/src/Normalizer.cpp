
//
// Normalizer
//
// 9 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "Normalizer.hpp"

char Normalizer::_ignore[] = {  
		';',  '\"',  '&',  '/',  '.',  ':',  '!',  '#',
        '?',   '$',  '%',  '(',  ')',  '@',  '^',
        '*', '+',   '-',  ',',  '=',  '>',  '<',  '[',
        ']', '\{',   '}',  '|',  '`' };

void Normalizer::process( std::string& term ) const {
    // remove periods, downcase
    std::string result;
    result.reserve( term.length() );

    for( int i=0; i<term.length(); i++ ) {
        char c = term[i];

        // downcase uppercase letters
        // keep numbers, letters and UTF-8
        // throw out the rest.
        if( (c >= 'A' && c <= 'Z') )
            result.push_back( c + ('a' - 'A') );				
        else if( (c >= '0' && c <= '9') ||
                 (c >= 'a' && c <= 'z') ||
                 (c & 0x80) )
            result.push_back( c );
    }

    term = result;
}

