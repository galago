/*
 * Document
 *
 * December 14, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Document {
    public Document() {
        this.metadata = new HashMap<String, String>();
    }

    public Document( String identifier, String text ) {
        this.identifier = identifier;
        this.text = text;
        this.metadata = new HashMap<String, String>();
    }

    public String text;

    public List<String> terms;
    public List<Tag> tags;
    public Map<String, String> metadata;
    public String identifier;
}