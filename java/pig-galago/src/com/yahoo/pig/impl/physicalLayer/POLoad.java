/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.impl.io.FileLocalizer;

public class POLoad extends PhysicalOperator {
    String   filename;
    StorageFunc lf;
    boolean bound = false;

    public POLoad(String filenameIn, StorageFunc lfIn, int outputType) {
    	super(outputType);
        inputs = new PhysicalOperator[0];

        filename = filenameIn;
        lf = lfIn;
    }

    public boolean open(boolean continueFromLast) throws IOException {
    	if (!bound){
    		lf.bindTo(FileLocalizer.open(ExecType.LOCAL, filename), 0, Long.MAX_VALUE);
    		bound = true;
    	}
        return true;
    }

    public Tuple getNext() throws IOException {
        return lf.getNext();
    }

}
