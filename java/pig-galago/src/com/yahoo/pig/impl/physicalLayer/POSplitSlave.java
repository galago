/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;

import com.yahoo.pig.data.Tuple;

class POSplitSlave extends POSplit {
    POSplitMaster master;

    public POSplitSlave(PhysicalOperator masterIn, int outputType) {
    	super(outputType);
    	master = (POSplitMaster) masterIn;
        inputs = new PhysicalOperator[0];

        master.registerSlave(this);
    }

    public Tuple getNext() throws IOException {
        return master.slaveGetNext(this);
    }

}
