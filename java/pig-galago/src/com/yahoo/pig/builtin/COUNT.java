/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;
import java.util.Iterator;

import com.yahoo.pig.AlgebraicAtomEvalFunc;
import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.TupleEvalFunc;
import com.yahoo.pig.data.*;

/**
 * Generates the count of the values of the first field of a tuple. This class is Algebraic in
 * implemenation, so if possible the execution will be split into a local and global functions
 */
public class COUNT extends AlgebraicAtomEvalFunc {

    public void exec(Tuple input, DataAtom output) throws IOException {
        output.setValue(count(input));
    }

    public TupleEvalFunc getInitial() {
        return new Initial();
    }

    public TupleEvalFunc getIntermed() {
        return new Intermed();
    }

    public AtomEvalFunc getFinal() {
        return new Final();
    }

    static public class Initial extends TupleEvalFunc {
        public void exec(Tuple input, Tuple output) throws IOException {
            output.appendField(new DataAtom(count(input)));
        }
    }

    static public class Intermed extends TupleEvalFunc {
        public void exec(Tuple input, Tuple output) throws IOException {
            output.appendField(new DataAtom(sum(input)));
        }
    }

    static public class Final extends AtomEvalFunc {
        public void exec(Tuple input, DataAtom output) throws IOException {
            output.setValue(sum(input));
        }
    }

    static protected long count(Tuple input) throws IOException {
        DataBag values = input.getBagField(0);
        
        // we should just be able to return the size of the bag here...
        return values.cardinality();
//        long count = 0;
//        for (Iterator it = values.content(); it.hasNext();) {
//            it.next();
//            count++;
//        }
//        return count;
    }

    static protected double sum(Tuple input) throws IOException {
        DataBag values = input.getBagField(0);
        double sum = 0;
        for (Iterator<Tuple> it = values.content(); it.hasNext();) {
            Tuple t = it.next();
            try {
                sum += t.getAtomField(0).numval();
            } catch (NumberFormatException exp) {
                throw new IOException(exp.getClass().getName() + ":" + exp.getMessage());
            }
        }
        return sum;
    }
}
