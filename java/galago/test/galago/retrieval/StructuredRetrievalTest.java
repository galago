/*
 * StructuredRetrievalTest.java
 * JUnit based test
 *
 * Created on October 10, 2007, 3:16 PM
 */

package galago.retrieval;

import galago.Utility;
import galago.index.DocumentLengthsWriter;
import galago.index.DocumentNameWriter;
import galago.index.ExtentListWriter;
import galago.index.PositionListWriter;
import galago.retrieval.query.Node;
import galago.tupleflow.FakeParameters;
import galago.tupleflow.Parameters;
import galago.tupleflow.TupleFlowParameters;
import galago.types.DocumentIdentifierNumber;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class StructuredRetrievalTest extends TestCase {
    File tempPath;
    
    public StructuredRetrievalTest(String testName) {
        super(testName);
    }
    
    @Override
    public void setUp() throws FileNotFoundException, IOException {
        // make a spot for the index
        tempPath = File.createTempFile( "galago-test-index", null );
        tempPath.delete();
        tempPath.mkdir();

        // put in a generic manifest
        new Parameters().write( tempPath + File.separator + "manifest" );
        
        Parameters p = new Parameters();
        p.add( "index", tempPath.toString() );
        
        TupleFlowParameters parameters = new FakeParameters(p);
        
        // build an empty extent index
        Parameters extp = new Parameters();
        extp.add( "index", tempPath.toString() + File.separator + "extents" );
        TupleFlowParameters extParameters = new FakeParameters(extp);
        
        ExtentListWriter ewriter = new ExtentListWriter( extParameters );
        ewriter.processExtentName( Utility.makeBytes( "title" ) );
        ewriter.processDocument( 1 );
        ewriter.processTuple( 1, 3 );
        ewriter.close();
        new Parameters().write( tempPath + File.separator + "extents" + File.separator + "manifest" );
        
        // write positions!
        Parameters pp = new Parameters();
        pp.add( "index", tempPath.toString() + File.separator + "terms" );
        TupleFlowParameters posParameters = new FakeParameters(pp);

        PositionListWriter pwriter = new PositionListWriter( posParameters );
        
        pwriter.processWord( Utility.makeBytes("a") );
        pwriter.processDocument( 1 );
        pwriter.processPosition( 1 );
        pwriter.processPosition( 2 );
        pwriter.processPosition( 3 );
        
        pwriter.processDocument( 3 );
        pwriter.processPosition( 1 );
        
        pwriter.processDocument( 5 );
        pwriter.processPosition( 1 );
        
        pwriter.processWord( Utility.makeBytes("b") );
        pwriter.processDocument( 1 );
        pwriter.processPosition( 2 );
        pwriter.processPosition( 4 );
        
        pwriter.processDocument( 2 );
        pwriter.processPosition( 1 );
        
        pwriter.processDocument( 3 );
        pwriter.processPosition( 4 );
        
        pwriter.processDocument( 18 );
        pwriter.processPosition( 9 );
        pwriter.close();
        new Parameters().write( tempPath + File.separator + "terms" + File.separator + "manifest" );
        
        // add some document names
        Parameters dnp = new Parameters();
        dnp.add( "filename", tempPath + File.separator + "documentNames" );
        
        DocumentNameWriter dnWriter = new DocumentNameWriter( new FakeParameters(dnp) );
        for( int i=0; i<20; i++ ) {
            dnWriter.process( new DocumentIdentifierNumber( i, "DOC" + i, 100 ) );
        }
        dnWriter.close();
        
        Parameters lp = new Parameters();
        lp.add( "filename", tempPath + File.separator + "documentLengths" );
        DocumentLengthsWriter lWriter = new DocumentLengthsWriter( new FakeParameters(lp) );
        
        for( int i=0; i<20; i++ ) {
            lWriter.process( new DocumentIdentifierNumber( i, "DOC" + i, 100 ) );
        }
        lWriter.close();
        
        // main manifest file
        Parameters mainParameters = new Parameters();
        mainParameters.add( "collectionLength", "10000" );
        
        mainParameters.write( tempPath + File.separator + "manifest" );
    }
    
    @Override
    public void tearDown() throws IOException {
        Utility.deleteDirectory( tempPath );
    }
    
    public void testDocuments() throws FileNotFoundException, IOException {
        StructuredRetrieval retrieval = new StructuredRetrieval( tempPath.toString() );
        String name;
        
        name = retrieval.getDocument( 1 );
        assertEquals( "DOC1", name );
        name = retrieval.getDocument( 2 );
        assertEquals( "DOC2", name );
        name = retrieval.getDocument( 3 );
        assertEquals( "DOC3", name );
        name = retrieval.getDocument( 4 );
        assertEquals( "DOC4", name );
        name = retrieval.getDocument( 5 );
        assertEquals( "DOC5", name );
    }
    
    public void testSimple() throws FileNotFoundException, IOException, Exception {
        StructuredRetrieval retrieval = new StructuredRetrieval( tempPath.toString() );
        
        Node aTerm = new Node( "counts", "a" );
        ArrayList<Node> aChild = new ArrayList<Node>();
        aChild.add(aTerm);
        Parameters a = new Parameters();
        a.add( "default", "dirichlet" );
        a.add( "mu", "1500" );
        Node aFeature = new Node( "feature", a, aChild, 0 );
        
        Node bTerm = new Node( "counts", "b" );
        ArrayList<Node> bChild = new ArrayList<Node>();
        Parameters b = new Parameters();
        b.add( "default", "dirichlet" );
        b.add( "mu", "1500" );
        bChild.add(bTerm);
        Node bFeature = new Node( "feature", b, bChild, 0 );
                
        ArrayList<Node> children = new ArrayList<Node>();
        children.add( aFeature );
        children.add( bFeature );
        Node root = new Node( "combine", children );
        
        ScoredDocument[] result = retrieval.runQuery(root, 5);
        
        assertEquals( result.length, 5 );

        HashMap<Integer, Double> realScores = new HashMap<Integer, Double>();
        
        realScores.put( 1, -12.422161102294922 );
        realScores.put( 3, -13.636285781860352 );
        realScores.put( 5, -14.483583450317383 );
        realScores.put( 18, -14.483583450317383 );
        realScores.put( 2, -14.483583450317383 );
        
        // make sure the results are sorted
        double lastScore = Double.MAX_VALUE;
        
        for( int i=0; i<result.length; i++ ) {
            double score = result[i].score;
            double expected = realScores.get(result[i].document);

            assertTrue( lastScore >= result[i].score );
            assertEquals( expected, score, 0.000001 );
            
            lastScore = score;
        }
    }
}
