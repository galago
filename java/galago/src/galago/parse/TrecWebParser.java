
//
// TrecWebParser.java
//
// 29 June 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.parse;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import galago.tupleflow.StreamCreator;
import java.net.URL;

/**
 *
 * @author trevor
 */
public class TrecWebParser implements DocumentStreamParser {
    BufferedReader reader;
    
    /** Creates a new instance of TrecWebParser */
    public TrecWebParser( String filename ) throws FileNotFoundException, IOException {
        FileInputStream stream = StreamCreator.realInputStream( filename );
        
        if( filename.endsWith(".gz") ) {
            reader = new BufferedReader( new InputStreamReader( new GZIPInputStream( stream ) ) );
        } else {
            reader = new BufferedReader( new InputStreamReader( stream ) );
        }
    }
    
    public String waitFor( String tag ) throws IOException {
        String line;
        
        while( (line = reader.readLine()) != null ) {
            if( line.startsWith( tag ) )
                return line;
        }
        
        return null;
    }
    
    public void close() throws IOException {
        reader.close();
        reader = null;
    }
    
    public String scrubUrl( String url ) {
        // remove a trailing pound sign
        if( url.charAt(url.length()-1) == '#' )
            url = url.substring( 0, url.length()-1 );
        
        // make it lowercase
        url = url.toLowerCase();
        
        // remove a port number
        url = url.replace( ":80/", "/" );
        if( url.endsWith( ":80" ) )
            url = url.replace( ":80", "" );
        
        // remove trailing slashes
        while( url.charAt(url.length()-1) == '/' )
            url = url.substring( 0, url.length()-1 );
        
        return url;
    }
    
    public String readUrl() throws IOException {
        String url = reader.readLine();
        int space = url.indexOf( ' ' );
        
        if( space < 0 )
            space = url.length();
        
        return scrubUrl( url.substring( 0, space ) );
    }
    
    public Document nextDocument() throws IOException {
        String line = null;
        
        if( null == waitFor( "<DOC>" ) ) {
            close();
            return null;
        }
        
        String identifier = waitFor( "<DOCNO>" );
        identifier = identifier.substring( 7 ).trim();
        identifier = identifier.substring( 0, identifier.length()-8 );
        identifier = new String( identifier.trim() );
        waitFor( "<DOCHDR>" );
        String url = readUrl();
        waitFor( "</DOCHDR>" );
        
        StringBuilder buffer = new StringBuilder( 20*1024 );
        
        while( (line = reader.readLine()) != null ) {
            if( line.startsWith( "</DOC>" ) )
                break;

            buffer.append(line);
            buffer.append(' ');
        }
        
        Document result = new Document( identifier, buffer.toString() );
        result.metadata.put( "url", new String(url) );
        result.metadata.put( "identifier", result.identifier );
        
        return result;
    }

    private void tagRemovingParser(final StringBuilder buffer) throws IOException {
        boolean inComment = false;
        boolean inScript = false;
        boolean inTag = false;
        String line;
        
        while( (line = reader.readLine()) != null ) {
            if( line.startsWith( "</DOC>" ) )
                break;
            
            // clean out multiline comments
            if( inComment ) {
                int end = line.indexOf( "-->" );
                if( end >= 0 ) {
                    line = line.substring( end+3 );
                    inComment = false;
                } else {
                    line = "";
                }
            }
            
            if( inTag ) {
                int end = line.indexOf( ">" );
                if( end >= 0 ) {
                    line = line.substring( end+1 );
                    inTag = false;
                } else {
                    line = "";
                }
            }
            
            int firstBracket = line.indexOf( "<" );
            
            if( firstBracket >= 0 ) {
                // strip inline comments
                while( !inComment && line.indexOf( "<!--" ) >= 0 ) {
                    // line has comments
                    int start = line.indexOf( "<!--" );
                    int end = line.indexOf( "-->", start+4 );
                    
                    if( end < 0 ) {
                        line = line.substring( 0, start );
                        inComment = true;
                    } else {
                        buffer.append( line.substring( 0, start ) );
                        buffer.append( " " );
                        line = line.substring( end+3 );
                    }
                }
                
                // strip remaining tags
                while( !inComment && !inTag && line.indexOf( "<" ) >= 0 ) {
                    int start = line.indexOf( "<" );
                    int end = line.indexOf( ">", start+1 );
                    
                    if( end >= 0 ) {
                        buffer.append( line.substring( 0, start ) );
                        buffer.append( " " );
                        line = line.substring( end+1 );
                    } else {
                        line = line.substring( 0, start );
                        inTag = true;
                    }
                }
            }
            
            buffer.append( line );
            buffer.append( ' ' );
        }
    }
}
