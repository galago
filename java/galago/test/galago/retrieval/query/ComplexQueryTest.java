/*
 * ComplexQueryTest.java
 * JUnit based test
 *
 * Created on August 10, 2007, 8:40 PM
 */

package galago.retrieval.query;

import junit.framework.*;
import java.util.ArrayList;
import galago.retrieval.query.Traversal;

/**
 *
 * @author trevor
 */
public class ComplexQueryTest extends TestCase {
    
    public ComplexQueryTest(String testName) {
        super(testName);
    }
    
    public static class SimpleCopyTraversal implements Traversal {
        public void beforeNode( Node node ) {
            // do nothing
        }
        
        public Node afterNode( Node node, ArrayList<Node> children ) {
            return new Node( node.getOperator(), node.getParameters(), children, node.getPosition() );
        }
    }
    
    public static class NullTraversal implements Traversal {
        public void beforeNode( Node n ) {}
        public Node afterNode( Node n, ArrayList<Node> an ) { return null; }
    }
    
    public static Node createQuery() {
        Node childB = new Node( "text", "b", 0 );
        Node childA = new Node( "text", "a", 0 );
        ArrayList<Node> childList = new ArrayList();
        childList.add(childA);
        Node featureA = new Node( "feature", "bm25", childList, 0 );
        ArrayList<Node> featureList = new ArrayList<Node>();
        featureList.add(featureA);
        featureList.add(childB);
        Node tree = new Node( "combine", featureList, 0 );
    
        return tree;
    }
    
    public void testCopy() throws Exception {
        System.out.println("copy");
        
        Traversal traversal = new SimpleCopyTraversal();
        Node tree = createQuery();
        Node result = StructuredQuery.copy(traversal, tree);
        
        assertEquals(tree, result);
    }

    public void testWalk() throws Exception {
        System.out.println("walk");
        Traversal traversal = new NullTraversal();
        Node tree = createQuery();
        
        StructuredQuery.walk(traversal, tree);
    }

    public void testParse() {
        System.out.println("parse");
        
        String query = "#combine( #feature:bm25(a) b )";
        Node tree = createQuery();
        int offset = 0;
        
        Node result = StructuredQuery.parse(query);
        assertEquals(tree, result);
    }   
    
    public void testFieldParse() {
        System.out.println("fieldparse");
        String query = "#combine( a.b c.d @/e/ @/f. h/.g )";
        Node result = StructuredQuery.parse(query);
    }
}
