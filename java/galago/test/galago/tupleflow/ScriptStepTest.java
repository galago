//
// ScriptStepTest.java
//
// Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.tupleflow;

import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class ScriptStepTest extends TestCase {
    public ScriptStepTest(String testName) {
        super(testName);
    }            

    // this currently fails, as it should, because I don't really yet know how to fix it.
    public void testCreation() throws Exception {
        Parameters p = new Parameters();
        p.add( "language", "JavaScript" );
        p.add( "script", "5" );
        TupleFlowParameters tfp = new FakeParameters(p);
        
        ScriptStep s = new ScriptStep(tfp);
    }
}
