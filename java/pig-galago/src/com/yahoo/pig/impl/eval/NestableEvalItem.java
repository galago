/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.yahoo.pig.BagEvalFunc;
import com.yahoo.pig.builtin.ShellBagEvalFunc;
import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.func.FuncEvalItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

public abstract class NestableEvalItem extends EvalItem {
    public EvalSpecPipe nestedEval = null;
    public EvalItemList subColSpec = null; 
    Map<Datum, DataCollector> persistentCollectors;
    
    public NestableEvalItem(PigContext pigContext) {
    	super(pigContext);
        persistentCollectors = new HashMap<Datum, DataCollector>();
    }
    
    public void addNestedEvalSpec(EvalSpec spec) {
        if (nestedEval == null) nestedEval = new EvalSpecPipe(pigContext);
        
        nestedEval.add(spec);
    }

    public boolean isSimple() {
    	if (this instanceof FuncEvalItem && ((FuncEvalItem)this).func instanceof BagEvalFunc)
    		return false;
    	return (subColSpec == null);
    }
    
    protected void atomEval(DataAtom d, DataCollector output) throws IOException {
        if (subColSpec != null || nestedEval != null) throw new IOException("Illegal nested evaluation on DataAtom.");
        
        output.add(new Tuple(d));
    }
    
    protected void tupleEval(Tuple d, DataCollector output) throws IOException {
        if (subColSpec != null) output = subColSpec.collector(output);
        else output = tupleifyCollector(output);
        
        if (nestedEval != null) output = nestedEval.noSADCollector(output);
        
        output.add(d);
    }
    
    protected DataCollector setupBagProcessingPipeline(Datum amendKey, DataCollector output) {
        
        if (amendKey != null && persistentCollectors.containsKey(amendKey)) {
            // reuse existing processing pipeline
            output = persistentCollectors.get(amendKey);
        } else {  
            // set up new processing pipeline
            if (subColSpec != null) output = subColSpec.collector(output);
            else output = bagifyCollector(output);
            
            if (nestedEval != null) output = nestedEval.collector(output);
            
            
            // if this is an amendable stream, remember this processing pipeline
            if (amendKey != null) persistentCollectors.put(amendKey, output);
        }
     
        return output;
    }
    
    protected void bagEval(DataBag d, Datum amendKey, DataCollector output) throws IOException {
    
        output = setupBagProcessingPipeline(amendKey, output);
        
        // insert tuples into processing pipeline        
        for (Iterator<Tuple> it = ((DataBag) d).content(); it.hasNext(); ) {
            output.add(it.next());
        }
        output.add(null);  // EOF marker
    }
    
    // wrap each input in a tuple
    protected DataCollector tupleifyCollector(final DataCollector output) {
        return new DataCollector(output) {

            public void add(Tuple t) throws IOException {
                if (t != null) output.add(new Tuple(t));
                else output.add(null);  // propagate EOF
            }
            
        };
    }
    
    // stuff inputs into a bag, and wrap bag in a tuple
    protected DataCollector bagifyCollector(final DataCollector output) {
        return new DataCollector(output) {
            DataBag bag = null;
            
            public void add(Tuple t) throws IOException {
                if (bag == null) bag = BagFactory.getInstance().getNewBag();    

                if (t != null) {
                    bag.add(t);
                } else {
                    output.add(new Tuple(bag));
                    output.add(null);  // EOF
                }
            }
            
          
        };
    }

    protected abstract SchemaItem mapInputSchemaInitial(SchemaItem input);
    
    @Override
    public SchemaItem mapInputSchema(SchemaItem input){
    	if (schema!=null)
    		return schema;
    	SchemaItem output = mapInputSchemaInitial(input);
    	if (nestedEval != null)
    		output = nestedEval.mapInputSchema(output);
    	
    	return output;
    }

    
}
