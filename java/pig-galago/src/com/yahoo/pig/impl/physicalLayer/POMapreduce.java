/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.HashMap;

import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.*;
import com.yahoo.pig.impl.eval.groupby.GroupBySpecList;
import com.yahoo.pig.impl.mapreduceExec.MapReduceLauncher;

public class POMapreduce extends PhysicalOperator {
    public EvalSpecPipeList toMap             = null;
    public EvalSpecPipe     toCombine         = null;
    public EvalSpecPipe     toReduce          = null;
    public GroupBySpecList  toGroup           = null;
    public HashMap<String, String> definedFunctions = null;
    public String           inputFile         = "";
    public String           outputFile        = "";
    public String           partitionFunction = "";
    int                     parallelism       = -1;
    public String storeFunc;
    PigContext pigContext;
    public boolean doesGrouping() {
        return !(toGroup == null);
    }

    public int numInputFiles() {
        if (inputFile == null || inputFile.equals(""))
            return 0;
        else
            return (inputFile.split(";", -1)).length;
    }

    public POMapreduce(PigContext pigContext, int requestedParallelism) {
        this(pigContext);
        parallelism = requestedParallelism;
    }

    public POMapreduce(POMapreduce copyFrom) {
    	pigContext = copyFrom.pigContext;
        inputs = new PhysicalOperator[copyFrom.inputs.length];
        for (int i = 0; i < copyFrom.inputs.length; i++)
            inputs[i] = copyFrom.inputs[i];
        toMap = copyFrom.toMap;
        toCombine = copyFrom.toCombine;
        toReduce = copyFrom.toReduce;
        toGroup = copyFrom.toGroup;
        inputFile = copyFrom.inputFile;
        outputFile = copyFrom.outputFile;
        storeFunc = copyFrom.storeFunc;
        partitionFunction = copyFrom.partitionFunction;
        parallelism = copyFrom.parallelism;
    }

    public POMapreduce(PigContext pigContext, PhysicalOperator[] inputsIn) {
    	this.pigContext = pigContext;
        inputs = inputsIn;
    }

    public POMapreduce(PigContext pigContext, PhysicalOperator inputIn) {
    	this.pigContext = pigContext;
        inputs = new PhysicalOperator[1];
        inputs[0] = inputIn;
    }

    public POMapreduce(PigContext pigContext) {
    	this.pigContext = pigContext;
        inputs = new PhysicalOperator[0];
    }

    public void addInput(PhysicalOperator newInput) {
        PhysicalOperator[] oldInputs = inputs;
        inputs = new PhysicalOperator[oldInputs.length + 1];
        for (int i = 0; i < oldInputs.length; i++)
            inputs[i] = oldInputs[i];
        inputs[inputs.length - 1] = newInput;
    }

    public void addInputs(PhysicalOperator[] newInputs) {
        for (int i = 0; i < newInputs.length; i++)
            addInput(newInputs[i]);
    }

    static MapReduceLauncher pig = new MapReduceLauncher();

    public boolean open(boolean continueFromLast) throws IOException {
        // first, call open() on all inputs
        if (inputs != null) {
            for (int i = 0; i < inputs.length; i++) {
                if (!inputs[i].open(continueFromLast))
                    return false;
            }
        }

        // then, have hadoop run this MapReduce job:
        if (MapReduceLauncher.debug) print();
        boolean success = pig.launchPig(toMap, toCombine, toGroup, toReduce, -1, parallelism, inputFile, outputFile, storeFunc, pigContext);
        if (!success) {
            // XXX: If we throw an exception on failure, why do we return a boolean ?!? - ben
            throw new IOException("Job failed");
        }
        return success;
    }

    public Tuple getNext() throws IOException {
        // drain all inputs
        for (int i = 0; i < inputs.length; i++) {
            while (inputs[i].getNext() != null)
                ;
        }

        // indicate that we are done
        return null;
    }
    
    public int numMRJobs() {
        int numInputJobs = 0;
        if (inputs != null) {
            for (PhysicalOperator i : inputs) {
                numInputJobs += ((POMapreduce) i).numMRJobs();
            }
        }
        return 1 + numInputJobs;
    }

    void print() {
        System.out.println("\n----- MapReduce Job -----");
        System.out.println("Input: " + inputFile);
        System.out.println("Map: " + toMap);
        System.out.println("Group: " + toGroup);
        System.out.println("Combine: " + toCombine);
        System.out.println("Reduce: " + toReduce);
        System.out.println("Output: " + outputFile);
        System.out.println("StoreFunc: " + storeFunc);
        System.out.println("Parallelism: " + parallelism);
    }
}
