/*
 * ThresholdGenerator
 *
 * May 2, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.binner;

import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Parameters;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.WordCount;
import galago.types.WordProbability;
import galago.types.XMLFragment;
import galago.scoring.DistributionSmoother;
import galago.scoring.DistributionSmootherFactory;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.Parameters.Value;
import galago.tupleflow.Processor;
import galago.types.DocumentProbability;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.WordCount", order={"+word"})
@OutputClass(className="galago.types.WordProbability", order={"+word"})
public class ThresholdGenerator extends StandardStep<WordCount, WordProbability> {
    public static class Feature {
        DistributionSmoother smoother;
        int length;
        double weight;
    }
    
    public static class Prior implements Processor<DocumentProbability> {
        double weight;
        double minimumValue;
        
        Prior( double weight ) {
            this.weight = weight;
            this.minimumValue = 0;
        }
        
        public void process( DocumentProbability object ) {
            minimumValue = Math.min( minimumValue, object.probability );
        }

        public void close() throws IOException {
        }
    }
    
    ArrayList<Feature> features;
    ArrayList<Prior> priors;
    double collectionLength;
    double minimumOffset;
    Processor<XMLFragment> minimumOffsetWriter;
    
    public ThresholdGenerator( TupleFlowParameters parameters ) throws IOException, IncompatibleProcessorException {
        features = new ArrayList<Feature>();
        priors = new ArrayList<Prior>();
        
        for( Parameters.Value v : parameters.getXML().list( "feature" ) ) {
            String name = v.get( "name" );
            int length = 1500;
            double weight = 1.0;
            
            if( v.containsKey( "length" ) ) {
                length = Integer.parseInt( v.get( "length" ) );
            }
            
            if( v.containsKey( "weight" ) ) {
                weight = Double.parseDouble( v.get( "weight" ) );
            }
            
            DistributionSmoother smoother = DistributionSmootherFactory.newInstance( v );
            Feature feature = new Feature();
            
            feature.length = length;
            feature.weight = weight;
            feature.smoother = smoother;
            
            features.add( feature );
        }
        
        if( parameters.getXML().containsKey("prior") ) {
            for( Parameters.Value v : parameters.getXML().list( "prior" ) ) {
                String name = v.get( "name" );
                double weight = Double.parseDouble(v.get( "weight" ));
                Prior prior = new Prior( weight );

                // this part reads the entire prior file in order to find the minimum value
                TypeReader<DocumentProbability> priorReader = parameters.getTypeReader(name);
                priorReader.setProcessor( prior );
                priorReader.run();

                priors.add( prior );
            }
        }
        
        TypeReader<XMLFragment> clr = parameters.getTypeReader( "collectionLength" );
        XMLFragment fragment = clr.read();
        
        minimumOffsetWriter = parameters.getTypeWriter( "minimumOffset" );
        minimumOffset = 0;
        
        collectionLength = Double.parseDouble(fragment.innerText);
    }
    
    public void process( WordCount wc ) throws IOException {
        double threshold = 0;
        double background = wc.count / collectionLength;

        for( Feature t : features ) {
            threshold += t.weight * Math.log( t.smoother.smooth( background, 0, t.length ) );
        }
        
        for( Prior p : priors ) {
            threshold += p.weight * p.minimumValue;
        }
        
        minimumOffset = Math.min( minimumOffset, threshold );
        processor.process( new WordProbability( wc.word, threshold ) );
    }
    
    @Override
    public void close() throws IOException {
        minimumOffsetWriter.process( new XMLFragment( "minimumOffset", Double.toString(minimumOffset) ) );
        this.minimumOffsetWriter.close();
        super.close();
    }
    
    public Class<WordCount> getInputClass() {
        return WordCount.class;
    }
    
    public Class<WordProbability> getOutputClass() {
        return WordProbability.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "collectionLength", XMLFragment.class, parameters, handler );
        Verification.verifyTypeWriter( "minimumOffset", XMLFragment.class, new String[] {"+nodePath"}, parameters, handler);
    
        if( parameters.getXML().containsKey( "prior" ) ) {
            List<Value> priors = parameters.getXML().list( "prior" ); 
            
            for( Value prior : priors ) {
                if( !prior.containsKey("name") )
                    handler.addError( "Each prior entry requires a name." );
                
                if( !prior.containsKey("weight") )
                    handler.addError( "Each prior entry requires a weight." );
                
                String name = prior.get("name");
                Verification.verifyTypeReader( name, DocumentProbability.class, parameters, handler );
            }
        }
    }
}
