/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.groupby;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.impl.PigContext;

public class GroupBySpecList {

    public List<GroupBySpec> list;
    PigContext pigContext;
    public GroupBySpecList(PigContext pigContext) {
    	this.pigContext = pigContext;
        list = new ArrayList<GroupBySpec>();
    }
    
    public GroupBySpecList(PigContext pigContext, GroupBySpec[] specs) {
    	this.pigContext = pigContext;
        list = new ArrayList<GroupBySpec>(specs.length);
        
        for (int i = 0; i < specs.length; i++) {
            list.add(specs[i]);
        }
    }
    
    public GroupBySpecList(PigContext pigContext, String str) throws IOException {
    	this.pigContext = pigContext;
        String splitStr[] = str.split(";");
        list = new ArrayList<GroupBySpec>(splitStr.length);
        
        for (int i = 0; i < splitStr.length; i++) {
            list.add(new GroupBySpec(pigContext, splitStr[i]));
        }
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        for (Iterator<GroupBySpec> it = list.iterator(); it.hasNext(); ) {
            sb.append(it.next());
            if (it.hasNext()) sb.append(";");
        }
    
        return sb.toString();
    }
    
    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        for (Iterator<GroupBySpec> it = list.iterator(); it.hasNext(); ) {
            funcs.addAll(it.next().getFuncs());
        }
        return funcs;
    }
}
