/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.io;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.PigServer.ExecType;

public class PigFile {
    private String file = null;
    private String delimiterExpression = null;
    boolean append = false;

    public PigFile(String filename, boolean append) {
        file = filename;
        this.append = append;
    }
    
    public PigFile(String filename){
    	file = filename;
    }

    public PigFile(String filename, String delimiterExpression) {
        file = filename;
        this.delimiterExpression = delimiterExpression;
    }

    public DataBag load(ExecType execType) throws IOException {
        
        StorageFunc lfunc = delimiterExpression == null ? new PigStorage() : new PigStorage(delimiterExpression);
        return load(execType, lfunc);
    }

    public DataBag load(ExecType execType, StorageFunc lfunc) throws IOException {
        DataBag content = BagFactory.getInstance().getNewBag();
        InputStream is = FileLocalizer.open(execType, file);
        lfunc.bindTo(is, 0, Long.MAX_VALUE);
        Tuple f = null;
        while ((f = lfunc.getNext()) != null) {
            content.add(f);
        }
        return content;
    }

    public void store(ExecType execType, DataBag data) throws IOException {
        store(execType, data, new PigStorage());
    }

    public void store(ExecType execType, DataBag data, StorageFunc sfunc) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(FileLocalizer.create(execType, file, append));
        sfunc.bindTo(bos);
        for (Iterator<Tuple> it = data.content(); it.hasNext();) {
            Tuple row = it.next();
            sfunc.putNext(row);
        }
        sfunc.done();
        bos.close();
    }

}
