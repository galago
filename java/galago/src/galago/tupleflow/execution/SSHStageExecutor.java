/*
 * SSHExecutor
 *
 * August 31, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author trevor
 */
public class SSHStageExecutor extends RemoteStageExecutor {
    private static String machineEndMarker = "STOP#MACHINE#SHUTDOWN";
    private LinkedBlockingQueue<String> machines = new LinkedBlockingQueue();
    private LinkedBlockingQueue<StageTask> tasks = new LinkedBlockingQueue();
    private ExecutorService pool = null;
    private String commandName;
    
    public SSHStageExecutor( String commandName, List<String> machines ) {
        this.commandName = commandName;
        this.machines.addAll( machines );
        this.pool = Executors.newCachedThreadPool();
    }
    
    public class SSHGroupFuture implements Future<Object> {
        CountDownLatch latch;
        
        public SSHGroupFuture( CountDownLatch latch ) {
            this.latch = latch;
        }
        
        public boolean cancel(boolean interrupt) { return false; }
        public boolean isCancelled() { return false; }
        public Object get( long timeout, TimeUnit unit ) throws ExecutionException, InterruptedException {
            latch.await( timeout, unit );
            return null;
        }

        public Object get() throws InterruptedException, ExecutionException {
            latch.await();
            return null;
        }

        public boolean isDone() {
            return latch.getCount() == 0;
        }
    }

    public class StageTask implements Runnable {
        private String commandName;
        private String jobFileArgument;
        private CountDownLatch latch;
        
        public StageTask( String commandName, String jobFileArgument, CountDownLatch latch ) {
            this.commandName = commandName;
            this.jobFileArgument = jobFileArgument;
            this.latch = latch;
        }
        
        public void run() {
            String machineName;
            
            try {
                // first, wait for a machine reservation
                machineName = machines.poll();
                
                // if we see a shutdown marker, quit, but put the marker back
                if( machineName.equals( machineEndMarker ) ) {
                    machines.offer( machineEndMarker );
                    return;
                }
                
                String[] arguments = new String[] { machineName, jobFileArgument };
                Process process = Runtime.getRuntime().exec( commandName, arguments );

                // close the process stdin
                process.getOutputStream().close();
                // BUGBUG: someday we need to trap process stdout/stderr here

                process.waitFor();
            } catch( Exception e ) {
                // BUGBUG: fix this too
                e.printStackTrace();
            } finally {
                latch.countDown();
            }
        }
        
        public boolean isNullTask() {
            return commandName == null && jobFileArgument == null && latch == null;
        }
    }

    public Future<Object> submit( String stageName, ArrayList<String> jobPaths, String temporary ) {
        CountDownLatch latch = new CountDownLatch( jobPaths.size() );
        
        for( String jobPath : jobPaths ) {
            // submit this job to the queue
            StageTask task = new StageTask( commandName, jobPath, latch );
            pool.execute( task );
        }
        
        return new SSHGroupFuture( latch );
    }

    public void shutdown() {
        machines.add( machineEndMarker );
        pool.shutdown();
    }
}
