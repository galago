/*
 * ScoreIterator
 *
 * August 17, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license) 
 */

package galago.retrieval.extents;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public interface ScoreIterator extends DocumentDataIterator {    
    public int nextCandidate();
    public boolean hasMatch( int document );
    public void moveTo( int document ) throws IOException;
    public void movePast( int document ) throws IOException;
    public double score( int document, int length );
    public boolean isDone();
    public void reset() throws IOException;
}
