/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;

import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class EFArity extends AtomEvalFunc {

    @Override
    public void exec(Tuple input, DataAtom output) throws IOException {
        output.setValue(input.arity());
    }
    @Override
    public SchemaItem outputSchema() {
        SchemaItemList schema = new SchemaItemList();
        schema.add(new SchemaField("arity"));
        return schema;
    }
}
