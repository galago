/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;

import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.eval.*;
import com.yahoo.pig.impl.util.DataBuffer;

// unary, non-blocking operator.
class POEval extends PhysicalOperator {
    EvalSpecPipe spec;
    private boolean alreadyOpen = false;
    private DataBuffer buf = null;
    private DataCollector evalPipeline = null;
    private boolean inputDrained;

    public POEval(PhysicalOperator input, EvalSpecPipe specIn, int outputType) {
    	super(outputType);
    	inputs = new PhysicalOperator[1];
        inputs[0] = input;

        spec = specIn;
    }

    public POEval(EvalSpecPipe specIn, int outputType) {
    	super(outputType);
        inputs = new PhysicalOperator[1];
        inputs[0] = null;

        spec = specIn;
    }

    public boolean open(boolean continueFromLast) throws IOException {
        if (!super.open(continueFromLast)) return false;
    
        if (buf==null)
        	buf = new DataBuffer();
        if (evalPipeline == null)
        	evalPipeline = spec.collector(buf);
            
        inputDrained = false;
        
        return true;
    }

    public Tuple getNext() throws IOException {
    	while (true) {        	
        	if (buf.isEmpty()){
                // no pending outputs, so look to input to provide more food
                
                if (inputDrained){
                		return null;
                }
                	                
                Tuple nextTuple = inputs[0].getNext();
                
                if (nextTuple == null)
                    inputDrained = true;

                evalPipeline.add(nextTuple);  // if nextTuple is null, then we're inserting an EOF marker
        	}else{
        		Tuple t = buf.removeFirst();
                return t;
        	}            
        }
    }
       
}
