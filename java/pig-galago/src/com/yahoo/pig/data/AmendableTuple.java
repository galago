/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

public class AmendableTuple extends Tuple {
    Datum amendKey;       // the identifier of the group to which this tuple belongs.

    public AmendableTuple(int numFields, Datum amendKey) {
        super(numFields);
        this.amendKey = amendKey;
    }
    
    public Datum getAmendKey() {
        return amendKey;
    }
    public void setAmendKey(Datum amendKey) {
        this.amendKey = amendKey;
    }

}
