/*
 * StageGroupDescription
 *
 * August 24, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.tupleflow.execution.StageInstanceDescription.PipeInput;
import galago.tupleflow.execution.StageInstanceDescription.PipeOutput;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author trevor
 */
public class StageGroupDescription {
    /// A stage object (probably genereated by the JobConstructor parser)
    Stage stage;
    
    public HashMap<String, DataPipeRegion> inputs;
    public HashMap<String, DataPipeRegion> outputs;
    
    /// Count of instances of this stage
    public int instanceCount;
    
    public static class DataPipeRegion {
        DataPipe pipe;
        int start;
        int end;
        ConnectionPointType direction;
        
        public DataPipeRegion( DataPipe pipe, int start, int end, ConnectionPointType direction ) {
            this.pipe = pipe;
            this.start = start;
            this.end = end;
            this.direction = direction;
        }
        
        public int fileCount() {
            int count = 0;
            
            for( int i=start; i<end; i++ ) {
                String[] filenames = pipe.getOutputFileNames( i );
                count += filenames.length;
            }
            
            return count;
        }
    }
    
    public StageGroupDescription( Stage stage ) {
        this( stage, 1 );
    }
    
    /** Creates a new instance of StageGroupDescription */
    public StageGroupDescription( Stage stage, int instanceCount ) {
        this.stage = stage;
        this.inputs = new HashMap<String, DataPipeRegion>();
        this.outputs = new HashMap<String, DataPipeRegion>();
        this.instanceCount = instanceCount;
    }
    
    public String getName() {
        return stage.name;
    }
    
    public boolean containsInput( String name ) {
        return inputs.containsKey(name);
    }
    
    public boolean containsOutput( String name ) {
        return outputs.containsKey(name);
    }
    
    public List<StageInstanceDescription> getInstances() {
        ArrayList<StageInstanceDescription> instances = new ArrayList();
        
        for( int i=0; i<instanceCount; i++ ) {
            Map<String, PipeInput> instanceOutputs = new HashMap<String, PipeInput>();
            
            for( String key : outputs.keySet() ) {
                DataPipeRegion region = outputs.get(key);
                
                if( region.end - region.start <= 1 ) {
                    instanceOutputs.put( key, new PipeInput( region.pipe, 0 ) );
                } else {
                    assert region.end - region.start == instanceCount;
                    instanceOutputs.put( key, new PipeInput( region.pipe, region.start + i ) );
                }
            }
            
            Map<String, PipeOutput> instanceInputs = new HashMap<String, PipeOutput>();

            for( String key : inputs.keySet() ) {
                DataPipeRegion region = inputs.get(key);
                
                if( region.end - region.start <= 1 ) {
                    instanceInputs.put( key, new PipeOutput( region.pipe, 0 ) );
                } else if( instanceCount == 1 && region.end - region.start > 1 ) {
                    // assignment == "combined"
                    instanceInputs.put( key, new PipeOutput( region.pipe, region.start, region.end ) );
                } else {
                    assert region.end - region.start == instanceCount;
                    instanceInputs.put( key, new PipeOutput( region.pipe, region.start + i ) );
                }
            }
            
            instances.add( new StageInstanceDescription( stage, i, instanceOutputs, instanceInputs ) );
        }
        
        return instances;
    }

    public Stage getStage() {
        return stage;
    }

    int getInstanceCount() {
        return instanceCount;
    }
    
    @Override
    public String toString() {
        return stage.toString();
    }
}
