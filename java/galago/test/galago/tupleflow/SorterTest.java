/*
 * SorterTest.java
 * JUnit based test
 *
 * Created on May 29, 2007, 12:00 PM
 */

package galago.tupleflow;

import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.NullProcessor;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.tupleflow.Sorter;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.types.DocumentLengthWordCount;
import java.io.IOException;
import junit.framework.*;

/**
 *
 * @author trevor
 */
public class SorterTest extends TestCase {
    
    public SorterTest(String testName) {
        super(testName);
    }

    public void testGetInputClass() {
        System.out.println("getInputClass");
        Sorter instance = new Sorter( new DocumentLengthWordCount().getOrder( "+document", "+length" ) );

        Parameters p = new Parameters();
        p.add( "class", DocumentLengthWordCount.class.toString() );
        String expResult = DocumentLengthWordCount.class.toString();
        String result = instance.getInputClass( new FakeParameters( p ) );
        assertEquals(expResult, result);
    }

    public void testGetOutputClass() {
        System.out.println("getOutputClass");
        Sorter instance = new Sorter( new DocumentLengthWordCount().getOrder( "+document", "+length" ) );

        Parameters p = new Parameters();
        p.add( "class", DocumentLengthWordCount.class.toString() );
        String expResult = DocumentLengthWordCount.class.toString();
        String result = instance.getOutputClass( new FakeParameters( p ) );
        assertEquals(expResult, result);
    }

    public void testProcess() throws Exception {
        System.out.println("process");
        DocumentLengthWordCount object = new DocumentLengthWordCount();
        Sorter instance = new Sorter( new DocumentLengthWordCount().getOrder( "+document", "+length" ) );
        
        instance.process(object);
    }
   
    public void testClose() throws Exception {
        System.out.println("close");
        System.out.println("process");
        DocumentLengthWordCount object = new DocumentLengthWordCount();
        Sorter instance = new Sorter( new DocumentLengthWordCount().getOrder( "+document", "+length" ) );

        instance.setProcessor( new NullProcessor( DocumentLengthWordCount.class ) );
        instance.process(object);
        instance.close();
    }

    public void testSetProcessor() throws IncompatibleProcessorException {
        System.out.println("close");
        System.out.println("process");
        DocumentLengthWordCount object = new DocumentLengthWordCount();
        Sorter instance = new Sorter( new DocumentLengthWordCount().getOrder( "+document", "+length" ) );

        instance.setProcessor( new NullProcessor( DocumentLengthWordCount.class ) );
    }
}
