/*
 * DocumentOrderedBinnedSimpleRetrievalTest.java
 * JUnit based test
 *
 * Created on October 5, 2007, 2:48 PM
 */

package galago.retrieval;

import galago.Utility;
import galago.retrieval.query.Node;
import galago.tupleflow.TupleFlowParameters;
import java.io.File;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class DocumentOrderedBinnedSimpleRetrievalTest extends TestCase {
    File tempPath;
    
    public DocumentOrderedBinnedSimpleRetrievalTest(String testName) {
        super(testName);
    }
    
    public void testSimpleTwoTermQuery() throws Exception {
        File path = RetrievalTestUtility.createIndexPath();
        DocumentOrderedIndexUtility.buildSimpleIndex(path);
        
        DocumentOrderedBinnedIndex index = new DocumentOrderedBinnedIndex( path.toString() );
        DocumentOrderedBinnedRetrieval retrieval = new DocumentOrderedBinnedSimpleRetrieval( index );
        Node combine = DocumentOrderedIndexUtility.getTwoTermQuery();

        // run the query
        ScoredDocument[] result = retrieval.runQuery( combine, 10 );
        
        // verify that there are enough results
        assertEquals( 10, result.length );
        
        // verify the results match known good results
        int[] documents = { 550, 549, 548, 547, 546, 545, 544, 543, 542, 541 };
        double[] scores = { 150, 147, 144, 141, 138, 135, 132, 129, 126, 123 };
        
        for( int i=0; i<documents.length; i++ ) {
            assertEquals( documents[i], result[i].document );
            assertEquals( scores[i], result[i].score );
        }
        
        // close the index
        index.close();
        
        Utility.deleteDirectory(path);
    }
  
    public void testPriorQuery() throws Exception {
        File path = RetrievalTestUtility.createIndexPath();
        DocumentOrderedIndexUtility.buildSimpleIndex(path);
        
        DocumentOrderedBinnedIndex index = new DocumentOrderedBinnedIndex( path.toString() );
        DocumentOrderedBinnedRetrieval retrieval = new DocumentOrderedBinnedSimpleRetrieval( index );
        Node combine = DocumentOrderedIndexUtility.getTwoTermPriorQuery();

        // run the query
        ScoredDocument[] result = retrieval.runQuery( combine, 10 );
        
        // verify that there are enough results
        assertEquals( 10, result.length );
        
        // verify the results match known good results
        int[] documents = { 549, 548, 547, 546, 545, 544, 543, 542, 541, 540 };
        double[] scores = { 147 + 49, 144 + 48, 141 + 47, 138 + 46, 135 + 45, 132 + 44, 129 + 43, 126 + 42, 123 + 41, 120 + 40 };
        
        for( int i=0; i<documents.length; i++ ) {
            assertEquals( documents[i], result[i].document );
            assertEquals( scores[i], result[i].score );
        }
        
        // close the index
        index.close();
        
        Utility.deleteDirectory(path);
    }
}
