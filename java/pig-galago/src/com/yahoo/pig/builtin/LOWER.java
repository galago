/*
 * LOWER
 *
 * September 6, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.builtin;

import com.yahoo.pig.BagEvalFunc;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class LOWER extends BagEvalFunc {
    @Override
    public void exec(Tuple input, DataCollector output) throws IOException {
        String str = input.getAtomField(0).strval();
        Tuple t = new Tuple(1);
        t.setField(0, str.toLowerCase());
        output.add(t);
    }
    
    @Override
    public SchemaItem outputSchema() {
        SchemaItemList schema = new SchemaItemList();
        schema.add(new SchemaField("lower"));
        return schema; 
    }
}
