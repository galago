
//
// UniqueWordCount.java
//
// 17 Oct 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.Verified;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import org.archive.util.BloomFilter64bit;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.parse.Document")
public class UniqueWordCount implements Processor<Document> {
    long seenTokens = 0;
    org.archive.util.BloomFilter seenTerms;
    HashSet<String> common = new HashSet<String>();
    BufferedWriter writer;
    
    public UniqueWordCount( TupleFlowParameters parameters ) throws IOException {
        writer = new BufferedWriter( new FileWriter( parameters.getXML().get( "filename" ) ) );
        int expectedElements = Integer.parseInt( parameters.getXML().get( "expectedElementCount" ) );
        int hashFunctions = Integer.parseInt( parameters.getXML().get( "hashFunctions" ) );
        seenTerms = new BloomFilter64bit( expectedElements, hashFunctions );
    }
    
    public void process( Document document ) throws IOException {
        for( int i=0; i<document.terms.size(); i++ ) {
            String term = document.terms.get(i);
            seenTokens += 1;
            
            if( common.contains(term) )
                continue;
            
            if( seenTerms.contains(term) )
                continue;
            
            seenTerms.add(term);
            writer.write( String.format( "%d\t%d\t%s\n", seenTerms.size(), seenTokens, term ) );
            
            if( common.size() < 1000000 )
                common.add(term);
        }
    }

    public void close() throws IOException {
        writer.close();
    }
}
