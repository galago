
//
// StageExecutor.java
//
// 30 March 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow.execution;

import java.util.concurrent.Future;

/**
 * This interface marks classes that can be used to execute TupleFlow job
 * stages.
 * 
 * The interface is not particularly good right now.  It needs more reporting
 * options, in order to expose data from stages (like number of documents parsed)
 * as well as the number of stages that have run, and if they threw exceptions.
 * 
 * @author trevor
 */
public interface StageExecutor {
    Future execute( StageGroupDescription stage, String temporary );
    void shutdown();
}
