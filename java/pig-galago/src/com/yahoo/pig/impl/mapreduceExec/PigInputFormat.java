/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.UTF8;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.mapred.InputFormat;
import org.apache.hadoop.mapred.InputSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobConfigurable;
import org.apache.hadoop.mapred.RecordReader;
import org.apache.hadoop.mapred.Reporter;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.data.Tuple;

public class PigInputFormat implements InputFormat, JobConfigurable {

    public boolean[] areValidInputDirectories(FileSystem fileSys, Path[] inputDirs) throws IOException {
        Vector<Boolean> rcs = new Vector<Boolean>();
        for (int i = 0; i < inputDirs.length; i++) {
            String fullPath = inputDirs[i].toString();
            String subPaths[] = fullPath.split(";", -1);
            for (int j = 0; j < subPaths.length; j++) {
                Path dir = getPathFromSpec(subPaths[j]);
                rcs.add(new Boolean(fileSys.exists(dir)));
            }
        }
        boolean rc[] = new boolean[rcs.size()];
        for (int i = 0; i < rc.length; i++) {
            rc[i] = rcs.get(i).booleanValue();
        }
        return rc;
    }

    public static Path getPathFromSpec(String name) {
        int colon = name.indexOf(':');
        if (colon != -1) {
            name = name.substring(0, colon);
        }
        return new Path(name);
    }

    public static String getParserFromSpec(String name) {
        int colon = name.indexOf(':');
        if (colon == -1)
            return PigStorage.class.getName(); // default parser is PigStorage
        return name.substring(colon + 1);
    }

    public InputSplit[] getSplits(JobConf job, int numSplits) throws IOException {
	String inputs[] = job.get("pig.inputs").split(";", -1);
        String mapFuncs[] = job.get("pig.mapFuncs", "").split(";");
        String groupFuncs[] = job.get("pig.groupFuncs", "").split(";");
        
        // added for UNION: set group func arity to match arity of inputs
        if (inputs.length > 0 && groupFuncs.length != inputs.length && groupFuncs[0].length() == 0) {
            groupFuncs = new String[inputs.length];
            for (int i = 0; i < groupFuncs.length; i++) {
                groupFuncs[i] = "";
            }
        }
        
        if (inputs.length != mapFuncs.length) {
            throw new IOException("number of inputs != number of map functions: " + inputs.length + " != "
                    + mapFuncs.length + ": " + job.get("pig.mapFuncs", "missing"));
        }
        if (inputs.length != groupFuncs.length) {
            throw new IOException("number of inputs != number of group functions: " + inputs.length + " != "
                    + groupFuncs.length);
        }
        ArrayList<InputSplit> splits = new ArrayList<InputSplit>();
        for (int i = 0; i < inputs.length; i++) {
            String pathWithSpec = inputs[i];
            String parser = getParserFromSpec(pathWithSpec);
            Path path = getPathFromSpec(pathWithSpec);
            FileSystem fs = path.getFileSystem(job);
            fs.setWorkingDirectory(new Path("/user", job.getUser()));
                ArrayList<Path> paths = new ArrayList<Path>();
            paths.add(path);
            for (int j = 0; j < paths.size(); j++) {
                Path fullPath = new Path(fs.getWorkingDirectory(), paths.get(j));
                if (fs.isDirectory(fullPath)) {
                	Path children[] = fs.listPaths(fullPath);
                	for(int k = 0; k < children.length; k++) {
                		paths.add(children[k]);
                	}
                	continue;
                }
                long bs = fs.getBlockSize(fullPath);
                long size = fs.getLength(fullPath);
                long pos = 0;
                String name = paths.get(j).getName();
                if (name.endsWith(".gz") || name.endsWith(".bz")) {
                    // Anything that ends with a ".gz" we must process as a complete file
                    splits.add(new PigSplit(fs, fullPath, parser, groupFuncs[i], mapFuncs[i], i, 0, size));
                } else {
                    while (pos < size) {
                        if (pos + bs > size)
                            bs = size - pos;
                        splits.add(new PigSplit(fs, fullPath, parser, groupFuncs[i], mapFuncs[i], i, pos, bs));
                        pos += bs;
                    }
                }
            }
        }
        return splits.toArray(new PigSplit[splits.size()]);
    }

   public RecordReader getRecordReader(InputSplit split, JobConf job, Reporter reporter) throws IOException {
        PigRecordReader r = new PigRecordReader(job, (PigSplit)split, compressionCodecs);
        return r;
    }

    private CompressionCodecFactory compressionCodecs = null;

    static public String codecList;
    public void configure(JobConf conf) {
        conf.set("io.compression.codecs", conf.get("io.compression.codecs") + "," + BZip.class.getName());
        compressionCodecs = new CompressionCodecFactory(conf);
        codecList = conf.get("io.compression.codecs", "none");
    }
    
    public static class PigRecordReader implements RecordReader {
        /**
         * This is a tremendously ugly hack to get around the fact that mappers do not have access
         * to their readers. We take advantage of the fact that RecordReader.next and Mapper.map is
         * run on same the thread to share information through a thread local variable.
         */
        static ThreadLocal<PigRecordReader> myReader = new ThreadLocal<PigRecordReader>();

        public static PigRecordReader getPigRecordReader() {
            return myReader.get();
        }

        private InputStream     is;
        private FSDataInputStream   fsis;
        private long            end;
        private PigSplit    split;
        StorageFunc                loader;
        CompressionCodecFactory compressionFactory;
        JobConf job;

        PigRecordReader(JobConf job, PigSplit split, CompressionCodecFactory compressionFactory) throws IOException {
            this.split = split;
            this.job = job;
            this.compressionFactory = compressionFactory;
            loader = split.getLoadFunction();
            Path path = split.getPath();
            FileSystem fs = path.getFileSystem(job);
            fs.setWorkingDirectory(new Path("/user/" + job.getUser()));
            fsis = fs.open(split.getPath());
            is = fsis;
            CompressionCodec codec = compressionFactory.getCodec(split.getPath());
            long start = split.getStart();
            if (codec != null) {
                is = codec.createInputStream(is);
                end = Long.MAX_VALUE;
            } else {
                ((FSDataInputStream) is).seek(start);
                end = start + split.getLength();
            }
            loader.bindTo(is, start, end);
            myReader.set(this);
        }

        public boolean next(Writable key, Writable value) throws IOException {
            Tuple t = loader.getNext();
            if (t == null) {
                return false;
            }

            ((UTF8) key).set(split.getPath().getName());
            ((Tuple)value).copyFrom(t);
            return true;
        }

        public long getPos() throws IOException {
            return fsis.getPos();
        }

        public void close() throws IOException {
            is.close();
        }

        public PigSplit getPigFileSplit() {
            return split;
        }

        public WritableComparable createKey() {
            return new UTF8();
        }

        public Writable createValue() {
            return new Tuple();
        }

	public float getProgress() throws IOException {
	    float progress = getPos() - split.getStart();
	    float finish = split.getLength();
	    return progress/finish;
	}
    }

    public void validateInput(JobConf arg0) throws IOException {
    }

 }
