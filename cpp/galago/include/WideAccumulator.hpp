
//
// WideAccumulator
//
// 23 February 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_WIDEACCUMULATOR_HPP
#define GALAGO_WIDEACCUMULATOR_HPP

#define WA_TERM_BITS        (64)
#define WA_TERM_MAX_SCORE   (MAX_INT32)

class WideAccumulator {
private:
    UINT32 _document;
    UINT32 _score;
    UINT64 _terms;

public:
    typedef WideAccumulator update_type;
    typedef UINT64 term_type;

    WideAccumulator() :
        _document(0), _score(0), _terms(0)
    {
    }
    
    WideAccumulator( const UINT32 document, const UINT32 terms, const UINT64 score ) {
        _document = document;
        _terms = terms;
        _score = score;
    }
    
    WideAccumulator( const UINT32 document, const update_type update ) {
        _document = document;
        _terms = update.terms();
        _score = update.score();
    }
    
    bool operator< ( const WideAccumulator& other ) const {
        return score() < other.score();
    }
    
    inline UINT32 document() const {
        return _document;
    }
    
    inline UINT32 score() const {
        return _score;
    }
    
    inline UINT32 terms() const {
        return _terms;
    }
    
    inline void update( const update_type update ) {
        _terms |= update.terms();
        _score += update.score();
    }

    inline UINT32 unseen( const std::vector<UINT32>& fullUnseen ) const {
        UINT32 result = 0;
        
        for( int b=0; b<8; b++ ) {
            UINT32 bits = (_terms >> (b*8)) & 0xFF;
            result += fullUnseen[ (b<<8) | bits ];
        }
        
        return result;
    }

    inline bool containsTerm( const term_type termBit ) const {
        return (termBit & _terms) ? true : false;
    }
    
    static int maxScore() {
        return WA_TERM_MAX_SCORE;
    }
    
    static int maxTerms() {
        return WA_TERM_BITS;
    }
    
    static term_type buildTerm( const int termIndex ) {
        return (1ULL<<termIndex);
    }
    
    static term_type buildNullTerm() {
        return 0;
    }
    
    static update_type buildUpdate( const int termIndex, const UINT32 score ) {
        WideAccumulator result;
        result._terms = buildTerm( termIndex );
        result._score = score;
        return result;
    } 
    
    static void computeUnseenScoreArray( std::vector<UINT32>& fullUnseen, const std::vector<UINT32>& maxUnseen ) {
        // fullUnseen is a precomputed array that helps make our job easier
        // when computing unseen statistics.  
        // For WideAccumulators, we use 2048 (256*8) entries.
        // The low 8-bits of the index into this array is a bitmap.  The upper 3 bits
        // are a byte index.  This code probably makes more sense if you look at the unseen method too.

        fullUnseen.resize( 2048, 0 );
    
        // b selects one byte from the terms bitmap
        for( UINT32 b = 0; b < 8; b++ ) {
            // bits iterates over all possible settings of 8 bits
            for( UINT32 bits = 0; bits < 256; bits++ ) {
                fullUnseen[ (b<<8) | bits ] = 0;
            
                for( UINT32 bitIndex = 0; bitIndex < 8; bitIndex++ ) {
                    UINT32 bit = 1<<bitIndex;
                    UINT32 term = bitIndex + b*8;
                    
                    if( (bits & bit) == 0 && term < maxUnseen.size() ) {
                        fullUnseen[ (b<<8) | bits ] += maxUnseen[ term ];
                    }
                }
            }
        }
    }
    
};

template<>
class AccumulatorTypes<WideAccumulator> {
public:
    typedef WideAccumulator update_type;
    typedef UINT64 term_type;
};

#endif // GALAGO_LONGACCUMULATOR_HPP



