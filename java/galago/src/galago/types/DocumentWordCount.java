// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/DocumentWordCount.java DocumentWordCount galago.types String:document String:word int:count order:+document+word order:+document
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class DocumentWordCount implements Type<DocumentWordCount> {
    public String document;
    public String word;
    public int count; 
    
    public DocumentWordCount() {}
    public DocumentWordCount( String document, String word, int count ) {
        this.document = document;
        this.word = word;
        this.count = count;
    }  
    
    public String toString() {
            return String.format( "%s,%s,%d",
                                   document, word, count );
    } 

    public Order<DocumentWordCount> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+document", "+word" } ) ) {
            return new DocumentWordOrder();
        }
        if ( Arrays.equals( spec, new String[] { "+document" } ) ) {
            return new DocumentOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<DocumentWordCount> {
        public void process( DocumentWordCount object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class DocumentWordOrder implements Order<DocumentWordCount> {
        public int hash( DocumentWordCount object ) {
            int h = 0;
            h += Utility.hash(object.document);
            h += Utility.hash(object.word);
            return h;
        } 
        public Comparator<DocumentWordCount> greaterThan() {
            return new Comparator<DocumentWordCount>() {
                public int compare( DocumentWordCount one, DocumentWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentWordCount> lessThan() {
            return new Comparator<DocumentWordCount>() {
                public int compare( DocumentWordCount one, DocumentWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentWordCount> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentWordCount> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentWordCount> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentWordCount > {
            DocumentWordCount last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentWordCount object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               if ( processAll || last == null || 0 != Utility.compare(object.word, last.word) ) { processAll = true; shreddedWriter.processWord( object.word ); }
               shreddedWriter.processTuple( object.count );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentWordCount> getInputClass() {
                return DocumentWordCount.class;
            }
        } 
        public ReaderSource<DocumentWordCount> orderedCombiner( Collection<TypeReader<DocumentWordCount>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentWordCount> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentWordCount clone( DocumentWordCount object ) {
            DocumentWordCount result = new DocumentWordCount();
            if ( object == null ) return result;
            result.document = object.document; 
            result.word = object.word; 
            result.count = object.count; 
            return result;
        }                 
        public Class<DocumentWordCount> getOrderedClass() {
            return DocumentWordCount.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+document", "+word"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processDocument( String document ) throws IOException;
            public void processWord( String word ) throws IOException;
            public void processTuple( int count ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastDocument;
            String lastWord;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processDocument( String document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public void processWord( String word ) {
                lastWord = word;
                buffer.processWord( word );
            }
            public final void processTuple( int count ) throws IOException {
                if( lastFlush ) {
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    if( buffer.words.size() == 0 ) buffer.processWord( lastWord );
                    lastFlush = false;
                }
                buffer.processTuple( count );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getCount() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushWord( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public final void flushWord( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getWordEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getWord() );
                    output.writeInt( count );
                    buffer.incrementWord();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushDocument( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> documents = new ArrayList();
            ArrayList<String> words = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            ArrayList<Integer> wordTupleIdx = new ArrayList();
            int documentReadIdx = 0;
            int wordReadIdx = 0;
                            
            int[] counts;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                counts = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processDocument( String document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processWord( String word ) {
                words.add( word );
                wordTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int count ) {
                assert documents.size() > 0;
                assert words.size() > 0;
                counts[writeTupleIndex] = count;
                writeTupleIndex++;
            }
            public void resetData() {
                documents.clear();
                words.clear();
                documentTupleIdx.clear();
                wordTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                documentReadIdx = 0;
                wordReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementWord() {
                wordReadIdx++;  
            }                                                                                              

            public void autoIncrementWord() {
                while( readTupleIndex >= getWordEndIndex() && readTupleIndex < writeTupleIndex )
                    wordReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }

            public int getWordEndIndex() {
                if( (wordReadIdx+1) >= wordTupleIdx.size() )
                    return writeTupleIndex;
                return wordTupleIdx.get(wordReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }
            public String getWord() {
                assert readTupleIndex < writeTupleIndex;
                assert wordReadIdx < words.size();
                
                return words.get( wordReadIdx );
            }
            public int getCount() {
                assert readTupleIndex < writeTupleIndex;
                return counts[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getCount() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyUntilIndexWord( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            } 
            public void copyUntilIndexWord( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processWord( getWord() );
                    assert getWordEndIndex() <= endIndex;
                    copyTuples( getWordEndIndex(), output );
                    incrementWord();
                }
            }  
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        if( c < 0 ) {
                            copyUntilIndexWord( getDocumentEndIndex(), output );
                        } else if( c == 0 ) {
                            copyUntilWord( other, output );
                            autoIncrementDocument();
                            break;
                        }
                    } else {
                        output.processDocument( getDocument() );
                        copyUntilIndexWord( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
               
                }
            }
            public void copyUntilWord( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getWord(), other.getWord() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processWord( getWord() );
                                      
                        copyTuples( getWordEndIndex(), output );
                    } else {
                        output.processWord( getWord() );
                        copyTuples( getWordEndIndex(), output );
                    }
                    incrementWord();  
                    
                    if( getDocumentEndIndex() <= readTupleIndex )
                        break;   
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilDocument( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentWordCount>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordCount> getOutputClass() {
                return DocumentWordCount.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentWordCount read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentWordCount result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentWordCount>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentWordCount last = new DocumentWordCount();         
            long updateDocumentCount = -1;
            long updateWordCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                    result = + Utility.compare( buffer.getWord(), otherBuffer.getWord() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentWordCount read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentWordCount result = new DocumentWordCount();
                
                result.document = buffer.getDocument();
                result.word = buffer.getWord();
                result.count = buffer.getCount();
                
                buffer.incrementTuple();
                buffer.autoIncrementDocument();
                buffer.autoIncrementWord();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }                              
                        if( updateWordCount - tupleCount > 0 ) {
                            buffer.words.add(last.word);
                            buffer.wordTupleIdx.add( (int) (updateWordCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateWord();
                        buffer.processTuple( input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                last.document = input.readString();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }
            public final void updateWord() throws IOException {
                if( updateWordCount > tupleCount )
                    return;
                     
                updateDocument();
                last.word = input.readString();
                updateWordCount = tupleCount + input.readInt();
                                      
                buffer.processWord( last.word );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordCount> getOutputClass() {
                return DocumentWordCount.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentWordCount last = new DocumentWordCount();
            boolean documentProcess = true;
            boolean wordProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processDocument( String document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
            resetWord();
                    documentProcess = false;
                }
            }
            public void processWord( String word ) throws IOException {  
                if( wordProcess || Utility.compare( word, last.word ) != 0 ) {
                    last.word = word;
                    processor.processWord( word );
                    wordProcess = false;
                }
            }  
            
            public void resetDocument() {
                 documentProcess = true;
            resetWord();
            }                                                
            public void resetWord() {
                 wordProcess = true;
            }                                                
                               
            public void processTuple( int count ) throws IOException {
                processor.processTuple( count );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentWordCount last = new DocumentWordCount();
            public galago.tupleflow.Processor<DocumentWordCount> processor;                               
            
            public TupleUnshredder( DocumentWordCount.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentWordCount> processor ) {
                this.processor = processor;
            }
            
            public DocumentWordCount clone( DocumentWordCount object ) {
                DocumentWordCount result = new DocumentWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void processDocument( String document ) throws IOException {
                last.document = document;
            }   
                
            public void processWord( String word ) throws IOException {
                last.word = word;
            }   
                
            
            public void processTuple( int count ) throws IOException {
                last.count = count;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentWordCount last = new DocumentWordCount();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentWordCount clone( DocumentWordCount object ) {
                DocumentWordCount result = new DocumentWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void process( DocumentWordCount object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                if( last == null || Utility.compare( last.word, object.word ) != 0 || processAll ) { processor.processWord( object.word ); processAll = true; }
                processor.processTuple( object.count );                                         
            }
                          
            public Class<DocumentWordCount> getInputClass() {
                return DocumentWordCount.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
    public static class DocumentOrder implements Order<DocumentWordCount> {
        public int hash( DocumentWordCount object ) {
            int h = 0;
            h += Utility.hash(object.document);
            return h;
        } 
        public Comparator<DocumentWordCount> greaterThan() {
            return new Comparator<DocumentWordCount>() {
                public int compare( DocumentWordCount one, DocumentWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentWordCount> lessThan() {
            return new Comparator<DocumentWordCount>() {
                public int compare( DocumentWordCount one, DocumentWordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentWordCount> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentWordCount> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentWordCount> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentWordCount > {
            DocumentWordCount last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentWordCount object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               shreddedWriter.processTuple( object.word, object.count );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentWordCount> getInputClass() {
                return DocumentWordCount.class;
            }
        } 
        public ReaderSource<DocumentWordCount> orderedCombiner( Collection<TypeReader<DocumentWordCount>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentWordCount> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentWordCount clone( DocumentWordCount object ) {
            DocumentWordCount result = new DocumentWordCount();
            if ( object == null ) return result;
            result.document = object.document; 
            result.word = object.word; 
            result.count = object.count; 
            return result;
        }                 
        public Class<DocumentWordCount> getOrderedClass() {
            return DocumentWordCount.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+document"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processDocument( String document ) throws IOException;
            public void processTuple( String word, int count ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastDocument;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processDocument( String document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public final void processTuple( String word, int count ) throws IOException {
                if( lastFlush ) {
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    lastFlush = false;
                }
                buffer.processTuple( word, count );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeString( buffer.getWord() );
                    output.writeInt( buffer.getCount() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushDocument( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> documents = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            int documentReadIdx = 0;
                            
            String[] words;
            int[] counts;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                words = new String[batchSize];
                counts = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processDocument( String document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( String word, int count ) {
                assert documents.size() > 0;
                words[writeTupleIndex] = word;
                counts[writeTupleIndex] = count;
                writeTupleIndex++;
            }
            public void resetData() {
                documents.clear();
                documentTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                documentReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }
            public String getWord() {
                assert readTupleIndex < writeTupleIndex;
                return words[readTupleIndex];
            }                                         
            public int getCount() {
                assert readTupleIndex < writeTupleIndex;
                return counts[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getWord(), getCount() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyTuples( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            }  
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        copyTuples( getDocumentEndIndex(), output );
                    } else {
                        output.processDocument( getDocument() );
                        copyTuples( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilDocument( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentWordCount>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordCount> getOutputClass() {
                return DocumentWordCount.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentWordCount read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentWordCount result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentWordCount>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentWordCount last = new DocumentWordCount();         
            long updateDocumentCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentWordCount read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentWordCount result = new DocumentWordCount();
                
                result.document = buffer.getDocument();
                result.word = buffer.getWord();
                result.count = buffer.getCount();
                
                buffer.incrementTuple();
                buffer.autoIncrementDocument();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateDocument();
                        buffer.processTuple( input.readString(), input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                last.document = input.readString();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordCount> getOutputClass() {
                return DocumentWordCount.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentWordCount last = new DocumentWordCount();
            boolean documentProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processDocument( String document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
                    documentProcess = false;
                }
            }  
            
            public void resetDocument() {
                 documentProcess = true;
            }                                                
                               
            public void processTuple( String word, int count ) throws IOException {
                processor.processTuple( word, count );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentWordCount last = new DocumentWordCount();
            public galago.tupleflow.Processor<DocumentWordCount> processor;                               
            
            public TupleUnshredder( DocumentWordCount.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentWordCount> processor ) {
                this.processor = processor;
            }
            
            public DocumentWordCount clone( DocumentWordCount object ) {
                DocumentWordCount result = new DocumentWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void processDocument( String document ) throws IOException {
                last.document = document;
            }   
                
            
            public void processTuple( String word, int count ) throws IOException {
                last.word = word;
                last.count = count;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentWordCount last = new DocumentWordCount();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentWordCount clone( DocumentWordCount object ) {
                DocumentWordCount result = new DocumentWordCount();
                if ( object == null ) return result;
                result.document = object.document; 
                result.word = object.word; 
                result.count = object.count; 
                return result;
            }                 
            
            public void process( DocumentWordCount object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                processor.processTuple( object.word, object.count );                                         
            }
                          
            public Class<DocumentWordCount> getInputClass() {
                return DocumentWordCount.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    