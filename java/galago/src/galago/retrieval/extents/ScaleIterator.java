/*
 * ScaleIterator
 * 
 * September 20, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import galago.tupleflow.Parameters;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */

@InputIterator(className="galago.retrieval.extents.ScoreIterator", cardinality=1)
public class ScaleIterator implements ScoreIterator {
    ScoreIterator iterator;
    double weight;
    
    public ScaleIterator( Parameters parameters, ArrayList<DocumentDataIterator> iterators ) throws IllegalArgumentException {
        iterator = (ScoreIterator) iterators.get(0);
        weight = parameters.get( "weight", 1.0 );
    }

    public int nextCandidate() {
        return iterator.nextCandidate();
    }

    public boolean hasMatch(int document) {
        return iterator.hasMatch(document);
    }
    
    public void moveTo(int document) throws IOException {
        iterator.moveTo(document);
    }
    
    public void movePast(int document) throws IOException {
        iterator.movePast(document);
    }

    public double score(int document, int length) {
        return weight * iterator.score(document, length );
    }

    public boolean isDone() {
        return iterator.isDone();
    }
    
    public void reset() throws IOException {
        iterator.reset();
    }
}
