/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.List;

import com.yahoo.pig.data.Tuple;

public class NotCond extends Cond {
    public Cond c1;
    
    public NotCond(Cond c1) {
        this.c1 = c1;
    }

    public List<String> getFuncs() {
        return c1.getFuncs();
    }

    public boolean eval(Tuple input) throws IOException {
        return !c1.eval(input);
    }
    
    public String toString() {
        return "(NOT " + c1 + ")";
    }
}
