/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import com.yahoo.pig.InstantiatedFunc;
import com.yahoo.pig.impl.logicalLayer.LogicalPlanBuilder;
import com.yahoo.pig.PigServer.ExecType;

public class PigContext implements Serializable {
	private static final long serialVersionUID = 1L;

	public PigContext(ExecType execType) {
		this.execType = execType;
	}
    /**
     * a table mapping function names to function specs.
     */
    public Map<String, String> definedFunctions = new HashMap<String, String>();
    
    /**
     * Defines an alias for the given function spec. This
     * is useful for functions that require arguments to the 
     * constructor.
     * 
     * @param alias - the new function alias to define.
     * @param functionSpec - the name of the function and any arguments.
     * It should have the form: classname('arg1', 'arg2', ...)
     */
    public void registerFunction(String function, String functionSpec) {
    	if (functionSpec == null) {
    		definedFunctions.remove(function);
    	} else {
    		definedFunctions.put(function, functionSpec);
    	}
    }
    
    ExecType execType = ExecType.MAPREDUCE;

    /**
     * Returns the type of execution currently in effect.
     * 
     * @return
     */
    public ExecType getExecType() {
        return execType;
    }
	
	// Load the necessary class
	static Vector<String> imports = new Vector<String>();
	static {
		imports.add("com.yahoo.pig.builtin.");
	}
	
    public static ArrayList<String> packageImportList = new ArrayList<String>();
    static {
		packageImportList.add("");
		packageImportList.add("com.yahoo.pig.builtin.");
    }
    
    public static Class resolveClassName(String name) throws ClassNotFoundException {
		for(String prefix: packageImportList) {
	    	Class c;
		    try {
				c = Class.forName(prefix+name);
				return c;
	    	} catch (ClassNotFoundException e) {
		    }
		}
		throw new ClassNotFoundException("Could not resolve " + name + " using imports: " + packageImportList);
    }
    
    @SuppressWarnings("unchecked")
	public static Object instantiateArgFunc(String funcSpec) throws IOException, ClassNotFoundException, SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
    	Object ret = null;
	    Vector<String> args = null;
	    int paren = funcSpec.indexOf('(');
	    if (paren != -1) {
	        args = new Vector<String>();
	        String argString = funcSpec.substring(paren + 1);
	        funcSpec = funcSpec.substring(0, paren);
	        int startIndex = 0;
	        int endIndex;
	        while (startIndex < argString.length()) {
    	        while (startIndex < argString.length() && argString.charAt(startIndex++) != '\'')
        	        ;
            	endIndex = startIndex;
	            while (endIndex < argString.length() && argString.charAt(endIndex) != '\'') {
    	            if (argString.charAt(endIndex) == '\\')
        	            endIndex++;
            	    endIndex++;
            	}
           		if (endIndex < argString.length()) {
               		args.add(argString.substring(startIndex, endIndex));
	            }
    	        startIndex = endIndex + 1;
        	}
		}
		Class objClass = resolveClassName(funcSpec);
		if (args != null && args.size() > 0) {
        	Class paramTypes[] = new Class[args.size()];
        	for (int i = 0; i < paramTypes.length; i++) {
    			paramTypes[i] = String.class;
        	}
        	Constructor c = objClass.getConstructor(paramTypes);
			ret =  c.newInstance(args.toArray());
		} else {
			ret = objClass.newInstance();
		}
		return ret;
	}
  
	public Object getUDF(String name) throws IOException {
		if (definedFunctions != null) {
			String funcSpec = definedFunctions.get(name);
			if (funcSpec != null) {
				try {
					Object o = instantiateArgFunc(funcSpec);
					((InstantiatedFunc)o).setName(name);
					return o;
				} catch(Exception e) {
					RuntimeException re = new RuntimeException(e);
					re.setStackTrace(e.getStackTrace());
				}
			}
		}
        Class<?> udfClass = null;
		try {
			udfClass = LogicalPlanBuilder.classloader.loadClass(name);
		} catch (ClassNotFoundException e1) {
			Iterator<String> it = imports.iterator();
			while(it.hasNext()) {
				try {
					String prefix = it.next();
					if (prefix.endsWith(".")) {
						udfClass = LogicalPlanBuilder.classloader.loadClass(prefix + name);
					} else if (prefix.endsWith("." + name)) {
						udfClass = LogicalPlanBuilder.classloader.loadClass(prefix);
					} else continue;
					break;
				} catch(ClassNotFoundException e2) {
				}
			}
			if (udfClass == null) throw new RuntimeException("Function " + name + " not found.");
		}
		
		Object udfObject;
		
		try {
			udfObject = udfClass.newInstance();
			return udfObject;	
		} catch (Exception e) {
			throw new RuntimeException("Unable to load function " + name + ": " + e.getMessage());
		}
	}	
}
