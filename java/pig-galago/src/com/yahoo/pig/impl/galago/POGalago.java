/*
 * POGalago
 *
 * September 1, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.impl.physicalLayer.PhysicalOperator;
import com.yahoo.pig.data.Tuple;
import galago.tupleflow.Order;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorStore;
import galago.tupleflow.execution.Job;
import galago.tupleflow.execution.JobExecutor;
import galago.tupleflow.execution.LocalStageExecutor;
import galago.tupleflow.execution.StageExecutor;
import galago.tupleflow.execution.StageExecutorFactory;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class POGalago extends PhysicalOperator {
    Job job;
    JobExecutor context;
    String[] orderSpec;
    String dataSourceName;
    TypeReader<com.yahoo.pig.impl.galago.Tuple> tupleReader;
    
    String outputFilename;
    String outputFunction;
    
    /** Creates a new instance of POGalago */
    public POGalago( Job job, String dataSourceName, String[] orderSpec ) {
        this.job = job;
        this.dataSourceName = dataSourceName;
        this.orderSpec = orderSpec;
    }

    public POGalago( Job job ) {
        this.job = job;
    }
    
    public Tuple getNext() throws IOException {
        if( tupleReader != null )
            return tupleReader.read();
        return null;
    }

    @Override
    public boolean open(boolean continueFromLast) throws IOException {
        if( !super.open(continueFromLast) )
            return false;
        
        String temporaryStorage = System.getProperty( "gwd" );
        
        if( temporaryStorage == null )
            temporaryStorage = "/tmp/pig-galago";
        
        String executorType = System.getProperty( "executorType" );
        
        ErrorStore errorStore = new ErrorStore();
        StageExecutor executor = StageExecutorFactory.newInstance( executorType );
        
        job = JobExecutor.optimize(job);
        System.out.println(job);
        context = new JobExecutor( job, temporaryStorage, errorStore );
        context.prepare();
        
        try {
            context.run( executor );
        } catch( Exception e ) {
            throw (IOException) new IOException( "Problems executing the Galago job." ).initCause(e);
        }
        
        executor.shutdown();
        System.err.println( errorStore.toString() );
        return true;
    }
    
    public void setOutput( String filename, String function ) {
        outputFilename = filename;
        outputFunction = function;
    }
}
