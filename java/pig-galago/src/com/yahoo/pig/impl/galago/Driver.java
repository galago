/*
 * Driver
 *
 * September 5, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.PigServer;
import com.yahoo.pig.PigServer.ExecType;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class Driver {
    public static void main( String[] args ) throws FileNotFoundException, IOException, Exception {
        PigServer server = new PigServer( ExecType.GALAGO );
        BufferedReader reader = new BufferedReader( new FileReader( args[0] ) );
        String line;
        
        if( args.length != 3 ) {
            System.err.println( "usage: pigScriptName outputVariable outputFilename" );
            return;
        }
        
        while( (line = reader.readLine()) != null ) {
            // skip comments
            if( line.startsWith( "#" ) ) {
                continue;
            }
            
            try {
                server.registerQuery( line );
            } catch( Exception e ) {
                throw new Exception( "Problem in query line: " + line, e );
            }
        }
        reader.close();
        
        String outName = args[1];
        String outputFilename = args[2];
        
        server.store( outName, outputFilename );
    }
}
