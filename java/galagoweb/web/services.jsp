<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Galago: Web Services</title>
    </head>
    <body>

    <h1>Galago Web Services</h1>
    
    This Galago web application contains REST-style web services that you can use from other
    applications.  There are three of these services:
    
    <ul>
        <li><strong>search</strong>, which takes a query as a parameter and returns a list of
        query results, including snippets (document summaries),</li>
        <li><strong>document</strong>, which returns the raw text of a document in the collection, and</li>
        <li><strong>snippet</strong>, which returns a document summary for a document in the collection.</li>
    </ul>
    
    Most people will use just the <strong>search</strong> service.  However, the <strong>document</strong>
    service lets you add cached document retrieval to a web interface.  The <strong>snippet</strong> service
    lets you add document summaries to an existing retriveal system that doesn't support them.
    
    </body>
</html>
