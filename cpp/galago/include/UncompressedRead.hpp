
//
// UncompressedRead
//
// 27 Feburary 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_UNCOMPRESSEDREAD_HPP
#define GALAGO_UNCOMPRESSEDREAD_HPP

class UncompressedRead {
public: 
    static UINT8 read_u8( const UINT8*& data ) {
        UINT8 a = *data;
        data++;
        return a;
    }
    
    static UINT16 read_u16( const UINT8*& data ) {
        UINT16 a = 0;
        
        for( int i=0; i<2; i++ ) {
            a <<= 8;
            a |= *data;
            data++;
        }          
        
        return a;
    }   
    
    static UINT32 read_u32( const UINT8*& data ) {
        UINT64 a = 0;
        
        for( int i=0; i<4; i++ ) {
            a <<= 8;
            a |= *data;
            data++;
        }          
        
        return a;
    }                             
        
    static UINT64 read_u64( const UINT8*& data ) {
        UINT64 a = 0;
        
        for( int i=0; i<8; i++ ) {
            a <<= 8;
            a |= *data;
            data++;
        }          
        
        return a;
    }

    static UINT8 peek_u8( const UINT8* data ) {
        UINT8 a = *data;
        data++;
        return a;
    }
    
    static UINT16 peek_u16( const UINT8* data ) {
        UINT16 a = 0;
        
        for( int i=0; i<2; i++ ) {
            a <<= 8;
            a |= *data;
            data++;
        }          
        
        return a;
    }   
    
    static UINT32 peek_u32( const UINT8* data ) {
        UINT64 a = 0;
        
        for( int i=0; i<4; i++ ) {
            a <<= 8;
            a |= *data;
            data++;
        }          
        
        return a;
    }                             
        
    static UINT64 peek_u64( const UINT8* data ) {
        UINT64 a = 0;
        
        for( int i=0; i<8; i++ ) {
            a <<= 8;
            a |= *data;
            data++;
        }          
        
        return a;
    }
};

#endif // GALAGO_UNCOMPRESSEDREAD_HPP       
