/*
 * OutputClass
 *
 * August 27, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author trevor
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface OutputClass {
    String className() default "java.lang.Object";
    String[] order() default {};
}
