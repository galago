
//
// IndexReader
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "IndexReader.hpp"
#include "indri/Path.hpp"
#include "Stemmer.hpp"
#include "Stopper.hpp"
#include "Query.hpp"
#include "UncompressedRead.hpp"
#include "lemur/RVLCompress.hpp"
#include "indri/RVLDecompressStream.hpp"

//
// IndexReader constructor
//

IndexReader::IndexReader() :
    invertedFileMapping(0),
    stemmer(0),
    stopper(0)
{   
}

//
// IndexReader deconstructor
//

IndexReader::~IndexReader() {
    close();
}
      
//
// openRead
//

void IndexReader::openRead( const std::string& indexPath ) {
    std::string invertedFilePath = indri::file::Path::combine( indexPath, "invertedFile" );
    std::string vocabularyPath = indri::file::Path::combine( indexPath, "vocabulary" );
    std::string manifestPath = indri::file::Path::combine( indexPath, "manifest" );
    
    invertedFile.openRead( invertedFilePath );
    invertedFileMapping = invertedFile.mapFileRead();

    manifest.loadFile( manifestPath );
    vocabulary.read( vocabularyPath, invertedFile.size() );
                 
    _build_stemmer();
    _build_stopper();
    _requiresWeighting = (manifest.get("queryBins", 0) == 0);
    _maximumDocumentCount = manifest.get( "maximumDocumentCount", (INT64)1 );
}                    

//
// _build_stopper
//

void IndexReader::_build_stopper() {
    stopper = 0;
    if( manifest.exists("stopper") ) {
        stopper = new Stopper( manifest );
    }
}

//
// _build_stemmer
//               

void IndexReader::_build_stemmer() {
    stemmer = 0;
    if( manifest.get( "stemmer", "" ) == "porter2" ) {
        stemmer = new Stemmer();
    }
}    

//
// close
//                                      

void IndexReader::close() {
    delete stemmer;
    delete stopper;
    stemmer = 0;
    stopper = 0;
    
    delete invertedFileMapping;
    invertedFile.close();
    invertedFileMapping = 0;
}

//
// processTerm
//

QueryTerm IndexReader::processTerm( const QueryTerm& term ) const {
    std::string processed = term.text;

    normalizer.process( processed );

    if( stopper )
        stopper->process( processed );

    if( stemmer )
        stemmer->process( processed );

    QueryTerm result = term;
    result.text = processed;

    return result;
}
  
//
// getTerm
//

ListRegion IndexReader::getTerm( const std::string& term ) const {
    const VocabularyReader::TermSlot& slot = vocabulary[term];
    return _loadTerm( term, slot.begin );
}

//
// getManifest
//

indri::api::Parameters& IndexReader::getManifest() {
    return manifest;
}

//
// maximumDocumentCount
//

UINT64 IndexReader::maximumDocumentCount() const {
    return _maximumDocumentCount;
}

//
// requiresWeighting
//

bool IndexReader::requiresWeighting() const {
    return _requiresWeighting;
}

//
// _word_block_end_offset
//

static UINT16 _word_block_end_offset( const UINT16* wordBlockEnds, int index ) {
    if ( index == 0 ) {
        return 0;
    } else {
        return UncompressedRead::peek_u16( (const UINT8*) (&wordBlockEnds[index-1]) );
    }
}
  

//
// _findVocabGroup
//

const UINT8* IndexReader::_findVocabGroup( const char* termSuffix, const UINT8* region, const UINT16* wordBlockEnds, int wordBlockCount, int& blockWordIndex ) const {
    // first, we do a binary search for the appropriate group
    int big = wordBlockCount-1;
    int small = 0;

    while( big-small > 1 ) {
        int middle = small + (big-small)/2;
        const UINT8* middleBlock = region + _word_block_end_offset( wordBlockEnds, middle );
        int length = *middleBlock;
        
        if ( strncmp( (const char*)middleBlock + 1, termSuffix, length ) <= 0 )
            small = middle;
        else
            big = middle;
    }
    
    const UINT8* bigBlock = region + _word_block_end_offset( wordBlockEnds, big );
    int bigSuffixLength = *bigBlock;
    const UINT8* block = 0;
    
    if( strncmp( (const char*)bigBlock + 1, termSuffix, bigSuffixLength ) <= 0 ) {
        blockWordIndex = big * IndexReader::vocabGroup;
        block = region + _word_block_end_offset( wordBlockEnds, big );
    } else {              
        blockWordIndex = small * IndexReader::vocabGroup;
        block = region + _word_block_end_offset( wordBlockEnds, small );
    }
    
    return block;
}

//
// _findTermInBlock
//

int IndexReader::_findTermInBlock( const UINT8* vocabBlock, const char* termSuffix, int blockWordIndex, int overlap, int wordCount ) const {
    // next, we do a linear scan for the actual term
    char blockTerm[ INDEXREADER_MAX_TERM_LENGTH ];
    
    // copy the first term
    int length = *vocabBlock;
    memcpy( blockTerm, vocabBlock+1, *vocabBlock );
    blockTerm[length] = 0;
                     
    // if blockTerm == termSuffix
    if( strcmp( blockTerm, termSuffix ) == 0 )
        return blockWordIndex;
        
    int end = std::min( wordCount, blockWordIndex + INDEXREADER_VOCAB_GROUP );
    vocabBlock += length + 1;
    
    for( int i = blockWordIndex+1; i < end; i++ ) {
        int common = vocabBlock[0];
        int wordLength = vocabBlock[1];
        
        strncpy( blockTerm + common - overlap, (const char*) vocabBlock + 2, wordLength - common );
        blockTerm[wordLength - overlap] = 0;
        
        if( strcmp( blockTerm, termSuffix ) == 0 )
            return i;  
            
        vocabBlock += 2;
        vocabBlock += wordLength - common;
    }                
    
    return -1;
}
 
//
// loadTerm
//
   
ListRegion IndexReader::_loadTerm( const std::string& term, UINT64 slotBegin ) const {
    const UINT8* region = (const UINT8*) invertedFileMapping->region + slotBegin;
    
    UINT64 endBlock = UncompressedRead::read_u64( region );
    UINT64 wordCount = UncompressedRead::read_u64( region );
    
    int prefixLength = UncompressedRead::read_u8( region );
    const char* prefixBytes = (const char*) region;
    region += prefixLength;  
    const char* termSuffix = term.c_str() + prefixLength;
    
    if( strncmp(prefixBytes, term.c_str(), prefixLength) != 0 ) {
        // the prefix of this term needs to match, otherwise it's not here
        return ListRegion();
    }                             
                              
    int wordBlockCount = (wordCount + INDEXREADER_VOCAB_GROUP - 1) / INDEXREADER_VOCAB_GROUP;
    
    // careful, dereference will fail on SPARC if improper alignment
    const UINT16* wordBlockEnds = (const UINT16*) region;
    region += wordBlockCount * sizeof(UINT16);
    const UINT16* invertedListEnds = (const UINT16*) region;
    region += wordCount * sizeof(UINT16);      
    
    // first, we do a binary search for the appropriate group
    int blockWordIndex = 0;
    const UINT8* vocabBlock = _findVocabGroup( termSuffix, region, wordBlockEnds, wordBlockCount, blockWordIndex ); 
    
    // next, we search for the term within the block
    int wordIndex = _findTermInBlock( vocabBlock, termSuffix, blockWordIndex, prefixLength, wordCount );
    
    if( wordIndex < 0 )
        return ListRegion(); 
        
    // assuming we've found the term, we get things set up
    int suffixBytes = UncompressedRead::peek_u16( (const UINT8*) (&wordBlockEnds[wordBlockCount-1]) );
    long headerLength = 8 + // word count
                        8 + // block end
                        1 + prefixLength +    // word prefix bytes
                        2 * wordBlockCount +  // word lengths 
                        2 * wordCount +       // inverted list endings
                        suffixBytes;          // suffix storage 
                                                  
    // inverted lists start here
    UINT64 startInvertedLists = slotBegin + headerLength;
    ListRegion listRegion;
    
    if( wordIndex == 0 ) {
        listRegion.start = startInvertedLists;
    } else {
        listRegion.start = endBlock - UncompressedRead::peek_u16( (const UINT8*) (&invertedListEnds[wordIndex-1]) );
    }      

    if( wordIndex == wordCount-1 ) {
        listRegion.end = endBlock;
    } else {
        listRegion.end = endBlock - UncompressedRead::peek_u16( (const UINT8*) (&invertedListEnds[wordIndex]) );
    }
    
    listRegion.data = (const char*) invertedFileMapping->region;
    return listRegion;
}          


