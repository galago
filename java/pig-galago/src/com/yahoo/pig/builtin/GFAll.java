/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import com.yahoo.pig.GroupFunc;
import com.yahoo.pig.data.*;


/**
 * built-in grouping function; sends all tuples to the same group.
 */
public class GFAll extends GroupFunc {
	
	public Datum[] exec(Tuple input) {
		return new Datum[]{new DataAtom("all")};
	}
}
