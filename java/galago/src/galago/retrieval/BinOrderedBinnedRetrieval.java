/*
 * BinOrderedBinnedRetrieval
 * 
 * 28 November 2006 -- Trevor Strohman
 *
 * BSD License (http://galagosearch.org/license)
 */

package galago.retrieval;

import galago.retrieval.query.SimpleQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import galago.retrieval.query.SimpleQuery.QueryTerm;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.parse.Document;
import galago.retrieval.query.Node;

/**
 *
 * @author trevor
 */
public class BinOrderedBinnedRetrieval extends Retrieval {
    BinOrderedBinnedIndex index;
    Parameters parameters;
    
    /** Creates a new instance of Retrieval */
    public BinOrderedBinnedRetrieval( String path ) throws IOException {
        index = new BinOrderedBinnedIndex(path);
    }
    
    public String getDocument( int doc ) throws IOException {
        return index.getDocument(doc);
    }
    
    void calculateQueryWeights( Collection<ScoredIterator> iterators, int binCount, long maximumDocumentCount ) {
        // 2. compute these log value things
        ArrayList<Double> logWeights = new ArrayList<Double>();
        double maxWeight = 0;
        for( ScoredIterator iter : iterators ) {
            double logWeight = Math.log( 1 + (double)maximumDocumentCount / (double)iter.iterator.documentCount() );
            logWeights.add( logWeight );
            maxWeight = Math.max( maxWeight, logWeight );
        }
        
        // 3. bin them to the bin levels
        ArrayList<Integer> binnedWeights = new ArrayList<Integer>();
        for( double logWeight : logWeights ) {
            int binned = (int) (binCount * logWeight / maxWeight);
            binned = Math.max( 1, binned );
            binnedWeights.add( binned );
        }
        
        // 4. apply those weights back to the iterators themselves
        Iterator<Integer> binWeightIterator = binnedWeights.iterator();
        for( ScoredIterator iter : iterators ) {
            iter.weight = binWeightIterator.next();
        }
    }
    
    public ArrayList<String> split( String query ) {
        String[] terms = query.split( " " );
        ArrayList<String> words = new ArrayList<String>();
        for( String t : terms ) {
            words.add(t);
        }
        return words;
    } 
    
    public List<QueryTerm> transformQuery( BinOrderedBinnedIndex index, List<QueryTerm> qTerms ) throws IOException {
        ArrayList<QueryTerm> resultingTerms = new ArrayList<QueryTerm>();
  
        // run through each query term, split it into word components (assume phrases)
        // stem and stop each part.  if there
        for( QueryTerm t : qTerms ) {
            Document queryTermDocument = new Document();
            queryTermDocument.terms = new ArrayList<String>();
            String[] words = t.text.split(" ");
            
            for( String word : words ) {
                queryTermDocument.terms.add(word);
            }
            
            List< Processor<Document> > transformations = index.transformations( t.field );
            
            for( Processor<Document> transform : transformations ) {
                transform.process( queryTermDocument );
            }
            
            String transformed = "";
            for( String word : queryTermDocument.terms ) {
                if (word == null) {
                    transformed = "";
                    break;
                }
                
                transformed += " " + word;
            }
            
            t.text = transformed.trim();
            
            if (t.text.length() > 0)
                resultingTerms.add(t);
        }
        
        return resultingTerms;
    }
    
    public boolean iteratorsExist( List<BinOrderedBinnedIndexReader.BinnedListIterator> iterators ) {
        for( BinOrderedBinnedIndexReader.BinnedListIterator iterator : iterators ) {
            if( iterator != null )
                return true;
        }
        return false;
    }
    
    private static class ScoredIterator implements Comparable<ScoredIterator> {
        public BinOrderedBinnedIndexReader.BinnedListIterator iterator;
        public int index;
        public int maxScore;
        public int weight;
        
        public ScoredIterator( BinOrderedBinnedIndexReader.BinnedListIterator iterator, int maxScore ) {
            this.iterator = iterator;
            this.maxScore = maxScore;
            this.index = -1;
            this.weight = 1;
        }
        
        public ScoredIterator( BinOrderedBinnedIndexReader.BinnedListIterator iterator ) {
            this(iterator, Short.MAX_VALUE);
        }
        
        public int compareTo( ScoredIterator other ) {
            return maxScore - other.maxScore;
        }
    }
    
    // BUGBUG: this doesn't seem to work properly (not sure why)
    ScoredDocument[] slowEval( ArrayList<ScoredIterator> iterators, int requested ) throws IOException {
        HashMap<Integer,Integer> m = new HashMap<Integer, Integer>();
        
        for( ScoredIterator iter : iterators ) {
            do {
                int[] documents = iter.iterator.documents();
                int score = iter.weight * iter.iterator.bin();
                
                for( int i=0; i<documents.length; i++ ) {
                    if( m.containsKey(documents[i]) )
                        m.put( documents[i], m.get(documents[i]) + score );
                    else
                        m.put( documents[i], score );
                }
            }
            while( iter.iterator.next() );
        }

        PriorityQueue<ScoredDocument> scores = new PriorityQueue<ScoredDocument>();
        
        for( Map.Entry<Integer, Integer> entry : m.entrySet() ) {
            int document = entry.getKey();
            int score = entry.getValue();
            
            if (scores.size() < requested || score > scores.peek().score )
                scores.add( new ScoredDocument(document,score) );
            
            if (scores.size() > requested)
                scores.poll();
            
        }
        
        ScoredDocument[] results = new ScoredDocument[scores.size()];
        
        for( int i=scores.size()-1; i>=0; i-- ) {
            results[i] = scores.poll();
        }
        
        return results;
    }
    
    /**
     * This is supposed to emulate the C++ efficient evaluation function.  Unfortunately,
     * while it did work once, I don't think it works now (it returns incorrect results).
     * 
     * @param iterators
     * @param requested
     * @return
     * @throws java.io.IOException
     */

    ScoredDocument[] fastEval( ArrayList<ScoredIterator> iterators, int requested ) throws IOException {
        LongArrayAccumulators accumulators = new LongArrayAccumulators( iterators.size() );
        PriorityQueue<ScoredIterator> queue = new PriorityQueue<ScoredIterator>();
        
        // now, queue up the iterators
        int count = 0;
        for( ScoredIterator iter : iterators ) {
            iter.maxScore = iter.weight * iter.iterator.bin();
            iter.index = count;
            queue.add(iter);
            count++;
            accumulators.setMaxUnseen( iter.index, iter.maxScore );
        }
        
        while( queue.size() > 0 ) {
            ScoredIterator iter = queue.poll();
            
            int score = iter.iterator.bin() * iter.weight;
            assert score <= iter.maxScore;
            int[] documents = iter.iterator.documents();
                
            accumulators.processBin( score, documents, iter.index );
            boolean hasMore = iter.iterator.next();
            
            if( hasMore ) {
                assert iter.iterator.bin() * iter.weight <= iter.maxScore;
                iter.maxScore = iter.iterator.bin() * iter.weight;
                accumulators.setMaxUnseen( iter.index, iter.maxScore );
                queue.add(iter);
            } else {
                accumulators.setMaxUnseen( iter.index, 0 );
            }
            
            System.out.println( accumulators.getStats() );
            
            accumulators.calculateThresholdScore( requested );
            accumulators.trim();
            
            boolean canIgnore = accumulators.canIgnoreFuturePostings( requested );
            if (canIgnore) break;
        }
        
        return accumulators.topResults( requested );
    }

    List<QueryTerm> extractTerms( Node query ) throws Exception {
        ArrayList<QueryTerm> terms = new ArrayList<QueryTerm>();
        
        for( Node child : query.getInternalNodes() ) {
            if( child.getOperator().equals( "scores" ) || child.getOperator().equals( "text" ) ) {
                terms.add( new QueryTerm( child.getDefaultParameter() ) );
            } else {
                throw new Exception( "Unrecognized query operator: " + child.getOperator() );
            }
        }
        
        return terms;
    }
    
    public ScoredDocument[] runQuery( Node query, int requested ) throws Exception {
        List<QueryTerm> qTerms = extractTerms( query );
        List<QueryTerm> terms = transformQuery( index, qTerms );
        ArrayList<ScoredIterator> iterators = new ArrayList<ScoredIterator>();
        PriorityQueue<ScoredIterator> queue = new PriorityQueue<ScoredIterator>();
        
        // fetch iterators for the query terms
        for( int i=0; i<terms.size(); i++ ) {
            QueryTerm term = terms.get(i);
            BinOrderedBinnedIndexReader.BinnedListIterator iterator = index.getTerm(term.text, term.field);
            ScoredIterator scoredIterator = new ScoredIterator( iterator );
            
            if( iterator != null && iterator.next() ) { 
                iterators.add(scoredIterator);
            }
        }
        
        if( index.needsQueryWeights() )
            calculateQueryWeights( iterators, index.bins(), index.maximumDocumentCount() );
        
        return slowEval( iterators, requested );
    }
    
    public ScoredDocument[] runQuery( String query, int requested ) throws Exception {
        Node qTerms = SimpleQuery.parseTree(query);
        return runQuery( qTerms, requested );
    }    
    
    public void close() throws IOException {
        index.close();
    }
    
    public static void main( String[] args ) throws Exception {
        // read in parameters
        Parameters parameters = new Parameters(args);
        List<Parameters.Value> queries = parameters.list("query");
        
        // open index
        BinOrderedBinnedRetrieval retrieval = new BinOrderedBinnedRetrieval( parameters.get("index") );

        // record results requested
        int requested = (int)parameters.get( "count", 1000 );
        
        // for each query, run it, get the results, look up the docnos, print in TREC format
        for( Parameters.Value query : queries ) {
            ScoredDocument[] results = retrieval.runQuery( query.get("text"), requested );
            
            for( int i=0; i<results.length; i++ ) {
                String document = retrieval.getDocument(results[i].document);
                int score = (int) results[i].score;
                int rank = i+1;
                
                System.out.format( "%s Q0 %s %s %d galago\n", query.get("number"), document, rank, score );
            }
        }
    }
}
