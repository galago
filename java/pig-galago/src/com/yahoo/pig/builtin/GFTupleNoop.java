/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;


import java.io.IOException;

import com.yahoo.pig.GroupFunc;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;


/**
 * This is the identity group function. It will simply
 * return the passed tuple as the tuple to define the group.
 */
public class GFTupleNoop extends GroupFunc {

	public Datum[] exec(Tuple input) {
		if (input.arity()==1){
			try{
				return new Datum[]{input.getField(0)};
			}catch(IOException e){
				throw new RuntimeException(e);
			}
		}
		return new Datum[]{input};
	}
}
