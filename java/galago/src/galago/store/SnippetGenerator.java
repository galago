/*
 * SnippetGenerator
 *
 * June 6, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.store;

import galago.Utility;
import galago.parse.Document;
import galago.parse.Porter2Stemmer;
import galago.parse.TagTokenizer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 *
 * @author trevor
 */
public class SnippetGenerator {
    public static final int width = 5;
    
    public static class Match {
        public Match( String term, int index ) {
            this(term, index, index+1);
        }
        
        public Match( String term, int start, int end ) {
            this.term = term;
            this.start = start;
            this.end = end;
        }
        
        String term;
        int start;
        int end;
    }
    
    public static class SnippetRegion {
        int start;
        int end;
        ArrayList<Match> matches;
        
        public SnippetRegion( String term, int index, int width, int maximum ) {
            matches = new ArrayList();
            matches.add( new Match(term, index) );
            start = Math.max( index - width, 0 );
            end = Math.min( maximum, index + width );
        }
        
        public SnippetRegion( ArrayList<Match> m, int s, int e ) {
            matches = m;
            start = s;
            end = e;
        }
        
        public boolean overlap( SnippetRegion o ) {
            return (start <= o.start && end >= o.start) ||
                    (start <= o.end && end >= o.end);
        }
        
        public boolean within( SnippetRegion o, int distance ) {
            if( overlap(o) )
                return true;
            
            if( Math.abs(start - o.end) <= distance )
                return true;
            
            if( Math.abs(end - o.start) <= distance )
                return true;
            
            return false;
        }
        
        public SnippetRegion merge( SnippetRegion o ) {
            ArrayList<Match> m = new ArrayList();
            m.addAll(matches);
            m.addAll(o.matches);
            
            SnippetRegion result = new SnippetRegion( m, Math.min(start, o.start), Math.max(end, o.end) );
            return result;
        }
        
        public boolean equals( SnippetRegion o ) {
            return start == o.start && end == o.end;
        }
        
        public int size() {
            return this.end - this.start;
        }
        
        public ArrayList<Match> getMatches() {
            return matches;
        }
    }
    
    public class Snippet {
        private ArrayList<SnippetRegion> regions;
        double score;
        
        public Snippet( ArrayList<SnippetRegion> regions ) {
            this.regions = regions;
        }
        
        public int hashCode() {
            int result = 0;
            
            for( SnippetRegion region : regions ) {
                result += region.end * 3 + region.start;
                result *= 5;
                result += region.getMatches().size();
            }
            
            return result;
        }
        
        public boolean equals( Snippet other ) {
            if( other.regions.size() != regions.size() )
                return false;
            
            for( int i=0; i<regions.size(); i++ ) {
                if( regions.get(i).equals(other.regions.get(i)) )
                    continue;
                return false;
            }
            
            return true;
        }
        
        public double score() {
            if( score == 0 )
                cacheScore();
            return score;
        }

        public void cacheScore() {
            // Factors:  big snippets are discounted
            //           coverage is good
            //           proximity is good
            //           close to document start is good
            
            int wordLength = 0;
            int prox = 0;
            
            HashSet<String> words = new HashSet<String>();
            
            for( SnippetRegion region : regions ) {
                wordLength += region.size();
                prox += Math.pow(2, region.getMatches().size());
                
                for( SnippetGenerator.Match m : region.getMatches() ) {
                    words.add( m.term );
                }
            }

            score = -Math.pow( 1.2, Math.min( 0, wordLength - 150 ) ) + prox + Math.pow( words.size(), 2 );
        }

        public ArrayList<Snippet> expand() {
            ArrayList<Snippet> results = new ArrayList();
            int size = 0;
            
            for( SnippetRegion region : regions ) {
                size += region.size();
            }
            
            if( size > 150 ) {
                // try deletions
                for( int i=0; i<regions.size(); i++ ) {
                    ArrayList<SnippetRegion> newRegions = new ArrayList();

                    newRegions.addAll( regions.subList(0, i) );
                    newRegions.addAll( regions.subList(i+1, regions.size()) );

                    results.add( new Snippet(newRegions) );
                }
            }
            
            // try merges
            for( int i=0; i<regions.size()-1; i++ ) {
                if( regions.get(i+1).start - regions.get(i).end > 100 )
                    continue;
                
                ArrayList<SnippetRegion> newRegions = new ArrayList();
                
                newRegions.addAll( regions.subList(0, i) );
                SnippetRegion merged = regions.get(i).merge( regions.get(i+1) );
                newRegions.add(merged);
                newRegions.addAll( regions.subList(i+2, regions.size()) );
                
                results.add( new Snippet(newRegions) );
            }
            
            return results;
        }
    }
    
    private Document parseAsDocument( String text ) throws IOException {
        return parseAsDocument( text, null );
    }
    
    private Document parseAsDocument( String text, ArrayList<TagTokenizer.Pair> positions ) throws IOException {
        Document document = new Document();
        document.text = text;
        
        // Tokenize the document
        TagTokenizer tokenizer = new TagTokenizer();
        tokenizer.process( document );
        
        if( positions != null )
            positions.addAll( tokenizer.getTokenPositions() );

        // stem it too
        Porter2Stemmer stemmer = new Porter2Stemmer();
        stemmer.process( document );
        
        return document;
    }
    
    public String highlight( String documentText, String queryText ) throws IOException {
        ArrayList<TagTokenizer.Pair> positions = new ArrayList();
        
        Document query = parseAsDocument( queryText );
        Document document = parseAsDocument( documentText, positions );

        SnippetRegion merged = findSingleRegion( document, query );
        Snippet best = new Snippet( new ArrayList(Collections.singletonList(merged)) );

        String result = buildHtmlString( best, document, positions );
        return result;
    }

    public String getSnippet( String documentText, String queryText ) throws IOException {
        ArrayList<TagTokenizer.Pair> positions = new ArrayList();
        
        Document query = parseAsDocument( queryText );
        Document document = parseAsDocument( documentText, positions );
        
        return generateSnippet(query, positions, document);
    }

    private String generateSnippet(final Document query, final ArrayList<TagTokenizer.Pair> positions, final Document document) {
        ArrayList<SnippetRegion> regions = findMatches(document, query);
        ArrayList<SnippetRegion> finalRegions = combineRegions(regions);
        Snippet best = new Snippet( finalRegions );

        String result = buildHtmlString( best, document, positions );
        return result;
    }
    
    private SnippetRegion findSingleRegion( final Document document, final Document query ) {
        // Add the query terms to a set so we can find them easily
        HashSet<String> queryTerms = new HashSet();
        for( String term : query.terms ) {
            queryTerms.add(term);
        }
        
        // Make a snippet region object for each term occurrence in the document,
        // while also counting matches
        ArrayList<Match> matches = new ArrayList();
        
        for( int i=0; i<document.terms.size(); i++ ) {
            String term = document.terms.get(i);
            if( queryTerms.contains( term ) ) {
                matches.add( new Match(term, i) );
            }
        }
        
        return new SnippetRegion( matches, 0, document.terms.size() );
    }

    private ArrayList<SnippetRegion> findMatches(final Document document, final Document query) {
        // Add the query terms to a set so we can find them easily
        HashSet<String> queryTerms = new HashSet();
        for( String term : query.terms ) {
            queryTerms.add(term);
        }
        
        // Make a snippet region object for each term occurrence in the document,
        // while also counting matches
        ArrayList<SnippetRegion> regions = new ArrayList();
        
        for( int i=0; i<document.terms.size(); i++ ) {
            String term = document.terms.get(i);
            if( queryTerms.contains( term ) ) {
                regions.add( new SnippetRegion(term, i, width, document.terms.size()) );
            }
        }
        return regions;
    }
    
    public String buildHtmlString( Snippet best, Document document, ArrayList<TagTokenizer.Pair> positions ) {
        StringBuilder builder = new StringBuilder();
        
        for( SnippetRegion region : best.regions ) {
            if( region.start != 0 )
                builder.append( "..." );
            
            // strip out script tags, find word segments, highlight them
            int startChar = positions.get(region.start).start;
            int endChar = positions.get(region.end-1).end;
            int start = 0;
            
            // section string
            String section = document.text.substring( startChar, endChar );
            
            for( Match m : region.matches ) {
                int startMatchChar = positions.get(m.start).start - startChar;
                int endMatchChar = positions.get(m.end-1).end - startChar;
                
                String intermediate = stripTags(section.substring( start, startMatchChar ));
                builder.append( intermediate );
                builder.append( "<strong>" );
                builder.append( stripTags(section.substring( startMatchChar, endMatchChar )) );
                builder.append( "</strong>" );
                start = endMatchChar;
            }
            
            if( start >= 0 ) {
                builder.append( stripTags( section.substring(start) ) );
            }
        }
        
        if( best.regions.size() > 1 && best.regions.get(best.regions.size()-1).end != document.terms.size() )
            builder.append( "..." );
        
        return builder.toString();
    }
    
    public String stripTag( String tag, String input ) {
        input = input.replaceAll( "<" + tag.toLowerCase() + "[^>]*>.*?</" + tag.toLowerCase() + ">", "" );
        input = input.replaceAll( "<" + tag.toUpperCase() + "[^>]*>.*?</" + tag.toUpperCase() + ">", "" );
        return input;
    }
    
    // BUGBUG: this also needs additional robustness
    public String stripTags( String input ) {
        input = stripTag( "script", input );
        input = stripTag( "style", input );
        input = input.replaceAll( "<!--.*?-->", "" );
        
        input = input.replaceAll( "&nbsp;", " " );
        input = input.replaceAll( "<[^>]*>", " " );
        input = input.replaceAll( "\\s+", " " );
        
        return input;
    }
    
    // Goals:  1. find as many terms as possible
    //         2. find terms that are close together
    //         3. break on sentences when possible (?)
    // BUGBUG: might not have all the terms highlighted here
    
    public ArrayList<SnippetRegion> combineRegions(final ArrayList<SnippetRegion> regions) {
        ArrayList<SnippetRegion> finalRegions = new ArrayList();
        SnippetRegion last = null;
        int snippetSize = 0;
        int maxSize = 40;
        
        for( int i=0; i<regions.size(); i++ ) {
            SnippetRegion current = regions.get(i);
            
            if( last == null ) {
                last = current;
            } else if( last.overlap(current) ) {
                SnippetRegion bigger = last.merge(current);
                
                if( bigger.size() + snippetSize > maxSize ) {
                    finalRegions.add( last );
                    last = null;
                } else {
                    last = bigger;
                }
            } else if( last.size() + snippetSize > maxSize ) {
                break;
            } else {
                finalRegions.add( last );
                snippetSize += last.size();
                last = current;
            }
        }
        
        if( last != null && snippetSize + last.size() < maxSize ) {
            finalRegions.add(last);
        }
        
        return finalRegions;
    }

    
    public static void main( String[] args ) throws Exception {
        SnippetGenerator generator = new SnippetGenerator();
        DocumentStore store = new DocumentStore( args[0], args[1] );
        Document document = store.get( "identifier", "GX000--00-7026384" );
        
        String text = generator.getSnippet( document.text, "white house" );
        System.out.println( Utility.wrap(text) );
        
        store.close();
    }
}
