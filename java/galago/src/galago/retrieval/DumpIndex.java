/*
 * DumpIndex
 *
 * December 5, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.Utility;
import galago.index.VocabularyReader;
import galago.retrieval.extents.ExtentArrayIterator;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import galago.tupleflow.FileOrderedReader;

public class DumpIndex {
    public static void dumpVocabulary( String filename ) throws Exception {
        BinOrderedBinnedIndex index = new BinOrderedBinnedIndex(filename);
        Set<String> featureSet = index.getFeatureNames();
        
        for( String feature : featureSet ) {
            BinOrderedBinnedIndexReader.BinnedListIterator iter = index.getIterator( feature );
            try {
                while(true) {
                    System.out.format( "%s,\"%s\"\n", feature, iter.currentTerm() );
                    iter.nextTerm();
                }
            } catch( EOFException e ) {}
        }
    }
    
    public static void dumpVocabularyFile( String filename, String invfile ) throws Exception {
        long length = 1<<30;
        
        if( invfile != null )
            length = new File(invfile).length();
        
        VocabularyReader reader = new VocabularyReader( filename, length );
        
        for( VocabularyReader.TermSlot slot : reader.getSlots() ) {
            System.out.format( "%s,%d,%d\n", Utility.makeString(slot.termData), slot.begin, slot.length );
        }
    }
    
    public static void dumpExtentList( String filename ) throws Exception {
        ExtentIndexReader index = new ExtentIndexReader(filename);
        ExtentIndexReader.ExtentListIterator iterator = index.getIterator();

        try {
            while (true) {
                while (!iterator.isDone()) {
                    ExtentArrayIterator iter = new ExtentArrayIterator(iterator.extents());
                    while (!iter.isDone()) {
                        System.out.println( iterator.currentTerm() + "," +
                                            iterator.document() + "," +
                                            iter.current().begin + "," +
                                            iter.current().end );
                        iter.next();
                    }
                    iterator.nextDocument();
                }

                iterator.nextTerm();
            } 
        } catch( EOFException e ) {}
        
        index.close();
    }

    public static void dumpDocumentOrderedIndex( String filename, String term ) throws Exception {
        DocumentOrderedBinnedIndex index = new DocumentOrderedBinnedIndex(filename);
        DocumentOrderedBinnedIndexReader.DocumentOrderedBinnedIndexIterator iter;
 
        if( term != null ) {
            iter = (DocumentOrderedBinnedIndexReader.DocumentOrderedBinnedIndexIterator) index.getTerm(term);        
            iter.printList( System.out );
        } else {
            iter = (DocumentOrderedBinnedIndexReader.DocumentOrderedBinnedIndexIterator) index.getIterator( "default" );

            try {
                do {
                    iter.printList( System.out );
                    iter.nextList();
                } while(true);
            } catch( EOFException e ) {}
        }        

        index.close();
    }
    
    public static void dumpBinnedInvertedList( String filename ) throws Exception {
        BinOrderedBinnedIndex index = new BinOrderedBinnedIndex(filename);
        Set<String> featureSet = index.getFeatureNames();
        
        for( String feature : featureSet ) {
            BinOrderedBinnedIndexReader.BinnedListIterator iter = index.getIterator( feature );
            try {
                do {
                    String term = iter.currentTerm();
                    while(iter.next()) {
                        int docs[] = iter.documents();
                        int bin = iter.bin();

                        for( int doc : docs ) {
                            System.out.println( feature + ",\"" + term + "\"," + iter.bin() + "," + doc );
                        }
                    }
                    iter.nextTerm();
                }
                while( true );
            } catch( EOFException e ) {}
        }
    }
    
    public static void dumpTermList( String filename, String term, String feature ) throws Exception {
        BinOrderedBinnedIndex index = new BinOrderedBinnedIndex(filename);
        BinOrderedBinnedIndexReader.BinnedListIterator iter = index.getTerm( term, feature );
        
        try {
            while(iter.next()) {
                int docs[] = iter.documents();
                int bin = iter.bin();

                for( int doc : docs ) {
                    System.out.println( feature + ",\"" + term + "\"," + iter.bin() + "," + doc );
                }
            }
        } catch( EOFException e ) {}
    }
    
    public static void dumpList( String filename ) throws IOException {
        FileOrderedReader reader = new FileOrderedReader( filename );
        Object o;
        
        while( (o = reader.read()) != null ) {
            System.out.println(o);
        }
    }
    
    public static void main( String[] args ) throws IOException, Exception {
        if( args.length == 0 ) {
            System.err.println( "DumpIndex requires at least a filename argument." );
            return;
        } 
        
        if( !new File(args[0]).exists() ) {
            System.err.println( args[0] + " is not a file." );
            return;
        }
        
        String filename = args[0];
        
        if( args.length == 1 || args[1].equals( "arbitrary" ) ) {
            dumpList( filename );
        } else {
            String operation = args[1];
            if( operation.equals( "extent" ) ) {
                dumpExtentList( filename );
            } else if( operation.equals( "vocabulary" ) ) {
                dumpVocabulary( filename );
            } else if( operation.equals( "vocabularyFile" ) ) {
                String invertedFile = args[2];
                dumpVocabularyFile( filename, invertedFile );
            } else if( operation.equals( "invertedList" ) ) {
                dumpBinnedInvertedList( filename );
            } else if( operation.equals( "docOrderedIndex" ) ) {
                String term = null;

                if( args.length > 2 )
                    term = args[2];

                dumpDocumentOrderedIndex( filename, term );
            } else {
                System.out.println( "Unknown operation: " + operation );
            }
        }
    }
}
