/*
 * DocumentOrderedBinnedMaxScoreRetrieval
 * 
 * October 4, 2007 -- Trevor Strohman
 *
 * BSD License (http://galagosearch.org/license)
 */

package galago.retrieval;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * This class implements Turtle and Flood's max score algorithm for document-at-a-time
 * retrieval.  Also includes a mode for score skipping (Strohman 2007).
 * 
 * @author trevor
 */
public class DocumentOrderedBinnedMaxScoreRetrieval extends DocumentOrderedBinnedRetrieval {
    boolean scoreSkipping;
    
    public DocumentOrderedBinnedMaxScoreRetrieval( String name ) throws IOException { 
        this( new DocumentOrderedBinnedIndex(name) );
    }
    
    public DocumentOrderedBinnedMaxScoreRetrieval( DocumentOrderedBinnedIndex index ) {
        this(index, false);
    }
    
    public DocumentOrderedBinnedMaxScoreRetrieval( DocumentOrderedBinnedIndex index, boolean scoreSkipping ) {
        super(index);
        this.scoreSkipping = scoreSkipping;
    }
    
    // BUGBUG:  This implementation doesn't quite match the C++ one, which is correct.
    PriorityQueue<ScoredDocument> runQueueQuery(List<DocumentOrderedIterator> iterators,
                                                List<DocumentOrderedIterator> priors,
                                                boolean conjunctive,
                                                int requested) throws Exception {
        assert requested > 0;
        assert conjunctive == false;
        ArrayList<DocumentOrderedIterator> allIterators = new ArrayList<DocumentOrderedIterator>();

        allIterators.addAll( iterators );
        allIterators.addAll( priors );
        
        PriorityQueue<ScoredDocument> queue = new PriorityQueue<ScoredDocument>();
        int[] bounds;
        int[] remaining;
        int threshold = 0;
        int document = Integer.MAX_VALUE;

        // find the smallest document in any iterator
        for( int i=0; i<iterators.size(); i++ ) {
            assert iterators.get(i).isDone() == false;
            document = Math.min( iterators.get(i).currentDocument(), document );
        }
        
        bounds = computeBounds( allIterators, threshold );
        remaining = computeRemaining( allIterators );
        
        retry:
        while (document < Integer.MAX_VALUE) {
            // score candidate
            int score = 0;
            
            for( int i=0; i<allIterators.size(); i++ ) {
                DocumentOrderedIterator iter = allIterators.get(i);
                assert iter.isDone() == false;
                
                int required = Math.max( 0, threshold - score - remaining[i] );
                
                // make sure we're pointing to the right spot
                if( scoreSkipping ) {
                    boolean success = iter.skipToDocument( document, required );
                    
                    if( !success && required > 0 )
                        break;
                } else {
                    iter.skipToDocument( document );
                }
                
                // the movement might have caused the iterator to end, so check that
                if( iter.isDone() ) {
                    if( conjunctive )
                        return queue;
                    
                    removeDoneIterators( allIterators );
                    removeDoneIterators( iterators );
                    bounds = computeBounds( allIterators, threshold );
                    remaining = computeRemaining( allIterators );
                    continue retry;
                }
                
                // if we're on this document, score it
                if( iter.currentDocument() == document ) {
                    score += iter.currentScore();
                }
                
                // if this score will never make it into the ranked list, break
                if( bounds[i] > score )
                    break;
            }
            
            // push score onto heap if it's good
            if( queue.size() < requested || queue.peek().score <= score ) {
                queue.add( new ScoredDocument( document, score ) );
                
                while( queue.size() > requested ) {
                    queue.poll();
                    threshold = (int)queue.peek().score;
                }
                
                bounds = computeBounds( allIterators, threshold );
            }
            
            // move to the next reasonable document
            int lastDocument = document;
            document = Integer.MAX_VALUE;
            
            // if the first term is required, skip forward to find a usable document
            if( scoreSkipping && bounds[0] > 0 ) {
                DocumentOrderedIterator iter = iterators.get(0);
                
                if( iter.currentDocument() == lastDocument )
                    iter.nextDocument();
                
                iter.skipToBound( bounds[0] );
                
                if( iter.isDone() )
                    break;
            }
            
            // try to find a usable document
            // note that we only consider the core iterators here, not priors
            for( int i=0; i<iterators.size(); i++ ) {
                DocumentOrderedIterator iter = iterators.get(i);
                assert iter.isDone() == false;

                // if this iterator is pointing to the last scored document, we need to move it
                if( iter.currentDocument() == lastDocument ) {
                    iter.nextDocument();
                    
                    // might have run off the end of the iterator, so check that
                    if( iter.isDone() ) {
                        if( conjunctive )
                            return queue;
                        
                        removeDoneIterators( allIterators );
                        removeDoneIterators( iterators );
                        bounds = computeBounds( allIterators, threshold );
                        i--;
                    } else {
                        document = Math.min( document, iter.currentDocument() );
                    }
                } else {
                    document = Math.min( document, iter.currentDocument() );
                }

                // if the bound on this iterator is greater than zero, that means
                // that we don't have to look any further for reasonable documents to score
                if( i >= 0 && bounds[i] > 0 )
                    break;
            }
        }
        
        return queue;
    }
    
    int[] computeRemaining( List<DocumentOrderedIterator> iterators ) {
        int[] remaining = new int[iterators.size()];
        
        if( iterators.size() > 0 )
            remaining[iterators.size()-1] = 0;
        
        for( int i=iterators.size()-2; i >= 0; i-- ) {
            remaining[i] = remaining[i+1] + iterators.get(i+1).listBound();
        }
        
        return remaining;
    }
    
    int[] computeBounds( List<DocumentOrderedIterator> iterators, int threshold ) {
        int[] bounds = new int[iterators.size()];
        
        int boundSum = 0;
        for( int i=iterators.size()-1; i>=0; i-- ) {
            DocumentOrderedIterator iter = iterators.get(i);
            bounds[i] = Math.max( 0, threshold - boundSum );
            boundSum += iter.listBound();
        }
        
        return bounds;
    }

    void removeDoneIterators(List<DocumentOrderedIterator> iterators) {
        Iterator<DocumentOrderedIterator> iter = iterators.iterator();
        
        while( iter.hasNext() ) {
            DocumentOrderedIterator docIterator = iter.next();
            
            if( docIterator.isDone() )
                iter.remove();
        }
    }
}
