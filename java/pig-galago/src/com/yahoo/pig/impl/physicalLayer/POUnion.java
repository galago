/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;

import com.yahoo.pig.data.Tuple;

class POUnion extends PhysicalOperator {
    int currentInput;

    public POUnion(PhysicalOperator[] inputsIn, int outputType) {
    	super(outputType);
        inputs = inputsIn;
        currentInput = 0;
    }

    public POUnion(int numInputs, int outputType) {
    	super(outputType);
        inputs = new PhysicalOperator[numInputs];
        for (int i = 0; i < inputs.length; i++)
            inputs[i] = null;
    }
    
    public boolean open(boolean continueFromLast) throws IOException{
    	if (!super.open(continueFromLast)){
    		return false;
    	}
    	currentInput = 0;
    	return true;
    }

    public Tuple getNext() throws IOException {
        while (currentInput < inputs.length) {
            Tuple t = inputs[currentInput].getNext();

            if (t == null) {
                currentInput++;
                continue;
            } else {
                return t;
            }
        }

        return null;
    }

}
