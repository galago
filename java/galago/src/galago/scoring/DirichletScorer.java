/*
 * DirichletScorer
 *
 * August 17, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.scoring;

import galago.retrieval.extents.RequiredStatistics;
import galago.retrieval.extents.CountIterator;
import galago.retrieval.extents.DocumentDataIterator;
import galago.retrieval.extents.InputIterator;
import galago.retrieval.extents.ScoringFunctionIterator;
import galago.tupleflow.Parameters;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */

@InputIterator(className="galago.retrieval.extents.CountIterator", cardinality=1)
@RequiredStatistics(statistics={"collectionProbability"})
public class DirichletScorer extends ScoringFunctionIterator {
    double background;
    double mu;
    
    public DirichletScorer( Parameters parameters, ArrayList<DocumentDataIterator> iterators ) {
        super( (CountIterator) iterators.get(0) );
        
        mu = parameters.get( "mu", 1500 );
        background = parameters.get( "collectionProbability", 0.0001 );
    }
    
    public double scoreCount( int count, int length ) {
        double numerator = count + mu * background;
        double denominator = length + mu;
        
        return Math.log( numerator / denominator );
    }
}

