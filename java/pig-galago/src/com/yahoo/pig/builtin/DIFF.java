/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;
import java.util.Iterator;

import com.yahoo.pig.BagEvalFunc;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;

/**
 * DIFF compares the fields of a tuple with arity 2. If the fields are DataBags, it
 * will emit any Tuples that are in on of the DataBags but not the other. If the
 * fields are values, it will emit tuples with values that do not match.
 * 
 * @author breed
 *
 */
public class DIFF extends BagEvalFunc {
    /**
     * Compares a tuple with two fields. Emits any differences.
     * @param input a tuple with exactly two fields.
     * @throws IOException if there are not exactly two fields in a tuple
     */
    @Override
    public void exec(Tuple input, DataCollector output) throws IOException {
        if (input.arity() != 2) {
            throw new IOException("DIFF must compare two fields not " + input.arity());
        }
        if (input.getField(0) instanceof DataBag) {
            DataBag field1 = input.getBagField(0);
            DataBag field2 = input.getBagField(1);
            Iterator<Tuple> it1 = field1.content();
            checkInBag(field2, it1, output);
            Iterator<Tuple> it2 = field2.content();
            checkInBag(field1, it2, output);
        } else {
            DataAtom d1 = input.getAtomField(0);
            DataAtom d2 = input.getAtomField(1);
            if (!d1.equals(d2)) {
                output.add(new Tuple(d1));
                output.add(new Tuple(d2));
            }
        }
    }

    private void checkInBag(DataBag bag, Iterator<Tuple> iterator, DataCollector emitTo) throws IOException {
        while(iterator.hasNext()) {
            Tuple t = iterator.next();
            Iterator<Tuple> it2 = bag.content();
            boolean found = false;
            while(it2.hasNext()) {
                if (t.equals(it2.next())) {
                    found = true;
                }
            }
            if (!found) {
                emitTo.add(t);
            }
        }
    }
    
    
}