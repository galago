/*
 * StructuredRetrieval
 *
 * May 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.parse.Document;
import galago.retrieval.extents.CountIterator;
import galago.retrieval.extents.DocumentDataIterator;
import galago.retrieval.extents.ExtentIterator;
import galago.retrieval.extents.ScoreIterator;
import galago.retrieval.extents.StatisticsGatherer;
import galago.retrieval.query.Node;
import galago.retrieval.query.StructuredQuery;
import galago.retrieval.query.Traversal;
import galago.retrieval.extents.FeatureFactory;
import galago.retrieval.extents.NullExtentIterator;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;

/**
 *
 * @author trevor
 */
public class StructuredRetrieval extends Retrieval {
    StructuredIndex index;
    FeatureFactory featureFactory;

    /**
     * Creates a new instance of StructuredRetrieval
     */
    public StructuredRetrieval(String filename) throws FileNotFoundException, IOException {
        index = new StructuredIndex(filename);
        featureFactory = new FeatureFactory();
    }

    public ScoredDocument[] getArrayResults(PriorityQueue<ScoredDocument> scores) {
        ScoredDocument[] results = new ScoredDocument[scores.size()];

        for (int i = scores.size() - 1; i >= 0; i--) {
            results[i] = scores.poll();
        }

        return results;
    }
    
    public boolean isScoreNode(Node node) throws Exception {
        if( node.getOperator().equals( "feature" ) )
            return featureFactory.isScoreNode( node );
        
        if( node.getOperator().equals( "combine" ) )
            return true;
        
        return false;
    }

    public boolean requiresScoreChildren(Node node) throws Exception {
        if( node.getOperator().equals( "combine" ) )
            return true;
        
        if( node.getOperator().equals( "weight" ) )
            return true;
        
        return featureFactory.getInputIteratorType(node).equals("galago.retrieval.extents.ScoreIterator");
    }
    
    public class StatisticsTraversal implements Traversal {
        StructuredIndex index;
        HashSet<String> skipOperators = new HashSet<String>();

        public StatisticsTraversal(StructuredIndex index) {
            this.index = index;
            
            skipOperators.add( "counts" );
            skipOperators.add( "positions" );
            skipOperators.add( "scores" );
            skipOperators.add( "extents" );
        }

        public Node afterNode(Node node, ArrayList<Node> children) throws Exception {
            String operator = node.getOperator();
            
            if( skipOperators.contains( operator ) )
                return node;
            
            String[] required = featureFactory.getRequiredStatistics(node);

            if( required.length > 0 ) {
                // for right now, we count statistics for every feature.
                // in the future we might do this selectively.
                IteratorBuilder builder = new IteratorBuilder();
                StructuredQuery.walk(builder, node.getInternalNodes().get(0));

                // now we've created an iterator we can use
                CountIterator iterator = builder.getCountIterator();
                StatisticsGatherer gatherer = new StatisticsGatherer(index, iterator, required);
                gatherer.run();

                // now that we have the statistics, store them back into the node.
                Parameters p = node.getParameters();
                gatherer.store( node.getParameters() );
            }

            return node;
        }

        public void beforeNode(Node object) throws Exception {
            // do nothing
        }
    }
    
    public class IteratorBuilder implements Traversal {
        Stack<ArrayList<DocumentDataIterator>> iteratorStack = new Stack<ArrayList<DocumentDataIterator>>();
        List<Processor<Document>> transformations = index.transformations();
        ScoreIterator iterator;

        public IteratorBuilder() {
            this.iteratorStack = new Stack<ArrayList<DocumentDataIterator>>();
            iteratorStack.add(new ArrayList<DocumentDataIterator>());
        }

        public ScoreIterator getScoreIterator() {
            assert iteratorStack.size() == 1;
            assert iteratorStack.peek().size() == 1;
            assert (iteratorStack.peek().get(0) instanceof ScoreIterator);

            return (ScoreIterator) iteratorStack.peek().get(0);
        }

        public CountIterator getCountIterator() {
            assert iteratorStack.size() == 1;
            assert iteratorStack.peek().size() == 1;
            assert (iteratorStack.peek().get(0) instanceof CountIterator);

            return (CountIterator) iteratorStack.peek().get(0);
        }

        public void beforeNode(Node node) {
            iteratorStack.add(new ArrayList<DocumentDataIterator>());
        }
        
        public String transform( String termText, boolean doTransform ) throws IOException {
            if( !doTransform )
                return termText;
            
            Document d = new Document();
            ArrayList<String> list = new ArrayList<String>();
            list.add(termText);
            d.terms = list;
            
            for( Processor<Document> transformation : this.transformations ) {
                transformation.process(d);
            }
            
            return d.terms.get(0);
        }

        @SuppressWarnings("unchecked")
        public Node afterNode(Node node, ArrayList<Node> copiedChildren) throws Exception {
            String operator = node.getOperator();

            ArrayList childIterators = iteratorStack.pop();
            ArrayList parentIterators = iteratorStack.peek();
            boolean doTransform = node.getParameters().get( "transform", true );

            if (operator.equals("counts")) {
                String text = transform( node.getDefaultParameter(), doTransform );
                CountIterator iter = index.getTermCounts( text );
                if( iter == null )
                    iter = new NullExtentIterator();
                parentIterators.add(iter);
            } else if (operator.equals("positions")) {
                String text = transform( node.getDefaultParameter(), doTransform );
                ExtentIterator iter = index.getTermExtents( text );
                if( iter == null )
                    iter = new NullExtentIterator();
                parentIterators.add(iter);
            } else if (operator.equals("extents")) {
                String text = node.getDefaultParameter();
                ExtentIterator iter = index.getFieldExtents( text );
                if( iter == null )
                    iter = new NullExtentIterator();
                parentIterators.add(iter);
            } else {
                DocumentDataIterator iter = featureFactory.getObject(node, childIterators);
                parentIterators.add(iter);
            } 
           
            return node;
        }
    }

    public ScoredDocument[] runQuery(Node queryTree, int requested) throws Exception {
        // collect statistics
        StructuredQuery.walk( new StatisticsTraversal( index ), queryTree );

        // construct the query iterators
        IteratorBuilder builder = new IteratorBuilder();
        StructuredQuery.walk( builder, queryTree );
        
        // now there should be an iterator at the root of this tree
        ScoreIterator iterator = builder.getScoreIterator();
        PriorityQueue<ScoredDocument> queue = new PriorityQueue<ScoredDocument>();

        while (!iterator.isDone()) {
            int document = iterator.nextCandidate();
            int length = index.getLength(document);
            double score = iterator.score(document, length);

            if (queue.size() <= requested || queue.peek().score < score) {
                ScoredDocument scoredDocument = new ScoredDocument(document, score);
                queue.add(scoredDocument);

                if (queue.size() > requested) {
                    queue.poll();
                }
            }

            iterator.movePast(document);
        }

        return getArrayResults(queue);
    }

    public String getDocument(int document) {
        return index.getDocument(document);
    }

    public void close() throws IOException {
        index.close();
    }

    public static void main(String[] args) throws Exception {
        // read in parameters
        Parameters parameters = new Parameters(args);
        List<Parameters.Value> queries = parameters.list("query");

        // open index
        StructuredRetrieval retrieval = new StructuredRetrieval(parameters.get("index"));

        // record results requested
        int requested = (int) parameters.get( "count", 1000 );

        // for each query, run it, get the results, look up the docnos, print in TREC format
        for (Parameters.Value query : queries) {
            String queryText = query.get("text");
            Node queryTree = StructuredQuery.parse(queryText);

            ScoredDocument[] results = retrieval.runQuery(queryTree, requested);

            for (int i = 0; i < results.length; i++) {
                String document = retrieval.getDocument(results[i].document);
                double score = results[i].score;
                int rank = i + 1;

                System.out.format("%s Q0 %s %s %10.8f galago\n", query.get("number"), document, rank, score);
            }
        }
    }
}
