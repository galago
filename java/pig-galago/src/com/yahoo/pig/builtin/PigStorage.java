/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.PigServer;
import com.yahoo.pig.data.TimestampedTuple;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.io.InputStreamPosition;

/**
 * A load function that parses a line of input into fields using a delimiter to set the fields. The
 * delimiter is given as a regular expression. See String.split(delimiter) and
 * http://java.sun.com/j2se/1.5.0/docs/api/java/util/regex/Pattern.html for more information.
 */
public class PigStorage extends StorageFunc {
    Iterator<Tuple>     i              = null;
    BufferedReader      fr             = null;
    InputStreamPosition posInputStream = null;
    InputStream         fsis           = null;
    long                start          = Long.MIN_VALUE;
    long                end            = Long.MAX_VALUE;
	private String recordDel = "\n";
	private String fieldDel = "\t";

	boolean toTimestamp = false;
    int timestampColumn = -1;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy:HH:mm:ss Z");	
    //e.g., 26/Jan/2006:09:43:37 -0700, as found in apache web logs
    
    public PigStorage() {
    }

    /**
     * Constructs a Pig loader that uses specified regex as a field delimiter.
     * 
     * @param delimiter
     *            the regular expression that is used to separate fields. ("\t" is the default.) See
     *            http://java.sun.com/j2se/1.5.0/docs/api/java/util/regex/Pattern.html for complete
     *            explanation.
     */
    public PigStorage(String delimiter) {
        this.fieldDel = delimiter;
    }

    public PigStorage(String fieldDel, String recordDel) {
    	this.fieldDel = fieldDel;
    	this.recordDel = recordDel;
	}

    public PigStorage(String delimiter, String recordDel, String timestampColumn){
    	this.fieldDel = delimiter;
    	this.recordDel = recordDel;
    	toTimestamp = true;
    	try{
    		this.timestampColumn = Integer.parseInt(timestampColumn);
    	}catch(NumberFormatException e){
    		new RuntimeException("Timestamp column should be a valid number, using 0").printStackTrace();
    		this.timestampColumn = 0;
    	}
    }
    
    public PigStorage(String delimiter, String recordDel, String timestampColumn, String dateFormat){
    	this.fieldDel = delimiter;
    	this.recordDel = recordDel;
    	toTimestamp = true;
    	try{
    		this.timestampColumn = Integer.parseInt(timestampColumn);
    	}catch(NumberFormatException e){
    		new RuntimeException("Timestamp column should be a valid number, using 0").printStackTrace();
    		this.timestampColumn = 0;
    	}
    	this.dateFormat = new SimpleDateFormat(dateFormat);
    }

	public Tuple getNext() throws IOException {
        if (fsis == null || (posInputStream != null && posInputStream.getPosition() > end)) {
            return null;
        }
        String line;
        if((line = fr.readLine()) != null) {            
        	if (!toTimestamp) {
                Tuple t = new Tuple(line, fieldDel);
        	 	return t;
            } else {
        		return new TimestampedTuple(line, fieldDel,timestampColumn,dateFormat);
            }
        }
        return null;
    }

    public void bindTo(InputStream is, long offset, long end) throws IOException {
        this.posInputStream = new InputStreamPosition(is, offset);
        this.fr = new BufferedReader(new InputStreamReader(posInputStream));
        this.start = offset;
        this.end = end;
        this.fsis = is;
       
        // Since we are not block aligned we throw away the first
        // record and cound on a different instance to read it
        if (offset != 0) {
            if(getNext() == null) {
                throw new RuntimeException("null tuple returned from skip " + this.start + " " + this.end + " " + posInputStream.getPosition() );
            }
        }
    }
    
    OutputStream os;
    @Override
    public void bindTo(OutputStream os) throws IOException {
        this.os = os;
    }

    @Override
    public void putNext(Tuple f) throws IOException {
        os.write((f.toDelimitedString(this.fieldDel) + this.recordDel).getBytes());
    }

    @Override
    public void done() throws IOException {
    }

}
