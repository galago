/*
 * DocumentOrderedBinnedRetrieval
 * 
 * 4 October 2007 -- Trevor Strohman
 *
 * BSD License (http://galagosearch.org/license)
 */

package galago.retrieval;

import galago.retrieval.query.Node;
import galago.retrieval.query.StructuredQuery;
import galago.retrieval.query.Traversal;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Base class of all the different methods of processing queries against a 
 * DocumentOrderedBinnedIndex.  Implement runQueueQuery in your subclass.
 * 
 * @author trevor
 */
public abstract class DocumentOrderedBinnedRetrieval extends Retrieval {
    DocumentOrderedBinnedIndex index;
    
    public DocumentOrderedBinnedRetrieval( DocumentOrderedBinnedIndex index ) {
        this.index = index;
    }
    
    public String getDocument( int document ) {
        return index.getDocument( document );
    }
    
    public void close() throws IOException {
        this.index.close();
        this.index = null;
    }

    public ScoredDocument[] runQuery( Node tree, boolean conjunctive, int requested ) throws Exception {
        TermsHarvester harvester = new TermsHarvester();
        StructuredQuery.walk( harvester, tree );
        ArrayList<DocumentOrderedIterator> iterators = harvester.getIterators();
        ArrayList<DocumentOrderedIterator> priors = harvester.getPriors();
        
        PriorityQueue<ScoredDocument> scores = runQueueQuery( iterators, priors, conjunctive, requested );
        return getArrayResults( scores );
    }

    public ScoredDocument[] runQuery(Node tree, int requested) throws Exception {
        return runQuery( tree, false, requested );
    }
    
    public static ScoredDocument[] getArrayResults(PriorityQueue<ScoredDocument> scores) {
        ScoredDocument[] results = new ScoredDocument[scores.size()];
        ArrayList<ScoredDocument> resultArray = new ArrayList<ScoredDocument>( scores );
        Collections.sort( resultArray );
        
        int countUp = 0;
        int countDown = resultArray.size() - 1;
        
        while( countUp < resultArray.size() ) {
            results[ countUp ] = resultArray.get( countDown );
            
            countUp++;
            countDown--;
        }
        
        return results;
    }
    
    /*
     * Note that this just looks for #counts terms, rips them
     * out of the query, and runs that.
     */
     
    class TermsHarvester implements Traversal {
        public ArrayList<DocumentOrderedIterator> iterators = new ArrayList<DocumentOrderedIterator>();
        public ArrayList<DocumentOrderedIterator> priors = new ArrayList<DocumentOrderedIterator>();
        
        public ArrayList<DocumentOrderedIterator> getIterators() {
            return iterators;
        }
        
        public void beforeNode(Node object) throws Exception {
            // do nothing
        }

        public Node afterNode(Node object, ArrayList<Node> children) throws Exception {
            if( object.getOperator().equals( "text" ) || object.getOperator().equals( "scores" ) ) {
                String indexName = object.getParameters().get( "index", "default" );
                String term = object.getDefaultParameter();
                DocumentOrderedIterator iterator = index.getTerm(term, indexName);

                if( indexName.equals( "priors" ) ) {
                    priors.add( iterator );
                } else if( iterator != null ) {
                    iterators.add( iterator );
                    Collections.sort( iterators, new IteratorOrder() );
                }
            }            
            
            return object;
        }

        private ArrayList<DocumentOrderedIterator> getPriors() {
            return priors;
        }
        
        class IteratorOrder implements Comparator<DocumentOrderedIterator> {
            public int compare( DocumentOrderedIterator one, DocumentOrderedIterator two ) {
                assert one != null;
                assert two != null;
                
                if( one.listByteLength() > two.listByteLength() )
                    return 1;
                if( one.listByteLength() < two.listByteLength() )
                    return -1;
                return 0;
            }
        }
    }
    
    abstract PriorityQueue<ScoredDocument> runQueueQuery( List<DocumentOrderedIterator> iterators,
                                                          List<DocumentOrderedIterator> priors,
                                                          boolean conjunctive, int requested ) throws Exception;
}
