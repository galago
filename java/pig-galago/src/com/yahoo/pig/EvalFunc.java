/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;

import java.io.IOException;

import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.mapreduceExec.PigMapReduce;

/**
 * The class is used to implement functions to be applied to
 * a dataset. The function is applied to each Tuple in the set.
 * The programmer should not make assumptions about state maintained
 * between invocations of the invoke() method since the Pig runtime
 * will schedule and localize invocations based on information provided
 * at runtime.
 * 
 * @author database-systems@yahoo.research
 *
 */
public abstract class EvalFunc extends InstantiatedFunc {
	
    // report that progress is being made (otherwise hadoop times out after 600 seconds working on one outer tuple)
    protected void progress() { 
        if (PigMapReduce.reporter != null) {
            PigMapReduce.reporter.progress();
        }
    }

    /**
	 * Returns the String representation of the function.
	 */
	public String toString() {
		return getName();
	}
    
    public SchemaItem outputSchema() {
        return null;
    }
}
