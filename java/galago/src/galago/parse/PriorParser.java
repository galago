/*
 * PriorParser
 *
 * May 10, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Processor;
import galago.tupleflow.Step;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentProbability;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@OutputClass(className="galago.types.DocumentProbability")
public class PriorParser implements ExNihiloSource<DocumentProbability> {
    public Processor<DocumentProbability> processor;
    BufferedReader reader;
    
    public PriorParser( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        String fileName = parameters.getXML().get( "filename" );
        reader = new BufferedReader( new FileReader( fileName ) );
    }
    
    public void setProcessor(Step processor) throws IncompatibleProcessorException {
        Linkage.link( this, processor );
    }

    public void run() throws IOException {
        String line;
        
        while( (line = reader.readLine()) != null ) {
            String[] fields = line.split( " " );
            if( fields.length != 2 )
                continue;
            String document = fields[0];
            String probability = fields[1];
            
            processor.process( new DocumentProbability( document, Double.parseDouble(probability) ) );
        }
        
        processor.close();
    }
    
    public Class<DocumentProbability> getOutputClass() {
        return DocumentProbability.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !Verification.requireParameters( new String[] { "filename" }, parameters.getXML(), handler) )
            return;
        
        String filename = parameters.getXML().get("filename");
        if( !new File(filename).isFile() ) {
            handler.addError( "File " + filename + " does not exist." );
        }
    }
}
