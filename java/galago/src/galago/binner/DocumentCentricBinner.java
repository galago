/*
 * DocumentCentricBinner
 *
 * December 11, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.binner;

public class DocumentCentricBinner {
    public static int[] computeBreakpoints( int bins, int documentTermCount ) {
        double b = Math.pow(1 + documentTermCount, 1.0/bins);
        int[] breakpoints = new int[bins+1];
        double total = 0;
        for( int bin=bins; bin >= 1; bin-- ) {
            double count = (b-1) * Math.pow(b, bins-bin);
            total += count;
            breakpoints[bin] = documentTermCount - (int)total;
        }
        breakpoints[1] = 0;
        breakpoints[0] = 0;
        
        return breakpoints;
    }
}
