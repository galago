
//
// BinOrderedBinnedRetrieval
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
 
#include "BinOrderedBinnedRetrieval.hpp"
#include "Logging.hpp"
#include <queue>
#include <string>
#include "Query.hpp"
#include "LongAccumulator.hpp"
#include "WideAccumulator.hpp"
#include "ArrayAccumulators.hpp"
#include <math.h>
#include <algorithm>

//
// openRead
//

void BinOrderedBinnedRetrieval::openRead( const std::string& indexPath ) {
    _index.openRead( indexPath );
}                               

//
// close
//      

void BinOrderedBinnedRetrieval::close() {
    _index.close();
}

//
// _fetchIterators
//

void BinOrderedBinnedRetrieval::_fetchIterators( std::vector<ScoredIterator*>& iterators, const std::vector<QueryTerm>& queryTerms ) {
    for( int i=0; i<queryTerms.size(); i++ ) {
        const QueryTerm& term = queryTerms[i];
        BinOrderedBinnedIterator* iterator = _index.getIterator( term.field, term.text );
        ScoredIterator* scoredIterator = new ScoredIterator( iterator );
        
        if( iterator != 0 && iterator->next() ) {
            iterators.push_back(scoredIterator); 
        }
    }
}


//
// _processExcessTerms
//

void BinOrderedBinnedRetrieval::_processExcessTerms( std::vector<ScoredIterator*>& iterators, Accumulators& accumulators ) {
    if( accumulators.maxTerms() < iterators.size() ) {
        std::sort( iterators.begin(), iterators.end(), ScoredIterator::order() );

        // process the terms that have the highest scores (and the shortest inverted lists)
        for( int i = accumulators.maxTerms(); i < iterators.size(); i++ ) {
            ScoredIterator* iter = iterators[i];

            do {
                int score = iter->iterator->bin() * iter->weight; 
                accumulators.processBin( score, iter->iterator );
            } while( iter->iterator->next() );

            delete iter;
        };

        // remove all the terms we've already processed
        iterators.resize( accumulators.maxTerms() );
    }
}

//
// _loadQueue
//

void BinOrderedBinnedRetrieval::_loadQueue( siter_queue& queue, std::vector<ScoredIterator*>& iterators, Accumulators& accumulators ) {
    // set up the bin weights, maxScore for each iterator that remains
    for( int i=0; i<iterators.size(); i++ ) {
        ScoredIterator* iter = iterators[i];

        UINT32 maxScore = iter->iterator->bin() * iter->weight;
        iter->maxScore = maxScore;
        iter->index = i;
        accumulators.setMaxUnseen( i, maxScore );
        queue.push( iter );
    }
}

//
// runQuery
//     

std::vector<indri::api::ScoredExtentResult>
    BinOrderedBinnedRetrieval::runQuery( const std::vector<QueryTerm>& inputTerms,
                                         int requested,
                                         int threshold ) {
    std::vector<ScoredIterator*> iterators;
    siter_queue queue;
    int maximumAccumulators = 0;

    // stem/stop query terms
    std::vector<QueryTerm> queryTerms = _index.processTerms( inputTerms );

    // fetch iterators for the query terms
    _fetchIterators( iterators, queryTerms );
    galago_log_query_terms( iterators.size() );

    // if we're using Anh/Moffat impact indexes, we need to calculate
    // extra weights at query time.
    if( _index.requiresWeighting() )
        _calculateQueryWeights( _index, iterators, _index.bins() );
    _assignMaxScore( iterators );

    // buid an accumulator table that's appropriate for the query length
    Accumulators* accumulators = 0;
    if( iterators.size() <= LongAccumulator::maxTerms() ) {
        accumulators = new ArrayAccumulators<LongAccumulator>( iterators.size() );
    } else {
        accumulators = new ArrayAccumulators<WideAccumulator>( iterators.size() );
    }

    // the standard accumulators table can only handle a few terms, so we have
    // to process any excess terms first
    _processExcessTerms( iterators, *accumulators );
    _loadQueue( queue, iterators, *accumulators );
    
    if( threshold >= 0 ) {
        accumulators->setThresholdScore( threshold );
    }
    
    int maxEstimate = 0;
	
    // for all remaining terms, we enter the optimized loop
    while( queue.size() > 0 ) {
        ScoredIterator* iter = queue.top();
        queue.pop();
		
        // if we can quit now, do it
	if( accumulators->canIgnoreFuturePostings( requested ) ) {
            delete iter;
            
            while( queue.size() > 0 ) {
                delete queue.top();
                queue.pop();
            }
            
            break;
	}
        
        int score = iter->iterator->bin() * iter->weight;
        accumulators->processBin( score, iter->index, iter->iterator );
        bool hasMore = iter->iterator->next();

        if( hasMore ) {
	    _assignMaxScore( iter );
            accumulators->setMaxUnseen( iter->index, iter->maxScore );
            queue.push(iter);
        } else {
            accumulators->setMaxUnseen( iter->index, 0 );
            delete iter;
        }

        accumulators->calculateThresholdScore( requested );
        maximumAccumulators = std::max( maximumAccumulators, accumulators->size() );
        accumulators->trim();
    }
    
    std::vector<indri::api::ScoredExtentResult> results = accumulators->topResults( requested );
    delete accumulators;
    return results;
}                                                      
           
//
// getDocument
//
                                     
std::string BinOrderedBinnedRetrieval::getDocument( int doc ) {
    return _index.getDocument(doc);
}                                

//
// _calculateQueryWeights
//
    
void BinOrderedBinnedRetrieval::_calculateQueryWeights( const BinnedIndex& index, std::vector<ScoredIterator*>& scored, int binCount ) {
    // compute the query weights; totally ad hoc approximation here
    
    // 1. determine the maximum count
    UINT64 maxCount = index.maximumDocumentCount();

    // 2. compute these log value things
    indri::utility::greedy_vector<double> logWeights;
    double maxWeight = 0;
    for( int i=0; i<scored.size(); i++ ) {
        double logWeight = log( 1 + (double)maxCount / (double)scored[i]->iterator->documentCount() );
        logWeights.push_back( logWeight );
        maxWeight = std::max( maxWeight, logWeight );
    }
    
    // 3. bin them to the bin levels
    for( int i=0; i<scored.size(); i++ ) {
        double logWeight = logWeights[i];
        int binned = (int) (binCount * logWeight / maxWeight);
        scored[i]->weight *= std::max( 1, binned );
    }
}   

//
// _assignMaxScore
//

void BinOrderedBinnedRetrieval::_assignMaxScore( std::vector<ScoredIterator*>& scored ) {
    for( int i=0; i<scored.size(); i++ ) {
        _assignMaxScore( scored[i] );
    }
}

//
// _assignMaxScore 
//

void BinOrderedBinnedRetrieval::_assignMaxScore( ScoredIterator* scored ) {
    scored->maxScore = scored->weight * scored->iterator->bin();
}
