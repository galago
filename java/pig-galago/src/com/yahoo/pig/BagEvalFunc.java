/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;

import java.io.IOException;

import com.yahoo.pig.data.*;

/**
 * The class is used to implement functions to be applied to
 * a dataset. The function is applied to each Tuple in the set.
 * The programmer should not make assumptions about state maintained
 * between invocations of the invoke() method since the Pig runtime
 * will schedule and localize invocations based on information provided
 * at runtime.
 * 
 * @author database-systems@yahoo.research
 *
 */
public abstract class BagEvalFunc extends EvalFunc {
    
    /**
     * This callback method must be implemented by all subclasses. This
     * is the method that will be invoked on every Tuple of a given dataset.
     * Since the dataset may be divided up in a variety of ways the programmer
     * should not make assumptions about state that is maintained between
     * invocations of this method.
     * 
     * @param input the Tuple to be processed.
     * @throws IOException
     */
    abstract public void exec(Tuple input, DataCollector output) throws IOException;

}
