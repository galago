//
// XMLMinimum.java
//
// Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.tupleflow;

import galago.tupleflow.execution.Verified;
import galago.types.XMLFragment;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.XMLFragment")
@OutputClass(className="galago.types.XMLFragment", order={"+nodePath"})
@Verified
public class XMLMinimum extends StandardStep<XMLFragment, XMLFragment> {
    double minimum = 0;
    String key = "";
    
    public void process( XMLFragment fragment ) {
        if( key == null ) 
            key = fragment.nodePath;
        minimum = Math.min( minimum, Double.parseDouble(fragment.innerText) );
        System.err.println( "XMLMinimum: " + minimum );
    }
    
    public void close() throws IOException {
        processor.process( new XMLFragment(key, Double.toString(minimum)) );
        super.close();
    }
}
