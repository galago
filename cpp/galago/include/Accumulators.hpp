
//
// Accumulators
//
// 23 February 2007 -- tds
// 
// BSD License (http://galagosearch.org/license)
//
// This interface abstracts the accumulator collection so we
// can use different implementations.
//

#ifndef GALAGO_ACCUMULATORS_HPP
#define GALAGO_ACCUMULATORS_HPP

class Accumulators {
public:
    virtual ~Accumulators() {};
    
    virtual bool canIgnoreFuturePostings( int requested ) = 0;
    virtual int size() = 0;
    virtual std::vector<indri::api::ScoredExtentResult> topResults( int requested ) = 0;
    virtual int totalUnseen() = 0;
    virtual void processBin( int score, BinOrderedBinnedIterator* iterator ) = 0;
    virtual void processBin( int score, int termIndex, BinOrderedBinnedIterator* iterator ) = 0;
    virtual void setMaxUnseen( int termIndex, int score ) = 0; 
    virtual void trim() = 0;
    virtual void calculateThresholdScore( int requested ) = 0;
    virtual void setThresholdScore( int threshold ) = 0;
    virtual int maxTerms() const = 0;
    virtual int threshold() const = 0;
};

#endif // GALAGO_ACCUMULATORS_HPP
