
//
// InvertedList.java
//
// 14 March 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.index;

import java.io.DataOutputStream;

/**
 *
 * @author trevor
 */
public interface InvertedList {
    public void write( final DataOutputStream stream ) throws java.io.IOException;
    public byte[] word();
    public long dataLength();
}
