/*
 * LocalByRankBinner
 *
 * December 6, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.binner;

import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.Step;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import galago.tupleflow.Processor;
import galago.tupleflow.Source;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.ErrorHandler;
import galago.types.DocumentWordBin;
import galago.types.DocumentWordBin;
import galago.types.DocumentWordCount;

public class ImpactBinner implements Processor<DocumentWordCount>, Source<DocumentWordBin> {
    public class IDF_TFComparator implements Comparator<DocumentWordCount> {      
        public int compare( DocumentWordCount one, DocumentWordCount two ) {
            boolean oneStopword = stoplist.contains( one.word );
            boolean twoStopword = stoplist.contains( two.word );
            
            if( oneStopword && !twoStopword )
                return -1;
            
            if( twoStopword && !oneStopword )
                return 1;
            
            long oneDF = documentFrequency.get(one.word);
            long twoDF = documentFrequency.get(two.word);

            if( oneDF != twoDF )
                Long.signum( twoDF - oneDF );
            
            return Long.signum( one.count - two.count );
        }
    }
    
    public class TF_IDFComparator implements Comparator<DocumentWordCount> {      
        public int compare( DocumentWordCount one, DocumentWordCount two ) {
            boolean oneStopword = stoplist.contains( one.word );
            boolean twoStopword = stoplist.contains( two.word );
            
            if( oneStopword && !twoStopword )
                return -1;
            
            if( twoStopword && !oneStopword )
                return 1;
            
            if( one.count != two.count ) {
                return Long.signum( one.count - two.count );
            }

            long oneDF = documentFrequency.get(one.word);
            long twoDF = documentFrequency.get(two.word);

            return Long.signum( twoDF - oneDF );
        }
    }
    
    public class TF_Comparator implements Comparator<DocumentWordCount> {
        public int compare( DocumentWordCount one, DocumentWordCount two ) {
            boolean oneStopword = stoplist.contains( one.word );
            boolean twoStopword = stoplist.contains( two.word );
            
            if( oneStopword && !twoStopword )
                return -1;
            
            if( twoStopword && !oneStopword )
                return 1;
            
            return Long.signum( one.count - two.count );
        }        
    }
    
    HashMap<String, Long> documentFrequency = new HashMap<String, Long>();
    private long maximumDocumentFrequency;
    private ArrayList<DocumentWordCount> document = new ArrayList<DocumentWordCount>();
    private Processor<DocumentWordBin> next;
    private final Comparator<DocumentWordCount> comparator;
    private final int bins;
    private final int queryBins;
    private final HashSet<String> stoplist;
    private final String binningStrategy;
    
    public ImpactBinner( TupleFlowParameters parameters ) throws IOException {
        this.bins = (int) parameters.getXML().get( "bins", 64 );
        this.stoplist = new HashSet<String>( parameters.getXML().stringList( "stoplist" ) );
        this.queryBins = (int) parameters.getXML().get( "querybins", 0 );
       
        DocumentFrequencyReader reader = DocumentFrequencyReader.read( parameters );
        this.documentFrequency = reader.getDF();
        this.maximumDocumentFrequency = reader.maximumDocumentFrequency();
        
        this.binningStrategy = parameters.getXML().get( "binningStrategy", "tf_idf" );

        if( binningStrategy.equals("tf") ) {
            this.comparator = new TF_Comparator();
        } else if( binningStrategy.equals("idf_tf") ) {
            this.comparator = new IDF_TFComparator();
        } else {
            this.comparator = new TF_IDFComparator();
        }
    }
    
    public void setProcessor( Step processor ) throws IncompatibleProcessorException {
        Linkage.link( this, processor );
    }

    public int binWeight( String word ) {
        if( queryBins == 0 )
            return 1;
        
        long frequency = documentFrequency.get(word);
        double logWeight = Math.log( 1 + (double)maximumDocumentFrequency / (double)frequency );
        double maximumWeight = Math.log( 1 + (double)maximumDocumentFrequency );
        int weight = (int) (queryBins * logWeight / maximumWeight);

        return weight;
    }
    
    public void flush() throws IOException {
        Collections.sort( document, comparator );
        int[] breakpoints = DocumentCentricBinner.computeBreakpoints( bins, document.size() );

        int bin=1;
        for( int i=0; i<document.size(); i++ ) {
            while( bin < breakpoints.length-1 && breakpoints[bin+1] <= i )
                bin++;

            DocumentWordCount dwc = document.get(i);
            if( stoplist.contains(dwc.word) == true )
                continue;
            
            next.process( new DocumentWordBin( dwc.document, dwc.word, bin * binWeight(dwc.word) ) );
        }

        document = new ArrayList<DocumentWordCount>();
    }

    public void process( DocumentWordCount entry ) throws IOException {
        if( document.size() > 0 && !document.get(0).document.equals(entry.document) ) {
            flush();
        }

        document.add(entry);
    }
    
    public Class<DocumentWordCount> getInputClass() {
        return DocumentWordCount.class;
    }
    
    public Class<DocumentWordBin> getOutputClass() {
        return DocumentWordBin.class;
    }
    
    public void close() throws IOException {
        flush();
        next.close();
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        DocumentFrequencyReader.verify( parameters, handler );
    }
}
