/*
 * ExNihiloSource.java
 *
 * March 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public interface ExNihiloSource<T> extends Source<T> {
    public void run() throws IOException;
}
