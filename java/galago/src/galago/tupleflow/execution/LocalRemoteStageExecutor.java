/*
 * LocalRemoteExecutor
 *
 * September 6, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.tupleflow.execution.LocalStageExecutor.ImmediateFuture;
import java.util.ArrayList;
import java.util.concurrent.Future;

/**
 * This executor has no practical use at all.  It's only here to make it easy
 * to test the RemoteExecutor base class.
 *
 * This executor is essentially the same as the LocalStageExecutor in that
 * it runs everything in a single thread, but it serializes the job information
 * and then reads it again.
 *
 * @author trevor
 */
public class LocalRemoteStageExecutor extends RemoteStageExecutor {
    public Future<Object> submit(String stageName, ArrayList<String> jobPaths, String temporary) {
        ImmediateFuture future = null;
        
        for( String jobPath : jobPaths ) {
            future = new LocalStageExecutor().execute(jobPath);
            
            if( future.exception != null )
                return future;
        }
        
        return future;
    }

    public void shutdown() {
    }
}
