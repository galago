/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.EvalItem;

public class RegexpCond extends Cond {
    EvalItem left;
    String re;
    
    public RegexpCond(EvalItem left, String re) {
        this.left = left;
        this.re = re;
    }

    @Override
    public List<String> getFuncs() {
        return new ArrayList<String>();
    }

    @Override
    public boolean eval(Tuple input) throws IOException {

        return left.stringVal(input).matches(re);
    }
    
    public String toString() {
        return "(" + left + " MATCHES " + "'" + re + "')";
    }
}
