

//
// BinnedIndex
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#include "BinnedIndex.hpp"
#include "indri/Path.hpp"
#include "indri/DirectoryIterator.hpp"
#include "Query.hpp"

//
// BinnedIndex destructor
//

BinnedIndex::~BinnedIndex() {
    close();
}

//
// openRead
// 

void BinnedIndex::openRead( const std::string& pathname ) {
    std::string documentPath = indri::file::Path::combine( pathname, "documentNames" );
    std::string parametersPath = indri::file::Path::combine( pathname, "manifest" );
    
    _documents.read( documentPath );
    _parameters.loadFile( parametersPath );
    
    indri::file::DirectoryIterator iterator( pathname, false );
    
    for( ; !(iterator == iterator.end()); iterator++ ) {
        std::string name = *iterator;
        std::string featureIndexPath = indri::file::Path::combine( pathname, name );

        if( indri::file::Path::isDirectory( featureIndexPath ) ) {
            BinnedFeatureIndex* featureIndex = new BinnedFeatureIndex();

            featureIndex->openRead( featureIndexPath );
            _features[name] = featureIndex;
        }
    }
	
    _requiresWeighting = _features["default"]->requiresWeighting();
}

//
// close
//

void BinnedIndex::close() {
    std::map<std::string, BinnedFeatureIndex*>::iterator iter;
    
    for( iter = _features.begin(); iter != _features.end(); iter++ ) {
        iter->second->close();
        delete iter->second;
    }
    _features.clear();
}

//
// bins
//

int BinnedIndex::bins() {
    return _parameters.get("bins", 8);
}

//
// getFeatureNames
//

std::vector<std::string> BinnedIndex::getFeatureNames() const {
    std::map<std::string, BinnedFeatureIndex*>::const_iterator iter;
    std::vector<std::string> featureNames;
    
    for( iter = _features.begin(); iter != _features.end(); iter++ ) {
        featureNames.push_back( iter->first );
    }
	
    return featureNames;
}

//
// _getIndex
//

BinnedFeatureIndex* BinnedIndex::_getIndex( const std::string& feature ) const {
    std::map<std::string, BinnedFeatureIndex*>::const_iterator iter;
    
    if( feature.length() == 0 ) {
        iter = _features.find("default");
    } else {
        iter = _features.find(feature);
    }    
    
    if( iter != _features.end() ) {
        return iter->second;
    } else {
        return 0;
    }
}

//
// processTerms
//

std::vector<QueryTerm> BinnedIndex::processTerms( const std::vector<QueryTerm>& terms ) const {
    std::vector<QueryTerm> results;

    for( int i=0; i<terms.size(); i++ ) {
        BinnedFeatureIndex* index = _getIndex( terms[i].field );

        if( !index )
            continue;

        QueryTerm result = index->processTerm( terms[i] );

        if( result.text.length() > 0 && result.weight > 0 )
            results.push_back( result );
    }

    return results;
}

//
// getIterator
//

BinOrderedBinnedIterator* BinnedIndex::getIterator( const std::string& feature, const std::string& term ) const {
    BinnedFeatureIndex* index = _getIndex( feature );
    
    if( !index )
        return 0;
        
    return index->getTerm(term);
}

//
// getDocument
//

std::string BinnedIndex::getDocument( int index ) const {
    return _documents.get(index);
}

//
// maximumDocumentCount
//

UINT64 BinnedIndex::maximumDocumentCount() const {
    return _getIndex( "default" )->maximumDocumentCount();
}

//
// requiresWeighting
//

bool BinnedIndex::requiresWeighting() const {
    return _requiresWeighting;
}


