/*
 * MakeReadSplits
 *
 * September 7, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.OutputClass;
import galago.tupleflow.Processor;
import galago.tupleflow.Step;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.Verified;
import java.io.File;
import java.util.List;
import java.io.IOException;

/**
 *
 * @author trevor
 */
@Verified
@OutputClass(className = "com.yahoo.pig.impl.galago.Tuple")
public class MakeReadSplits implements ExNihiloSource<Tuple> {

    public Processor<Tuple> processor;
    public List<String> filenames;
    public long increment;

    /** Creates a new instance of MakeReadSplits */
    public MakeReadSplits(TupleFlowParameters parameters) {
        filenames = parameters.getXML().stringList("filename");
        increment = parameters.getXML().get("splitLength", 10 * 1024 * 1024);
    }

    public void run() throws IOException {
        for (String filename : filenames) {
            if (filename.endsWith(".gz")) {
                // compressed files can't be split
                Tuple t = new Tuple(4);
                t.setField(0, filename + "-" + 0 + "-" + Long.MAX_VALUE);
                t.setField(1, filename);
                t.setField(2, 0);
                t.setField(3, Long.MAX_VALUE);
            } else {
                long fileLength = new File(filename).length();

                for (long start = 0; start < fileLength; start += increment) {
                    long end = Math.min(fileLength, start + increment);

                    Tuple t = new Tuple(4);
                    t.setField(0, filename + "-" + start + "-" + end);
                    t.setField(1, filename);
                    t.setField(2, start);
                    t.setField(3, end);

                    processor.process(t);
                }
            }
        }

        processor.close();
    }

    public void setProcessor(Step next) throws IncompatibleProcessorException {
        Linkage.link(this, next);
    }
}
