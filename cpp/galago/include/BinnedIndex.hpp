
//
// BinnedIndex
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#ifndef GALAGO_BINNEDINDEX_HPP
#define GALAGO_BINNEDINDEX_HPP

#include "DocumentNameReader.hpp"
#include "indri/Parameters.hpp"
#include <map>
#include "BinnedFeatureIndex.hpp"
#include <vector>
#include <string>
#include "Query.hpp"

class BinnedIndex {
private:
    DocumentNameReader _documents;
    indri::api::Parameters _parameters;
    std::map<std::string, BinnedFeatureIndex*> _features;
    int _bins;
    bool _requiresWeighting;

    BinnedFeatureIndex* _getIndex( const std::string& feature ) const;
    
public:
    ~BinnedIndex();                              
    void openRead( const std::string& pathname );
    
    void close();
    int bins();
    std::vector<std::string> getFeatureNames() const;
    BinOrderedBinnedIterator* getIterator( const std::string& feature, const std::string& term ) const;
    std::string getDocument( int index ) const;
    std::vector<QueryTerm> processTerms( const std::vector<QueryTerm>& terms ) const;

    bool requiresWeighting() const;
    UINT64 maximumDocumentCount() const;
};

#endif // GALAGO_BINNEDINDEX_HPP
