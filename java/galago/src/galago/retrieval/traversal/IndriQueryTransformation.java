/*
 * IndriQueryTraversal
 * 
 * September 20, 2007 -- Trevor Strohman
 * 
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.traversal;

import galago.retrieval.query.*;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class IndriQueryTransformation implements Traversal {
    public void beforeNode(Node object) throws Exception {
    }

    /*
     * What needs to be solved?
     *
     * term.field
     * term.field1.field2
     * #1(term1 term2).field
     * #1(term1 term2).field1.field2
     * #combine( term )
     *
     * Need to be able to automatically add default #feature operators
     * where they should go.
     */
    
    public Node afterNode(Node object, ArrayList<Node> children) throws Exception {
        for( Node child : children ) {
            if( child.getOperator().equals( "text" ) ) {
                String text = object.getDefaultParameter();
            }
        }
        
        if( object.getOperator().equals( "text" ) ) {
            String text = object.getDefaultParameter();
            String[] fields = text.split( "\\." );
            
            Node term = new Node( "positions", fields[0] );
            Node complete = term;

            for( int i=1; i<fields.length; i++ ) {
                ArrayList<Node> kids = new ArrayList<Node>();
                Node extentList = new Node( "extents", fields[i] );
                kids.add( extentList );
                kids.add( complete );
                complete = new Node( "inextent", kids );
            }
            
            return complete;
        }
        
        return object;
    }
}
