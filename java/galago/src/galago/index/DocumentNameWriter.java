//
// DocumentNameWriter.java
//
// 28 November 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.index;

import galago.tupleflow.InputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Processor;
import galago.tupleflow.execution.ErrorHandler;
import galago.types.DocumentIdentifierNumber;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.io.IOException;

/**
 * Writes a list of document names to a binary file.
 * This class assumes that a document name is a string that contains at least
 * one hyphen ('-') followed entirely by numbers.  All TREC document names
 * follow this convention, e.g.:  WTX-B01-0001.
 *
 * @author Trevor Strohman
 */

@InputClass(className="galago.types.DocumentIdentifierNumber")
public class DocumentNameWriter implements Processor<DocumentIdentifierNumber> {
    String lastHeader = null;
    DataOutputStream output;
    int lastFooterWidth = 0;
    int lastDocument = -1;
    ArrayList<Integer> footers;
    
    /** Creates a new instance of DocumentNameWriter */
    public DocumentNameWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        String filename = parameters.getXML().get( "filename" );
        footers = new ArrayList<Integer>();
        output = new DataOutputStream( new BufferedOutputStream( new FileOutputStream( filename ) ) );
    }
    
    public void flush() throws IOException {
        if (footers.size() == 0)
            return;
        
        output.writeInt( lastHeader.getBytes().length );
        output.write( lastHeader.getBytes() );
        output.writeInt( lastFooterWidth );
        output.writeInt( footers.size() );
        
        for( int footerValue : footers ) {
            output.writeInt( footerValue );
        }
    }
    
    public void process( DocumentIdentifierNumber documentNameNumber ) throws IOException {
        assert documentNameNumber.document - 1 == lastDocument;
        lastDocument = documentNameNumber.document;
        
        String documentName = documentNameNumber.identifier;
        int lastDash = documentName.lastIndexOf( "-" );
        
        if( lastDash == -1 ) {
            putName( documentName, 0, 0 );
        } else {
            String header = documentName.substring( 0, lastDash );
            String footer = documentName.substring( lastDash+1 );
            
            try {
                int footerValue = Integer.parseInt(footer);
                putName( header, footerValue, footer.length() );
            } catch( NumberFormatException e ) {
                putName( documentName, 0, 0 );
            }
        }
    }
    
    public void putName( String header, int footer, int footerWidth ) throws IOException {
        if( header.equals(lastHeader) && footerWidth == lastFooterWidth ) {
            footers.add( footer );
        } else {
            flush();
            lastHeader = header;
            footers = new ArrayList<Integer>();
            footers.add(footer);
            lastFooterWidth = footerWidth;
        }
    }
    
    public void close() throws IOException {
        flush();
        output.close();
    }

    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !parameters.getXML().containsKey( "filename" ) ) {
            handler.addError( "DocumentNameWriter requires an 'filename' parameter." );
            return;
        }
    }
}
