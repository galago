/*
 * DocumentLengthsWriter
 *
 * May 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.index;

import galago.tupleflow.InputClass;
import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentIdentifierNumber;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.DocumentIdentifierNumber", order={"+document"})
public class DocumentLengthsWriter implements Processor<DocumentIdentifierNumber> {
    DataOutputStream output;
    int document = 0;
    
    /** Creates a new instance of DocumentLengthsWriter */
    public DocumentLengthsWriter( TupleFlowParameters parameters ) throws FileNotFoundException {
        String filename = parameters.getXML().get( "filename" );
        output = new DataOutputStream( new BufferedOutputStream( new FileOutputStream( filename ) ) );
    }

    public void close() throws IOException {
        output.close();
    }
   
    public void process( DocumentIdentifierNumber object ) throws IOException {
        assert document <= object.document : "d: " + document + " o.d:" + object.document;
        
        while( document < object.document ) {
            output.writeInt( 0 );
            document++;
        }
        
        output.writeInt( object.length );
        document++;
    }
    
    public Class<DocumentIdentifierNumber> getInputClass() {
        return DocumentIdentifierNumber.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !parameters.getXML().containsKey( "filename" ) ) {
            handler.addError( "DocumentLengthsWriter requires an 'filename' parameter." );
            return;
        }
        
        String filename = parameters.getXML().get( "filename" );
        Verification.requireWriteableFile( filename, handler );
    }
}
