/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.List;

import com.yahoo.pig.data.Tuple;

public abstract class Cond {

    public abstract List<String> getFuncs();
    
    public abstract boolean eval(Tuple input) throws IOException;

}
