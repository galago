/*
 * MapTuples
 *
 * September 3, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.Verified;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@InputClass(className="com.yahoo.pig.impl.galago.Tuple")
@OutputClass(className="com.yahoo.pig.impl.galago.Tuple")
@Verified
public class MapTuples extends StandardStep<Tuple, Tuple> {
    GroupBySpec group;
    EvalSpecPipe evalPipeSpec;
    DataCollector collector;
    PigContext context = new PigContext( ExecType.GALAGO );
    
    private class MapCollector extends DataCollector {
        public void add(com.yahoo.pig.data.Tuple t) throws IOException {
            if( t == null )
                return;
            
            Datum[] multGroups = group.eval(t);
            
            for( int i=0; i<multGroups.length; i++ ) {
                Datum key = multGroups[i];
                Tuple result = new Tuple();
                
                result.appendField(key);
                result.appendTuple(new Tuple(t));
                processor.process(result);
            }
        }
    }
    
    public MapTuples( TupleFlowParameters parameters ) throws IOException {
        group = new GroupBySpec( context, parameters.getXML().get("group") );
        collector = new MapCollector();
    }

    public void process(Tuple object) throws IOException {
        collector.add(object);
    }
    
    public void close() throws IOException {
        collector.add(null);
        super.close();
    }
}
