
//
// Bits
//
// 13 March 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_BITS_HPP
#define GALAGO_BITS_HPP

//
// bits_nextUnsetBit
//

inline int bits_nextUnsetBit( UINT64 bitfield, int start, int end ) {
    for( int i=start; i<end; i++ ) {
        if( bitfield & (1<<i) )
            continue;
            
        return i;
    }

    return end;
}

//
// bits_nextUnsetBit
//

inline int bits_nextUnsetBit( UINT64 bitfield, int start ) {
    return bits_nextUnsetBit( bitfield, start, 64 );
} 

//
// bits_nextSetBit
//

inline int bits_nextSetBit( UINT64 bitfield, int start, int end ) {
    for( int i=start; i<end; i++ ) {
        if( !(bitfield & (1<<i)) )
            continue;
            
        return i;
    }

    return end;
}

//
// bits_nextSetBit
//

inline int bits_nextSetBit( UINT64 bitfield, int start ) {
    return bits_nextSetBit( bitfield, start, 64 );
} 

//
// bits
//




#endif // GALAGO_BITS_HPP


