/*
 * StoreTuples
 *
 * September 5, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.impl.PigContext;
import galago.tupleflow.InputClass;
import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.Verified;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author trevor
 */
@Verified
@InputClass(className="com.yahoo.pig.impl.galago.Tuple")
public class StoreTuples implements Processor<Tuple> {
    BufferedWriter writer;
    StorageFunc function;
    
    /** Creates a new instance of StoreTuples */
    public StoreTuples( TupleFlowParameters parameters ) throws IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        String filename = parameters.getXML().get( "filename" );
        String funcSpec = parameters.getXML().get( "function" );
        
        function = (StorageFunc) PigContext.instantiateArgFunc(funcSpec);
        writer = new BufferedWriter( new FileWriter( filename ) );
    }
    
    public void process( Tuple tuple ) throws IOException {
        String result = tuple.toDelimitedString( "\t" );
        writer.append( result );
        writer.append( "\n" );
    }
    
    public void close() throws IOException {
        writer.close();
    }
}
