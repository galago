/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.File;
import java.io.IOException;

public class BagFactory {

    private File              tmpdir = null;
    private static BagFactory instance = new BagFactory();

    public static BagFactory getInstance() {
        return instance;
    }

    private BagFactory() {
    }

    public static void init(File tmpdir) {
        instance.setTmpDir(tmpdir);
    }

    private void setTmpDir(File tmpdir) {
        this.tmpdir = tmpdir;
        this.tmpdir.mkdirs();
    }
    
    // Get BigBag or Bag, depending on whether the temp directory has been set up
    public DataBag getNewBag() throws IOException {
        if (tmpdir == null) return new DataBag();
        else return getNewBigBag();
    }
    
    // Need a Big Bag, dammit!
    public BigDataBag getNewBigBag() throws IOException {
        if (tmpdir == null) throw new IOException("No temp directory given for BigDataBag.");
        else return new BigDataBag(tmpdir);
    }

}
