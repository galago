/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * This is an internal class that keeps track of the specific input that a Tuple came from
 */
public class IndexedTuple extends Tuple implements WritableComparable {

	public int index = -1;
	
	public IndexedTuple() {
	}
	
	public IndexedTuple(Tuple t, int indexIn) {
		fields = t.fields;
		index = indexIn;
	}

	public String toString() {
		return super.toString() + "[" + index + "]";
	}

	// Writable methods:
	public void write(DataOutput out) throws IOException {
		super.write(out);
		encodeInt(out, index);
	}
	public void readFields(DataInput in) throws IOException {
		super.readFields(in);
		index = decodeInt(in);
	}
}
