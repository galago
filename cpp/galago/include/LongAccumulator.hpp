
//
// LongAccumulator
//
// 23 February 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_LONGACCUMULATOR_HPP
#define GALAGO_LONGACCUMULATOR_HPP

#include "AccumulatorTypes.hpp"

#define LA_DOCUMENT_BITS  32
#define LA_DOCUMENT_MASK  0x00000000ffffffffULL
#define LA_DOCUMENT_SHIFT 0

#define LA_SCORE_MASK     (0x00ffffff00000000ULL)
#define LA_SCORE_SHIFT    (32)

#define LA_TERM_MASK      0xff00000000000000ULL
#define LA_TERM_BITS      8
#define LA_TERM_SIZE      256
#define LA_TERM_SHIFT     56

#define LA_TERM_MAX_SCORE    (1<<16)-1

class LongAccumulator {
private:
    UINT64 _accumulator;

public:
    typedef UINT64 update_type;
    typedef UINT64 term_type;

    LongAccumulator() :
        _accumulator(0)
    {
    }
    
    LongAccumulator( const UINT32 document, const update_type update ) {
        _accumulator = document + update;
    }
    
    bool operator< ( const LongAccumulator& other ) const {
        return score() < other.score();
    }
    
    inline UINT32 document() const {
        return (UINT32) ((_accumulator & LA_DOCUMENT_MASK) >> LA_DOCUMENT_SHIFT);
    }
    
    inline UINT32 score() const {
        return (UINT32) ((_accumulator & LA_SCORE_MASK) >> LA_SCORE_SHIFT);
    }
    
    inline UINT32 terms() const {
        return (UINT32) ((_accumulator & LA_TERM_MASK) >> LA_TERM_SHIFT);
    }
    
    inline void update( const update_type update ) {
        _accumulator += update;
    }
    
    inline UINT32 unseen( const std::vector<UINT32>& fullUnseen ) const {
        return fullUnseen[terms()];
    }

    inline bool containsTerm( const term_type termBit ) const {
        return (termBit & _accumulator) ? true : false;
    }
    
    static int maxScore() {
        return LA_TERM_MAX_SCORE;
    }
    
    static int maxTerms() {
        return LA_TERM_BITS;
    }
    
    static term_type buildTerm( const int termIndex ) {
        UINT64 shiftedBit = (UINT64(1ULL<<termIndex) << LA_TERM_SHIFT);
        return shiftedBit;
    }
    
    static term_type buildNullTerm() {
        return 0;
    }
    
    static update_type buildUpdate( const int termIndex, const UINT32 score ) {
        UINT64 shiftedBit = (UINT64(1ULL<<termIndex) << LA_TERM_SHIFT);
        UINT64 shiftedScore = ((UINT64)score) << LA_SCORE_SHIFT;
    
        return shiftedBit | shiftedScore;
    }
    
    static void computeUnseenScoreArray( std::vector<UINT32>& fullUnseen, const std::vector<UINT32>& maxUnseen ) {
        fullUnseen.resize( 256, 0 );
        
        for( UINT32 i=0; i < fullUnseen.size(); i++ ) {
            fullUnseen[i] = 0;
            
            for( UINT32 j=0; j < maxUnseen.size(); j++ ) {
                UINT32 termBit = 1<<j;
                
                if( (i & termBit) == 0 ) {
                    fullUnseen[i] += maxUnseen[j];
                }
            }
        }
    }
};

template<>
class AccumulatorTypes<LongAccumulator> {
public:
    typedef UINT64 update_type;
    typedef UINT64 term_type;
};

#endif // GALAGO_LONGACCUMULATOR_HPP


