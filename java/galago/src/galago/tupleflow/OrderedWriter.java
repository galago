
//
// OrderedWriter
//
// 2 December 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.tupleflow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.prefs.Preferences;

public abstract class OrderedWriter<T> implements Processor<T> {
    public abstract void process( T object ) throws IOException;
}
