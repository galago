/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

/**
 * A marker class for a basic data unit.
 */
public interface Datum extends Comparable {
    
	public boolean equals(Datum other);
	     
}
