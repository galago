/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.func;

import java.io.IOException;

import com.yahoo.pig.BagEvalFunc;
import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalItemList;

public class BagFuncEvalItem extends FuncEvalItem {

    public BagFuncEvalItem(PigContext pigContext, BagEvalFunc func, EvalItemList args) {
        super(pigContext, func, args);
    }

    @Override
    public void eval(Tuple input, DataCollector output) throws IOException {
        
        Datum amendKey = null;
        if (input instanceof AmendableTuple) amendKey = ((AmendableTuple) input).getAmendKey();

        output = setupBagProcessingPipeline(amendKey, output);
        
        ((BagEvalFunc) func).exec(args.simpleEval(input), output);
        if (!output.isStale())
        	output.add(null);  // EOF
    }

    @Override
    public DataBag simpleEval(Tuple input) throws IOException {
        DataBag d = BagFactory.getInstance().getNewBag();
        
        DataCollector emitTo = d;
        
        if (nestedEval != null) emitTo = nestedEval.collector(d);
        
        ((BagEvalFunc) func).exec(args.simpleEval(input), emitTo);
        
        return d;
    }
    
    @Override
    public String stringVal(Tuple tup) throws IOException {
        throw new IOException("Internal error: illegal to call stringVal() on FuncEvalItem");
    }
    
    @Override
    public Double doubleVal(Tuple tup) throws IOException {
        throw new IOException("Internal error: illegal to call doubleVal() on FuncEvalItem");
    }
    
}
