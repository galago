/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.IOException;


/**
 * This interface is used to represent tuple collectors.
 * It may be an in memory DataBag, an input to another function,
 * or a file.
 */
public abstract class DataCollector {
	Integer staleCount = 0;
	
	DataCollector output = null;
	
	public DataCollector(){}
	
	public DataCollector(DataCollector output){
		this.output = output;
	}
	
	/**
     * Add a tuple to the collector.
	 */
	public abstract void add(Tuple t) throws IOException;

	public void markStale(boolean stale){
		synchronized(staleCount){
			if (stale){
				staleCount++;
			}else{
				if (staleCount > 0){
					synchronized(this){
						staleCount--;
						notifyAll();
					}
				}
			}
		}
		if (output!=null)
			output.markStale(stale);
	}
	
	public void setOutput(DataCollector output){
		this.output = output;
	}
	
	public boolean isStale(){
		synchronized(staleCount){
			return staleCount>0;
		}
	}
	
}

