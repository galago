/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import com.yahoo.pig.GroupFunc;
import com.yahoo.pig.data.*;


/**
 * built-in grouping function; permits system to choose any grouping.
 */
public class GFAny extends GroupFunc {

	public Datum[] exec(Tuple input) {
		return new DataAtom[]{new DataAtom("any")};  // TODO: for now, group everything together; change this later
	}
}
