/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;

/**
 * With function aliases, functions can be instantiated with different
 * names. This class keeps track of that.
 */
public abstract class InstantiatedFunc {
	/**
	 * The name that was used to instantiate this function.
	 */
	protected String name = this.getClass().getName();
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
}
