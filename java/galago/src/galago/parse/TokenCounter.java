/*
 * TokenCounter
 *
 * December 28, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import galago.tupleflow.Processor;
import galago.types.DocumentWordCount;
import galago.types.WordCount;

/**
 *
 * @author trevor
 */
public class TokenCounter {
    public Processor<DocumentWordCount> postings;
    public Processor<WordCount> counts;
    
    /** Creates a new instance of TokenCounter */
    public TokenCounter( Processor<DocumentWordCount> postings, Processor<WordCount> counts ) {
        this.counts = counts;
        this.postings = postings;
    }
    
    public void process( String id, ArrayList<String> tokens ) throws IOException {
        Collections.sort(tokens);

        String lastToken = null;
        int tokenCount = 0;

        // since the tokens are in sorted order, we can now count them easily
        for( String token : tokens ) {
            if (lastToken != null && lastToken.equals(token)) {
                tokenCount++;
            } else {
                if (lastToken != null) {
                    postings.process( new DocumentWordCount( id, lastToken, tokenCount ) );
                    counts.process( new WordCount( lastToken, tokenCount, 1 ) );
                }

                lastToken = token;
                tokenCount = 1;
            }
        }

        // make sure to output the last token
        if( lastToken != null ) {
            postings.process( new DocumentWordCount( id, lastToken, tokenCount ) );
            counts.process( new WordCount( lastToken, tokenCount, 1 ) );
        }
    }
    
    public void close() throws IOException {
        postings.close();
        counts.close();
    }
}
