//
// VocabularyWriter.java
//
// 28 November 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.index;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.types.WordOffset;

/**
 *
 * @author trevor
 */
public class VocabularyWriter implements Processor<WordOffset> {
    String lastDocno = null;
    DataOutputStream output;
    
    /** Creates a new instance of VocabularyWriter */
    public VocabularyWriter( String filename ) throws IOException {
        output = new DataOutputStream( new BufferedOutputStream( new FileOutputStream( filename ) ) );
    }
    
    public VocabularyWriter( Parameters params ) throws IOException {
        this( params.get( "filename" ) );
    }
    
    public void process( WordOffset object ) throws IOException {
        output.writeShort(object.word.length);
        output.write(object.word);
        output.writeLong(object.offset);
    }
    
    public void close() throws IOException {
        output.close();
    }
    
    public Class<WordOffset> getInputClass() {
        return WordOffset.class;
    }
}

