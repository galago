/*
 * ReadTuples
 *
 * September 3, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.io.FileLocalizer;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.ErrorHandler;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author trevor
 */

@InputClass(className = "com.yahoo.pig.impl.galago.Tuple")
@OutputClass(className = "com.yahoo.pig.impl.galago.Tuple")
public class ReadTuples extends StandardStep<Tuple, Tuple> {

    String funcSpec;

    /** Creates a new instance of ReadTuples */
    public ReadTuples(TupleFlowParameters parameters) throws IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        funcSpec = parameters.getXML().get("function");
    }

    public void process(Tuple t) throws IOException {
        String filename = ((DataAtom) t.getField(1)).strval();
        long offset = ((DataAtom) t.getField(2)).numval().longValue();
        long end = ((DataAtom) t.getField(3)).numval().longValue();
        StorageFunc function = null;

        try {
            function = (StorageFunc) PigContext.instantiateArgFunc(funcSpec);
        } catch (Exception e) {
            throw (IOException) new IOException( "Couldn't instantiate storage function" ).initCause(e);
        }

        InputStream stream = FileLocalizer.open(ExecType.LOCAL, filename);

        if (filename.endsWith(".gz")) {
            stream = new GZIPInputStream(stream);
        }

        stream.skip(offset);
        function.bindTo(stream, offset, end);
        com.yahoo.pig.data.Tuple tuple;

        while ((tuple = function.getNext()) != null) {
            Tuple wrapped = new Tuple();
            wrapped.copyFrom(tuple);
            processor.process(wrapped);
        }

        stream.close();
    }

    public static void verify(TupleFlowParameters parameters, ErrorHandler handler) {
        if (!parameters.getXML().containsKey("function")) {
            handler.addError("'function' is a required parameter of ReadTuples.");
        }
    }
}
