/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Iterator;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapRunnable;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.RecordReader;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.BigDataBag;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.IndexedTuple;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.*;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;
import com.yahoo.pig.impl.mapreduceExec.PigOutputFormat.PigRecordWriter;

/**
 * This class is a wrapper of sorts for Pig Map/Reduce jobs. Both the Mapper and the Reducer are
 * implemented by this class. The methods of this class are driven by job configuration variables:
 * <dl>
 * <dt>pig.inputs</dt>
 * <dd>A semi-colon separated list of inputs. If an input uses a special parser, it will be
 * specified by adding a colon and the name of the parser to the input. For example:
 * /tmp/names.txt;/tmp/logs.dat:com.yahoo.research.pig.parser.LogParser will parse /tmp/names.txt
 * using the default parser and /tmp/logs.dat using com.yahoo.research.pig.parser.LogParser.</dd>
 * <dt>pig.mapFuncs</dt>
 * <dd>A semi-colon separated list of functions-specification to be applied to the inputs in the
 * Map phase. This list must have the same number of items as pig.inputs because the each
 * functions-spectification will be matched to the corresponding input.</dd>
 * <dt>pig.groupFuncs</dt>
 * <dd>A semi-colon separated list of group functions. As with pig.mapFuncs, this list must have
 * the same number of items as pig.inputs because the each group function will be matched to the
 * corresponding input.</dd>
 * <dt>pig.reduceFuncs</dt>
 * <dd>functions-specification to be applied to the tuples passed into the Reduce phase.</dd>
 * </dl>
 * 
 * @author breed
 */
public class PigMapReduce implements MapRunnable, Reducer {

    public static Reporter reporter = null;

    private JobConf                   job;
    private DataCollector             evalPipe;
    private OutputCollector           oc;
    private GroupBySpec               group;
    private PigRecordWriter           pigWriter;
    private int                       index;
    private int                       inputCount;
    private boolean                   isInner[];
    private BigDataBag                bags[];
    private File                      tmpdir;

    /**
     * This function is called in MapTask by Hadoop as the Mapper.run() method. We basically pull
     * the tuples from our PigRecordReader (see ugly ThreadLocal hack), pipe the tuples through the
     * function pipeline and then close the writer.
     */
    public void run(RecordReader input, OutputCollector output, Reporter reporter) throws IOException {
        PigMapReduce.reporter = reporter;

        oc = output;
        tmpdir = new File(job.get("mapred.task.id"));
        tmpdir.mkdirs();
        BagFactory.init(tmpdir);

        setupMapPipe(reporter);

        // allocate key & value instances that are re-used for all entries
        WritableComparable key = input.createKey();
        Writable value = input.createValue();
        while (input.next(key, value)) {
            evalPipe.add((Tuple) value);
        }
        evalPipe.add(null);  // EOF marker
        
        if (pigWriter != null) {
            pigWriter.close(reporter);
        }
    }

    public void reduce(WritableComparable key, Iterator values, OutputCollector output, Reporter reporter)
            throws IOException {

        PigMapReduce.reporter = reporter;

        try {
            tmpdir = new File(job.get("mapred.task.id"));
            tmpdir.mkdirs();

            BagFactory.init(tmpdir);

            oc = output;
            if (evalPipe == null) {
                setupReducePipe();
            }

            Datum groupName = ((Tuple) key).getField(0);
            Tuple t = new Tuple(1 + inputCount);
            t.setField(0, groupName);
            for (int i = 1; i < 1 + inputCount; i++) {
                bags[i - 1].clear();
                t.setField(i, bags[i - 1]);
            }

            while (values.hasNext()) {
                IndexedTuple it = (IndexedTuple) values.next();
                t.getBagField(it.index + 1).add(it);
            }
            for (int i = 0; i < inputCount; i++) {
                if (isInner[i] && t.getBagField(1 + i).isEmpty())
                    return;
            }
            evalPipe.add(t);
            evalPipe.add(null);  // EOF marker
        } catch (Throwable tr) {
            tr.printStackTrace();
            RuntimeException exp = new RuntimeException(tr.getMessage());
            exp.setStackTrace(tr.getStackTrace());
            throw exp;
        }
    }

    /**
     * Just save off the JobConf for later use.
     */
    public void configure(JobConf job) {
        this.job = job;
    }

    /**
     * Nothing happens here.
     */
    public void close() throws IOException {
    }

    static PigContext getPigContext() {
    	try {
			InputStream is = PigMapReduce.class.getResourceAsStream("/pigContext");
			if (is == null) throw new RuntimeException("/pigContext not found!");
			return (PigContext)new ObjectInputStream(is).readObject();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
    }
    private void setupMapPipe(Reporter reporter) throws IOException {
        PigSplit split = PigInputFormat.PigRecordReader.getPigRecordReader().getPigFileSplit();
        
        index = split.getIndex();
        String evalSpec = split.getApplyFunction();
        String groupSpec = split.getGroupFunction();
        PigContext pigContext = getPigContext();
        if (groupSpec.length() == 0) {

            String taskidParts[] = job.get("mapred.task.id").split("_"); // The format is
            // tip_job_m_partition_try
            String taskid = "map-" + taskidParts[taskidParts.length - 2];
            pigWriter = (PigRecordWriter) job.getOutputFormat().getRecordWriter(FileSystem.get(job), job, taskid,
                    reporter);
            oc = new OutputCollector() {
                public void collect(WritableComparable key, Writable value) throws IOException {
                    pigWriter.write(key, value);
                }
            };
        } else {
            group = new GroupBySpec(pigContext, groupSpec);
        }

        evalPipe = (new EvalSpecPipe(pigContext, evalSpec)).collector(new MapDataOutputCollector());
    }

    private void setupReducePipe() throws IOException {
        String evalSpec = job.get("pig.reduceFunc", "");
        String[] groupSpecs = job.get("pig.groupFuncs", "").split(";");
        PigContext pigContext = getPigContext();
        isInner = new boolean[groupSpecs.length];
        for (int i = 0; i < groupSpecs.length; i++) {
            isInner[i] = (new GroupBySpec(pigContext, groupSpecs[i])).isInner;
        }

        evalPipe = (new EvalSpecPipe(pigContext, evalSpec)).collector(new ReduceDataOutputCollector());
        
        inputCount = job.get("pig.inputs").split(";", -1).length;

        bags = new BigDataBag[inputCount];
        for (int i = 0; i < inputCount; i++) {
            bags[i] = BagFactory.getInstance().getNewBigBag();
        }
    }

    class MapDataOutputCollector extends DataCollector {
        public void add(Tuple t) throws IOException {
            if (t == null) return;  // EOF marker from eval pipeline; ignore
            
            if (group == null) {
                oc.collect(null, t);
            } else {
                Datum[] multGroups = group.eval(t);
                for (int i = 0; i < multGroups.length; i++) {
                    oc.collect(new Tuple(multGroups[i]), new IndexedTuple(t, index));
                    // wrap group label in a tuple, so it becomes writable.
                }
            }
        }
    }

    class ReduceDataOutputCollector extends DataCollector {
        public void add(Tuple t) throws IOException {
            if (t == null) return;  // EOF marker from eval pipeline; ignore
            
            oc.collect(null, t);
        }
    }

}
