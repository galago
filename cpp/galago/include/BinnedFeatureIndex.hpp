
//
// BinnedFeatureIndex
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#ifndef GALAGO_BINNEDFEATUREINDEX_HPP
#define GALAGO_BINNEDFEATUREINDEX_HPP                               

#include "indri/Parameters.hpp"
#include "BinOrderedBinnedIterator.hpp"
#include "Stemmer.hpp"
#include "Normalizer.hpp"
#include "Query.hpp"
#include "Stopper.hpp"
#include "IndexReader.hpp"

#define BINNEDFEATUREINDEX_MAX_TERM_LENGTH (512)
#define BINNEDFEATUREINDEX_VOCAB_GROUP (16)
#define BINNEDFEATUREINDEX_BLOCK_SIZE (1<<16)

class BinnedFeatureIndex {
private:
    IndexReader _reader;
    
    Stemmer* stemmer;
    Stopper* stopper;
    Normalizer normalizer;

    UINT64 _maximumDocumentCount;
    bool _requiresWeighting;
    
    void _build_stemmer();
    void _build_stopper();

public:
    static const int blockSize = BINNEDFEATUREINDEX_BLOCK_SIZE;
    static const int vocabGroup = BINNEDFEATUREINDEX_VOCAB_GROUP;
    static const char maxTermLength = BINNEDFEATUREINDEX_MAX_TERM_LENGTH;
    
public:
    BinnedFeatureIndex();
    ~BinnedFeatureIndex();
    
    void openRead( const std::string& indexPath );
    void close();
    BinOrderedBinnedIterator* getTerm( const std::string& term ) const;
    QueryTerm processTerm( const QueryTerm& term ) const;
    UINT64 maximumDocumentCount() const;
    bool requiresWeighting() const;
};

#endif // GALAGO_BINNEDFEATUREINDEX_HPP
