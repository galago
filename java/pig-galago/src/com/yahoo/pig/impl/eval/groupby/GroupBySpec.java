/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.groupby;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

import com.yahoo.pig.GroupFunc;
import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalItemList;
import com.yahoo.pig.impl.logicalLayer.parser.QueryParser;
import com.yahoo.pig.impl.logicalLayer.parser.ParseException;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class GroupBySpec {
    public GroupFunc func;
    public EvalItemList args;
    public boolean isInner;
    PigContext pigContext;
    
    public GroupBySpec(PigContext pigContext, GroupFunc func, EvalItemList args, boolean isInner) {
    	this.pigContext = pigContext;
        this.func = func;
        this.args = args;
        this.isInner = isInner;
    }
    
    public Datum[] eval(Tuple input) throws IOException {
        try {
            return func.exec(args.simpleEval(input));
        } catch (IOException e) {
            System.out.println("Warning: group function " + func.name() + " failed. Substituting default value (no groups).");
            return new Datum[0];
        }
    }

    public GroupBySpec(PigContext pigContext, String specStr) throws IOException {
    	this.pigContext = pigContext;
        ByteArrayInputStream in = new ByteArrayInputStream(specStr.getBytes());       
        QueryParser parser = new QueryParser(in, pigContext, null);
        GroupBySpec copyFrom;
        try {
            copyFrom = parser.GroupBySpec();
        } catch (ParseException e) {
            throw new IOException(specStr + ":" + e.getMessage());
        }  
        this.func = copyFrom.func;
        this.args = copyFrom.args;
        this.isInner = copyFrom.isInner;
    }
    
    public String toString() {
        return (func + "(" + args + ") " + (isInner? "INNER" : "OUTER"));
    }
    
    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        funcs.add(func.toString());
        return funcs;
    }
    
    public SchemaItem mapInputSchema(SchemaItem input){
    	SchemaItem inputToFunc;
    	if (args.columns.size() == 1)
    		inputToFunc = args.mapInputSchema(input).schemaFor(0).copy();
    	else
    		inputToFunc = args.mapInputSchema(input);
    	
    	SchemaItem output = func.outputSchema(inputToFunc);
    	
    	if (output == null)
    		output = new SchemaItemList();
    	
    	return output;
    }
}
