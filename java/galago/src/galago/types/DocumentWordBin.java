// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/DocumentWordBin.java DocumentWordBin galago.types String:word String:document int:bin order:+word-bin+document order:+document+word
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class DocumentWordBin implements Type<DocumentWordBin> {
    public String word;
    public String document;
    public int bin; 
    
    public DocumentWordBin() {}
    public DocumentWordBin( String word, String document, int bin ) {
        this.word = word;
        this.document = document;
        this.bin = bin;
    }  
    
    public String toString() {
            return String.format( "%s,%s,%d",
                                   word, document, bin );
    } 

    public Order<DocumentWordBin> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+word", "-bin", "+document" } ) ) {
            return new WordDescBinDocumentOrder();
        }
        if ( Arrays.equals( spec, new String[] { "+document", "+word" } ) ) {
            return new DocumentWordOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<DocumentWordBin> {
        public void process( DocumentWordBin object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class WordDescBinDocumentOrder implements Order<DocumentWordBin> {
        public int hash( DocumentWordBin object ) {
            int h = 0;
            h += Utility.hash(object.word);
            h += Utility.hash(object.bin);
            h += Utility.hash(object.document);
            return h;
        } 
        public Comparator<DocumentWordBin> greaterThan() {
            return new Comparator<DocumentWordBin>() {
                public int compare( DocumentWordBin one, DocumentWordBin two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                        result = - Utility.compare( one.bin, two.bin );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentWordBin> lessThan() {
            return new Comparator<DocumentWordBin>() {
                public int compare( DocumentWordBin one, DocumentWordBin two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                        result = - Utility.compare( one.bin, two.bin );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentWordBin> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentWordBin> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentWordBin> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentWordBin > {
            DocumentWordBin last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentWordBin object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.word, last.word) ) { processAll = true; shreddedWriter.processWord( object.word ); }
               if ( processAll || last == null || 0 != Utility.compare(object.bin, last.bin) ) { processAll = true; shreddedWriter.processBin( object.bin ); }
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               shreddedWriter.processTuple(  );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentWordBin> getInputClass() {
                return DocumentWordBin.class;
            }
        } 
        public ReaderSource<DocumentWordBin> orderedCombiner( Collection<TypeReader<DocumentWordBin>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentWordBin> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentWordBin clone( DocumentWordBin object ) {
            DocumentWordBin result = new DocumentWordBin();
            if ( object == null ) return result;
            result.word = object.word; 
            result.document = object.document; 
            result.bin = object.bin; 
            return result;
        }                 
        public Class<DocumentWordBin> getOrderedClass() {
            return DocumentWordBin.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+word", "-bin", "+document"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processWord( String word ) throws IOException;
            public void processBin( int bin ) throws IOException;
            public void processDocument( String document ) throws IOException;
            public void processTuple(  ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastWord;
            int lastBin;
            String lastDocument;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processWord( String word ) {
                lastWord = word;
                buffer.processWord( word );
            }
            public void processBin( int bin ) {
                lastBin = bin;
                buffer.processBin( bin );
            }
            public void processDocument( String document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public final void processTuple(  ) throws IOException {
                if( lastFlush ) {
                    if( buffer.words.size() == 0 ) buffer.processWord( lastWord );
                    if( buffer.bins.size() == 0 ) buffer.processBin( lastBin );
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    lastFlush = false;
                }
                buffer.processTuple(  );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    buffer.incrementTuple();
                }
            }  
            public final void flushWord( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getWordEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getWord() );
                    output.writeInt( count );
                    buffer.incrementWord();
                      
                    flushBin( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public final void flushBin( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getBinEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeInt( buffer.getBin() );
                    output.writeInt( count );
                    buffer.incrementBin();
                      
                    flushDocument( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushWord( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> words = new ArrayList();
            ArrayList<Integer> bins = new ArrayList();
            ArrayList<String> documents = new ArrayList();
            ArrayList<Integer> wordTupleIdx = new ArrayList();
            ArrayList<Integer> binTupleIdx = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            int wordReadIdx = 0;
            int binReadIdx = 0;
            int documentReadIdx = 0;
                            
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processWord( String word ) {
                words.add( word );
                wordTupleIdx.add( writeTupleIndex );
            }                                      
            public void processBin( int bin ) {
                bins.add( bin );
                binTupleIdx.add( writeTupleIndex );
            }                                      
            public void processDocument( String document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple(  ) {
                assert words.size() > 0;
                assert bins.size() > 0;
                assert documents.size() > 0;
                writeTupleIndex++;
            }
            public void resetData() {
                words.clear();
                bins.clear();
                documents.clear();
                wordTupleIdx.clear();
                binTupleIdx.clear();
                documentTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                wordReadIdx = 0;
                binReadIdx = 0;
                documentReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementWord() {
                wordReadIdx++;  
            }                                                                                              

            public void autoIncrementWord() {
                while( readTupleIndex >= getWordEndIndex() && readTupleIndex < writeTupleIndex )
                    wordReadIdx++;
            }                 
            public void incrementBin() {
                binReadIdx++;  
            }                                                                                              

            public void autoIncrementBin() {
                while( readTupleIndex >= getBinEndIndex() && readTupleIndex < writeTupleIndex )
                    binReadIdx++;
            }                 
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getWordEndIndex() {
                if( (wordReadIdx+1) >= wordTupleIdx.size() )
                    return writeTupleIndex;
                return wordTupleIdx.get(wordReadIdx+1);
            }

            public int getBinEndIndex() {
                if( (binReadIdx+1) >= binTupleIdx.size() )
                    return writeTupleIndex;
                return binTupleIdx.get(binReadIdx+1);
            }

            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getWord() {
                assert readTupleIndex < writeTupleIndex;
                assert wordReadIdx < words.size();
                
                return words.get( wordReadIdx );
            }
            public int getBin() {
                assert readTupleIndex < writeTupleIndex;
                assert binReadIdx < bins.size();
                
                return bins.get( binReadIdx );
            }
            public String getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }

            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple(  );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexWord( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processWord( getWord() );
                    assert getWordEndIndex() <= endIndex;
                    copyUntilIndexBin( getWordEndIndex(), output );
                    incrementWord();
                }
            } 
            public void copyUntilIndexBin( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processBin( getBin() );
                    assert getBinEndIndex() <= endIndex;
                    copyUntilIndexDocument( getBinEndIndex(), output );
                    incrementBin();
                }
            } 
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyTuples( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            }  
            public void copyUntilWord( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getWord(), other.getWord() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processWord( getWord() );
                                      
                        if( c < 0 ) {
                            copyUntilIndexBin( getWordEndIndex(), output );
                        } else if( c == 0 ) {
                            copyUntilBin( other, output );
                            autoIncrementWord();
                            break;
                        }
                    } else {
                        output.processWord( getWord() );
                        copyUntilIndexBin( getWordEndIndex(), output );
                    }
                    incrementWord();  
                    
               
                }
            }
            public void copyUntilBin( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = - Utility.compare( getBin(), other.getBin() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processBin( getBin() );
                                      
                        if( c < 0 ) {
                            copyUntilIndexDocument( getBinEndIndex(), output );
                        } else if( c == 0 ) {
                            copyUntilDocument( other, output );
                            autoIncrementBin();
                            break;
                        }
                    } else {
                        output.processBin( getBin() );
                        copyUntilIndexDocument( getBinEndIndex(), output );
                    }
                    incrementBin();  
                    
                    if( getWordEndIndex() <= readTupleIndex )
                        break;   
                }
            }
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        copyTuples( getDocumentEndIndex(), output );
                    } else {
                        output.processDocument( getDocument() );
                        copyTuples( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
                    if( getBinEndIndex() <= readTupleIndex )
                        break;   
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilWord( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentWordBin>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordBin.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordBin.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordBin>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordBin> getOutputClass() {
                return DocumentWordBin.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentWordBin read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentWordBin result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentWordBin>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentWordBin last = new DocumentWordBin();         
            long updateWordCount = -1;
            long updateBinCount = -1;
            long updateDocumentCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getWord(), otherBuffer.getWord() );
                    if( result != 0 ) break;
                    result = - Utility.compare( buffer.getBin(), otherBuffer.getBin() );
                    if( result != 0 ) break;
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentWordBin read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentWordBin result = new DocumentWordBin();
                
                result.word = buffer.getWord();
                result.bin = buffer.getBin();
                result.document = buffer.getDocument();
                
                buffer.incrementTuple();
                buffer.autoIncrementWord();
                buffer.autoIncrementBin();
                buffer.autoIncrementDocument();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateWordCount - tupleCount > 0 ) {
                            buffer.words.add(last.word);
                            buffer.wordTupleIdx.add( (int) (updateWordCount - tupleCount) );
                        }                              
                        if( updateBinCount - tupleCount > 0 ) {
                            buffer.bins.add(last.bin);
                            buffer.binTupleIdx.add( (int) (updateBinCount - tupleCount) );
                        }                              
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateDocument();
                        buffer.processTuple(  );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateWord() throws IOException {
                if( updateWordCount > tupleCount )
                    return;
                     
                last.word = input.readString();
                updateWordCount = tupleCount + input.readInt();
                                      
                buffer.processWord( last.word );
            }
            public final void updateBin() throws IOException {
                if( updateBinCount > tupleCount )
                    return;
                     
                updateWord();
                last.bin = input.readInt();
                updateBinCount = tupleCount + input.readInt();
                                      
                buffer.processBin( last.bin );
            }
            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                updateBin();
                last.document = input.readString();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordBin.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordBin.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordBin>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordBin> getOutputClass() {
                return DocumentWordBin.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentWordBin last = new DocumentWordBin();
            boolean wordProcess = true;
            boolean binProcess = true;
            boolean documentProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processWord( String word ) throws IOException {  
                if( wordProcess || Utility.compare( word, last.word ) != 0 ) {
                    last.word = word;
                    processor.processWord( word );
            resetBin();
                    wordProcess = false;
                }
            }
            public void processBin( int bin ) throws IOException {  
                if( binProcess || Utility.compare( bin, last.bin ) != 0 ) {
                    last.bin = bin;
                    processor.processBin( bin );
            resetDocument();
                    binProcess = false;
                }
            }
            public void processDocument( String document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
                    documentProcess = false;
                }
            }  
            
            public void resetWord() {
                 wordProcess = true;
            resetBin();
            }                                                
            public void resetBin() {
                 binProcess = true;
            resetDocument();
            }                                                
            public void resetDocument() {
                 documentProcess = true;
            }                                                
                               
            public void processTuple(  ) throws IOException {
                processor.processTuple(  );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentWordBin last = new DocumentWordBin();
            public galago.tupleflow.Processor<DocumentWordBin> processor;                               
            
            public TupleUnshredder( DocumentWordBin.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentWordBin> processor ) {
                this.processor = processor;
            }
            
            public DocumentWordBin clone( DocumentWordBin object ) {
                DocumentWordBin result = new DocumentWordBin();
                if ( object == null ) return result;
                result.word = object.word; 
                result.document = object.document; 
                result.bin = object.bin; 
                return result;
            }                 
            
            public void processWord( String word ) throws IOException {
                last.word = word;
            }   
                
            public void processBin( int bin ) throws IOException {
                last.bin = bin;
            }   
                
            public void processDocument( String document ) throws IOException {
                last.document = document;
            }   
                
            
            public void processTuple(  ) throws IOException {
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentWordBin last = new DocumentWordBin();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentWordBin clone( DocumentWordBin object ) {
                DocumentWordBin result = new DocumentWordBin();
                if ( object == null ) return result;
                result.word = object.word; 
                result.document = object.document; 
                result.bin = object.bin; 
                return result;
            }                 
            
            public void process( DocumentWordBin object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.word, object.word ) != 0 || processAll ) { processor.processWord( object.word ); processAll = true; }
                if( last == null || Utility.compare( last.bin, object.bin ) != 0 || processAll ) { processor.processBin( object.bin ); processAll = true; }
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                processor.processTuple(  );                                         
            }
                          
            public Class<DocumentWordBin> getInputClass() {
                return DocumentWordBin.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
    public static class DocumentWordOrder implements Order<DocumentWordBin> {
        public int hash( DocumentWordBin object ) {
            int h = 0;
            h += Utility.hash(object.document);
            h += Utility.hash(object.word);
            return h;
        } 
        public Comparator<DocumentWordBin> greaterThan() {
            return new Comparator<DocumentWordBin>() {
                public int compare( DocumentWordBin one, DocumentWordBin two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentWordBin> lessThan() {
            return new Comparator<DocumentWordBin>() {
                public int compare( DocumentWordBin one, DocumentWordBin two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentWordBin> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentWordBin> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentWordBin> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentWordBin > {
            DocumentWordBin last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentWordBin object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               if ( processAll || last == null || 0 != Utility.compare(object.word, last.word) ) { processAll = true; shreddedWriter.processWord( object.word ); }
               shreddedWriter.processTuple( object.bin );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentWordBin> getInputClass() {
                return DocumentWordBin.class;
            }
        } 
        public ReaderSource<DocumentWordBin> orderedCombiner( Collection<TypeReader<DocumentWordBin>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentWordBin> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentWordBin clone( DocumentWordBin object ) {
            DocumentWordBin result = new DocumentWordBin();
            if ( object == null ) return result;
            result.word = object.word; 
            result.document = object.document; 
            result.bin = object.bin; 
            return result;
        }                 
        public Class<DocumentWordBin> getOrderedClass() {
            return DocumentWordBin.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+document", "+word"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processDocument( String document ) throws IOException;
            public void processWord( String word ) throws IOException;
            public void processTuple( int bin ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastDocument;
            String lastWord;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processDocument( String document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public void processWord( String word ) {
                lastWord = word;
                buffer.processWord( word );
            }
            public final void processTuple( int bin ) throws IOException {
                if( lastFlush ) {
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    if( buffer.words.size() == 0 ) buffer.processWord( lastWord );
                    lastFlush = false;
                }
                buffer.processTuple( bin );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getBin() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushWord( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public final void flushWord( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getWordEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getWord() );
                    output.writeInt( count );
                    buffer.incrementWord();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushDocument( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> documents = new ArrayList();
            ArrayList<String> words = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            ArrayList<Integer> wordTupleIdx = new ArrayList();
            int documentReadIdx = 0;
            int wordReadIdx = 0;
                            
            int[] bins;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                bins = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processDocument( String document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processWord( String word ) {
                words.add( word );
                wordTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int bin ) {
                assert documents.size() > 0;
                assert words.size() > 0;
                bins[writeTupleIndex] = bin;
                writeTupleIndex++;
            }
            public void resetData() {
                documents.clear();
                words.clear();
                documentTupleIdx.clear();
                wordTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                documentReadIdx = 0;
                wordReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementWord() {
                wordReadIdx++;  
            }                                                                                              

            public void autoIncrementWord() {
                while( readTupleIndex >= getWordEndIndex() && readTupleIndex < writeTupleIndex )
                    wordReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }

            public int getWordEndIndex() {
                if( (wordReadIdx+1) >= wordTupleIdx.size() )
                    return writeTupleIndex;
                return wordTupleIdx.get(wordReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }
            public String getWord() {
                assert readTupleIndex < writeTupleIndex;
                assert wordReadIdx < words.size();
                
                return words.get( wordReadIdx );
            }
            public int getBin() {
                assert readTupleIndex < writeTupleIndex;
                return bins[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getBin() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyUntilIndexWord( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            } 
            public void copyUntilIndexWord( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processWord( getWord() );
                    assert getWordEndIndex() <= endIndex;
                    copyTuples( getWordEndIndex(), output );
                    incrementWord();
                }
            }  
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        if( c < 0 ) {
                            copyUntilIndexWord( getDocumentEndIndex(), output );
                        } else if( c == 0 ) {
                            copyUntilWord( other, output );
                            autoIncrementDocument();
                            break;
                        }
                    } else {
                        output.processDocument( getDocument() );
                        copyUntilIndexWord( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
               
                }
            }
            public void copyUntilWord( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getWord(), other.getWord() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processWord( getWord() );
                                      
                        copyTuples( getWordEndIndex(), output );
                    } else {
                        output.processWord( getWord() );
                        copyTuples( getWordEndIndex(), output );
                    }
                    incrementWord();  
                    
                    if( getDocumentEndIndex() <= readTupleIndex )
                        break;   
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilDocument( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentWordBin>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordBin.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordBin.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordBin>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordBin> getOutputClass() {
                return DocumentWordBin.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentWordBin read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentWordBin result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentWordBin>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentWordBin last = new DocumentWordBin();         
            long updateDocumentCount = -1;
            long updateWordCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                    result = + Utility.compare( buffer.getWord(), otherBuffer.getWord() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentWordBin read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentWordBin result = new DocumentWordBin();
                
                result.document = buffer.getDocument();
                result.word = buffer.getWord();
                result.bin = buffer.getBin();
                
                buffer.incrementTuple();
                buffer.autoIncrementDocument();
                buffer.autoIncrementWord();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }                              
                        if( updateWordCount - tupleCount > 0 ) {
                            buffer.words.add(last.word);
                            buffer.wordTupleIdx.add( (int) (updateWordCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateWord();
                        buffer.processTuple( input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                last.document = input.readString();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }
            public final void updateWord() throws IOException {
                if( updateWordCount > tupleCount )
                    return;
                     
                updateDocument();
                last.word = input.readString();
                updateWordCount = tupleCount + input.readInt();
                                      
                buffer.processWord( last.word );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentWordBin.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentWordBin.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentWordBin>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentWordBin> getOutputClass() {
                return DocumentWordBin.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentWordBin last = new DocumentWordBin();
            boolean documentProcess = true;
            boolean wordProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processDocument( String document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
            resetWord();
                    documentProcess = false;
                }
            }
            public void processWord( String word ) throws IOException {  
                if( wordProcess || Utility.compare( word, last.word ) != 0 ) {
                    last.word = word;
                    processor.processWord( word );
                    wordProcess = false;
                }
            }  
            
            public void resetDocument() {
                 documentProcess = true;
            resetWord();
            }                                                
            public void resetWord() {
                 wordProcess = true;
            }                                                
                               
            public void processTuple( int bin ) throws IOException {
                processor.processTuple( bin );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentWordBin last = new DocumentWordBin();
            public galago.tupleflow.Processor<DocumentWordBin> processor;                               
            
            public TupleUnshredder( DocumentWordBin.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentWordBin> processor ) {
                this.processor = processor;
            }
            
            public DocumentWordBin clone( DocumentWordBin object ) {
                DocumentWordBin result = new DocumentWordBin();
                if ( object == null ) return result;
                result.word = object.word; 
                result.document = object.document; 
                result.bin = object.bin; 
                return result;
            }                 
            
            public void processDocument( String document ) throws IOException {
                last.document = document;
            }   
                
            public void processWord( String word ) throws IOException {
                last.word = word;
            }   
                
            
            public void processTuple( int bin ) throws IOException {
                last.bin = bin;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentWordBin last = new DocumentWordBin();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentWordBin clone( DocumentWordBin object ) {
                DocumentWordBin result = new DocumentWordBin();
                if ( object == null ) return result;
                result.word = object.word; 
                result.document = object.document; 
                result.bin = object.bin; 
                return result;
            }                 
            
            public void process( DocumentWordBin object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                if( last == null || Utility.compare( last.word, object.word ) != 0 || processAll ) { processor.processWord( object.word ); processAll = true; }
                processor.processTuple( object.bin );                                         
            }
                          
            public Class<DocumentWordBin> getInputClass() {
                return DocumentWordBin.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    