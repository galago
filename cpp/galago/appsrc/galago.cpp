
//
// galago
//
// 5 January 2007 -- tds
// 
// BSD License (http://galagosearch.org/license)
//       

#include "indri/ScoredExtentResult.hpp"
#include "indri/Parameters.hpp"
#include "lemur/Exception.hpp"
#include "Retrieval.hpp"
#include <iostream>
#include <stdlib.h>
#include <fstream> 

//
// usage
//

static void usage() {
    std::cerr << "galago --query.text=hi --query.number=1 --count=10 --index=myindex" << std::endl;
    exit(-1);
}

//
// require
//

static void require( indri::api::Parameters param, const std::string& p ) {
  if( !param.exists(p) )
    usage();
}

//
// command_line
//

static void command_line( Retrieval& retrieval, indri::api::Parameters& parameters ) {
    // record results requested
    int requested = parameters.get( "count", 1000 );

    // for each query, run it, get the results, look up the docnos, print in TREC format
    indri::api::Parameters queries = parameters["query"];

    for( int i=0; i<queries.size(); i++ ) {        
        std::string queryText = queries[i]["text"];
        std::string queryName = queries[i]["number"];
        int threshold = (int)(INT64)queries[i].get( "threshold", -1 );
        
        std::vector<indri::api::ScoredExtentResult> results;
        std::vector<QueryTerm> terms = Query::parse( queryText );
        results = retrieval.runQuery( terms, requested, threshold );

        for( int i=0; i<results.size(); i++ ) {
            std::string document = retrieval.getDocument(results[i].document);
            int score = (int)results[i].score;
            int rank = i+1;

            printf( "%s Q0 %s %d %d galago\n", queryName.c_str(), document.c_str(), rank, score );
        }
    }  
}

//
// main
//
        
int main( int argc, char** argv ) {
    try {
        // read in parameters
        indri::api::Parameters::instance().loadCommandLine( argc, argv );
        indri::api::Parameters& parameters = indri::api::Parameters::instance();     

        // open index
        std::string indexPath = parameters["index"];
        Retrieval* retrieval = Retrieval::open( indexPath );
        
        command_line( *retrieval, parameters );
    } catch( lemur::api::Exception& e ) {
        LEMUR_ABORT(e);
    }
    
    return 0;
}

