/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.Map;

import com.yahoo.pig.builtin.BinStorage;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.logicalLayer.*;

public class PigbodyPlanCompiler extends PlanCompiler {

    protected PigbodyPlanCompiler(PigContext pigContext) {
        super(pigContext);
    }

    @Override
    public PhysicalOperator compile(LogicalOperator lo, Map queryResults) throws IOException {
        return new POPigbody(pigContext, new LogicalPlan(resolveReads(lo, queryResults), pigContext));
    }
    
    private LogicalOperator resolveReads(LogicalOperator lo, Map queryResults) throws IOException {
        if (lo instanceof LORead) {
            IntermedResult readFrom = ((LORead) lo).readFrom;

            if (readFrom.executed()) {
                // result should already exist as a temp file
                if (readFrom.file == null) readFrom.toDFSFile(PlanCompiler.getTempFile(), BinStorage.class.getName());
                String inputFile = readFrom.file;
                if (inputFile.startsWith(FileLocalizer.HADOOP_PREFIX)) {
                    inputFile = inputFile.substring(FileLocalizer.HADOOP_PREFIX.length());
                }
                return new LOLoad(pigContext, inputFile, new BinStorage(), new String[0]);
            } else {
                // compile other plan (idempotent)
                readFrom.compile(queryResults);
    
                // return root of other plan
                return ((POPigbody) readFrom.pp.root).logicalPlan.root();
            }

        } else {
            // resolve inputs
            for (int i = 0; i < lo.inputs.length; i++)
                lo.inputs[i] = (LogicalOperator) resolveReads(lo.inputs[i], queryResults);

            return lo;
        }
    }
}
