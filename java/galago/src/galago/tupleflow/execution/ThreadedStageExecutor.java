/*
 * ThreadedExecutor
 *
 * August 29, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.tupleflow.ExNihiloSource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author trevor
 */
public class ThreadedStageExecutor implements StageExecutor {
    public static class InstanceRunnable implements Runnable {
        StageInstanceDescription description;
        Exception exception;
        boolean isRunning;
        CountDownLatch latch;

        public InstanceRunnable( StageInstanceDescription description, CountDownLatch latch ) {
            this.isRunning = true;
            this.description = description;
            this.exception = null;
            this.latch = latch;
        }
        
        public synchronized boolean isRunning() {
            return isRunning;
        }
        
        public synchronized Exception getException() {
            return exception;
        }
        
        public void run() {
            try {
                ExNihiloSource source = description.instantiate();
                source.run();
            } catch( Exception e ) {
                exception = e;
            } finally {
                latch.countDown();
                isRunning = false;
            }
        }
    }
    
    public static class GroupFuture implements Future<Object> {
        List<InstanceRunnable> runnables;
        CountDownLatch latch;
        
        public GroupFuture( List<InstanceRunnable> runnables, CountDownLatch latch ) {
            this.runnables = runnables;
            this.latch = latch;
        }
        
        private Exception pollExceptions() throws ExecutionException {
            Exception e;
            for( InstanceRunnable runnable : runnables ) {
                e = runnable.getException();
                if( e != null )
                    throw new ExecutionException( "Instance threw an exception", e );
            }
            return null;
        }

        public Object get( long distance, TimeUnit unit ) throws InterruptedException, ExecutionException, TimeoutException {
            latch.await( distance, unit );
            pollExceptions();
            return null;
        }

        public Object get() throws InterruptedException, ExecutionException {
            latch.await();
            pollExceptions();
            return null;
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        public boolean isDone() {
            return latch.getCount() == 0;
        }

        public boolean isCancelled() {
            return false;
        }
    }
    
    Executor threadPool;
    
    public ThreadedStageExecutor() {
        threadPool = Executors.newFixedThreadPool( Runtime.getRuntime().availableProcessors() );
    }
    
    public Future<Object> execute( StageGroupDescription stage, String temporary ) {
        List<StageInstanceDescription> instances = stage.getInstances();
        ArrayList<InstanceRunnable> runnables = new ArrayList();
        CountDownLatch latch = new CountDownLatch(instances.size());
        
        for( StageInstanceDescription instance : instances ) {
            InstanceRunnable runnable = new InstanceRunnable( instance, latch );
            runnables.add(runnable);
            threadPool.execute(runnable);
        }
        
        return new GroupFuture( runnables, latch );
    }
    
    public void shutdown() {
        ((ThreadPoolExecutor)threadPool).shutdown();
    }
}
