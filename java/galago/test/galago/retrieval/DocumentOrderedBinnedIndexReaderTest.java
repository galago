/*
 * DocumentOrderedBinnedIndexReaderTest.java
 * JUnit based test
 *
 * Created on October 9, 2007, 4:48 PM
 */

package galago.retrieval;

import galago.Utility;
import galago.index.DocumentOrderedBinnedListWriter;
import galago.tupleflow.FakeParameters;
import galago.tupleflow.Parameters;
import galago.tupleflow.TupleFlowParameters;
import java.io.File;
import java.io.IOException;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class DocumentOrderedBinnedIndexReaderTest extends TestCase {
    private File tempPath;
    
    int maxABits = 6;
    int maxBBits = 9;
    int maxCBits = 12;

    int maxAScore = 20;
    int maxBScore = 10;
    int maxCScore = 8;    
    
    public DocumentOrderedBinnedIndexReaderTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        // make a spot for the index
        tempPath = File.createTempFile( "galago-test-index", null );
        tempPath.delete();
        tempPath.mkdir();

        // put in a generic manifest
        new Parameters().write( tempPath + File.separator + "manifest" );
        
        Parameters p = new Parameters();
        p.add( "index", tempPath.toString() );
        
        TupleFlowParameters parameters = new FakeParameters(p);
        
        // build a score index
        DocumentOrderedBinnedListWriter writer = new DocumentOrderedBinnedListWriter( parameters );
        writer.processWord( Utility.makeBytes("a") );
        
        for( int i=1; i<10; i++ ) {
            int score = i;
            int document = i;
            
            writer.processDocument(document);
            writer.processTuple(score);
        }
        
        writer.processWord( Utility.makeBytes("b") );
        
        for( int i=1; i<100000; i++ ) {
            int score = i % 83;
            int document = i;
            
            writer.processDocument(document);
            writer.processTuple(score);
        }
        
        writer.processWord( Utility.makeBytes("c") );
        
        for( int i=593; i<100000; i+=53 ) {
            int score = i % 69;
            int document = i;

            writer.processDocument(document);
            writer.processTuple(score);
        }
        
        writer.close();
    }
    
    @Override
    protected void tearDown() throws IOException {
        // delete the index
        Utility.deleteDirectory( tempPath );
    }
    
    public void testGetSimpleTerm() throws Exception {
        System.out.println("getSimpleTerm");
        DocumentOrderedBinnedIndexReader instance = new DocumentOrderedBinnedIndexReader(tempPath.toString());
        DocumentOrderedIterator iter = instance.getTerm("a");
        
        assertNotNull( iter );
        assertFalse( iter.isDone() );
        
        for( int i=1; i<10; i++ ) {
            assertFalse( iter.isDone() );
            
            int document = iter.currentDocument();
            int score = iter.currentScore();
            
            assertEquals( i, document );
            assertEquals( i, score );
            
            iter.nextDocument();
        }

        assertTrue( iter.isDone() );
    }
    
    public void testGetRealisticTerm() throws Exception {
        System.out.println("getRealisticTerm");
        DocumentOrderedBinnedIndexReader instance = new DocumentOrderedBinnedIndexReader(tempPath.toString());
        DocumentOrderedIterator iter = instance.getTerm("c");
        
        assertNotNull( iter );
        assertFalse( iter.isDone() );
        
        for( int i=593; i<100000; i+=53 ) {
            int expectedScore = i % 69;
            int expectedDocument = i;
            assertFalse( iter.isDone() );
            
            int document = iter.currentDocument();
            int score = iter.currentScore();
            
            assertEquals( expectedDocument, document );
            assertEquals( expectedScore, score );
            
            iter.nextDocument();
        }

        assertTrue( iter.isDone() );
    }
    
    public void testSkipRealisticTerm() throws Exception {
        System.out.println("getRealisticTerm");
        DocumentOrderedBinnedIndexReader instance = new DocumentOrderedBinnedIndexReader(tempPath.toString());
        DocumentOrderedIterator iter = instance.getTerm("c");
        
        assertNotNull( iter );
        assertFalse( iter.isDone() );
        
        iter.nextDocument();
        iter.nextDocument();

        boolean success = iter.skipToDocument( 593 + 53*10 );
        assertTrue( success );
        
        for( int i=593 + 53*10; i<100000; i+=53*105 ) {
            int expectedScore = i % 69;
            int expectedDocument = i;
            assertFalse( iter.isDone() );
            
            int document = iter.currentDocument();
            int score = iter.currentScore();
            
            assertEquals( expectedDocument, document );
            assertEquals( expectedScore, score );
            
            iter.skipToDocument( i + 53*105 );
        }

        assertTrue( iter.isDone() );
    }
    public void testGetLongTerm() throws Exception {
        System.out.println("getLongTerm");
        DocumentOrderedBinnedIndexReader instance = new DocumentOrderedBinnedIndexReader(tempPath.toString());
        DocumentOrderedIterator iter = instance.getTerm("b");
        
        assertNotNull( iter );
        assertFalse( iter.isDone() );
        
        for( int i=1; i<100000; i++ ) {
            assertFalse( iter.isDone() );
            
            int document = iter.currentDocument();
            int score = iter.currentScore();
            
            assertEquals( i % 83, score );
            assertEquals( i, document );
            
            iter.nextDocument();
        }

        assertTrue( iter.isDone() );
    }
    
    public void testSkipLongTerm() throws IOException {
        System.out.println("getLongTerm");
        DocumentOrderedBinnedIndexReader instance = new DocumentOrderedBinnedIndexReader(tempPath.toString());
        DocumentOrderedIterator iter = instance.getTerm("b");
        
        assertNotNull( iter );
        assertFalse( iter.isDone() );
        int nextDocument = 1000;
        
        iter.skipToDocument( nextDocument );
        
        while( !iter.isDone() ) {
            int document = iter.currentDocument();
            int score = iter.currentScore();
            
            assertEquals( nextDocument % 83, score );
            assertEquals( nextDocument, document );
            
            nextDocument += 500;
            iter.skipToDocument( nextDocument );
        }

        assertEquals( 100000, nextDocument );
        assertTrue( iter.isDone() );
    }
    
    public void testSkipBound() throws IOException {
        System.out.println("skipBound");
        DocumentOrderedBinnedIndexReader instance = new DocumentOrderedBinnedIndexReader(tempPath.toString());
        DocumentOrderedIterator iter = instance.getTerm("b");
        
        assertNotNull( iter );
        assertFalse( iter.isDone() );
        
        while( !iter.isDone() ) {
            int document = iter.currentDocument();
            int score = iter.currentScore();
            
            assertEquals( document % 83, score );
            
            iter.nextDocument();
            iter.skipToBound( 50 );
        }

        assertTrue( iter.isDone() );    
    }

    public void testSimpleRead() throws Exception {
        File path = RetrievalTestUtility.createIndexPath();
        DocumentOrderedIndexUtility.buildSimpleIndex(path);

        DocumentOrderedBinnedIndexReader reader = new DocumentOrderedBinnedIndexReader( path.toString() + File.separator + "default" );
        DocumentOrderedIterator iterator = reader.getTerm("a");
                
        while( !iterator.isDone() ) {
            int document = iterator.currentDocument();
            int score = iterator.currentScore();
            
            if( document <= 500 || document > 550 )
                assertEquals( 1, score );
            else
                assertEquals( document-500, score );
            
            iterator.skipToDocument( document + (document % 10) + 3 );
        }
        
        iterator = reader.getTerm("b");
        
        while( !iterator.isDone() ) {
            int document = iterator.currentDocument();
            int score = iterator.currentScore();
            
            if( document <= 500 || document > 550 )
                assertEquals( 1, score );
            else
                assertEquals( 2*(document-500), score );

            iterator.skipToDocument( document + 1 );
            iterator.skipToDocument( document + (document % 100) + 17, 13 );
            iterator.nextDocument();
        }
    }
}
