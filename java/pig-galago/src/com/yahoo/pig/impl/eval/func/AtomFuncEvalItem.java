/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.func;

import java.io.IOException;

import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalItemList;

public class AtomFuncEvalItem extends FuncEvalItem {

    public AtomFuncEvalItem(PigContext pigContext, AtomEvalFunc func, EvalItemList args) {
        super(pigContext, func, args);
    }

    @Override
    public void eval(Tuple input, DataCollector output) throws IOException {
        atomEval(simpleEval(input), output);
    }

    @Override
    public DataAtom simpleEval(Tuple input) throws IOException {
        DataAtom d = new DataAtom();
        
        ((AtomEvalFunc) func).exec(args.simpleEval(input), d);
        
        return d;
    }

    public String stringVal(Tuple tup) throws IOException {
        return simpleEval(tup).strval();
        
    }
    
    @Override
    public Double doubleVal(Tuple tup) throws IOException {
        return simpleEval(tup).numval();
    }
    
}
