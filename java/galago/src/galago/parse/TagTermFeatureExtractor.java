/*
 * TagTermFeatureExtractor
 *
 * December 11, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.io.IOException;
import java.util.ArrayList;
import galago.tupleflow.Processor;
import galago.types.DocumentWordCount;
import galago.types.WordCount;

/**
 *
 * @author trevor
 */
public class TagTermFeatureExtractor implements FeatureExtractor {
    String tagName;
    TokenCounter counter;
    
    public TagTermFeatureExtractor( String tagName,Processor<DocumentWordCount> postings, Processor<WordCount> counts ) {
        this.tagName = tagName;
        this.counter = new TokenCounter( postings, counts );
    }

    public void extract( String id, Document document ) throws IOException {
        ArrayList<String> tokens = new ArrayList<String>();
        int largestEnd = 0;

        for( Tag t : document.tags ) {
            if( t.equals(tagName) && t.begin >= largestEnd ) {
                for( int i=t.begin; i < t.end; i++ ) {
                    tokens.add( document.terms.get(i) );
                }

                largestEnd = t.end;
            }
        }
        
        counter.process( id, tokens );
    }
    
    public void close() throws IOException {
        counter.close();
    }
}
