/*
 * StageExecutorFactory
 *
 * September 6, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.Utility;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author trevor
 */
public class StageExecutorFactory {
    public static StageExecutor newInstance( String name, String... args ) {
        if( name == null )
            name = "local";
        
        name = name.toLowerCase();
        
        if( name.equals( "drmaa" ) || name.equals( "condor" ) || name.startsWith( "grid" ) || name.equals( "sge" ) ) {
            try {
                return new DRMAAStageExecutor( args );
            } catch( IOException e ) {
                return null;
            }
        } else if( name.startsWith( "thread") || name.startsWith( "local" ) ) {
            return new ThreadedStageExecutor();
        } else if( name.startsWith( "ssh" ) ) {
            return new SSHStageExecutor( args[0], Arrays.asList(Utility.subarray(args, 1)) );
        } else if( name.equals( "remotedebug" ) ) {
            return new LocalRemoteStageExecutor();
        } else {
            return new LocalStageExecutor();
        }
    }
    
    /** Creates a new instance of StageExecutorFactory */
    public StageExecutorFactory() {
    }
}
