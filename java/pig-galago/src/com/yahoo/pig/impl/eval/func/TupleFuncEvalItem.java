/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.func;

import java.io.IOException;

import com.yahoo.pig.TupleEvalFunc;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalItemList;

public class TupleFuncEvalItem extends FuncEvalItem {

    public TupleFuncEvalItem(PigContext pigContext, TupleEvalFunc func, EvalItemList args) {
        super(pigContext, func, args);
    }

    @Override
    public void eval(Tuple input, DataCollector output) throws IOException {
        Tuple d = new Tuple();
        
        ((TupleEvalFunc) func).exec(args.simpleEval(input), d);
        
        tupleEval(d, output);
    }

    @Override
    public Tuple simpleEval(Tuple input) throws IOException {
        Tuple d = new Tuple();
        
        ((TupleEvalFunc) func).exec(args.simpleEval(input), d);
        
        if (nestedEval != null) d = nestedEval.simpleEval(d);
        
        return d;
    }
    
    @Override
    public String stringVal(Tuple tup) throws IOException {
        throw new IOException("Internal error: illegal to call stringVal() on FuncEvalItem");
    }
    
    @Override
    public Double doubleVal(Tuple tup) throws IOException {
        throw new IOException("Internal error: illegal to call doubleVal() on FuncEvalItem");
    }

}
