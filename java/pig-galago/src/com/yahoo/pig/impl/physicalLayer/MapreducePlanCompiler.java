/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.Map;

import com.yahoo.pig.builtin.BinStorage;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import com.yahoo.pig.impl.eval.EvalSpecPipeList;
import com.yahoo.pig.impl.eval.groupby.GroupBySpecList;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.logicalLayer.LOCogroup;
import com.yahoo.pig.impl.logicalLayer.LOEval;
import com.yahoo.pig.impl.logicalLayer.LOLoad;
import com.yahoo.pig.impl.logicalLayer.LORead;
import com.yahoo.pig.impl.logicalLayer.LOStore;
import com.yahoo.pig.impl.logicalLayer.LOUnion;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;

// compiler for mapreduce physical plans
public class MapreducePlanCompiler extends PlanCompiler {

    protected MapreducePlanCompiler(PigContext pigContext) {
		super(pigContext);
	}

	public PhysicalOperator compile(LogicalOperator lo, Map queryResults) throws IOException {
        // first, compile inputs into MapReduce operators
        POMapreduce[] compiledInputs = new POMapreduce[lo.inputs.length];
        for (int i = 0; i < lo.inputs.length; i++)
            compiledInputs[i] = (POMapreduce) compile(lo.inputs[i], queryResults);

        // then, compile this operator; if possible, merge with previous MapReduce
        // operator rather than introducing a new one
        if (lo instanceof LOEval) {
            POMapreduce pom = new POMapreduce(compiledInputs[0]); // make a copy of the previous
            // MapReduce operator (NOTE:
            // it's important that we make a
            // copy rather than using it
            // directly; this matters in the
            // case of a "split")
            pushInto((LOEval) lo, pom); // add the computation specified by "lo" to this mapreduce
            // operator
            return pom;
        } else if (lo instanceof LOCogroup) {
            POMapreduce pom = new POMapreduce(pigContext, ((LOCogroup) lo).requestedParallelism);

            pom.toGroup = new GroupBySpecList(pigContext, ((LOCogroup) lo).specs);
            pom.partitionFunction = "hash";
            
            connectInputs(compiledInputs, pom);

            return pom;
        }  else if (lo instanceof LOLoad) {
            POMapreduce pom = new POMapreduce(pigContext, compiledInputs);
            LOLoad loLoad = (LOLoad) lo;
            String filename = FileLocalizer.fullPath(loLoad.filename);
            pom.inputFile = filename + ':' + loLoad.lf.getClass().getName()
                    + (loLoad.args == null ? "" : "(" + joinString(loLoad.args, ',') + ")");
            pom.toMap = addEval(pom.toMap, null);
            return pom;
        } else if (lo instanceof LORead) {
            IntermedResult readFrom = ((LORead) lo).readFrom;

            if (readFrom.executed()) {
                // result should already exist as a temp file
                POMapreduce pom = new POMapreduce(pigContext);
                if (readFrom.file == null) readFrom.toDFSFile(PlanCompiler.getTempFile(), BinStorage.class.getName());
                String inputFile = readFrom.file;
                if (inputFile.startsWith(FileLocalizer.HADOOP_PREFIX)) {
                    inputFile = inputFile.substring(FileLocalizer.HADOOP_PREFIX.length());
                }
                pom.inputFile = inputFile + ':' + (new BinStorage()).getClass().getName();
                return pom;
            } else {
                // compile other plan (idempotent)
                readFrom.compile(queryResults);

                // return root of other plan
                return (POMapreduce) readFrom.pp.root;
            }

        } else if (lo instanceof LOStore) {
            LOStore los = (LOStore) lo;
            compiledInputs[0].outputFile = los.filename;
            compiledInputs[0].storeFunc = los.sf;
            return compiledInputs[0];
        } else if (lo instanceof LOUnion) {
            POMapreduce pom = new POMapreduce(pigContext, ((LOUnion) lo).requestedParallelism);
            
            connectInputs(compiledInputs, pom);

            return pom;
        } else
            throw new IOException("Unknown logical operator.");
    }
    
    // added for UNION:
    // returns true iff okay to merge this operator with a subsequent binary op (e.g., co-group or union).
    // this is the case iff (1) this operator doesn't do grouping (which requires its own reduce phase), and (2) this operator isn't itself a binary op
    private boolean okayToMergeWithBinaryOp(POMapreduce mro) {
        return ((!mro.doesGrouping()) && (mro.numInputFiles() == 1));
    }
    
    private void connectInputs(POMapreduce[] compiledInputs, POMapreduce pom) throws IOException {
        // connect inputs (by merging operators, if possible; else connect via temp files)
        for (int i = 0; i < compiledInputs.length; i++) {
            if (okayToMergeWithBinaryOp(compiledInputs[i])) {
                // can merge input i with this operator
                pom.toMap = addEval(pom.toMap, compiledInputs[i].toMap);
                if (compiledInputs[i].numInputFiles() != 1)
                    throw new IOException(
                            "Error during compilation. Expected operator to have exactly one input file.");
                pom.inputFile = addFile(pom.inputFile, compiledInputs[i].inputFile);
                pom.addInputs(compiledInputs[i].inputs);
            } else {
                // chain together via a temp file
                String tempFile = getTempFile();
                compiledInputs[i].outputFile = tempFile;
                compiledInputs[i].storeFunc = BinStorage.class.getName() + "()";
                pom.inputFile = addFile(pom.inputFile, tempFile + ":BinStorage()");
                pom.addInput(compiledInputs[i]);

                pom.toMap = addEval(pom.toMap, null);
            }
        }
    }

    // push the function evaluated by "lo" into the map-reduce operator "mro"
    private void pushInto(LOEval lo, POMapreduce mro) throws IOException {

        if (!mro.doesGrouping()) { // push into "map" phase
            if (mro.toMap == null) {
                mro.toMap = new EvalSpecPipeList(pigContext);
                mro.toMap.add(new EvalSpecPipe(pigContext));
            }
            
            // changed for UNION:
            for (int index = 0; index < mro.toMap.list.size(); index++) {
                mro.toMap.list.get(index).add(lo.spec);
            }
            //int index = mro.toMap.list.size() - 1;
            //mro.toMap.list.get(index).add(lo.spec);

        } else { // push into "reduce" phase
            
            // use combiner, if amenable
            if (mro.toReduce == null && lo.spec.amenableToCombiner()) {
                mro.toCombine = new EvalSpecPipe(lo.pigContext);
                mro.toCombine.add(lo.spec.makeIntoToCombine());
              
                mro.toReduce = new EvalSpecPipe(lo.pigContext);
                mro.toReduce.add(lo.spec.makeIntoToReduce());
            } else {
                if (mro.toReduce == null) {
                    mro.toReduce = new EvalSpecPipe(lo.pigContext);
                }
                mro.toReduce.add(lo.spec); // otherwise, don't use combiner
            }

        }
    }

}
