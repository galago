/*
 * ExtentListWriter
 *
 * August 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.index;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.Parameters;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.Verified;
import galago.types.NumberWordCount;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import galago.types.DocumentExtent;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.DocumentExtent", order={"+extentName", "+document"})
@Verified
public class ExtentListWriter implements DocumentExtent.ExtentNameDocumentOrder.ShreddedProcessor {
    int blockSize = 32768;
    long minimumSkipListLength = 2048;
    int skipByteLength = 128;

    byte[] lastWord;
    long lastPosition = 0;
    long lastDocument = 0;
    
    public class ExtentList implements InvertedList {
        public ExtentList() {
            documentCount = 0;
            extentCount = 0;
            
            data = new CompressedByteBuffer();
            header = new CompressedByteBuffer();
            documentExtents = new CompressedByteBuffer();
        }
        
        public long dataLength() {
            long listLength = 0;
            
            listLength += header.length();
            listLength += data.length();
            
            return listLength;
        }

        public void write( final DataOutputStream output ) throws IOException {
            output.write( header.getBytes(), 0, header.length() );
            output.write( data.getBytes(), 0, data.length() );
        }

        public byte[] word() {
            return word;
        }
        
        public void setWord( byte[] word ) {
            this.word = word;
            this.lastDocument = 0;
            this.lastPosition = 0;
            this.extentCount = 0;
        }
        
        private void storePreviousDocument() {
            if( data.position > 0 ) {
                data.add( extentCount );
                data.add( documentExtents );
                documentExtents.clear();
            }
        }
        
        public void addDocument( long documentID ) {
            storePreviousDocument();
            
            data.add( documentID - lastDocument );
            lastDocument = documentID;
            
            lastStartExtent = 0;
            extentCount = 0;
            documentCount++;
        }
        
        public void addExtent( int begin, int end ) {
            extentCount++;
            
            documentExtents.add( begin - lastStartExtent );
            documentExtents.add( end - begin );
            lastStartExtent = begin;
        }
        
        public void close() {
            int options = 0;
            storePreviousDocument();
            
            header.add( options );
            header.add( documentCount );
        }
        
        private long lastDocument;
        private int lastStartExtent;
        private int lastPosition;
        private int extentCount;
        private int documentCount;
        
        public byte[] word;
        public CompressedByteBuffer header;
        public CompressedByteBuffer data;
        public CompressedByteBuffer documentExtents;
    }
    
    ExtentList invertedList;
    DataOutputStream output;
    long filePosition;
    
    InvertedListWriter writer;
    long documentCount = 0;
    long collectionLength = 0;
    Parameters header;
      
    /**
     * Creates a new instance of ExtentListWriter
     */
    public ExtentListWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        writer = new InvertedListWriter( parameters );
        header = parameters.getXML();
    }
    
    public void processExtentName( byte[] wordBytes ) throws IOException {
        if( invertedList != null ) {
            invertedList.close();
            writer.add( invertedList );
            invertedList = null;
        }

        invertedList = new ExtentList();
        invertedList.setWord( wordBytes );
        
        assert lastWord == null || 0 != Utility.compare( lastWord, wordBytes ) : "Duplicate word";
        lastWord = wordBytes;
    }
    
    public void processDocument( long document ) {
        invertedList.addDocument( document );
    }

    public void processTuple( int begin, int end ) throws IOException {
        invertedList.addExtent( begin, end );
    }
    
    public void close() throws IOException {
        if( invertedList != null ) {
            invertedList.close();
            writer.add( invertedList );
        }

        writer.close();
    }
    
    public Class<NumberWordCount> getInputClass() {
        return NumberWordCount.class;
    }
}
