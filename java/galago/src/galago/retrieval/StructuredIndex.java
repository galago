/*
 * StructuredIndex
 *
 * September 14, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.index.DocumentLengthsReader;
import galago.index.DocumentNameReader;
import galago.parse.Document;
import galago.retrieval.extents.CountIterator;
import galago.retrieval.extents.ExtentIterator;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author trevor
 */
public class StructuredIndex {
    DocumentLengthsReader documentLengths;
    DocumentNameReader documentNames;
    PositionIndexReader terms;
    ExtentIndexReader extents;
    //ScoreIndex scores;
    Parameters manifest;
    
    public StructuredIndex( String filename ) throws IOException {
        documentLengths = new DocumentLengthsReader(filename + File.separator + "documentLengths");
        documentNames = new DocumentNameReader(filename + File.separator + "documentNames");
        terms = new PositionIndexReader( filename + File.separator + "terms" );
        extents = new ExtentIndexReader( filename + File.separator + "extents" );
        manifest = new Parameters();
        manifest.parse( filename + File.separator + "manifest" );
    }
    
    public CountIterator getTermCounts( String text ) throws IOException {
        return terms.getTermCounts( text );
    }
    
    public ExtentIterator getTermExtents( String text ) throws IOException {
        return terms.getTermExtents( text );
    }
    
    public CountIterator getFieldCounts( String text ) throws IOException {
        return extents.getCounts( text );
    }
    
    public ExtentIterator getFieldExtents( String text ) throws IOException {
        return extents.getExtents( text );
    }
    
    List< Processor<Document> > transformations() {
        return terms.transformations();
    }

    public long getCollectionLength() {
        return manifest.get( "collectionLength", (long)0 );
    }
    
    public long getDocumentCount() {
        return manifest.get( "documentCount", (long)0 );
    }
    
    public void close() throws IOException {
        terms.close();
    }

    public int getLength(int document) {
        return documentLengths.getLength(document);
    }

    public String getDocument(int document) {
        return documentNames.get(document);
    }
}
