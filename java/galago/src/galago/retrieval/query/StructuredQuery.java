/*
 * StructuredQuery
 *
 * August 10, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.query;

import galago.tupleflow.Parameters;
import java.util.ArrayList;

/**
 * Valid query language syntax:
 *
 * #operator:argument( ... )
 * term, or term.field, or term.field.field, etc.
 *
 * @author trevor
 */
public class StructuredQuery {

    /**
     * Copies a query tree using a traversal object.
     *
     * Both traversal.beforeNode and traversal.afterNode will be called
     * for every Node object in the query tree.
     * The traversal.beforeNode method will be called with a parent node
     * before any of its children (pre-order), while traversal.afterNode method
     * will be called on the parent after all of its children (post-order).
     *
     * The afterNode method should return a new copy of the node, using the
     * provided array of children nodes.
     */

    public static Node copy(Traversal traversal, Node tree) throws Exception {
        ArrayList<Node> children = new ArrayList<Node>();
        traversal.beforeNode(tree);

        for (Node n : tree.getInternalNodes()) {
            Node child = copy(traversal, n);
            children.add(child);
        }

        return traversal.afterNode(tree, children);
    }

    /**
     * Walks a query tree with a traversal object.
     *
     * Both traversal.beforeNode and traversal.afterNode will be called
     * for every Node object in the query tree.
     * The traversal.beforeNode method will be called with a parent node
     * before any of its children (pre-order), while traversal.afterNode method
     * will be called on the parent after all of its children (post-order).
     */

    public static void walk(Traversal traversal, Node tree) throws Exception {
        traversal.beforeNode(tree);

        for (Node n : tree.getInternalNodes()) {
            walk(traversal, n);
        }
        traversal.afterNode(tree, null);
    }

    /**
     * Splits an input string, which may include escapes,
     * into chunks based on a delimiter character.  It does not split
     * on delimiters that appear in escaped regions.
     */

    public static ArrayList<String> splitOn(String text, char delimiter) {
        int start = 0;
        ArrayList<String> result = new ArrayList<String>();

        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);

            if (c == '@') {
                i = StructuredQuery.findEscapedEnd(text, i);
            } else if (c == delimiter) {
                result.add(text.substring(start, i));
                start = i + 1;
            }
        }

        if (start != text.length()) {
            result.add(text.substring(start));
        }

        return result;
    }

    /**
     * <p>Parses an operator from the string query.  This method assumes that
     * operator is a pound sign ('#') followed by some text, followed by an open 
     * parentheses.  Parsing stops at the parenthesis.</p>
     *
     * <p>Note that parsing starts at index 0, not at "offset".  The offset is used purely
     * for giving parse error information, and represents the offset of the operator string
     * in the larger query string.</p>
     */

    public static Node parseOperator(String operator, int offset) {
        assert operator.charAt(0) == '#';
        int firstParen = operator.indexOf('(');

        if (firstParen < 0) {
            return new Node("unknown", new ArrayList<Node>(), offset);
        }

        String operatorText = operator.substring(0, firstParen);
        ArrayList<String> operatorParts = splitOn(operatorText.substring(1), ':');

        String operatorName = operatorParts.get(0);
        Parameters parameters = new Parameters();

        for (String part : operatorParts.subList(1, operatorParts.size())) {
            ArrayList<String> keyValue = splitOn(part, '=');

            if (keyValue.size() == 1) {
                parameters.add("default", decodeEscapes(part));
            } else if (keyValue.size() > 1) {
                String key = keyValue.get(0);
                String value = keyValue.get(1);
                parameters.add(decodeEscapes(key), decodeEscapes(value));
            }
        }

        int endOperator = operator.length();

        if (operator.charAt(operator.length() - 1) == ')') {
            endOperator--;
        }

        ArrayList<Node> children = parseArguments(operator.substring(firstParen + 1, endOperator), offset + firstParen + 1);
        return new Node(operatorName, parameters, children, offset);
    }

    /**
     * Find the end of an escaped query region.
     * We assume that query.charAt(start) == '@'.  The
     * next charater is the boundary symbol for the escaped text.
     * We move forward in the string until we see the boundary symbol again.
     * If the escaped region isn't well formed (that is, there is no 
     * initial boundary symbol, or we only see the boundary symbol once),
     * query.length() is returned.
     */

    public static int findEscapedEnd(String query, int start) {
        assert query.charAt(start) == '@';

        // guard against the error case
        if (query.length() < start + 2) {
            return query.length();
        }
        char doneChar = query.charAt(start + 1);
        int result = query.indexOf(doneChar, start + 2);

        if (result < 0) {
            return query.length();
        }
        return result;
    }

    /**
     * Find the end of an operator.  We assume that query.charAt(start)
     * is a pound sign.  We skip forward in the query looking for
     * the end of the operator by looking at parentheses; we know we're
     * done when the parentheses are balanced and we've seen at least
     * one open parenthesis.  This method skips over escaped regions.
     */

    public static int findOperatorEnd(String query, int start) {
        int i = start;
        int open = 0;
        int closed = 0;

        for (; i < query.length() && (open != closed || open == 0); i++) {
            char current = query.charAt(i);

            if (current == '(') {
                open++;
            } else if (current == ')') {
                closed++;
            } else if (current == '@') {
                i = findEscapedEnd(query, i);
            }
        }

        return i;
    }

    public static int findTermEnd(String query, int start) {
        int i = start;

        for (; i < query.length(); i++) {
            char current = query.charAt(i);

            if (Character.isWhitespace(current)) {
                break;
            } else if (current == '@') {
                i = findEscapedEnd(query, i);
            }
        }

        return i;
    }

    public static String decodeEscapes(String escapedString) {
        StringBuilder builder = new StringBuilder();
        char escapeChar = ' ';
        boolean inEscape = false;

        for (int i = 0; i < escapedString.length(); i++) {
            char current = escapedString.charAt(i);

            if (!inEscape && current == '@' && i + 1 < escapedString.length()) {
                escapeChar = escapedString.charAt(i + 1);
                inEscape = true;
                i += 1;
            } else if (inEscape && current == escapeChar) {
                inEscape = false;
            } else {
                builder.append(escapedString.charAt(i));
            }
        }

        return builder.toString();
    }

    public static Node parseTerm(String query, int offset) {
        // step 1, split at periods
        int i = 0;
        int start = 0;
        ArrayList<String> chunks = new ArrayList<String>();

        for (i = 0; i < query.length(); i++) {
            char current = query.charAt(i);

            if (current == '.') {
                chunks.add(query.substring(start, i));
                start = i + 1;
            } else if (current == '@') {
                i = findEscapedEnd(query, i);
            }
        }

        if (start < query.length()) {
            chunks.add(query.substring(start));
        }
        // step 2, decode the chunks
        Node result = new Node("text", decodeEscapes(chunks.get(0)), offset);

        for (i = 1; i < chunks.size(); i++) {
            ArrayList<Node> children = new ArrayList<Node>();
            children.add(result);
            result = new Node("infield", decodeEscapes(chunks.get(1)), children, offset);
        }

        return result;
    }

    public static ArrayList<Node> parseArguments(String query, int offset) {
        ArrayList<Node> arguments = new ArrayList<Node>();
        int start = 0;
        boolean inElement = false;

        // scan the string, looking for tokens.  Tokens are either operators (#\w+( ... )), words, or escaped.
        for (int i = 0; i < query.length(); i++) {
            char current = query.charAt(i);

            if (!Character.isWhitespace(current)) {
                if (current == '#') {
                    // this is an operator, so scan for balanced parentheses,
                    // not including escaped regions
                    int end = findOperatorEnd(query, i);
                    Node child = parseOperator(query.substring(i, end), offset + i);
                    arguments.add(child);
                    i = end;
                } else {
                    int end = findTermEnd(query, i);
                    Node child = parseTerm(query.substring(i, end), offset + i);
                    arguments.add(child);
                    i = end;
                }
            }
        }

        // we're at the end of the string
        if (inElement) {
            Node child = new Node("text", query.substring(start), offset + start);
            arguments.add(child);
        }

        return arguments;
    }

    public static Node parse(String query) {
        ArrayList<Node> arguments = parseArguments(query, 0);

        if (arguments.size() == 1) {
            return arguments.get(0);
        } else {
            return new Node("combine", "", arguments, 0);
        }
    }
}
