/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.parser.ParseException;
import com.yahoo.pig.impl.logicalLayer.parser.QueryParser;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

public abstract class EvalItem {

	protected PigContext pigContext;
	protected EvalItem(PigContext pigContext) {
		this.pigContext = pigContext;
	}
	
	public SchemaItem schema = null;
	
    public abstract void eval(Tuple input, DataCollector output) throws IOException;

    public abstract Datum simpleEval(Tuple input) throws IOException;

    public abstract List<String> getFuncs();

    public abstract boolean isSimple();

    public abstract String stringVal(Tuple tup) throws IOException;

    public abstract Double doubleVal(Tuple tup) throws IOException;

    public abstract SchemaItem mapInputSchema(SchemaItem input);

    public void finish(){}
    
    /**
     * Creates a copy of this eval item
     */
    public EvalItem copy() throws ParseException {
        EvalItem item;
    	ByteArrayInputStream in = new ByteArrayInputStream(toString().getBytes());
        QueryParser parser = new QueryParser(in, pigContext, null);
        item = parser.PEvalItem();
        return item;
    }
    
    
}
