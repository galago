/*
 * ErrorHandler
 *
 * August 21, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

/**
 *
 * @author trevor
 */
public interface ErrorHandler {
    public void addError( String errorString );
    public void addWarning( String warningString );
}
