/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.Tuple;

public class OrCond extends Cond {
    public List<Cond> cList;
    
    public OrCond(List<Cond> cList) {
        this.cList = cList;
    }

    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        for (Iterator<Cond> it = cList.iterator(); it.hasNext(); ) {
            funcs.addAll(it.next().getFuncs());
        }
        return funcs;
    }
    
    public boolean eval(Tuple input) throws IOException {
        for (Iterator<Cond> it = cList.iterator(); it.hasNext(); ) {
            if (it.next().eval(input) == true) return true;
        }
        return false;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("(");
        for (Iterator<Cond> it = cList.iterator(); it.hasNext(); ) {
            sb.append(it.next());
            if (it.hasNext()) sb.append(" OR ");
        }
        sb.append(")");
        return sb.toString();
    }
}
