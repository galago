/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.InputSplit;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.parser.QueryParser;

public class PigSplit implements InputSplit {
    Path   file;
    long start;
    long length;
    String groupFunc;
    String applyFunc;
    int    index;
    String parser;
    FileSystem fs;

    public PigSplit() {}

    public PigSplit(FileSystem fs, Path path, String parser, String groupFunc, String mapFuncs, int index, long start, long length) {
	this.fs = fs;
	this.file = path;
	this.start = start;
	this.length = length;
	this.groupFunc = groupFunc;
	this.applyFunc = mapFuncs;
	this.index = index;
	this.parser = parser;
    }

    public String getParser() {
        return parser;
    }

    public long getStart() {
	return start;
    }
    
    public long getLength() {
	return length;
    }
    public String getGroupFunction() {
        return groupFunc;
    }

    public String getApplyFunction() {
        return applyFunc;
    }

    public Path getPath() {
        return file;
    }

    public int getIndex() {
        return index;
    }

    public StorageFunc getLoadFunction() {
        StorageFunc loader = null;
        if (this.parser == null) {
            loader = new PigStorage();
        } else {
            try {
                loader = (StorageFunc) PigContext.instantiateArgFunc(this.parser);
            }catch(Exception exp) {
                exp.printStackTrace();
                loader = new PigStorage();
            }
        }
        return loader;
    }


    public String[] getLocations() throws IOException {
	String hints[][] = fs.getFileCacheHints(file, start, length);
	int total = 0;
	for(int i = 0; i < hints.length; i++) {
	    total += hints[i].length;
	}
	String locations[] = new String[total];
	int count = 0;
	for(int i = 0; i < hints.length; i++) {
	    for(int j = 0; j < hints[i].length; j++) {
		locations[count++] = hints[i][j];
	    }
	}
	return locations;
    }

    public void readFields(DataInput is) throws IOException {
	file = new Path(is.readUTF());
	start = is.readLong();
	length = is.readLong();
	groupFunc = is.readUTF();
	applyFunc = is.readUTF();
	index = is.readInt();
	parser = is.readUTF();
    }

    public void write(DataOutput os) throws IOException {
	os.writeUTF(file.toString());
	os.writeLong(start);
	os.writeLong(length);
	os.writeUTF(groupFunc);
	os.writeUTF(applyFunc);
	os.writeInt(index);
	os.writeUTF(parser);
    }
}
