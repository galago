/*
 * Extent
 *
 * August 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

/**
 *
 * @author trevor
 */
public class Extent {
    public float weight;
    public int document;
    public int begin;
    public int end;

    public int compareTo( Extent other ) {
        return other.document - document;
    }
    
    public boolean contains( Extent other ) {
        return begin <= other.begin && end >= other.end;
    }
}
