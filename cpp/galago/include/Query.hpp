
//
// Query
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#ifndef GALAGO_QUERY_HPP
#define GALAGO_QUERY_HPP

class QueryTerm {    
public:
    std::string text;
    std::string field;
    double weight;
    
    QueryTerm( const std::string& _text ) {
        weight = 1.0;
        field = "";
        text = _text;
    }
    
	QueryTerm( const std::string& _text, const std::string& _field, double _weight ) {
        text = _text;
        field = _field;
        weight = _weight;
    }
    
	std::string toString() {
        std::string term;
        
        // if this is a multi-word query, enclose it in quotes
        if( text.find(' ') > 0 )
            term = "\"" + text + "\"";
        else
            term = text;
        
        // use the minimum amount of syntax necessary to
        // express the query.  If everything is specified, 
        // the format is field:term^weight. 
        std::stringstream out;
        
        if( field.size() && weight != 1.0 ) {
            out << field
                << ':'
                << term
                << '^'
                << weight;
        } else if( field.size() ) {
            out << field
                << ':'
                << term;
        } else if( weight != 1.0 ) {
            out << term
                << '^'
                << weight;
        } else {
            out << term;
        }               
        
        return out.str();
    }
};
       
class Query {
public:
    /** 
     * The format of the query term is <tt>field:term^weight</tt>.
     * Both the field and the weight are optional, and the term may
     * be enclosed in quotes.
     *
     * @return A QueryTerm object describing the query term.
     */
    
     static QueryTerm parseQueryTerm( const std::string& t ) {
        double weight = 1.0;
        std::string field;
		std::string term = t;
        
        int colon = t.find(':');
        if( colon >= 0 ) {
            field = t.substr( 0, colon );
            term = t.substr( colon+1 );
        }
        
        int caret = term.find('^');
        if( caret >= 0 ) {
            weight = atof(term.substr(caret+1).c_str());
            term = term.substr(0, caret);
        }
        
        if( term.size() > 0 && term[0] == '"' )
            term = term.substr(1);
        
        if( term.size() > 0 && term[term.length()-1] == '"' )
            term = term.substr(0, term.length()-1);
        
        return QueryTerm( term, field, weight );
    }
        
    static std::vector<std::string> textQueryTerms( const std::string& query ) {
        bool inQuote = false;
        int firstNonSpace = query.length()+1;
        int i=0;
        std::vector<std::string> results;

        // each loop parses a single term
        while( i < query.length() ) {
            // parsing goes in two phases; first we're trying to bypass inital
            // spaces before a query term.  after that point, we parse until the
            // next space that's not in quotes.
            for( ; i < query.length(); i++ ) {
                char c = query[i];
                
                if( c == ' ' || c == '\t' || c == '\n' || c == '\r' ) {
                    if( !inQuote ) {
                        if( firstNonSpace < i ) {
                            std::string term = query.substr(firstNonSpace, i-firstNonSpace);
                            results.push_back( term );
                        }
                        firstNonSpace = query.length()+1;
                    }
                } else if( c == '"' ) {
                    firstNonSpace = std::min(firstNonSpace, i);
                    inQuote = !inQuote;
                } else {
                    firstNonSpace = std::min(firstNonSpace, i);
                }
            }
        }
        
        if( firstNonSpace < query.length() ) {
            results.push_back( query.substr(firstNonSpace, query.length()) );
        }
        
        return results;
    }
       
    static std::vector<QueryTerm> parse( const std::string& query ) {
        std::vector<QueryTerm> results;
        
        std::vector<std::string> textTerms = textQueryTerms( query );
        std::vector<QueryTerm> parsedTerms;
        
        for( int i=0; i<textTerms.size(); i++ ) {
            parsedTerms.push_back( parseQueryTerm( textTerms[i] ) );
        }
        
        return parsedTerms;
    }
};

#endif // GALAGO_QUERY_HPP

