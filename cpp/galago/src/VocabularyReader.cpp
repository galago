
//
// VocabularyReader
//
// 8 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "VocabularyReader.hpp"
#include "indri/File.hpp"

VocabularyReader::~VocabularyReader() {
    for( int i=0; i<slots.size(); i++ ) {
        delete[] slots[i].term;
    }
}    

void VocabularyReader::read( const std::string& fileName, UINT64 invertedFileLength ) {
    UINT64 last = 0; 
    indri::file::File input;
    
    input.openRead( fileName );
    indri::file::FileMapping* mapping = input.mapFileRead(); 
    const UINT8* region = (const UINT8*) mapping->region;
    const UINT8* end = region + mapping->length;
    
    while( region < end ) {
        UINT16 length = UncompressedRead::read_u16( region );

        char* data = new char[length+1]; 
        memcpy( data, region, length );
        data[length] = 0;
        region += length;

        UINT64 offset = UncompressedRead::read_u64( region );
        TermSlot slot;
        
        if( slots.size() > 0 )
            slots[slots.size()-1].length = offset - last;
        
        slot.begin = offset;
        slot.term = data;
        slots.push_back(slot);
        last = offset;
    }
    
    if( slots.size() > 0 )
        slots[slots.size()-1].length = invertedFileLength - last;
               
    delete mapping;
    input.close();
    
    assert( invertedFileLength >= last );
}

const VocabularyReader::TermSlot& VocabularyReader::operator[] ( const std::string& term ) const {
    return get(term);
}

const VocabularyReader::TermSlot& VocabularyReader::get( const std::string& term ) const {
    int big = slots.size()-1;
    int small = 0;
    
    while( big-small > 1 ) {
        int middle = small + (big-small)/2;
        const char* middleTerm = slots[middle].term;
        
        if ( strcmp( middleTerm, term.c_str() ) <= 0)
            small = middle;
        else
            big = middle;
    }

    const TermSlot& one = slots[small];
    const TermSlot& two = slots[big];
    
    if ( strcmp( two.term, term.c_str() ) <= 0)
        return two;
    else
        return one;
}
