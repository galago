/*
 * OrderedWindowIteratorTest.java
 * JUnit based test
 *
 * Created on September 13, 2007, 7:00 PM
 */

package galago.retrieval.extents;

import galago.tupleflow.Parameters;
import galago.util.ExtentArray;
import java.io.IOException;
import java.util.ArrayList;
import junit.framework.*;

/**
 *
 * @author trevor
 */
public class OrderedWindowIteratorTest extends TestCase {
    
    public OrderedWindowIteratorTest(String testName) {
        super(testName);
    }
    
    public void testPhrase() throws IOException {
        System.out.println("testPhrase");
        
        int[][] dataOne = { { 1, 3 } };
        int[][] dataTwo = { { 1, 4 } };
        FakeExtentIterator one = new FakeExtentIterator( dataOne );
        FakeExtentIterator two = new FakeExtentIterator( dataTwo );
        ArrayList<DocumentDataIterator> iters = new ArrayList<DocumentDataIterator>();
        
        iters.add( one );
        iters.add( two );
        
        Parameters oneParam = new Parameters();
        oneParam.add( "width", "1" );
        OrderedWindowIterator instance = new OrderedWindowIterator( oneParam, iters );
        
        ExtentArray array = instance.extents();

        assertEquals( array.getPosition(), 1 );
        assertEquals( array.getBuffer()[0].document, 1 );
        assertEquals( array.getBuffer()[0].begin, 3 );
        assertEquals( array.getBuffer()[0].end, 5 );
    }

    public void testUnordered() throws IOException {
        System.out.println("testUnordered");
        
        int[][] dataOne = { { 1, 3 } };
        int[][] dataTwo = { { 1, 4 } };
        FakeExtentIterator one = new FakeExtentIterator( dataOne );
        FakeExtentIterator two = new FakeExtentIterator( dataTwo );
        ArrayList<DocumentDataIterator> iters = new ArrayList<DocumentDataIterator>();
        
        iters.add( two );
        iters.add( one );
        
        Parameters oneParam = new Parameters();
        oneParam.add( "width", "1" );
        OrderedWindowIterator instance = new OrderedWindowIterator( oneParam, iters );
        
        ExtentArray array = instance.extents();
        assertEquals( array.getPosition(), 0 );
    }
}
