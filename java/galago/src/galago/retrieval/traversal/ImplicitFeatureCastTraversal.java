/*
 * ImplicitFeatureCastTraversal
 * 
 * September 20, 2007 -- Trevor Strohman
 * 
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.traversal;

import galago.retrieval.StructuredRetrieval;
import galago.retrieval.query.Node;
import galago.retrieval.query.Traversal;
import java.util.ArrayList;

/**
 * For many kinds of queries, it may be preferable to not have to type
 * an explicit #feature operator around a count or extents term.  For example,
 * we want #combine( #feature:dirichlet( #counts:dog() ) ) to be the same as
 * #combine(dog).  This transformation automatically adds the #feature:dirichlet
 * operator.
 * 
 * @author trevor
 */
public class ImplicitFeatureCastTraversal implements Traversal {
    StructuredRetrieval retrieval;
    
    public ImplicitFeatureCastTraversal( StructuredRetrieval retrieval ) {
        this.retrieval = retrieval;
    }

    public void beforeNode(Node node) throws Exception {
    }
    
    public Node afterNode(Node node, ArrayList<Node> children) throws Exception {
        ArrayList<Node> newChildren = new ArrayList<Node>();
        
        if( retrieval.requiresScoreChildren(node) ) {
            for( Node child : children ) {
                if( !retrieval.isScoreNode(child) ) {
                    ArrayList<Node> data = new ArrayList<Node>();
                    data.add( child );
                    
                    Node feature = new Node( "feature",
                                             "dirichlet",
                                             data,
                                             child.getPosition() );
                    newChildren.add( feature );
                } else {
                    newChildren.add( child );
                }
            }
        }

        return new Node( node.getOperator(),
                         node.getParameters(),
                         newChildren,
                         node.getPosition() );
    }
}
