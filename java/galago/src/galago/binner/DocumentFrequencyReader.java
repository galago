
//
// DocumentFrequencyReader.java
//
// Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.binner;

import java.io.IOException;
import java.util.HashMap;
import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.types.WordCount;

public class DocumentFrequencyReader implements Processor<WordCount> {
    HashMap<String, Long> df = new HashMap<String, Long>();
    long maximumDocumentFrequency = 0;
    
    public void process(WordCount count) {
        if( !df.containsKey(count.word) )
            df.put( count.word, new Long(count.documents) );
    }
    
    public void close() {}
    
    public HashMap<String, Long> getDF() {
        return df;
    }
    
    public void setMaximumDocumentFrequency(final long maxCount) {
        maximumDocumentFrequency = maxCount;
    }
    
    public long maximumDocumentFrequency() {
        return maximumDocumentFrequency;
    }
    
    @SuppressWarnings("unchecked")
    public static DocumentFrequencyReader read( TupleFlowParameters parameters ) throws IOException {
        DocumentFrequencyReader dfReader = new DocumentFrequencyReader();
        TypeReader<WordCount> lmReader = parameters.getTypeReader( "lm" );
        TypeReader<WordCount> countsReader = parameters.getTypeReader( "counts" );
        filteredRead( lmReader, countsReader, dfReader );
        return dfReader;
    }

    private static void filteredRead(final TypeReader<WordCount> lmReader, final TypeReader<WordCount> countsReader, final DocumentFrequencyReader cfReader) throws IOException {
        WordCount lmCount = lmReader.read();
        WordCount cCount = countsReader.read();
        long maxCount = 0;
        
        while( lmCount != null && cCount != null ) {
            maxCount = Math.max( maxCount, lmCount.count );
            int comparison = lmCount.word.compareTo(cCount.word);
            
            if( comparison == 0 ) {
                cfReader.process( lmCount );
                
                lmCount = lmReader.read();
                cCount = countsReader.read();
            } else if( comparison < 0 ) {
                lmCount = lmReader.read();
            } else {
                // cCount < lmCount
                cfReader.process( cCount );
                cCount = countsReader.read();
            }
        }
        
        cfReader.setMaximumDocumentFrequency(maxCount);
    }
    
    public Class<WordCount> getInputClass() {
        return WordCount.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !parameters.readerExists( "counts", "galago.types.WordCount", new String[] { "+word" } ) )
            handler.addError( "DocumentFrequencyReader requires a 'counts' parameter, sorted by word." );
        if( !parameters.readerExists( "lm", "galago.types.WordCount", new String[] { "+word" } ) )
            handler.addError( "DocumentFrequencyReader requires a 'lm' parameter, sorted by word." );
    }
}