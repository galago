/*
 * DocumentDataIterator.java
 * 
 * September 12, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

/**
 * This is a marker interface that represents any kind of
 * iterator over an inverted list or query operator.
 * 
 * @author trevor
 */
public interface DocumentDataIterator {
}
