/*
 * PostingsCounter
 *
 * March 22, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentWordCount;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.types.DocumentWordCount")
public class PostingsCounter extends StandardStep<Document, DocumentWordCount> {
    HashMap<String, String> common = new HashMap<String, String>();
    
    public void process( Document document ) throws IOException {
        HashMap<String, DocumentWordCount> countObjects = new HashMap<String, DocumentWordCount>();

        // since the tokens are in sorted order, we can now count them easily
        for( String token : document.terms ) {
            DocumentWordCount wc = countObjects.get(token);
            
            if (wc == null) {
                // we copy the string explicitly here because token is almost certainly
                // a substring of the full document text.  copying the string here allows
                // the garbage collector to eventually collect the document text.
                wc = new DocumentWordCount( document.identifier, token, 1 );
                countObjects.put(token, wc);
            } else {
                wc.count += 1;
            }
        }
        
        for( DocumentWordCount wc : countObjects.values() ) {
            processor.process(wc);
        }
    }
}
