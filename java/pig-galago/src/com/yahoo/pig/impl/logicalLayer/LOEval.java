/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.List;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class LOEval extends LogicalOperator {
    public EvalSpecPipe spec;

    public LOEval(PigContext pigContext, LogicalOperator input, EvalSpecPipe specIn) {
    	super(pigContext);
        inputs = new LogicalOperator[1];
        inputs[0] = input;
        spec = specIn;
        getOutputType();
    }

    public String name() {
        return "Eval";
    }

    public String arguments() {
        return spec.toString();
    }

    public SchemaItemList outputSchema() {
        if (schema == null) {
            //System.out.println("LOEval input: " + inputs[0].outputSchema());
            //System.out.println("LOEval spec: " + spec);
            schema = (SchemaItemList)spec.mapInputSchema(inputs[0].outputSchema());
            
            //System.out.println("LOEval output: " + schema);
        }
        schema.alias = alias;
        return schema;
    }

    public int getOutputType() {
        switch (inputs[0].getOutputType()) {
        case FIXED:
            return FIXED;
        case MONOTONE:
        case AMENDABLE:
            return MONOTONE;
        default:
            throw new RuntimeException("Wrong type of input to EVAL");
        }
    }

    public List<String> getFuncs() {
        List<String> funcs = super.getFuncs();
        funcs.addAll(spec.getFuncs());
        return funcs;
    }
}
