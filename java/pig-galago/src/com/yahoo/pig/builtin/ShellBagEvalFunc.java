/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import com.yahoo.pig.BagEvalFunc;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;

public class ShellBagEvalFunc extends BagEvalFunc {
	byte groupDelim = '\n';
	byte recordDelim = '\n';
	byte fieldDelim = '\t';
	String fieldDelimString = "\t";
	OutputStream os;
	InputStream is;
	InputStream es;
	String cmd;
	
	LinkedBlockingQueue<DataCollector> bags = new LinkedBlockingQueue<DataCollector>();
	
	
	public ShellBagEvalFunc(String cmd) {
		this.cmd = cmd;
	}

	private class EndOfQueue extends DataCollector{
		public void add(Tuple t){}
	}
	
	private void startProcess() throws IOException {
		Process p = Runtime.getRuntime().exec(cmd);
		is = p.getInputStream();
		os = p.getOutputStream();
		es = p.getErrorStream();
		
		/*
		new Thread() {
			public void run() {
				byte b[] = new byte[256];
				int rc;
				try {
					while((rc = es.read(b)) > 0) {
						System.err.write(b, 0, rc);
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
		*/
		
		new Thread() {
			public void run() {
				while(true){
					DataCollector dc = bags.poll();
					if (dc instanceof EndOfQueue)
						break;
					try {
						readBag(dc);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	@Override
	public void exec(Tuple input, DataCollector output) throws IOException {
		if (os == null) {
			startProcess();
		}
		os.write(input.toDelimitedString(fieldDelimString).getBytes());
		os.write(recordDelim);
		os.write(groupDelim);
		os.flush();
		try{
			bags.put(output);
		}catch(InterruptedException e){}
		
		//Since returning before ensuring that output is present
		output.markStale(true);
		
	}
	
	public void finish(){
		try{
			os.close();
			try{
				bags.put(new EndOfQueue());
			}catch(InterruptedException e){}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private void readBag(DataCollector output) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		boolean inRecord = false;
		int c;
		while((c = is.read()) != -1) {
			System.out.print(((char)c));
			if ((inRecord == false) && (c == groupDelim)) {
				output.add(null);
				output.markStale(false);
				return;
			}
			inRecord = true;
			if (c == recordDelim) {
				inRecord = false;
				Tuple t = new Tuple(baos.toString(), fieldDelimString);
				System.err.println(Thread.currentThread().getName() + ": Adding tuple " + t + " to collector " + output);
				output.add(t);
				baos = new ByteArrayOutputStream();
				continue;
			}
			baos.write(c);
		}
	}
}
