/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;

import java.io.IOException;

import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

/**
 * The class is used to implement functions that generate an array of Datums to represent 
 * the group of a given tuple. The group can be thought of as the "key", in Map-Reduce 
 * terminology.
 * 
 * The programmer should not make assumptions about state maintained
 * between invocations of the invoke() method since the Pig runtime
 * will schedule and localize invocations based on information provided
 * at runtime.
 * 
 * @author database-systems@yahoo.research
 *
 */
public abstract class GroupFunc extends InstantiatedFunc {
    
	/**
	 * This callback method must be implemented by all subclasses. This method
	 * defines the group that will be used by the grouping operator.
	 * Since the dataset may be divided up in a variety of ways the programmer
	 * should not make assumptions about state that is maintained between
	 * invocations of this method.
	 * 
	 * @param input the tuple to be processed.
	 * @return the group that the specified tuple belongs to.
	 * @throws IOException
	 */
	abstract public Datum[] exec(Tuple input);

	public String name() { return this.getClass().getName();}
    /**
     * Returns the String representation of the function. It will
     * have a function syntax: name(ColumnList).
     */
	public String toString() {
		return this.name();
	}
	
	public SchemaItem outputSchema(SchemaItem input){
		return input;
	}
}
