/*
 * PostingsReducer
 *
 * May 5, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Sorts;
import galago.tupleflow.Reducer;
import galago.types.DocumentLengthWordCount;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author trevor
 */
public class PostingsReducer implements Reducer<DocumentLengthWordCount> {
    public ArrayList<DocumentLengthWordCount> reduce(List<DocumentLengthWordCount> input) throws IOException {
        Collections.sort(input, new DocumentLengthWordCount().getOrder( "+document", "+word" ).lessThan() );
        List<DocumentLengthWordCount> sorted = input;

        DocumentLengthWordCount o = null;
        ArrayList<DocumentLengthWordCount> newList = new ArrayList<DocumentLengthWordCount>();
        
        for( DocumentLengthWordCount wc : input ) {
            if( o != null && wc.word.equals(o.word) && wc.document.equals(o.document) ) {
                o.count += wc.count;
            } else {
                if( o != null )
                    newList.add(o);
                o = wc;
            }
        }

        return newList;
    }
}
