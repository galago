
//
// Retrieval.java
//
// 9 June 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.retrieval;

import galago.retrieval.query.Node;
import galago.tupleflow.Parameters;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public abstract class Retrieval {
    public abstract String getDocument(int document) throws IOException;
    public abstract ScoredDocument[] runQuery(Node query, int requested) throws Exception;
    public abstract void close() throws IOException;
    
    static Retrieval instance( String indexPath ) throws IOException {
        Parameters parameters = new Parameters( new File(indexPath + File.separator + "manifest") );
        String type = parameters.get( "indexType", "structured" );
        
        if( type.equals( "documentOrdered" ) ) {
            return new DocumentOrderedBinnedMaxScoreRetrieval( indexPath );
        } else if( type.equals( "binOrdered" ) ) {
            return new BinOrderedBinnedRetrieval( indexPath );
        } else {
            return new StructuredRetrieval( indexPath );
        }
    }
}
