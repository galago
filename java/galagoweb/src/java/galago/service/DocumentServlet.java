package galago.service;
/*
 * DocumentServlet
 *
 * June 8, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

import galago.parse.Document;
import galago.store.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author trevor
 */
public class DocumentServlet extends HttpServlet {
    DocumentStore store;
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException { 
        String identifier = request.getParameter( "identifier" );
        Document document;
        
        if( identifier == null ) {
            response.sendError( response.SC_BAD_REQUEST, "'identifier' is a required parameter." );
            return;
        }
        try {
            document = store.get( "identifier", identifier );
        } catch( SQLException e ) {
            response.sendError( response.SC_INTERNAL_SERVER_ERROR );
            return;
        }

        if( document.metadata.containsKey( "url" ) )
            response.setContentType( "text/html" );
        else
            response.setContentType( "text/plain" );
        PrintWriter out = response.getWriter();
            
        out.write( document.text );
        out.close();
    }

    public void destroy() {
        try {
            store.close();
        } catch( Exception e ) {}
        super.destroy();
    }

    public void init(ServletConfig config) throws ServletException {
        try {
            String driverName = config.getInitParameter( "driverName" );
            String jdbcUrl = config.getInitParameter( "databaseUrl" );

            store = new DocumentStore( driverName, jdbcUrl );
        } catch( SQLException e ) {
            throw new ServletException(e);
        } catch( ClassNotFoundException e ) {
            throw new ServletException(e);
        }
    }
}
