/*
 * UnorderedWindowIterator
 *
 * April 2, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import galago.Utility;
import galago.tupleflow.Parameters;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */

@InputIterator(className="galago.retrieval.extents.ExtentIterator")
public class UnorderedWindowIterator extends ExtentConjunctionIterator {
    int width;
    boolean overlap;
    
    /** Creates a new instance of UnorderedWindowIterator */
    public UnorderedWindowIterator( Parameters parameters, ArrayList<DocumentDataIterator> extentIterators ) throws IOException {
        super( Utility.checkedCast(extentIterators, ExtentIterator.class) );
        this.width = (int) parameters.get( "width", -1 );
        this.overlap = parameters.get( "overlap", false );
        findDocument();
    }
    
    public void loadExtents() {
        extents.reset();
        
        ExtentArrayIterator[] iterators;
        int maximumPosition = 0;
        int minimumPosition = Integer.MAX_VALUE;
        
        // someday this will be a heap/priorityQueue for the overlapping case
        iterators = new ExtentArrayIterator[extentIterators.size()];
        
        for( int i=0; i<extentIterators.size(); i++ ) {
            iterators[i] = new ExtentArrayIterator(extentIterators.get(i).extents());
        
            minimumPosition = Math.min( iterators[i].current().begin, minimumPosition );
            maximumPosition = Math.max( iterators[i].current().end, maximumPosition );
        }
        
        do {
            // try to emit an extent here, but only if the width is small enough
            if( maximumPosition - minimumPosition <= width )
                extents.add( document, minimumPosition, maximumPosition );
            
            // reset the minimumPosition
            minimumPosition = Integer.MAX_VALUE;
            
            // now, move forward
            for( int i=0; i<iterators.length; i++ ) {
                int currentBegin = iterators[i].current().begin;
                
                if( !overlap || currentBegin == minimumPosition ) {
                    boolean result = iterators[i].next();
                    
                    if (!result)
                        return;
                    
                    minimumPosition = Math.min( minimumPosition, iterators[i].current().begin );
                    maximumPosition = Math.max( maximumPosition, iterators[i].current().end );
                }
            }
        } while(true);
    }
}
