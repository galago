/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputFormat;
import org.apache.hadoop.mapred.RecordWriter;
import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.util.Progressable;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.parser.QueryParser;

public class PigOutputFormat implements OutputFormat {
    public static final String PIG_OUTPUT_FUNC = "pig.output.func";

    public RecordWriter getRecordWriter(FileSystem fs, JobConf job, String name, Progressable progress)
            throws IOException {
    	StorageFunc store;
    	Path outputDir = job.getOutputPath();
    	String storeFunc = job.get("pig.storeFunc", "");
    	if (storeFunc.length() == 0) {
    	    store = new PigStorage();
    	} else {
    	    try {
    		store = (StorageFunc) PigContext.instantiateArgFunc(storeFunc);
    	    } catch (Exception e) {
    		RuntimeException re = new RuntimeException(e.getClass().getName() + ": " + e.getMessage());
    		re.setStackTrace(e.getStackTrace());
    		throw re;
    	    }
    	}
        return new PigRecordWriter(fs, new Path(outputDir, name), store);
    }

    public void checkOutputSpecs(FileSystem fs, JobConf job) throws IOException {
        // TODO We really should validate things here
        return;
    }

    static public class PigRecordWriter implements RecordWriter {
        private FSDataOutputStream os    = null;
        private StorageFunc          sfunc = null;

        public PigRecordWriter(FileSystem fs, Path file, StorageFunc sfunc) throws IOException {
            this.sfunc = sfunc;
            fs.delete(file);
            this.os = fs.create(file);
            this.sfunc.bindTo(os);
        }

        /**
         * We only care about the values, so we are going to skip the keys when we write.
         * 
         * @see org.apache.hadoop.mapred.RecordWriter#write(org.apache.hadoop.io.WritableComparable,
         *      org.apache.hadoop.io.Writable)
         */
        public void write(WritableComparable key, Writable value) throws IOException {
            this.sfunc.putNext((Tuple) value);
        }

        public void close(Reporter reporter) throws IOException {
            sfunc.done();
            os.close();
        }

    }
}
