/*
 * OffsetBinner
 *
 * April 25, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.binner;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.NumberWordBin;
import galago.types.NumberWordProbability;
import galago.types.WordCount;
import galago.types.WordProbability;
import galago.types.XMLFragment;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.NumberWordProbability", order="+word")
@OutputClass(className="galago.types.NumberWordBin", order="+word")
public class OffsetBinner extends StandardStep<NumberWordProbability, NumberWordBin> {
    private double multiplier;
    private int maximumBin;
    
    long tooSmall;
    long tooBig;
    long justRight;
    Logger logger;
    
    String currentWord = null;
    double currentOffset = 0.0;
    TypeReader<WordProbability> offsets;
    
    @SuppressWarnings("unchecked")
    public OffsetBinner( TupleFlowParameters parameters ) throws IOException {
        // get a list of all the offsets for the vocabulary
        offsets = parameters.getTypeReader( "offsets" );

        // read in the minimum offset from the thresholds stage
        TypeReader<XMLFragment> reader = parameters.getTypeReader( "minimumOffset" );
        XMLFragment fragment = reader.read();
        double minimumOffset = Double.parseDouble(fragment.innerText);
        
        // this is the maximum bin value for anything from this binner
        maximumBin = (int) parameters.getXML().get( "bins", 8 );
        
        // since it's highly unlikely that any real posting will
        // reach probability = 1.0, we can set an arbitrary saturation point;
        // any probabilities over this threshold are "maxed out"
        double saturation = parameters.getXML().get( "saturation", 0.001 );        
        double range = Math.log(saturation) - minimumOffset;
        range = Math.max( 0.0000001, range );
        multiplier = maximumBin / range;
        
        logger = Logger.getLogger(OffsetBinner.class.toString());
    }
    
    public void process( NumberWordProbability object ) throws IOException {
        String word = Utility.makeString(object.word);
        
        double offset = getOffset(word);
        double shifted = object.probability - offset;
        int bin = (int) (shifted * multiplier);
        log(offset, shifted, bin, object);
        
        // bin is no greater than the maximum
        bin = Math.min( maximumBin, bin );
        // since the word exists in the document, ensure that 
        // bin is not less than one
        bin = Math.max( 1, bin );
        
        processor.process( new NumberWordBin( object.number, object.word, bin ) );
    }
    
    private double getOffset( String word ) throws IOException {
        if( !word.equals(currentWord) ) {
            WordProbability wp = null;
            
            while ((wp = offsets.read()) != null) {
                if( word.equals(wp.word) ) {
                    currentOffset = wp.probability;
                    currentWord = word;
                    return currentOffset;
                }
            }
        } else {
            return currentOffset;
        }
        
        throw new IOException( "Couldn't find word " + word + " in offsets table." );
    }
    
    private void log(final double offset, final double shifted, final int bin, final NumberWordProbability object) {
        if( maximumBin <= bin ) {
            //System.out.println( "MAXBIN: " + object.toString() + " (" + bin + ") " + " " + offset + " " + shifted + " " + multiplier );
            tooBig++;
        } else if( bin < 1 ) {
            //System.out.println( "TINYBIN: " + object.toString() + " (" + bin + "," + maximumBin + ") " + " o:" + offset + " sh:" + shifted + " m:" + multiplier );
            tooSmall++;
        } else {
            justRight++;
        }
    }
    
    @Override
    public void close() throws IOException {
        logger.info( tooBig + "b " + tooSmall + "s " + justRight + "r" );
        super.close();
    }

    public Class<NumberWordProbability> getInputClass() {
        return NumberWordProbability.class;
    }
    
    public Class<NumberWordBin> getOutputClass() {
        return NumberWordBin.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "offsets", WordProbability.class, parameters, handler );
        Verification.verifyTypeReader( "minimumOffset", XMLFragment.class, parameters, handler );
    }
}
