/*
 * IndexReader
 *
 * March 29, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import galago.index.VocabularyReader;
import galago.tupleflow.BufferedDataStream;
import galago.tupleflow.Parameters;

/**
 * This implements the core functionality for all inverted list readers.
 * 
 * The main purpose of this class is to provide term-level
 * access to an inverted file.  Using a vocabulary file (accessed
 * by a VocabularyReader), it can return byte ranges for 
 * inverted lists in the index.
 * 
 * Typically this class is extended by composition instead of inheritance.
 * 
 * @author trevor
 */
public class IndexReader {
    VocabularyReader vocabulary;
    RandomAccessFile input;
    Parameters manifest;
    final int blockSize = 65536;
    final int vocabGroup = 16;
    
    private static class VocabularyBlock {
        long startOffset;
        long[] invertedListEnds;
        long startInvertedLists;
        String[] words;

        public VocabularyBlock( long startOffset, long startLists, long[] invertedListEnds, String[] words ) {
            this.words = words;
            this.startOffset = startOffset;
            this.startInvertedLists = startLists;
            this.invertedListEnds = invertedListEnds;
        }

        public long getListStart( int index ) {
            if( index == 0 )
                return startInvertedLists;
            return invertedListEnds[index-1];
        }

        public long getListEnd( int index ) {
            return invertedListEnds[index];
        }

        public long getBlockEnd() {
            return invertedListEnds[invertedListEnds.length-1];
        }

        public boolean hasMore( int index ) {
            return invertedListEnds.length > (index+1);
        }

        public int findIndex( String term ) {
            // Need to do a linear search here because the vocabulary order
            // does not necessarily match Java's idea of alphabetical order
            for( int i=0; i<words.length; i++ ) {
                if( words[i].equals(term) )
                    return i;
            }
            return -1;
        }

        private String getTerm(int termIndex) {
            return this.words[termIndex];
        }
    }
 
    public class Iterator {
        VocabularyBlock block;
        String term;
        int termIndex;
        
        long listStart;
        long listEnd;
        
        Iterator( VocabularyBlock block, int index ) throws IOException {
            this.block = block;
            termIndex = index;

            loadIndex();
        }
        
        void loadIndex() throws IOException {
            listStart = 0;
            listEnd = 0;
            term = "";
            
            if( block == null || termIndex < 0 )
                return;
            
            term = block.getTerm( termIndex );
            listStart = block.getListStart( termIndex );
            listEnd = block.getListEnd( termIndex );
        }
        
        /**
         * Returns the term associated with the current inverted list.
         */
        
        String currentTerm() {
            return term;
        }
        
        /**
         * Returns the byte offset
         * of the beginning of the current inverted list,
         * relative to the start of the whole inverted file.
         */
        
        public long getListStart() {
            return block.getListStart( termIndex );
        }
        
        /**
         * Returns the byte offset
         * of the end of the current inverted list,
         * relative to the start of the whole inverted file.
         */

        public long getListEnd() {
            return block.getListEnd( termIndex );
        }
        
        /**
         * Jumps to the next term in the inverted file.
         * If no such term exists, throws an EOFException.
         */
        
        void nextTerm() throws IOException {
            if( block.hasMore( termIndex ) ) {
                termIndex++;
            } else {
                block = readVocabularyBlock( block.getBlockEnd() );
                termIndex = 0;
            }
            
            loadIndex();
        }
        
        boolean foundTerm() {
            return termIndex >= 0;
        }
    }
    
    /**
     * Opens an index found in the directory specified by pathname.  
     * In the pathname directory, there need to be at least three files:
     * 'invertedFile', 'vocabulary', and 'manifest'.  The first two need to have
     * been created by InvertedListWriter (or by something that writes in the 
     * same format).  The manifest can be any XML file that has a root tag of
     * 'parameters'.
     */
    
    public IndexReader( String pathname ) throws FileNotFoundException, IOException {
        String invertedFilePath = pathname + File.separator + "invertedFile";
        input = new RandomAccessFile(invertedFilePath, "r");
        String vocabularyPath = pathname + File.separator + "vocabulary";
        vocabulary = new VocabularyReader( vocabularyPath, new File(invertedFilePath).length() );
        String manifestPath = pathname + File.separator + "manifest";
        manifest = new Parameters( new File(manifestPath) );
    }
    
    /**
     * Identical to the {@link #IndexReader(String) other constructor}, except this
     * one takes a File object instead of a string as the parameter.
     * 
     * @param pathname
     * @throws java.io.FileNotFoundException
     * @throws java.io.IOException
     */
    
    public IndexReader( File pathname ) throws FileNotFoundException, IOException {
        this(pathname.toString());
    }
    
    /**
     * Returns an iterator pointing to the very first term in the inverted file.
     * This is typically used for iterating through the entire inverted file,
     * which might be useful for testing and debugging tools, but probably
     * not for traditional document retrieval.
     */

    public Iterator getIterator() throws IOException {
        VocabularyBlock block = readVocabularyBlock(0);
        Iterator result = new Iterator( block, 0 );
        result.loadIndex();
        return result;
    }
    
    /**
     * Returns an iterator pointing at a specific term.  Returns
     * null if the term is not found in the inverted file.
     */

    public Iterator getTerm( String term ) throws IOException {
        // read from offset to offset in the vocab structure (right?)
        VocabularyReader.TermSlot slot = vocabulary.get(term);
        
        if( slot == null )
            return null;
        
        VocabularyBlock block = readVocabularyBlock( slot.begin );
        int index = block.findIndex( term );
        
        if( index >= 0 )
            return new Iterator( block, index );
        
        return null;
    }
    
    /**
     * Returns a Parameters object that contains the contents
     * of the manifest file found in the index directory.  This 
     * is the place to store important data about the index contents,
     * like what stemmer was used or the total number of terms in the
     * collection.
     */

    public Parameters getManifest() {
        return manifest;
    }
    
    /**
     * Reads vocabulary data from a block of the inverted file.
     * 
     * The inverted file is structured into blocks which contain a little
     * bit of compressed vocabulary information followed by inverted list
     * information.  The inverted list information is application specific,
     * and is not handled by this class.  This method reads in the vocabulary
     * information for a particular block and returns it in a VocabularyBlock
     * object, which allows for quick navigation to a particular term.
     * 
     * The C++ version of this is much more efficient, because it makes no
     * attempt to decode the whole block of information.  However, for the 
     * Java version I thought that simplicity (and testability) was more
     * important than speed.  The VocabularyBlock structure helps make iteration
     * over the entire inverted file possible.
     */

    VocabularyBlock readVocabularyBlock( long slotBegin ) throws IOException {
        // read in a block of data here
        BufferedDataStream blockStream = blockStream( slotBegin, blockSize );

        // now we decode everything from the stream
        long endBlock = blockStream.readLong();
        long wordCount = blockStream.readLong();

        int prefixLength = blockStream.readUnsignedByte();
        byte[] prefixBytes = new byte[prefixLength];
        blockStream.readFully(prefixBytes);

        int wordBlockCount = (int)Math.ceil((double)wordCount / vocabGroup);
        short[] wordBlockEnds = new short[wordBlockCount];
        String[] words = new String[(int)wordCount];
        long[] invertedListEnds = new long[(int)wordCount];

        for( int i=0; i<wordBlockCount; i++ ) {
            wordBlockEnds[i] = blockStream.readShort();
        }

        for( int i=0; i<wordCount; i++ ) {
            invertedListEnds[i] = endBlock - blockStream.readShort();
        }

        for( int i=0; i<wordCount; i+=vocabGroup ) {
            int suffixLength = blockStream.readUnsignedByte();
            int wordLength = suffixLength + prefixLength;
            byte[] wordBytes = new byte[wordLength];
            byte[] lastWordBytes = wordBytes;
            System.arraycopy( prefixBytes, 0, wordBytes, 0, prefixBytes.length );
            int end = (int)Math.min( wordCount, i + vocabGroup );

            blockStream.readFully( wordBytes, prefixBytes.length, wordBytes.length - prefixBytes.length );
            words[i] = new String(wordBytes);

            for( int j = i+1; j < end; j++ ) {
                int common = blockStream.readUnsignedByte();
                wordLength = blockStream.readUnsignedByte();
                assert wordLength >= 0 : "Negative word length: " + wordLength + " " + j;
                assert wordLength >= common : "word length too small: " + wordLength + " " + common + " " + j;
                wordBytes = new byte[wordLength];

                try {
                    System.arraycopy( lastWordBytes, 0, wordBytes, 0, common );
                    blockStream.readFully( wordBytes, common, wordLength - common );
                } catch( ArrayIndexOutOfBoundsException e ) {
                    System.out.println( "wl: " +wordLength + " c: " + common );
                    throw e;
                }
                words[j] = new String(wordBytes);
                lastWordBytes = wordBytes;
            }
        }

        int suffixBytes = wordBlockEnds[wordBlockEnds.length-1];
        long headerLength = 8 + // word count
                            8 + // block end
                            1 + prefixLength +    // word prefix bytes
                            2 * wordBlockCount +  // word lengths 
                            2 * wordCount +       // inverted list endings
                            suffixBytes;          // suffix storage 

        long startInvertedLists = slotBegin + headerLength;
        return new VocabularyBlock( slotBegin, startInvertedLists, invertedListEnds, words );
    }
    
    /**
     * Like the other blockStream variant, but this one uses
     * the current file location as the starting offset.
     */
    
    public BufferedDataStream blockStream( long len ) throws IOException {
        return blockStream( input.getFilePointer(), len );
    }
    
    /**
     * This convenience method returns a BufferedDataStream for
     * a region of an inverted file.
     */

    public BufferedDataStream blockStream( long offset, long length ) throws IOException {
        long fileLength = input.length();
        assert offset <= fileLength;
        length = Math.min( fileLength - offset, length );
        
        return new BufferedDataStream( input, offset, length + offset );
    }

    /**
     * This convenience method returns a BufferedDataStream for
     * the region of the inverted file pointed to by the iterator.
     */
    
    public BufferedDataStream blockStream( Iterator iter ) throws IOException {
        return new BufferedDataStream( input, iter.getListStart(), iter.getListEnd() );
    }
    
    /**
     * Returns the file object for the inverted file.  This is useful for actually
     * reading the data from a byte range returned by the iterator.
     */
    
    RandomAccessFile getInput() {
        return input;
    }

    /**
     * Closes all files associated with the IndexReader.
     */
    
    void close() throws IOException {
        input.close();
    }
}
