/*
 * DocumentOrderedListReader
 * 
 * October 12, 2007 -- Trevor Strohman
 * 
 * BSD License (http://galagosearch.org/license)
 */

package galago.retrieval;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public interface DocumentOrderedBinnedListReader {
    public DocumentOrderedIterator getIterator() throws IOException;
    public DocumentOrderedIterator getTerm( String term ) throws IOException;
    public void close() throws IOException;
}
