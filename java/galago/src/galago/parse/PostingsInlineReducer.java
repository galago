/*
 * PostingsInlineReducer
 *
 * June 14, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentLengthWordCount;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.types.DocumentLengthWordCount")
@OutputClass(className="galago.types.DocumentLengthWordCount")
public class PostingsInlineReducer extends StandardStep<DocumentLengthWordCount, DocumentLengthWordCount> {
    DocumentLengthWordCount last = null;

    public void process( DocumentLengthWordCount dlwc ) throws IOException {
        if ( last == null ) {
            last = dlwc;
        } else if ( dlwc.document.equals(last.document) &&
                    dlwc.word.equals(last.word) ) {
            last.count += dlwc.count;
        } else {
            processor.process(last);
            last = dlwc;
        }
    }
    
    public void close() throws IOException {
        if (last != null)
            processor.process(last);
        last = null;
    }
}
