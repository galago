/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.yahoo.pig.FilterFunc;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.EvalItemList;

public class FuncCond extends Cond {
    public FilterFunc func;
    public EvalItemList args;
    
    public FuncCond(FilterFunc func, EvalItemList args) {
        this.func = func;
        this.args = args;
    }

    @Override
    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        funcs.add(func.getName());
        return funcs;
    }

    @Override
    public boolean eval(Tuple input) throws IOException {
        try {
            return func.exec(args.simpleEval(input));
        } catch (IOException e) {
            System.out.println("Warning: filter function " + func.getName() + " failed. Substituting default value \'false\'.");
            return false;
        }
    }
    
    public String toString() {
        return func + "(" + args + ")";
    }
}
