
//
// TrecTextParser.java
//
// 5 June 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.parse;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author trevor
 */
public class TrecTextParser implements DocumentStreamParser {
    BufferedReader reader;
    
    /** Creates a new instance of TrecTextParser */
    public TrecTextParser( String filename ) throws FileNotFoundException, IOException {
        if( filename.endsWith(".gz") ) {
            reader = new BufferedReader( new InputStreamReader( new GZIPInputStream( new FileInputStream( filename ) ) ) );
        } else {
            reader = new BufferedReader( new FileReader(filename) );
        }
    }
    
    public String waitFor( String tag ) throws IOException {
        String line;
        
        while( (line = reader.readLine()) != null ) {
            if( line.startsWith( tag ) )
                return line;
        }
        
        return null;
    }
    
    public String parseDocNumber() throws IOException {
        String allText = waitFor( "<DOCNO>" );
        
        while( allText.contains( "</DOCNO>" ) == false ) {
            String line = reader.readLine();
            if (line == null)
                break;
            allText += line;
        }
        
        int start = allText.indexOf( "<DOCNO>" ) + 7;
        int end = allText.indexOf( "</DOCNO>" );
        
        return new String(allText.substring( start, end ).trim());
    }
    
    public Document nextDocument() throws IOException {
        String line;
        
        if( null == waitFor( "<DOC>" ) )
            return null;
        
        String identifier = parseDocNumber();
        StringBuffer buffer = new StringBuffer();
        
        String[] startTags = { "<TEXT>", "<HEADLINE>", "<TITLE>", "<HL>", "<HEAD>",
                               "<TTL>", "<DD>", "<DATE>", "<LP>", "<LEADPARA>" };
        String[] endTags = { "</TEXT>", "</HEADLINE>", "</TITLE>", "</HL>", "</HEAD>",
                               "</TTL>", "</DD>", "</DATE>", "</LP>", "</LEADPARA>" };
        
        int inTag = -1;
        
        while( (line = reader.readLine()) != null ) {
            if( line.startsWith( "</DOC>" ) )
                break;
            
            if( line.startsWith( "<" ) ) {
                if (inTag >= 0 && line.startsWith(endTags[inTag])) {
                    inTag = -1;
                    
                    buffer.append( line );
                    buffer.append( ' ' );
                } else if( inTag < 0 ) {
                    for( int i=0; i<startTags.length; i++ ) {
                        if( line.startsWith( startTags[i] ) ) {
                            inTag = i;
                            break;
                        }
                    }
                }
            }
            
            if( inTag >= 0 ) {            
                buffer.append( line ); 
                buffer.append( ' ' );
            }
        }
        
        return new Document( identifier, buffer.toString() );
    }
}
