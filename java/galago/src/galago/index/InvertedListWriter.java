//
// InvertedListWriter.java
//
// 28 November 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.index;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import galago.tupleflow.Parameters;
import galago.tupleflow.TupleFlowParameters;
import galago.types.WordOffset;
import java.io.File;

/**
 *
 * @author trevor
 */
public class InvertedListWriter {
    long blockSize = 32768;
    
    DataOutputStream output;
    long filePosition;
    
    final VocabularyWriter vocabulary;
    long listBytes = 0;
    ArrayList<InvertedList> lists;
        
    /**
     * Creates a new instance of InvertedListWriter
     */
    public InvertedListWriter( String outputFilename, String vocabFilename, Parameters parameters ) throws FileNotFoundException, IOException {
        output = new DataOutputStream( new BufferedOutputStream( new FileOutputStream( outputFilename ) ) );
        vocabulary = new VocabularyWriter( vocabFilename );
        lists = new ArrayList<InvertedList>();
        listBytes = 0;
    }
    
    public InvertedListWriter( String indexFilename ) throws FileNotFoundException, IOException {
        new File(indexFilename).mkdirs();
        
        String vocabFilename = indexFilename + File.separator + "vocabulary";
        String invertedFilename = indexFilename + File.separator + "invertedFile";

        output = new DataOutputStream( new BufferedOutputStream( new FileOutputStream( invertedFilename ) ) );
        vocabulary = new VocabularyWriter( vocabFilename );
        lists = new ArrayList<InvertedList>();
        listBytes = 0;
    }
        
    public InvertedListWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        this( parameters.getXML().get( "index" ) );
    }
    
    /** 
     * Gives a conservative estimate of the buffered size of the data,
     * excluding the most recent inverted list.
     * Does not include savings due to word overlap compression.
     */
    
    public long bufferedSize() {
        long extra = 8 + // end of block
                     8 + // word count
                     1; // overlap length
        
        return listBytes + extra;
    }
    
    public void updateBufferedSize( InvertedList list ) {
        long extra = 1 + // byte for word length
                     1;  // byte for overlap with previous word
        
        listBytes += invertedListLength( list );
        listBytes += extra;
    }
    
    private long invertedListLength( InvertedList list ) {
        long listLength = 0;

        listLength += list.word().length;
        listLength += 2; // word length bytes
        listLength += 2; // file offset bytes

        listLength += list.dataLength();
        return listLength;
    }

    /**
     * Flush all lists out to disk.
     */
    public void flush() throws IOException {
        // if there aren't any lists, quit now
        if( lists.size() == 0 )
            return;
        
        // write everything out
        writeBlock( lists, bufferedSize() );
        
        // remove all of the current data
        lists = new ArrayList<InvertedList>();
        listBytes = 0;
    }
    
    private int prefixOverlap( byte[] firstTerm, byte[] lastTerm, int start ) {
        int maximum = Math.min( firstTerm.length - start, lastTerm.length - start );
        maximum = Math.min( Byte.MAX_VALUE-1, maximum );
        
        for( int i=start; i<maximum; i++ ) {
            if( firstTerm[i] != lastTerm[i] )
                return i-start;
        }
        
        return maximum;
    }
    
    private int prefixOverlap( byte[] firstTerm, byte[] secondTerm ) {
        return prefixOverlap( firstTerm, secondTerm, 0 );
    }

    public long getBlockSize() {
        return blockSize;
    }
    
    private boolean lessThanOrEqualTo( byte[] one, byte[] two ) {
        for( int i=0; ; i++ ) {
            if( i >= one.length )
                return true;
            if( i >= two.length )
                return false;
            int a = one[i];
            int b = two[i];
            a &= 0xFF;
            b &= 0xFF;
            if( a < b )
                return true;
            if( b < a )
                return false;
        }
    }
    
    public boolean wordsInOrder( List<InvertedList> blockLists ) {
        for( int i=0; i<blockLists.size()-1; i++ ) {
            boolean result = lessThanOrEqualTo( blockLists.get(i).word(), blockLists.get(i+1).word() );
            if ( result == false )
                return false;
        }
        return true;
    }
    
    public void writeBlock( List<InvertedList> blockLists, long length ) throws IOException {
        assert length <= blockSize || blockLists.size() == 1;
        assert wordsInOrder( blockLists );
        
        if( blockLists.size() == 0 )
            return;

        // vocabulary group (prefix sharing)
        final int vocabGroup = 16;
        byte[] firstWord = blockLists.get(0).word();
        byte[] lastWord = blockLists.get(blockLists.size()-1).word();
        
        // determine how many prefix characters are in common among all terms in this block
        int overlap = prefixOverlap( firstWord, lastWord );
        
        // -- compute the length of the block --
        long totalInvertedLength = 0;
        long totalWordBytes = 0;
        
        for( InvertedList list : blockLists ) {
            totalInvertedLength += list.dataLength();
            totalWordBytes += list.word().length + 2;
        }

        ByteArrayOutputStream wordByteStream = new ByteArrayOutputStream();
        DataOutputStream vocabOutput = new DataOutputStream( wordByteStream );
        
        short[] wordBlockEnds = writeVocabularyList( vocabOutput, blockLists, overlap, vocabGroup );
        int actualWordBytes = wordBlockEnds[ wordBlockEnds.length-1 ];
        long headerBytes = 8 + // word count
                           8 + // block end
                           1 + overlap +  // word prefix bytes
                           2 * wordBlockEnds.length + // word lengths 
                           2 * blockLists.size() + // inverted list endings
                           actualWordBytes;    // word data 

        long startPosition = filePosition;
        long endPosition = filePosition + headerBytes + totalInvertedLength;
        assert endPosition <= startPosition + length;
        assert endPosition > startPosition;
        assert filePosition >= Integer.MAX_VALUE || filePosition == output.size();
        
        // -- begin writing the block -- 
        vocabulary.process( new WordOffset( firstWord, startPosition ) );
        
        // write block data end
        output.writeLong( endPosition );
        
        // write word count
        output.writeLong( blockLists.size() );
        
        // write word prefix
        output.writeByte( (byte)overlap );
        output.write( firstWord, 0, overlap );
        
        // write word block lengths
        for( short wordBlockEnd : wordBlockEnds ) {
            output.writeShort( wordBlockEnd );
        }
        
        // write inverted list end positions
        long invertedListBytes = 0;
        for( InvertedList list : blockLists ) {
            invertedListBytes += list.dataLength();
            long endInvertedList = invertedListBytes + startPosition + headerBytes;
            assert (endPosition - endInvertedList) < Short.MAX_VALUE;
            
            output.writeShort( (short) (endPosition - endInvertedList) );
        }
        assert invertedListBytes == totalInvertedLength;
        
        // word data
        output.write( wordByteStream.toByteArray() );
        
        // write inverted list binary data
        for( InvertedList list : blockLists ) {
            list.write( output );
        }

        filePosition = endPosition;
        assert filePosition >= Integer.MAX_VALUE || filePosition == output.size();
        assert endPosition - startPosition <= blockSize || blockLists.size() == 1;
    }

    private short[] writeVocabularyList( final DataOutputStream output,
                                         final List<InvertedList> blockLists,
                                         final int overlap, final int vocabGroup ) throws IOException {
        int blockCount = (int)Math.ceil( (float)blockLists.size() / vocabGroup );
        short[] ends = new short[blockCount];
        
        // write word data
        for( int i=0; i < blockLists.size(); i += vocabGroup ) {
            byte[] word = blockLists.get(i).word();
            byte[] lastWord = word;
            assert word.length >= overlap : "Overlap: " + overlap + " too small for " + word.length + " (" + new String(word) + ")";
            assert word.length < 256;
            
            output.writeByte( word.length - overlap );
            output.write( word, overlap, word.length - overlap );
            int end = Math.min( blockLists.size(), i + vocabGroup );

            for( int j = i+1; j < end; j++ ) {
                assert word.length < 256;
                
                word = blockLists.get(j).word();
                int common = this.prefixOverlap( lastWord, word );
                output.writeByte( (byte)common );
                output.writeByte( word.length );
                output.write( word, common, word.length - common );
                lastWord = word;
            }
            
            ends[i/vocabGroup] = (short)output.size();
        }
        
        return ends;
    }

    private boolean needsFlush( InvertedList list ) {
        long listExtra = 1 + // byte for word length
                         1;  // byte for overlap with previous word
        
        long bufferedBytes = bufferedSize() +
                             invertedListLength( list ) +
                             listExtra;

        return bufferedBytes >= blockSize;
    }
    
    public void add( InvertedList list ) throws IOException {
        if (list.word().length >= 256 || list.word().length >= blockSize/4)
            throw new IOException( "Word is too long." );
        
        if( needsFlush( list ) )
            flush();
        
        lists.add( list );
        updateBufferedSize( list );
    }
    
    public void close() throws IOException {
        flush();
        output.close();
        vocabulary.close();
    }
}

