/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;


public abstract class AlgebraicTupleEvalFunc extends TupleEvalFunc {
    public abstract TupleEvalFunc getInitial();

    public abstract TupleEvalFunc getIntermed();

    public abstract TupleEvalFunc getFinal();
}
