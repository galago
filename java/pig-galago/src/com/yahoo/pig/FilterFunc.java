/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;

import java.io.IOException;

import com.yahoo.pig.data.Tuple;

public abstract class FilterFunc extends InstantiatedFunc {
    
    /**
     * This callback method must be implemented by all subclasses. This
     * is the method that will be invoked on every Tuple of a given dataset.
     * Since the dataset may be divided up in a variety of ways the programmer
     * should not make assumptions about state that is maintained between
     * invocations of this method.
     * 
     * @param input the Tuple to be processed.
     * @throws IOException
     */
    abstract public boolean exec(Tuple input) throws IOException;
        
    /**
     * Returns the String representation of the function.
     */
    public String toString() {
        return getName();
    }
}

