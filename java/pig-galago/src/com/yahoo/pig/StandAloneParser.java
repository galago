/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;

import java.io.*;
import java.util.*;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.impl.logicalLayer.LogicalPlan;
import com.yahoo.pig.impl.logicalLayer.LogicalPlanBuilder;
import com.yahoo.pig.impl.logicalLayer.parser.ParseException;
import com.yahoo.pig.impl.logicalLayer.parser.QueryParser;
import com.yahoo.pig.impl.physicalLayer.IntermedResult;

public class StandAloneParser {
    public static ClassLoader classloader = LogicalPlanBuilder.class.getClassLoader();
    
    public static void main(String args[]) throws IOException {
        
        Map<String, IntermedResult> aliases = new HashMap<String, IntermedResult>();
        
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        while (true) {
            System.out.print("> ");
            
            String line;
            try {
                line = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            
            if (line.toLowerCase().equals("quit")) break;
            if (line.toLowerCase().startsWith("#")) continue;
            else tryParse(line, aliases);
            
        }
           
    }
    
    private static void tryParse(String query, Map<String, IntermedResult> aliases) {
    	if (query.trim().equals(""))
    		return;
        ByteArrayInputStream in = new ByteArrayInputStream(query.getBytes());       
        QueryParser parser = new QueryParser(in, new PigContext(ExecType.LOCAL), aliases);
        try {
            LogicalPlan lp = parser.Parse();
            System.out.println("--- Query parsed successfully ---");
            if (lp.alias != null) aliases.put(lp.alias, new IntermedResult(lp));
            System.out.print("Current aliases: ");
            for (Iterator<String> it = aliases.keySet().iterator(); it.hasNext(); ) {
                String alias = it.next();
                IntermedResult ir = aliases.get(alias);
                System.out.print(alias + "->" + ir.lp.root().outputSchema());
                if (it.hasNext()) System.out.print(", \n");
                else System.out.print("\n");
            }
        } catch (ParseException e) {
            System.out.println("Parse error: " + e);
        }
    }
}
