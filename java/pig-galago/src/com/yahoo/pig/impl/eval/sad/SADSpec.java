/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.sad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.EvalSpec;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

public abstract class SADSpec extends EvalSpec {

    public void eval(Tuple input, DataCollector output) throws IOException {
        throw new IOException("Internal error: attempt to apply per-tuple eval function with SAD EvalSpec.");
    }
    
    public List<String> getFuncs() {
        return new ArrayList<String>();
    }
    
    @Override
    public SchemaItem mapInputSchema(SchemaItem input){
    	return input;
    }
    
}
