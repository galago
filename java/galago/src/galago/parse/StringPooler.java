/*
 * StringPooler
 *
 * December 14, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.util.HashMap;

/**
 * The point of this class is to replace strings in document objects with
 * already-used copies.  This can greatly reduce the amount of memory used
 * by the system.
 *
 * @author trevor
 */
public class StringPooler {
    HashMap<String, String> pool = new HashMap<String, String>();
    
    public void transform( Document document ) {
        for( int i=0; i<document.terms.size(); i++ ) {
            String term = document.terms.get(i);
            
            if( term == null )
                continue;
            
            String cached = pool.get(term);
            
            if( cached == null ) {
                term = new String(term);
                pool.put(term, term);
            } else {
                term = cached;
            }  

            document.terms.set(i, term);
        }
        
        if( pool.size() > 10000 )
            pool.clear();
    }
}
