
//
// Type.java
//
// 27 September 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.rmi.dgc.VMID;
import java.util.Comparator;

/**
 *
 * @author trevor
 */
public interface Type<T> {    
    public Order<T> getOrder( String... fields );
}