/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.util;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;

public class DataBuffer extends DataCollector {
    
    LinkedList<Tuple> buf = new LinkedList<Tuple>();

    public synchronized void add(Tuple t) throws IOException {
        if (t != null) buf.add(t);
    }
    
    public synchronized Tuple removeFirst() {
        if (buf.isEmpty()) return null;
        else return buf.remove(0);
    }
    
    public synchronized boolean isEmpty() {
        return buf.isEmpty();
    }

}
