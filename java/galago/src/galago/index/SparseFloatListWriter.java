/*
 * DoubleListWriter
 *
 * September 15, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.index;

import galago.tupleflow.TupleFlowParameters;
import galago.types.NumberWordProbability;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class SparseFloatListWriter implements NumberWordProbability.NumberWordOrder.ShreddedProcessor {
    InvertedListWriter writer;
    DoubleInvertedList list;
    
    public class DoubleInvertedList implements InvertedList {
        BackedCompressedByteBuffer data = new BackedCompressedByteBuffer();
        CompressedByteBuffer header = new CompressedByteBuffer();
        int lastDocument;
        int documentCount;
        byte[] word;
        
        public DoubleInvertedList( byte[] word ) {
            this.word = word;
            this.lastDocument = 0;
            this.documentCount = 0;
        }
        
        public void write(final DataOutputStream stream) throws IOException {
            header.write( stream );
            data.write( stream );
        }

        public void addDocument( int document ) throws IOException {
            data.add( document - lastDocument );
            documentCount++;
            lastDocument = document;
        }
        
        public void addProbability( double probability ) throws IOException {
            data.addFloat( (float) probability );
        }
        
        public byte[] word() {
            return word;
        }

        public long dataLength() {
            return data.length() + header.length();
        }
        
        public void close() {
            header.add( documentCount );
        }
    }
    
    /** Creates a new instance of DoubleListWriter */
    public SparseFloatListWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        writer = new InvertedListWriter( parameters );
    }

    public void processWord(byte[] word) throws IOException {
        if( list != null ) {
            list.close();
            writer.add( list );
        }
        
        list = new DoubleInvertedList( word );
    }

    public void processNumber(int number) throws IOException {
        list.addDocument( number );
    }

    public void processTuple(double probability) throws IOException {
        list.addProbability( probability );
    }

    public void close() throws IOException {
        if( list != null ) {
            list.close();
            writer.add( list );
        }

        writer.close();
    }
}
