/*
 * ExtentInsideIterator
 *
 * September 13, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import galago.Utility;
import galago.tupleflow.Parameters;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */

@InputIterator(className="galago.retrieval.extents.ExtentIterator", cardinality=2)
public class ExtentInsideIterator extends ExtentConjunctionIterator {
    ExtentIterator innerIterator;
    ExtentIterator outerIterator;
    
    /** Creates a new instance of ExtentInsideIterator */
    public ExtentInsideIterator( Parameters parameters, ArrayList<DocumentDataIterator> extentIterators ) throws IOException {
        super( Utility.checkedCast( extentIterators, ExtentIterator.class ) );
        assert extentIterators.size() == 2;
        innerIterator = (ExtentIterator) extentIterators.get(0);
        outerIterator = (ExtentIterator) extentIterators.get(1);
        findDocument();
    }

    public void loadExtents() {
        ExtentArrayIterator inner = new ExtentArrayIterator(innerIterator.extents());
        ExtentArrayIterator outer = new ExtentArrayIterator(outerIterator.extents());

        while (!inner.isDone() && !outer.isDone()) {
            if ( outer.current().contains(inner.current()) ) {
                extents.add( inner.current() );
                inner.next();
            } else if ( outer.current().end <= inner.current().begin ) {
                outer.next();
            } else {
                inner.next();
            }
        }
    }
    
    public void reset() throws IOException {
        innerIterator.reset();
        outerIterator.reset();
        findDocument();
    }
}
