/*
 * DRMAAExecutor
 *
 * March 30, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.ggf.drmaa.DrmaaException;
import org.ggf.drmaa.JobTemplate;
import org.ggf.drmaa.Session;
import org.ggf.drmaa.SessionFactory;

/**
 *
 * @author trevor
 */
public class DRMAAStageExecutor extends RemoteStageExecutor {
    Session session;
    String command = "/home/strohman/galago-tupleflow";

    public class DRMAAResult implements Future<Object> {
        ArrayList<String> jobs;
        Exception exception;
        
        public DRMAAResult( ArrayList<String> jobs ) {
            this(jobs, null);
        }

        public DRMAAResult( ArrayList<String> jobs, Exception e ) {
            this.exception = e;
            this.jobs = jobs;
        }
        
        public Object get() throws ExecutionException {
            if( exception != null )
                throw new ExecutionException( "Caught exception while launching jobs: ", exception );
            
            try {
                session.synchronize(jobs, Session.TIMEOUT_WAIT_FOREVER, true);
            } catch( DrmaaException e ) {
                throw new ExecutionException( "Caught exception while waiting for jobs to complete: ", e );
            }
            
            return null;
        }

        public Object get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            if( exception != null )
                throw new ExecutionException( "Caught exception while launching jobs: ", exception );
            
            if( unit == TimeUnit.SECONDS ) {
                timeout *= 1000;
            } else if( unit == TimeUnit.MILLISECONDS ) {
                // do nothing
            } else if( unit == TimeUnit.MICROSECONDS ) {
                timeout /= 1000;
            } else if( unit == TimeUnit.NANOSECONDS ) {
                timeout /= 10000000;
            }
            
            try {
                session.synchronize(jobs, timeout, true);
            } catch( DrmaaException e ) {
                throw new ExecutionException( "Caught exception while waiting for jobs to complete: ", e );
            }
            
            return null;
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        public boolean isDone() {
            try {
                for( String job : jobs ) {
                    int status = session.getJobProgramStatus( job );

                    if( status == session.DONE || status == session.FAILED )
                        continue;

                    return false;
                }
            } catch( DrmaaException e ) {
                return false;
            }

            return true;
        }

        public boolean isCancelled() {
            return false;
        }
    }
    
    /** Creates a new instance of DRMAAExecutor */
    public DRMAAStageExecutor( String[] arguments ) throws IOException {
        try {
            session = SessionFactory.getFactory().getSession();
            session.init("");
        } catch( DrmaaException e ) {
            throw (IOException) new IOException( "Problem starting DRMAA interface" ).initCause(e);
        }
        
        if( arguments.length > 0 )
            command = arguments[0];
    }
    
    public void shutdown() {
        try {
            session.exit();
        } catch( DrmaaException e ) {
        }
    }

    public Future<Object> submit( String stageName, ArrayList<String> jobPaths, String temporary ) {
        ArrayList<String> jobs = new ArrayList<String>();

        try {
            for( int i=0; i<jobPaths.size(); i++ ) {
                String[] arguments = new String[] { jobPaths.get(i) };
                JobTemplate template = session.createJobTemplate();
                template.setJobName( "galago-" + stageName + "-" + i );
                template.setRemoteCommand( command );
                template.setArgs( arguments );
                template.setOutputPath( ":" + temporary + File.separator + "stdout" );
                template.setErrorPath( ":" + temporary + File.separator + "stderr" );

                String id = session.runJob(template);
                jobs.add(id);
                session.deleteJobTemplate(template);
            }
        } catch( Exception e ) {
            return new DRMAAResult( jobs, e );
        }
        
        return new DRMAAResult( jobs );
    }
}
