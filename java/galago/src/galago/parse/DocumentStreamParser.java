
//
// DocumentStreamParser.java
//
// 5 June 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.parse;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public interface DocumentStreamParser {
    public Document nextDocument() throws IOException;
}
