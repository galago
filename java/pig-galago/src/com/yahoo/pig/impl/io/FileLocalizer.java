/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.Stack;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.impl.mapreduceExec.MapReduceLauncher;

public class FileLocalizer {
    static public final String LOCAL_PREFIX  = "file:";
    static public final String HADOOP_PREFIX = "hadoop:";

    static FileSystem getDfs() throws IOException {
        return MapReduceLauncher.getDfs();
    }

    static FileSystem getLfs() throws IOException {
        return MapReduceLauncher.getLfs();
    }

    static Configuration getConf() throws IOException {
        return MapReduceLauncher.getConf();
    }

    static class DFSInputStreamIterator extends InputStream {
        InputStream current;
        Path        paths[];
        int         currentPath;

        public DFSInputStreamIterator(Path paths[]) {
            this.paths = paths;
        }

        private boolean isEOF() throws IOException {
            if (current == null) {
                if (currentPath == paths.length)
                    return true;
                current = getDfs().open(paths[currentPath++]);
            }
            return false;
        }

        private void doNext() throws IOException {
            current.close();
            current = null;
        }

        public int read() throws IOException {
            while (!isEOF()) {
                int rc = current.read();
                if (rc != -1)
                    return rc;
                doNext();
            }
            return -1;
        }

        public int available() throws IOException {
            if (isEOF())
                return 0;
            return current.available();
        }

        public void close() throws IOException {
            if (current != null) {
                current.close();
                current = null;
            }
            currentPath = paths.length;
        }

        public int read(byte[] b, int off, int len) throws IOException {
            int count = 0;
            while (!isEOF() && len > 0) {
                int rc = current.read(b, off, len);
                if (rc <= 0) {
                    doNext();
                    continue;
                }
                off += rc;
                len -= rc;
                count += rc;
            }
            return count == 0 ? (isEOF() ? -1 : 0) : count;
        }

        public int read(byte[] b) throws IOException {
            return read(b, 0, b.length);
        }

        public long skip(long n) throws IOException {
            while (!isEOF() && n > 0) {
                n -= current.skip(n);
            }
            return n;
        }

    }

    static String checkDefaultPrefix(ExecType execType, String fileSpec) {
        if (fileSpec.startsWith(HADOOP_PREFIX) || fileSpec.startsWith(LOCAL_PREFIX))
            return fileSpec;
        return (execType == ExecType.LOCAL ? LOCAL_PREFIX : HADOOP_PREFIX) + fileSpec;
    }

    static public InputStream open(ExecType execType, String fileSpec) throws IOException {
        fileSpec = checkDefaultPrefix(execType, fileSpec);
        if (fileSpec.startsWith(HADOOP_PREFIX)) {
            init();
            fileSpec = fileSpec.substring(HADOOP_PREFIX.length());
            Path path = new Path(fileSpec);
            if (getDfs().isFile(path))
                return getDfs().open(path);
            Path paths[] = getDfs().listPaths(path);
            return new DFSInputStreamIterator(paths);
        } else {
            if (fileSpec.startsWith(LOCAL_PREFIX)) {
                fileSpec = fileSpec.substring(LOCAL_PREFIX.length());
            }
            return new FileInputStream(fileSpec);
        }
    }
    
    static public OutputStream create(ExecType execType, String fileSpec) throws IOException{
    	return create(execType,fileSpec,false);
    }

    static public OutputStream create(ExecType execType, String fileSpec, boolean append) throws IOException {
        fileSpec = checkDefaultPrefix(execType, fileSpec);
        if (fileSpec.startsWith(HADOOP_PREFIX)) {
            init();
            fileSpec = fileSpec.substring(HADOOP_PREFIX.length());
            return getDfs().create(new Path(fileSpec));
        } else {
            if (fileSpec.startsWith(LOCAL_PREFIX)) {
                fileSpec = fileSpec.substring(LOCAL_PREFIX.length());
            }
            return new FileOutputStream(fileSpec,append);
        }

    }

    static Stack<Path> toDelete    = new Stack<Path>();
    static Random      r           = new Random();
    static Path        relativeRoot;
    static boolean     initialized = false;
    static private void init() {
        if (!initialized) {
            initialized = true;
            relativeRoot = new Path("/tmp/temp" + r.nextInt());
            toDelete.push(relativeRoot);
            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    while (!toDelete.empty()) {
                        try {
                            Path path = toDelete.pop();
                            getDfs().delete(path);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    public static synchronized Path getTemporaryPath(Path relative) throws IOException {
        init();
        if (relative == null) {
            relative = relativeRoot;
        }
        if (!getDfs().exists(relativeRoot))
            getDfs().mkdirs(relativeRoot);
        Path path = new Path(relative, "tmp" + r.nextInt());
        toDelete.push(path);
        return path;
    }

    public static String hadoopify(String filename) throws IOException {
        if (filename.startsWith(LOCAL_PREFIX)) {
            filename = filename.substring(LOCAL_PREFIX.length());
        }
        Path localPath = new Path(filename);
        if (!getLfs().exists(localPath)) {
            throw new FileNotFoundException(filename);
        }
        Path newPath = getTemporaryPath(null);
        int suffixStart = filename.lastIndexOf('.');
        if (suffixStart != -1) {
            newPath = newPath.suffix(filename.substring(suffixStart));
        }
        boolean success = FileUtil.copy(getLfs(), new Path(filename), getDfs(), newPath, false, getConf());
        if (!success) {
            throw new IOException("Couldn't copy " + filename);
        }
        return newPath.toString();
    }

    public static String fullPath(String filename) throws IOException {
        if (filename.charAt(0) != '/') {
            return new Path(getDfs().getWorkingDirectory(), filename).toString();
        }
        return filename;
    }
}
