/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;
import java.util.Random;

import com.yahoo.pig.GroupFunc;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.data.Tuple;

public class GFCross extends GroupFunc {
	int numInputs, myNumber, numGroupsPerInput;
	
	public static int DEFAULT_PARALLELISM = 96;
	@Override
	public Datum[] exec(Tuple input) {
	
		try{
			numInputs = Integer.parseInt(input.getAtomField(0).strval());
			myNumber = Integer.parseInt(input.getAtomField(1).strval());
		
		
			numGroupsPerInput = (int) Math.ceil(Math.pow(DEFAULT_PARALLELISM, 1.0/numInputs));
			int numGroupsGoingTo = (int) Math.pow(numGroupsPerInput,numInputs - 1);
			
			int[] digits = new int[numInputs];
			for (int i=0; i<numInputs; i++){
				if (i == myNumber){
					Random r = new Random(System.currentTimeMillis());
					digits[i] = r.nextInt(numGroupsPerInput);
				}else{
					digits[i] = 0;
				}
			}
			
			Tuple[] groups = new Tuple[numGroupsGoingTo];
			for (int i=0; i<numGroupsGoingTo; i++){
				groups[i] = toTuple(digits);
				next(digits);
			}			
			return groups;

		}catch(IOException e){
			RuntimeException re = new RuntimeException(e.getMessage());
			re.setStackTrace(e.getStackTrace());
			throw re;
		}
	}
	
	private Tuple toTuple(int[] digits) throws IOException{
		Tuple t = new Tuple(numInputs);
		for (int i=0; i<numInputs; i++){
			t.setField(i, digits[i]);
		}
		return t;
	}
	
	private void next(int[] digits){
		for (int i=0; i<numInputs; i++){
			if (i== myNumber)
				continue;
			else{
				if (digits[i] == numGroupsPerInput - 1){
					digits[i] = 0;
				}else{
					digits[i]++;
					break;
				}
			}
		}
	}

}
