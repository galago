/*
 * OrderedFeatureExtractor
 *
 * December 11, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.io.IOException;
import java.util.ArrayList;
import galago.tupleflow.Processor;
import galago.types.DocumentWordCount;
import galago.types.WordCount;

/**
 *
 * @author trevor
 */
public class OrderedFeatureExtractor implements FeatureExtractor {
    String tagName;
    int windowSize;
    TokenCounter counter;
    
    public OrderedFeatureExtractor( String tagName, int windowSize, Processor<DocumentWordCount> postings, Processor<WordCount> counts ) {
        this.tagName = tagName;
        this.windowSize = windowSize;
        this.counter = new TokenCounter( postings, counts );
    }
    
    private void extractInRange( Document document, ArrayList<String> tokens, int start, int end ) {
        for( int i = start; i < end-windowSize+1; i++ ) {
            StringBuilder builder = new StringBuilder();
            
            int j;
            for( j = i; j < windowSize; j++ ) {
                builder.append( document.terms.get(j) );
                if( j < windowSize-1 )
                    builder.append( ' ' );
            }
            
            tokens.add( builder.toString() );
        }
    }
    
    public void extract( String id, Document document ) throws IOException {
        ArrayList<String> tokens = new ArrayList<String>();
        
        if( tagName == null ) {
            extractInRange( document, tokens, 0, document.terms.size() );
        } else {
            int largestEnd = 0;
            
            for( Tag t : document.tags ) {
                if( t.equals(tagName) && t.begin >= largestEnd ) {
                    extractInRange( document, tokens, t.begin, t.end );
                    largestEnd = t.end;
                }
            }
        }
        
        counter.process( id, tokens );
    }
    
    public void close() throws IOException {
        counter.close();
    }
}
