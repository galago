/*
 * ExtentIndex
 *
 * September 14, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.retrieval.extents.CountIterator;
import galago.retrieval.extents.ExtentIterator;
import galago.tupleflow.VByteInput;
import galago.util.ExtentArray;
import java.io.DataInput;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 *
 * @author trevor
 */
public class ExtentIndexReader {
    public class ExtentListIterator extends ExtentIterator {
        IndexReader.Iterator iterator;
        DataInput stream;
        int documentCount;
        int options;
        
        boolean done;
        int document;
        ExtentArray extents;
        int termDocs;
        
        public ExtentListIterator( IndexReader.Iterator iterator ) throws IOException {
            this.iterator = iterator;
            this.extents = new ExtentArray();
            this.done = false;
            loadIndex();
        }
        
        public void loadIndex() throws IOException {
            readHeader( iterator.getListStart(), iterator.getListEnd() );
        }
        
        public void reset() throws IOException {
            done = false;
            
            document = 0;
            extents.reset();
            documentCount = 0;
            termDocs = 0;
            
            loadIndex();
        }
        
        String currentTerm() {
            return iterator.term;
        }
        
        void nextTerm() throws IOException {
            iterator.nextTerm();           
            loadIndex();
        }
        
        private void readHeader( long startPosition, long endPosition ) throws IOException {
            assert startPosition <= endPosition;
            
            RandomAccessFile input = reader.getInput();
            input.seek( startPosition );
            stream = new VByteInput( reader.getInput() );

            options = stream.readInt();
            documentCount = stream.readInt();
            termDocs = 0;
            done = false;
            
            nextDocument();
        }

        public void nextDocument() throws IOException {
            extents.reset();

            if( termDocs >= documentCount ) {
                done = true;
                return;
            }
            
            termDocs++;
            int deltaDocument = stream.readInt();
            document += deltaDocument;
            
            int extentCount = stream.readInt();
            int begin = 0;
            
            for( int i=0; i<extentCount; i++ ) {
                int deltaBegin = stream.readInt();
                int extentLength = stream.readInt();
                
                begin = deltaBegin + begin;
                int end = begin + extentLength;
                
                extents.add( document, begin, end );
            }
        }
        
        public int document() {
            return document;
        }

        public int count() {
            return extents.getPosition();
        }

        public ExtentArray extents() {
            return extents;
        }

        public boolean isDone() {
            return done;
        }
    }
    
    IndexReader reader;
    
    public ExtentIndexReader( String pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }

    public ExtentListIterator getIterator() throws IOException {
        return new ExtentListIterator( reader.getIterator() );
    }

    public ExtentListIterator getExtents( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new ExtentListIterator( iterator );
        
        return null;
    }
    
    public CountIterator getCounts( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new ExtentListIterator( iterator );
        
        return null;
    }
    
    void close() throws IOException {
        reader.close();
    }
}
