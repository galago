/*
 * LinkCombiner
 *
 * May 1, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Processor;
import galago.tupleflow.Step;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentURL;
import galago.types.ExtractedLink;
import galago.types.IdentifiedLink;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@OutputClass(className="galago.types.IdentifiedLink")
public class LinkCombiner implements ExNihiloSource<IdentifiedLink>, IdentifiedLink.Source {
    TypeReader<ExtractedLink> extractedLinks;
    TypeReader<DocumentURL> documentURLs;
    public Processor<IdentifiedLink> processor;
    
    @SuppressWarnings("unchecked")
    public LinkCombiner( TupleFlowParameters parameters ) throws IOException {
        extractedLinks = parameters.getTypeReader( "extractedLinks" );
        documentURLs = parameters.getTypeReader( "documentURLs" );
    }

    public void setProcessor( Step processor ) throws IncompatibleProcessorException {
        Linkage.link( this, processor );
    }

    public void run() throws IOException {
        ExtractedLink link = extractedLinks.read();
        DocumentURL docUrl = documentURLs.read();
        int count = 0;
        while ( docUrl != null && link != null ) {
            int result = link.destUrl.compareTo( docUrl.url );

            if( result == 0 ) {
                processor.process( new IdentifiedLink( docUrl.identifier, docUrl.url, link.anchorText ) );
                link = extractedLinks.read();
            } else if( result < 0 ) {
                link = extractedLinks.read();
            } else {
                docUrl = documentURLs.read();
            }
        }
        
        processor.close();
    }
        
    public Class<IdentifiedLink> getOutputClass() {
        return IdentifiedLink.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "extractedLinks", ExtractedLink.class, parameters, handler );
        Verification.verifyTypeReader( "documentURLs", DocumentURL.class, parameters, handler );
    }
}
