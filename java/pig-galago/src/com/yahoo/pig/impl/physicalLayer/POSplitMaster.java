/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import com.yahoo.pig.data.Tuple;

/*
 * 	Right now putting in a very dumb implementation which replicates the queue
	every slave
 */
class POSplitMaster extends POSplit {
    
	
    Map<POSplit, LinkedList<Tuple>> lists;

    public POSplitMaster(PhysicalOperator input, int outputType) {
    	super(outputType);
        inputs = new PhysicalOperator[1];
        inputs[0] = input;

        lists = new HashMap<POSplit, LinkedList<Tuple>>();
        registerSlave(this); // register self as one consumer
    }

    public Tuple getNext() throws IOException {
        return slaveGetNext(this);
    }

    // should only be called by POSplitSlave or POSplitMaster
    public void registerSlave(POSplit slave) {
        lists.put(slave, new LinkedList<Tuple>());
    }

    // should only be called by POSplitSlave or POSplitMaster
    public Tuple slaveGetNext(POSplit slave) throws IOException {
        
    	LinkedList<Tuple> list = lists.get(slave);
    	
    	if (list.isEmpty()){
    		//Get more input
    		Tuple t = inputs[0].getNext();
    		Iterator<LinkedList<Tuple>> iter = lists.values().iterator();
    		
    		while(iter.hasNext()){
    			iter.next().add(t);
    		}
       	}
    	
    	return list.removeFirst();
    }

    /*
    private Tuple ReadFromBuffer(int pos) throws IOException {
        while (buffer.size() <= pos)
            buffer.add(inputs[0].getNext());
        return buffer.get(pos);
    }
    
    
    private void ShrinkBuffer() {
        Set<POSplit> slaves = positions.keySet();

        // determine how many buffer positions we can safely remove
        int n = buffer.size(); // begin optimistically
        for (Iterator<POSplit> it = slaves.iterator(); it.hasNext();) {
            int pos = positions.get(it.next()).intValue();
            if (pos < n)
                n = pos; // if our current "n" would screw up this slave, reduce it
        }

        // now, perform the removal
        for (int i = 0; i < n; i++)
            buffer.removeFirst();

        // update the positions based on the removal
        if (n > 0) {
            for (Iterator<POSplit> it = slaves.iterator(); it.hasNext();) {
                POSplit slave = it.next();
                int pos = positions.get(slave).intValue();
                positions.put(slave, new Integer(pos - n));
            }
        }
    }
	*/
}
