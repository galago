/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.impl.io.PigFile;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;

public class POStore extends PhysicalOperator {
    private PigFile f;
    private String funcSpec;
    boolean append = false;

    public POStore(PhysicalOperator input, String filename, String sfunc, boolean append) {
    	super(LogicalOperator.FIXED);
    	funcSpec = sfunc;
        inputs = new PhysicalOperator[1];
        inputs[0] = input;
        System.out.println("Creating " + filename);
        f = new PigFile(filename, append);
        
    }

    public POStore(String filename, String sfunc, boolean append) {
    	super(LogicalOperator.FIXED);
        inputs = new PhysicalOperator[1];
        inputs[0] = null;
        funcSpec = sfunc;
        f = new PigFile(filename,append);
        this.append = append;
    }

    public Tuple getNext() throws IOException {
        // get all tuples from input, and store them.
        DataBag b = new DataBag();
        Tuple t;
        while ((t = (Tuple) inputs[0].getNext()) != null) {
            b.add(t);
        }
        try {
        	StorageFunc func = (StorageFunc) PigContext.instantiateArgFunc(funcSpec);
        	f.store(ExecType.LOCAL, b, func);
        } catch(IOException e) {
        	throw e;
        } catch(Exception e) {
        	IOException ne = new IOException(e.getClass().getName() + ": " + e.getMessage());
        	ne.setStackTrace(e.getStackTrace());
        	throw ne;
        }

        return null;
    }
    
    public int getOutputType(){
    	System.err.println("No one should be asking my output type");
    	new RuntimeException().printStackTrace();
    	System.exit(1);
    	return -1;
    }

}
