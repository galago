
//
// DocumentOrderedBinnedIndex
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "DocumentOrderedBinnedIndex.hpp"
#include "indri/Path.hpp"
#include "indri/DirectoryIterator.hpp"

//
// openRead
//

void DocumentOrderedBinnedIndex::openRead( const std::string& pathname ) {
    std::string documentPath = indri::file::Path::combine( pathname, "documentNames" );
    std::string parametersPath = indri::file::Path::combine( pathname, "manifest" );
    
    _documents.read( documentPath );
    _parameters.loadFile( parametersPath );

    indri::file::DirectoryIterator iterator( pathname, false );
    
    for( ; !(iterator == iterator.end()); iterator++ ) {
        std::string name = *iterator;
        std::string featureIndexPath = indri::file::Path::combine( pathname, name );

        if( indri::file::Path::isDirectory( featureIndexPath ) ) {
            DocumentOrderedBinnedListReader* featureIndex = new DocumentOrderedBinnedListReader();

            featureIndex->openRead( featureIndexPath );
            _readers[name] = featureIndex;
        }
    }
}

//
// getTerm
//

DocumentOrderedBinnedIterator* DocumentOrderedBinnedIndex::getTerm( const std::string& term ) const {
    return getTerm( term, "default" );
}

//
// getTerm
// 

DocumentOrderedBinnedIterator* DocumentOrderedBinnedIndex::getTerm( const std::string& term,
                                                                    const std::string& field ) const {
    std::map< std::string, DocumentOrderedBinnedListReader* >::const_iterator iter;    
    
    if( field.length() == 0 ) {
        iter = _readers.find( "default" );
    } else {
        iter = _readers.find( field );
    }
    
    if( iter != _readers.end() ) {
        const DocumentOrderedBinnedListReader* reader = iter->second;
        return reader->getTerm( term );
    }

    return 0;
}

//
// getDocument
//

std::string DocumentOrderedBinnedIndex::getDocument( int document ) {
    return _documents.get(document);
}

//
// close
//

void DocumentOrderedBinnedIndex::close() {
    std::map< std::string, DocumentOrderedBinnedListReader* >::iterator iter;
    
    for( iter = _readers.begin(); iter != _readers.end(); iter++ ) {
        iter->second->close();
        delete iter->second;
    }
    
    _readers.clear();
}
