
//
// Processor.java
//
// 27 September 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//

package galago.tupleflow;

import java.io.IOException;

public interface Processor<T> extends Step {
    public void process( T object ) throws IOException;
    public void close() throws IOException;
}
