package galago.service;
/*
 * QueryServlet
 *
 * June 9, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

import galago.Utility;
import galago.retrieval.Search;
import galago.retrieval.Search.SearchResult;
import galago.retrieval.Search.SearchResultItem;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author trevor
 */
public class SearchServlet extends HttpServlet {
    Search searcher;
    
    public int getInt( String s, int def ) {
        if( s == null )
            return def;
        try {
            def = Integer.parseInt(s);
        } catch( Exception e ) {}
        return def;
    }

    public boolean getBoolean( String s, boolean def ) {
        if( s == null )
            return def;
        if( s.equals( "false" ) || s.equals( "0" ) )
            return false;
        return true;
    }
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException { 
        String query = request.getParameter( "query" );
        String snippet;
        
        if( query == null ) {
            response.sendError( response.SC_BAD_REQUEST, "'query' is a required parameter." );
            return;
        }
        
        String startAtString = request.getParameter( "start" );
        String countString = request.getParameter( "count" );
        String summarizeString = request.getParameter( "summarize" );

        int startAt = getInt( startAtString, 0 );
        int count = getInt( countString, 10 );
        boolean summarize = getBoolean( summarizeString, true );

        SearchResult result;
        
        try {
            result = searcher.runQuery( query, startAt, count, summarize );
        } catch( Exception e ) {
            response.sendError( response.SC_INTERNAL_SERVER_ERROR, e.getMessage() );
            return;
        }

        String format = request.getParameter( "format" );
        
        if( format != null && format.equals( "xml" ) ) {
            writeXml( response, summarize, result );
        } else {
            request.setAttribute( "results", result );
            
            ServletContext context = getServletContext();
            RequestDispatcher dispatcher = context.getRequestDispatcher( "/websearch.jsp" );
            dispatcher.forward(request, response);
        }
    }

    private void writeXml(final HttpServletResponse response, final boolean summarize, final SearchResult result) throws IOException {
        response.setContentType( "text/xml" );
        response.setBufferSize( 8192 );
        PrintWriter out = response.getWriter();
        out.write( "<results>" );
        
        for( SearchResultItem item : result.items ) {
            out.write( "<result>" );
            out.write( "<rank>" + item.rank + "</rank>" );
            out.write( "<identifier>" + Utility.escape(item.identifier) + "</identifier>" );

            if( summarize ) {
                out.write( "<summary>" + Utility.escape(item.summary) + "</summary>" );
            }

            if( item.metadata.size() > 0 ) {
                out.write( "<metadata>" );

                for( Map.Entry<String, String> entry : item.metadata.entrySet() ) {
                    String key = Utility.escape(entry.getKey());
                    String value = Utility.escape(entry.getValue());

                    out.write( "<entry name=\"" + key + "\">" + value + "</entry>" );
                }

                out.write( "</metadata>" );
            }
            
            out.write( "</result>" );
        }

        out.write( "</results>" );
        out.close();
    }

    public void destroy() {
        try {
            searcher.close();
        } catch( Exception e ) {}
        super.destroy();
    }

    public void init(ServletConfig config) throws ServletException {
        try {
            String index = config.getInitParameter( "index" );
            String driverName = config.getInitParameter( "driverName" );
            String databaseUrl = config.getInitParameter( "databaseUrl" );
            
            searcher = new Search( index, databaseUrl, driverName );
            super.init(config);
        } catch( SQLException e ) {
            throw new ServletException(e);
        } catch( ClassNotFoundException e ) {
            throw new ServletException(e);
        } catch( IOException e ) {
            throw new ServletException(e);
        }
    }
}
