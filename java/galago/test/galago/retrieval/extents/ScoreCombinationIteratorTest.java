/*
 * ScoreCombinationIteratorTest.java
 * JUnit based test
 *
 * Created on October 9, 2007, 2:43 PM
 */

package galago.retrieval.extents;

import galago.tupleflow.Parameters;
import java.io.IOException;
import java.util.ArrayList;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class ScoreCombinationIteratorTest extends TestCase {
    int[] docsA = new int[] { 5, 10, 15, 20 };
    double[] scoresA = new double[] { 1.0, 2.0, 3.0, 4.0 };
    
    int[] docsB = new int[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
    double[] scoresB = new double[] { 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 };
    
    int[] docsTogether = new int[] { 2, 4, 5, 6, 8, 10, 12, 14, 15, 16, 18, 20 };
    double[] scoresTogether = new double[] { 2, 4, 1, 6, 8, 12, 12, 14, 3, 16, 18, 24 };
    
    public ScoreCombinationIteratorTest(String testName) {
        super(testName);
    }
    
    public void testNextCandidateFilter() throws IOException {
        System.out.println("nextCandidate");
        FakeScoreIterator one = new FakeScoreIterator( docsA, scoresA );
        FakeScoreIterator two = new FakeScoreIterator( docsB, scoresB );
        ArrayList<DocumentDataIterator> iterators = new ArrayList<DocumentDataIterator>();
        iterators.add(one);
        iterators.add(two);
        
        Parameters filterParameters = new Parameters();
        filterParameters.add( "requireAll", "1" );
        
        ScoreCombinationIterator instance = new ScoreCombinationIterator( filterParameters, iterators );

        assertEquals( 5, instance.nextCandidate() );
        instance.movePast( 10 );
        assertEquals( 15, instance.nextCandidate() );
    }

    public void testNextCandidateAny() throws IOException {
        System.out.println("nextCandidate");
        FakeScoreIterator one = new FakeScoreIterator( docsA, scoresA );
        FakeScoreIterator two = new FakeScoreIterator( docsB, scoresB );
        ArrayList<DocumentDataIterator> iterators = new ArrayList<DocumentDataIterator>();
        iterators.add(one);
        iterators.add(two);
        
        Parameters anyParameters = new Parameters();
        ScoreCombinationIterator instance = new ScoreCombinationIterator( anyParameters, iterators );

        assertEquals( 2, instance.nextCandidate() );
        instance.movePast( 2 );
        assertEquals( 4, instance.nextCandidate() );
        instance.movePast( 4 );
        assertEquals( 5, instance.nextCandidate() );
        instance.movePast( 5 );
        assertEquals( 6, instance.nextCandidate() );
    }
    
    public void testHasMatchAny() {
        System.out.println("hasMatch");
        FakeScoreIterator one = new FakeScoreIterator( docsA, scoresA );
        FakeScoreIterator two = new FakeScoreIterator( docsB, scoresB );
        ArrayList<DocumentDataIterator> iterators = new ArrayList<DocumentDataIterator>();
        iterators.add(one);
        iterators.add(two);

        Parameters anyParameters = new Parameters();
        ScoreCombinationIterator instance = new ScoreCombinationIterator( anyParameters, iterators );
        
        assertFalse( instance.hasMatch( 1 ) );
        assertTrue( instance.hasMatch( 2 ) );
        assertFalse( instance.hasMatch( 3 ) );        
    }

    public void testScore() throws IOException {
        System.out.println("score");
        FakeScoreIterator one = new FakeScoreIterator( docsA, scoresA );
        FakeScoreIterator two = new FakeScoreIterator( docsB, scoresB );
        ArrayList<DocumentDataIterator> iterators = new ArrayList<DocumentDataIterator>();
        iterators.add(one);
        iterators.add(two);

        Parameters anyParameters = new Parameters();
        ScoreCombinationIterator instance = new ScoreCombinationIterator( anyParameters, iterators );
        
        for( int i=0; i<12; i++ ) {
            assertFalse( instance.isDone() );
            assertTrue( instance.hasMatch( docsTogether[i] ) );
            assertEquals( scoresTogether[i], instance.score( docsTogether[i], 100 ) );

            instance.movePast( docsTogether[i] );
        }

        assertTrue( instance.isDone() );
    }

    public void testMovePast() throws Exception {
        FakeScoreIterator one = new FakeScoreIterator( docsA, scoresA );
        FakeScoreIterator two = new FakeScoreIterator( docsB, scoresB );
        ArrayList<DocumentDataIterator> iterators = new ArrayList<DocumentDataIterator>();
        iterators.add(one);
        iterators.add(two);

        Parameters anyParameters = new Parameters();
        ScoreCombinationIterator instance = new ScoreCombinationIterator( anyParameters, iterators );

        instance.movePast(5);
        assertEquals( 6, instance.nextCandidate() );
    }

    public void testMoveTo() throws Exception {
        FakeScoreIterator one = new FakeScoreIterator( docsA, scoresA );
        FakeScoreIterator two = new FakeScoreIterator( docsB, scoresB );
        ArrayList<DocumentDataIterator> iterators = new ArrayList<DocumentDataIterator>();
        iterators.add(one);
        iterators.add(two);

        Parameters anyParameters = new Parameters();
        ScoreCombinationIterator instance = new ScoreCombinationIterator( anyParameters, iterators );

        instance.moveTo(5);
        assertEquals( 5, instance.nextCandidate() );
    }
}
