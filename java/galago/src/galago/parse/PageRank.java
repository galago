/*
 * PageRank
 *
 * March 28, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.FileOrderedReader;
import galago.tupleflow.FileOrderedWriter;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.Processor;
import galago.tupleflow.Reducer;
import galago.tupleflow.Sorter;
import galago.tupleflow.Source;
import galago.tupleflow.Step;
import galago.types.DocumentFeature;
import galago.types.NumberedLink;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Computes PageRank iteratively given a file of links.
 *
 * Typically PageRank is defined as the principal eigenvector of the links matrix, but
 * it's simpler to just compute it iteratively.
 * 
 * We assume that for some random surfer at some document, there is a probability 
 * lambda that they'll choose to click on a link, and a (1-lambda) chance that they'll
 * just make a random jump.  The PageRank is the steady state probability of being...
 * 
 * BUGBUG: This code needs to be updated to work with TupleFlow property.
 *
 * @author trevor
 */
public class PageRank {
    String linksFilename;
    String probabilitiesFilename;
    double lambda;

    private String destProbsFilename;
    
    public static class ProbabilityReducer implements Reducer<DocumentFeature>, DocumentFeature.Processor, Source<DocumentFeature> {
        private DocumentFeature last;
        private Processor<DocumentFeature> processor;
        
        public void process( DocumentFeature feature ) throws IOException {
            if( last == null )
                last = feature;
            else if( last.document == feature.document )
                last.value += feature.document;
            else {
                processor.process( last );
                last = feature;
            }
        }
        
        public void setProcessor( Step stage ) throws IncompatibleProcessorException {
            Linkage.link( this, stage );
        }
        
        public Class<DocumentFeature> getOutputClass() {
            return DocumentFeature.class;
        }
        
        public Class<DocumentFeature> getInputClass() {
            return DocumentFeature.class;
        }
        
        public void close() throws IOException {
            if( last != null )
                processor.process( last );
            processor.close();
        }

        public ArrayList<DocumentFeature> reduce( List<DocumentFeature> input ) throws IOException {
            HashMap<Long, DocumentFeature> items = new HashMap();
            
            for( DocumentFeature item : input ) {
                DocumentFeature match = items.get(item.document);
                
                if( match == null ) {
                    items.put(item.document, match);
                } else {
                    match.value += item.value;
                }
            }

            return new ArrayList<DocumentFeature>( items.values() );
        }
    }
    
    
    /** Creates a new instance of PageRank */
    public PageRank() {
    }
    
    public void iteration() throws IOException, IncompatibleProcessorException {
        // open links and probabilities files
        FileOrderedReader<NumberedLink> linksReader = new FileOrderedReader( linksFilename, new NumberedLink().getOrder( "+source" ) );
        FileOrderedReader<DocumentFeature> probabilitiesReader = new FileOrderedReader( probabilitiesFilename, new DocumentFeature().getOrder( "+document" ) );
        
        // set up the output chain
        FileOrderedWriter<DocumentFeature> probabilitiesWriter = new FileOrderedWriter( destProbsFilename, new DocumentFeature().getOrder( "+document" ) );
        Sorter<DocumentFeature> sorter = new Sorter( new DocumentFeature().getOrder( "+document" ), new ProbabilityReducer() );
        ProbabilityReducer second = new ProbabilityReducer();
        second.setProcessor( probabilitiesWriter );
        sorter.setProcessor( second );
        
        NumberedLink lastLink = linksReader.read();
        DocumentFeature lastFeature = probabilitiesReader.read();
        ArrayList<NumberedLink> documentLinks = new ArrayList<NumberedLink>();
        
        // iterate through the links and probabilities file simultaneously
        while ( lastLink != null && lastFeature != null ) {
            // try to find a link that matches a feature
            if( lastLink.source > lastFeature.document ) {
                lastFeature = probabilitiesReader.read();
            } else {
                long document = lastLink.source;
                
                do {
                    documentLinks.add( lastLink );
                    lastLink = linksReader.read();
                } while( lastLink != null && lastLink.source == document );
                
                // this is the the probability of being at <document>.
                double probability = (1.0 - lambda);
                
                if( lastFeature.document == document )
                    probability += lastFeature.value;
                
                // from <document>, we can either go to one of the links,
                // (with probability lambda), or to a random location in
                // the collection (with probability 1-lambda).  

                // therefore, we send [(probability * lambda) / N] to each destination.
                double mass = lambda * probability / documentLinks.size();
                
                for( NumberedLink link : documentLinks ) {
                    sorter.process( new DocumentFeature( link.destination, mass ) );
                }
            }
        }
        
        sorter.close();
        
        probabilitiesReader.close();
        linksReader.close();
    }
    
    public void run() {
    }
    
}
