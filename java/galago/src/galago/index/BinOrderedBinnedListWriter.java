//
// BinOrderedBinnedListWriter.java
//
// 28 November 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.index;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.TupleFlowParameters;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import galago.tupleflow.Parameters;
import galago.tupleflow.Parameters.Value;
import galago.tupleflow.Processor;
import galago.tupleflow.VByteInput;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.NumberWordBin;
import galago.types.XMLFragment;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.NumberWordBin", order={"+word", "-bin", "+document"})
public class BinOrderedBinnedListWriter implements NumberWordBin.WordDescBinDocumentOrder.ShreddedProcessor {
    byte[] lastWord = null;
    long lastBin = 0;
    long lastDocument = 0;
    long blockSize = 32768;
    
    int skipMinimumBinLength;
    TreeMap<Integer, Integer> skipLengths;
    Processor<XMLFragment> statistics;
    
    public class BinnedList implements InvertedList {
        public BinnedList() {
            documentLists = new TreeMap<Long, BackedCompressedByteBuffer>();
            skipLists = new TreeMap<Long, BackedCompressedByteBuffer>();
        }
        
        public void close() {
            CompressedByteBuffer headerTail = new CompressedByteBuffer();
            ArrayList<Long> bins = new ArrayList<Long>(documentLists.keySet());
            Collections.reverse(bins);
            int totalLength = 0;
            boolean useSkips = false;

            // check to see if any of the bin lists is long enough
            // to have a skip in it; if so, we'll use skips for all the bins
            for( Long bin : bins ) {
                BackedCompressedByteBuffer binList = documentLists.get(bin);
                useSkips = useSkips || binList.length() > skipMinimumBinLength;
            }

            for( Long bin : bins ) {
                BackedCompressedByteBuffer binList = documentLists.get(bin);
                headerTail.add( bin.longValue() );

                if( useSkips ) {
                    BackedCompressedByteBuffer skips = null;

                    if( binList.length() > skipMinimumBinLength ) {
                        try {
                            assert binList.length() < Integer.MAX_VALUE;
                            int binLength = (int) binList.length();
                            int skipLength = skipLengths.get(skipLengths.headMap(binLength).lastKey());
                            skips = createSkips(binList, skipLength);
                        } catch (IOException ex) {
                            Logger.getLogger(BinOrderedBinnedListWriter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        skips = new BackedCompressedByteBuffer();
                    }

                    skipLists.put( bin, skips );
                    headerTail.add( skips.length() );
                }

                headerTail.add( binList.length() );
            }

            int options = 0;
            options |= (useSkips ? 1 : 0);

            header = new CompressedByteBuffer();
            header.add( headerTail.length() );
            header.add( options );
            header.add( documentCount );
            header.add( headerTail );
        }

        private BackedCompressedByteBuffer createSkips( BackedCompressedByteBuffer list, int distance ) throws IOException {
            BackedCompressedByteBuffer skips = new BackedCompressedByteBuffer();
            PositionalByteStream byteStream = new PositionalByteStream( list.getInputStream() );
            DataInputStream dataStream = new DataInputStream( byteStream );
            VByteInput stream = new VByteInput( dataStream );

            int lastDocument = 0;
            int lastPosition = 0;
            int document = 0;

            try {
                while (true) {
                    int delta = stream.readInt();
                    document += delta;

                    if (byteStream.position() > lastPosition + distance) {
                        skips.add( document - lastDocument );
                        skips.add( byteStream.position() - lastPosition );

                        lastPosition = byteStream.position();
                        lastDocument = document;
                    }
                }
            } catch( IOException e ) {}

            return skips;
        }
        
        public byte[] word() {
            return word;
        }
        
        public long dataLength() {
            long listLength = 0;
            if( header != null )
                listLength += header.length();

            for( BackedCompressedByteBuffer docList : documentLists.values() ) {
                listLength += docList.length();
            }

            for( BackedCompressedByteBuffer skipList : skipLists.values() ) {
                listLength += skipList.length();
            }

            return listLength;
        }

        public void write( final DataOutputStream output ) throws IOException {
            // first, write the data header
            header.write( output );
            
            // write each impact level in turn, in descending order
            ArrayList<Long> bins = new ArrayList<Long>(documentLists.keySet());
            Collections.reverse(bins);
            
            for( Long binKey : bins ) {
                BackedCompressedByteBuffer binList = documentLists.get(binKey);
                BackedCompressedByteBuffer skipList = skipLists.get(binKey);
                
                // write the skip list first
                if( skipList != null )
                    skipList.write( output );

                // inverted list data
                binList.write( output );
            }
            
            // this object is only written once.  once we're done writing, we
            // can delete all the temporary files
            for( BackedCompressedByteBuffer buffer : skipLists.values() ) {
                buffer.clear();
            }
            
            for( BackedCompressedByteBuffer buffer : documentLists.values() ) {
                buffer.clear();
            }
        }

        public byte[] word;
        public CompressedByteBuffer header;
        public Map<Long, BackedCompressedByteBuffer> skipLists;
        public Map<Long, BackedCompressedByteBuffer> documentLists;
    }
    
    BackedCompressedByteBuffer documentList;
    BinnedList invertedList;
    InvertedListWriter writer;
    long documentCount = 0;
    long maximumDocumentCount = 0;
    long maximumDocumentNumber = 0;
    
    private void initializeSkipLengths( final Parameters parameters ) {
        skipMinimumBinLength = Integer.MAX_VALUE;
        skipLengths = new TreeMap<Integer, Integer>();

        if( parameters.containsKey( "skipLengths" ) ) {
            //decode it
            for( Value v : parameters.list( "skipLengths/pair") ) {
                int binLength = Integer.parseInt(v.get("binLength"));
                int skipLength = Integer.parseInt(v.get("skipLength"));
                skipLengths.put( binLength, skipLength );
                skipMinimumBinLength = Math.min( skipMinimumBinLength, binLength );
            }
        } else {
            // for lists longer than 2048 bytes, skip every 128.
            skipLengths.put( 2048, 128 );
            skipMinimumBinLength = 2048;
        }
    }
    
    /**
     * Creates a new instance of BinnedListWriter
     */
    @SuppressWarnings("unchecked")
    public BinOrderedBinnedListWriter( TupleFlowParameters parameters ) throws FileNotFoundException, IOException {
        documentList = null;
        invertedList = null;
        writer = new InvertedListWriter( parameters );
        
        initializeSkipLengths( parameters.getXML() );
        statistics = parameters.getTypeWriter( "statistics" );
    }
    
    public void processWord( byte[] wordBytes ) throws IOException {
        if( invertedList != null ) {
            assert Utility.compare(invertedList.word, wordBytes) < 0 : Utility.makeString(invertedList.word) + " " + Utility.makeString(wordBytes);
            invertedList.close();
            writer.add( invertedList );
        }
        
        invertedList = new BinnedList();
        invertedList.word = wordBytes;
    }
    
    public void processBin( int bin ) {
        documentList = new BackedCompressedByteBuffer();
        invertedList.documentLists.put( (long)bin, documentList );
        lastDocument = 0;
    }
    
    public void processDocument( int document ) throws IOException {
        documentList.add( document - lastDocument );
        documentCount++;
        lastDocument = document;
        maximumDocumentNumber = Math.max( document, maximumDocumentNumber );
    }
    
    public void processTuple() {}
    
    private void resetDocumentCount() {
        maximumDocumentCount = Math.max( documentCount, maximumDocumentCount );
        documentCount = 0;
    }
    
    public void close() throws IOException {
        if( invertedList != null ) {
            invertedList.close();
            writer.add( invertedList );
        }

        writer.close();
        
        if( statistics != null ) {
            statistics.process( new XMLFragment( "documentCount", Long.toString(maximumDocumentNumber) ) );
            statistics.process( new XMLFragment( "maximumDocumentCount", Long.toString(maximumDocumentCount) ) );
            statistics.close();
        }
    }
    
    public long documentCount() {
        return maximumDocumentNumber;
    }
    
    public long maximumDocumentCount() {
        return maximumDocumentCount;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !parameters.getXML().containsKey( "index" ) ) {
            handler.addError( "BinOrderedBinnedListWriter requires an 'index' parameter." );
            return;
        }
        
        String index = parameters.getXML().get( "index" );
        Verification.requireWriteableDirectory( index, handler );
    }
}

