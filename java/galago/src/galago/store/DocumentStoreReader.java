/*
 * DocumentStoreReader
 *
 * May 22, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.store;

import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.Processor;
import galago.tupleflow.Step;
import galago.parse.Document;
import java.io.IOException;

/**
 * A DocumentStoreReader creates a stream of Document objects suitable for
 * parsing.  You can use this class when you'd like to use a DocumentStore
 * database as a source of text for indexing.
 *
 * @author trevor
 */
public class DocumentStoreReader implements ExNihiloSource<Document> {
    Processor<Document> processor;
    
    public void setProcessor( Step processor ) throws IncompatibleProcessorException {
        Linkage.link( this, processor );
    }

    public void run() throws IOException {
        // TODO: fill in this method
        throw new IOException( "Haven't implemented this yet, sorry" );
    }

    public Class<Document> getOutputClass() {
        return Document.class;
    }
}
