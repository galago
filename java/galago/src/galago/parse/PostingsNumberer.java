/*
 * PostingsNumberer
 *
 * May 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentIdentifierNumber;
import galago.types.DocumentLengthWordCount;
import galago.types.NumberWordCount;
import galago.Utility;
import galago.tupleflow.execution.ErrorHandler;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author trevor
 */
public class PostingsNumberer extends StandardStep<DocumentLengthWordCount, NumberWordCount>
                              implements DocumentLengthWordCount.Processor, NumberWordCount.Source {
    HashMap<String, Integer> documentNumbers = new HashMap();
    
    public void process( DocumentLengthWordCount object ) throws IOException {
        processor.process( new NumberWordCount( documentNumbers.get(object.document), Utility.makeBytes(object.word), object.count ) );
    }
    
    public PostingsNumberer( TupleFlowParameters parameters ) throws IOException {
        TypeReader<DocumentIdentifierNumber> reader = parameters.getTypeReader( "documentNumbers" );
        DocumentIdentifierNumber dn;
        
        while( (dn = reader.read()) != null ) {
            documentNumbers.put( dn.identifier, dn.document );
        }
    }
    
    public Class<DocumentLengthWordCount> getInputClass() {
        return DocumentLengthWordCount.class;
    }
    
    public Class<NumberWordCount> getOutputClass() {
        return NumberWordCount.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "documentNumbers", DocumentIdentifierNumber.class, parameters, handler );
    }
}
