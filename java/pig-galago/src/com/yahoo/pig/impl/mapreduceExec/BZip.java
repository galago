/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.mapreduceExec;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.CompressionInputStream;
import org.apache.hadoop.io.compress.CompressionOutputStream;
import org.apache.hadoop.io.compress.Compressor;
import org.apache.hadoop.io.compress.Decompressor;
import org.apache.tools.bzip2.CBZip2InputStream;
import org.apache.tools.bzip2.CBZip2OutputStream;

public class BZip implements CompressionCodec {
    static class SillyInputWrapper extends CompressionInputStream {
        CBZip2InputStream zin;
        public SillyInputWrapper(InputStream in) throws IOException {
            super(in);
            int B = in.read();
            int Z = in.read();
            if (B != 'B' || Z != 'Z') throw new IOException("Not a BZip file");
            zin = new CBZip2InputStream(in);
        }

        public int read(byte[] b, int off, int len) throws IOException {
            return zin.read(b, off, len);
        }

        public void resetState() throws IOException {
            int B = in.read();
            int Z = in.read();
            if (B != 'B' || Z != 'Z') throw new IOException("Not a BZip file");
            zin = new CBZip2InputStream(in); 
        }

        public int read() throws IOException {
            return zin.read();
        }
        public void close() throws IOException {
            zin.close();
        }
    }
    
    static class SillyOutputWrapper extends CompressionOutputStream {
        CBZip2OutputStream zout;
        public SillyOutputWrapper(OutputStream os) throws IOException {
            super(os);
            os.write("BZ".getBytes());
            zout = new CBZip2OutputStream(os);
        }
        public void finish() throws IOException {
            zout.flush();
        }

        public void resetState() throws IOException {
            zout.flush();
            zout.write("BZ".getBytes());
            zout = new CBZip2OutputStream(out);
            
        }

        public void write(byte[] b, int off, int len) throws IOException {
            zout.write(b, off, len);
        }

        public void write(int b) throws IOException {
            write(b);
        }
        
    }
    public static void registerCodec() {
        
    }
    public CompressionInputStream createInputStream(InputStream in)
            throws IOException {
        return new SillyInputWrapper(in);
    }

    public CompressionOutputStream createOutputStream(OutputStream out)
            throws IOException {
        return new SillyOutputWrapper(out);
    }

    public String getDefaultExtension() {
        return ".bz";
    }

    public CompressionOutputStream createOutputStream(OutputStream outputStream, Compressor compressor) throws IOException {
        return null;
    }

    public Class getCompressorType() {
        return null;
    }

    public Compressor createCompressor() {
        return null;
    }

    public CompressionInputStream createInputStream(InputStream inputStream, Decompressor decompressor) throws IOException {
        return null;
    }

    public Class getDecompressorType() {
        return null;
    }

    public Decompressor createDecompressor() {
        return null;
    }
}
