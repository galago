/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.window;

import java.util.ArrayList;
import java.util.List;

import com.yahoo.pig.impl.eval.EvalSpec;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

public abstract class WindowSpec extends EvalSpec {

	public enum windowType {SLIDING, HOPPING};
	windowType type;
	
	public WindowSpec(windowType type){
		this.type = type;
	}

	@Override
	public List<String> getFuncs() {
		return new ArrayList<String>();
	}

	@Override
	public SchemaItem mapInputSchema(SchemaItem input){
		return input;
	}
}
