/*
 * WeightedCombination
 *
 * April 5, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

/*****
 *
 *<step class="galago.parse.WeightedCombination"
					  input:heading="headingPostings"
					  input:title="titlePostings"
					  input:body="bodyPostings"
					  input:anchor="anchorPostings">  
					
					<feature>
						<name>heading</name>
						<weight>0.2</weight>
						<smoothing>dirichlet</smoothing>
						<mu>2500</mu>
					</feature>
					
					<feature>
						<name>title</name>
						<weight>0.3</weight>
						<smoothing>dirichlet</smoothing>
						<mu>2500</mu>
					</feature>       
					
					<feature>
						<name>body</name>
						<weight>0.5</weight>
						<smoothing>dirichlet</smoothing>
						<mu>2500</mu>
					</feature>  
					
					<feature>
						<name>anchor</name>
						<weight>0.2</weight>
						<smoothing>dirichlet</smoothing>
						<mu>2500</mu>
					</feature>
					
				</step>                                      
 **/

package galago.binner;

import galago.Utility;
import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.tupleflow.Step;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentLengthWordCount;
import galago.types.DocumentProbability;
import galago.types.DocumentWordProbability;
import galago.types.WordCount;
import galago.types.XMLFragment;
import galago.scoring.DistributionSmoother;
import galago.scoring.DistributionSmootherFactory;
import galago.tupleflow.OutputClass;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

/**
 *
 * @author trevor
 */

@OutputClass(className="galago.types.DocumentWordProbability", order="+word")
public class WeightedCombination implements ExNihiloSource<DocumentWordProbability>,
                                            DocumentWordProbability.Source {
    public class Source implements Comparable<Source> {
        TypeReader<DocumentLengthWordCount> postings;
        double weight;
        DistributionSmoother smoother;
        String name;
        int length;
        boolean scored = false;
        
        DocumentLengthWordCount top;
        
        public int compareTo( Source other ) {
            int result = top.word.compareTo( other.top.word );
            if( result != 0 ) return result;
            return top.document.compareTo( other.top.document );
        }

        public void next() throws IOException {
            top = postings.read();
        }
        
        private double scoreMatch() throws IOException {
            double result = this.weight * smoother.smooth( background, top.count, top.length );
            if( result == Double.NEGATIVE_INFINITY )
                System.err.println( "returning -inf from match: " + name + " " + top.count + " " + top.length + " " + background );
            scored = true;
            return result;
        }
        
        private double scoreNonMatch() throws IOException {
            if( scored ) {
                scored = false;
                return 0;
            }
            
            double result = this.weight * smoother.smooth( background, 0, length );
            if( result == Double.NEGATIVE_INFINITY )
                System.err.println( "returning -inf from non match: " + name + " " + background + " " + length );
            return result;
        }
    }
    
    ArrayList<Source> sources = new ArrayList<Source>();
    public Processor<DocumentWordProbability> processor;
    HashMap<String, Double> priors;
    
    String currentBackgroundWord = null;
    double background;
    private double collectionLength;
    
    TypeReader<WordCount> lm;
    
    public WeightedCombination( TupleFlowParameters parameters ) throws IOException {
        Parameters p = parameters.getXML();

        getCollectionLength( parameters.getTypeReader( "collectionLength" ) );
        priors = new HashMap();

        for( Parameters.Value feature : p.list( "feature" ) ) {
            String name = feature.get( "name" );
            double weight = Double.parseDouble( feature.get("weight") );
            
            Source s = new Source();
            s.name = name;
            s.postings = parameters.getTypeReader( name );
            s.weight = weight;
            s.smoother = DistributionSmootherFactory.newInstance( feature );
            s.length = Integer.parseInt(feature.get("length"));

            s.next();
            sources.add(s);
        }
        
        lm = parameters.getTypeReader("lm");
    }
    
    public void getCollectionLength( TypeReader<XMLFragment> reader ) throws IOException {
        XMLFragment fragment = null;
        
        while( (fragment = reader.read()) != null ) {
            if( fragment.nodePath.equals( "collectionLength" ) ) {
                collectionLength = Double.parseDouble(fragment.innerText);
                break;
            }
        }
    }
    
    public void updateBackground( String word ) throws IOException {
        WordCount wc = null;
        
        if( currentBackgroundWord != null && word.equals(currentBackgroundWord) )
            return;

        background = 0;
        while( (wc = lm.read()) != null ) {
            int result = wc.word.compareTo(word);
            
            if( result == 0 ) {
                currentBackgroundWord = word;
                background = (double)wc.count / collectionLength;
                assert wc.count > 0;
                return;
            }

            if( result > 0 ) {
                // we passed it!
                throw new IOException( "Couldn't find word: " + word + " in the language model." );
            }
        }
        
        throw new IOException( "Couldn't find word: " + word + " in the language model." );
    }
    
    public void run() throws IOException {
        PriorityQueue<Source> queue = new PriorityQueue<Source>();
        
        for( Source source : sources ) {
            if( source.top != null )
                queue.add(source);
        }
                
        while (queue.size() > 0) {
            // pick the smallest iterator from the queue
            Source first = queue.poll();
            updateBackground(first.top.word);
            double probability = 0;
            
            String document = first.top.document;
            String word = first.top.word;
            
            assert document != null;
            assert word != null;
            
            probability += first.scoreMatch();
            
            // now, we want to find all other iterators that
            // are also pointing to this document/word combination and score them
            while (queue.size() > 0) {
                Source next = queue.peek();
                assert next.top != null;
                
                if (document.equals(next.top.document) &&
                    word.equals(next.top.word) ) {
                    probability += next.scoreMatch();
                
                    queue.poll();
                    next.next();
                    
                    if( next.top != null )
                        queue.add(next);
                } else {
                    // this one didn't match, so none of the others will.
                    break;
                }
            }
            
            // we now run through the queue, scoring everything that didn't match.
            // any source that has already been scored will return 0 here.
            for( Source nonMatching : queue ) {
                probability += nonMatching.scoreNonMatch();
            }
            
            // add the first item back into the queue
            first.next();
            if( first.top != null ) {
                queue.add(first);
            }
            
            probability = Math.log(probability);
            processor.process( new DocumentWordProbability( document, Utility.makeBytes(word), probability ) );
        }
        
        processor.close();
    }

    public void setProcessor( Step processor ) throws IncompatibleProcessorException {
        Linkage.link( this, processor );
    }
    
    public Class<DocumentWordProbability> getOutputClass() {
        return DocumentWordProbability.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "collectionLength", XMLFragment.class, parameters, handler );

        for( Parameters.Value feature : parameters.getXML().list( "feature" ) ) {
            String name = feature.get( "name" );
            double weight = Double.parseDouble( feature.get("weight") );
            
            if( feature.containsKey( "type" ) && feature.get( "type" ).equals( "prior" ) ) {
                Verification.verifyTypeReader( name, DocumentProbability.class, parameters, handler );
            } else {
                Verification.verifyTypeReader( name, DocumentLengthWordCount.class, parameters, handler );
            }
        }
        
        Verification.verifyTypeReader( "lm", WordCount.class, parameters, handler );
    }
}
