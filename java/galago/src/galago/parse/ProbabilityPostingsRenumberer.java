/*
 * DocumentRenumberer
 *
 * April 18, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentIdentifierNumber;
import galago.types.DocumentWordProbability;
import galago.types.NumberWordProbability;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.DocumentWordProbability", order="+document")
@OutputClass(className="galago.types.NumberWordProbability")
public class ProbabilityPostingsRenumberer extends StandardStep<DocumentWordProbability, NumberWordProbability> {
    DocumentIdentifierNumber current = null;
    TypeReader<DocumentIdentifierNumber> reader;
    
    /** Creates a new instance of DocumentRenumberer */
    public ProbabilityPostingsRenumberer( TupleFlowParameters parameters ) throws IOException {
        reader = parameters.getTypeReader( "documentNumbers" );
        current = reader.read();
    }

    public void process( DocumentWordProbability object ) throws IOException {
        // we assume that the documentNumbers input contains one mapping for each
        // document we'll find in this list.
        assert current != null;
        int result;
        
        while( (result = current.identifier.compareTo(object.document)) < 0 ) {
            current = reader.read();
        }
        
        assert result == 0;
        assert current != null;
        
        processor.process( new NumberWordProbability( current.document, object.word, object.probability ) );
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "documentNumbers", DocumentIdentifierNumber.class, parameters, handler );
    }
}
