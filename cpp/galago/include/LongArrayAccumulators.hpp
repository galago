
//
// LongArrayAccumulators
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef THEATER_LONGARRAYACCUMULATORS_HPP
#define THEATER_LONGARRAYACCUMULATORS_HPP

#include "indri/indri-platform.h"
#include <vector>
#include "BinOrderedBinnedIterator.hpp"
#include "indri/ScoredExtentResult.hpp"
                           
class LongArrayAccumulators {
private:                 
    std::vector<UINT64> _accumulators;
    std::vector<UINT32> _maxUnseen;
    std::vector<UINT32> _fullUnseen;
	std::vector<UINT32> _scoreCounts;
                                    
    int _threshold;
    bool _needsTrim;
    bool _canIgnorePostings;
    
    void processBinOr( int score, int termIndex, BinOrderedBinnedIterator* iterator );
    void processBinAnd( int score, int termIndex, BinOrderedBinnedIterator* iterator );
    void computeFullUnseen();

	void _newScore( UINT32 newScore );
	void _updateScore( UINT32 oldScore, UINT32 newScore );

public:
    LongArrayAccumulators( int termCount );
    
    bool useAndMode();                                                          
    void processBin( int score, int termIndex, BinOrderedBinnedIterator* iterator );  
    void processBin( int score, BinOrderedBinnedIterator* iterator );  
    void setMaxUnseen( int termIndex, int score );
    
    void calculateThresholdScore( int k );
    void trim();
    int size();
    int totalUnseen();                                          
    bool canIgnoreFuturePostings( int requested );              
    std::vector<indri::api::ScoredExtentResult> topResults( int k ); 
    
    static int maxTerms();
};

#endif // THEATER_LONGARRAYACCUMULATORS_HPP
