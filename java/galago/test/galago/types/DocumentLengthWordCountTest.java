/*
 * DocumentLengthWordCountTest.java
 * JUnit based test
 *
 * Created on May 25, 2007, 5:40 PM
 */

package galago.types;

import junit.framework.*;
import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;
import galago.tupleflow.OrderedReader;
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Processor;
import galago.tupleflow.Type;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Comparator;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author trevor
 */
public class DocumentLengthWordCountTest extends TestCase {
    
    public DocumentLengthWordCountTest(String testName) {
        super(testName);
    }

    public void testToString() {
        System.out.println("toString");
        DocumentLengthWordCount instance = new DocumentLengthWordCount();
        String result = instance.toString();
        assertEquals( result, "null,0,null,0" );
        
        instance = new DocumentLengthWordCount( "hey", 2, "you", 5 );
        result = instance.toString();
        assertEquals( "hey,2,you,5", result );
    }

    public void testGetOrder() {
        System.out.println("getOrder");
        
        String spec = "";
        DocumentLengthWordCount instance = new DocumentLengthWordCount();
        
        Order<DocumentLengthWordCount> expResult = null;
        Order<DocumentLengthWordCount> result = instance.getOrder(spec);
        assertEquals(expResult, result);
        
        spec = "+document";
        result = instance.getOrder(spec);
        assertNotNull( result );
    }
}
