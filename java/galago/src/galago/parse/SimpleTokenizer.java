
//
// SimpleTokenizer.java
//
// 28 September 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.parse;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * @author trevor
 */
public class SimpleTokenizer {
    boolean[] splits = new boolean[256];
    
    /** Creates a new instance of SimpleTokenizer */
    public SimpleTokenizer() {
        for(int i=0; i<splits.length;i++)
            splits[i] = false;
        
        splits[(byte)' '] = true;
        splits[(byte)'\t'] = true;
        splits[(byte)'\n'] = true;
        splits[(byte)'\r'] = true;
        splits[(byte)';'] = true;
        splits[(byte)'\"'] = true;
        splits[(byte)'&'] = true;
        splits[(byte)'/'] = true;
        splits[(byte)'.'] = true;
        splits[(byte)':'] = true;
        splits[(byte)'!'] = true;
        splits[(byte)'#'] = true;
        splits[(byte)'\''] = true;
        splits[(byte)'?'] = true;
        splits[(byte)'$'] = true;
        splits[(byte)'%'] = true;
        splits[(byte)'('] = true;
        splits[(byte)')'] = true;
        splits[(byte)'@'] = true;
        splits[(byte)'^'] = true;
        splits[(byte)'*'] = true;
        splits[(byte)'+'] = true;
        splits[(byte)'-'] = true;
        splits[(byte)','] = true;
        splits[(byte)'='] = true;
        splits[(byte)'>'] = true;
        splits[(byte)'<'] = true;
        splits[(byte)'['] = true;
        splits[(byte)']'] = true;
        splits[(byte)'{'] = true;
        splits[(byte)'}'] = true;
        splits[(byte)'|'] = true;
        splits[(byte)'`'] = true;
        
        for( char c = 0; c <= 32; c++ ) {
            splits[c] = true;
        }
    }
    
    public String asciiLower( String s ) {
        StringBuilder builder = new StringBuilder(s.length());
        
        for( int i=0; i<s.length(); i++ ) {
            char c = s.charAt(i);
            
            if( c >= 'A' && c <= 'Z' )
                c += ('a' - 'A');
            
            builder.append(c);
        }
        
        return builder.toString();
    }
    
    public ArrayList<String> tokenize( String text ) {
        StringTokenizer tokenizer = new StringTokenizer( text, " \n\r\t;\"&/:." );
        ArrayList<String> result = new ArrayList<String>();
        
        result.ensureCapacity( text.length() / 4 );
        int start = 0;
        boolean isLower = true;
        boolean isAscii = true;
        
        int i;
        for( i=0; i<text.length(); i++ ) {
            char c = text.charAt(i);
            if( c>=0 && c<256 && splits[c] ) {
                if( start != i ) {
                    String token = text.substring(start, i);

                    if( !isLower ) {
                        if( !isAscii )
                            token = token.toLowerCase().trim();
                        else
                            token = asciiLower(token).trim();
                    }
                    
                    result.add( token );
                }
                
                start = i+1;
                isLower = true;
                isAscii = true;
            } else if( c < 'a' || c > 'z' ) {
                isLower = false;
                isAscii = (c >= 0 && c <= 255);
            }
        }

        if( start != i )
            result.add( text.substring(start, i-start).trim().toLowerCase() );
        
        return result;
    }
}

