/*
 * NullExtentIteratorTest.java
 * JUnit based test
 *
 * Created on September 13, 2007, 6:56 PM
 */

package galago.retrieval.extents;

import junit.framework.*;
import galago.util.ExtentArray;

/**
 *
 * @author trevor
 */
public class NullExtentIteratorTest extends TestCase {
    public NullExtentIteratorTest(String testName) {
        super(testName);
    }

    public void testNextDocument() {
        NullExtentIterator instance = new NullExtentIterator();
        instance.nextDocument();
    }

    public void testIsDone() {
        NullExtentIterator instance = new NullExtentIterator();
        assertEquals( true, instance.isDone() );
    }
}
