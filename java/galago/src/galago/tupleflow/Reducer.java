//
// Reducer.java
//
// 7 December 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.tupleflow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface Reducer<T> {
    public ArrayList<T> reduce( List<T> input ) throws IOException;
}
