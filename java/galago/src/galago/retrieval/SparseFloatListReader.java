/*
 * ScoreIndex
 *
 * October 2, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import galago.retrieval.extents.ScoreIterator;
import galago.tupleflow.BufferedDataStream;
import galago.tupleflow.VByteInput;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class SparseFloatListReader {
    public class ScoreListIterator implements ScoreIterator {
        IndexReader.Iterator iterator;
        
        VByteInput stream;
        int documentCount;
        int index;
        
        int currentDocument;
        double currentScore;
        
        public ScoreListIterator( IndexReader.Iterator iterator ) throws IOException {
            this.iterator = iterator;
            load();
        }
        
        void load() throws IOException {
            BufferedDataStream buffered = reader.blockStream(iterator);
            stream = new VByteInput( buffered );
            documentCount = stream.readInt();
            index = -1;
            currentDocument = 0;
            
            if( documentCount > 0 )
                read();
        }
        
        void read() throws IOException {
            index += 1;

            if( index < documentCount ) {
                currentDocument += stream.readInt();
                currentScore = stream.readFloat();
            }
        }
        
        public void reset() throws IOException {
            currentDocument = 0;
            currentScore = 0;
            load();
        }

        public int nextCandidate() {
            return currentDocument;
        }
        
        public String currentTerm() {
            return iterator.currentTerm();
        }
        
        public void nextTerm() throws IOException {
            iterator.nextTerm();
            load();
        }

        public boolean hasMatch(int document) {
            return document == currentDocument;
        }
        
        public void moveTo(int document) throws IOException {
            while( !isDone() && document > currentDocument )
                read();
        }

        public void movePast(int document) throws IOException {
            while( !isDone() && document >= currentDocument )
                read();
        }

        public double score(int document, int length) {
            if( document == currentDocument )
                return currentScore;
            return Double.NEGATIVE_INFINITY;
        }

        public boolean isDone() {
            return index >= documentCount;
        }
    }
    
    IndexReader reader;
    
    public SparseFloatListReader( String pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }

    public ScoreListIterator getIterator() throws IOException {
        return new ScoreListIterator( reader.getIterator() );
    }

    public ScoreListIterator getScores( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new ScoreListIterator( iterator );
        
        return null;
    }
    
    void close() throws IOException {
        reader.close();
    }
}
