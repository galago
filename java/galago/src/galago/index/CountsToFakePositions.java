/*
 * CountsToFakePositions
 *
 * May 17, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.index;

import galago.tupleflow.StandardStep;
import galago.types.NumberWordPosition;
import galago.types.NumberWordCount;
import java.io.IOException;

/**
 * Converts a NumberWordCount object into many NumberWordPosition objects;
 * this is an easy way to encode count information as positions that can be
 * used with the PositionsListWriter.
 *
 * @author trevor
 */
public class CountsToFakePositions extends StandardStep<NumberWordCount, NumberWordPosition> {
    public void process( NumberWordCount object ) throws IOException {
        for( int i=0; i<object.count; i++ ) {
            processor.process( new NumberWordPosition( object.document, object.word, i ) );
        }
    }
    
    public Class<NumberWordCount> getInputClass() {
        return NumberWordCount.class;
    }
    
    public Class<NumberWordPosition> getOutputClass() {
        return NumberWordPosition.class;
    }
}
