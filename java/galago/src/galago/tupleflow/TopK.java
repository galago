/*
 * Max.java
 */

package galago.tupleflow;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author trevor
 */
public class TopK<T> implements Processor<T> {
    Comparator<T> key;
    Comparator<T> combined;
    int limit;
    int batch;
    ArrayList<T> compacted;
    ArrayList<T> objects;
    Processor<T> processor;
    
    /** Creates a new instance of Max */
    public TopK( Processor<T> processor, Comparator<T> key, Comparator<T> combined, int limit, int batch ) {
        this.key = key;
        this.combined = combined;
        this.batch = batch;
        this.limit = limit;
        this.processor = processor;
        this.objects = new ArrayList();
        this.compacted = new ArrayList();
    }
    
    public void process( T object ) throws IOException {
        objects.add(object);
        
        if( objects.size() > batch )
            compact();
    }
    
    public void close() throws IOException {
        compactAndFlushCompletely();
        processor.close();
    }

    public void compact() throws IOException {
        compacted.addAll( compact( objects ) );
        objects = new ArrayList<T>();
        
        // if we got rid of less than 20% of the stuff,
        // we'll just flush now
        if( compacted.size() > batch ) {
            compacted = compact( compacted );
            flush( limit );
        }
    }
    
    public ArrayList<T> compact( ArrayList<T> array ) throws IOException {
        // sort current objects by combined key
        Collections.sort( array, combined );
        T last = null;
        int lastCount = 0;
        ArrayList<T> newObjects = new ArrayList<T>();
        
        // remove any objects that aren't in the max limit
        for( T object : array ) {
            if( last != null && key.compare( last, object ) == 0 ) {
                if( lastCount < limit )
                    newObjects.add(object);
                lastCount++;
            } else {
                last = object;
                lastCount = 1;
                newObjects.add(object);
            }
        }

        return newObjects;
    }
    
    /**
     * Flushes all but the last few objects (specified by retain)
     * from the object buffer.  By holding out a few objects at each
     * compaction, we ensure that we get perfect output when the 
     * input to this Max stage is already sorted.
     */
    
    public void flush( int retain ) throws IOException {
        if( compacted.size() != 0 ) {
            int endPoint = compacted.size() - retain;
            
            for( int i=0; i < endPoint; i++ ) {
                T object = compacted.get(i);
                processor.process(object);
            }
            
            List<T> endList = compacted.subList( endPoint, compacted.size() );
            compacted = new ArrayList<T>( endList );
        }
    }
    
    public void compactAndFlushCompletely() throws IOException {
        compact();
        flush(0);
    }
}
