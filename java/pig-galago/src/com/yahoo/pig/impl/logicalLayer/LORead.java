/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;
import com.yahoo.pig.impl.physicalLayer.IntermedResult;


public class LORead extends LogicalOperator {
    public IntermedResult readFrom = null;
		
	public LORead(PigContext pigContext, IntermedResult readFromIn) {
		super(pigContext);
		inputs = new LogicalOperator[0];
		
        readFrom = readFromIn;
	}
	public String name() {
		return "Read";
	}
	public String arguments() {
		return alias;
	}
     
    public SchemaItemList outputSchema() {
    	if (schema == null) {
            if (readFrom.lp != null && readFrom.lp.root != null && readFrom.lp.root.outputSchema() != null) {
                schema = readFrom.lp.root.outputSchema().copy();
            } else {
                schema = new SchemaItemList();
            }
    	}
    	
        schema.alias = alias;
        
        return schema;
    }
    
    
	
	public int getOutputType(){
		return readFrom.getOutputType();
	}
}
