/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.func;

import java.util.*;

import com.yahoo.pig.EvalFunc;
import com.yahoo.pig.builtin.ShellBagEvalFunc;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalItemList;
import com.yahoo.pig.impl.eval.NestableEvalItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public abstract class FuncEvalItem extends NestableEvalItem {
    public EvalFunc func = null;
    public EvalItemList args = null;
     
    public FuncEvalItem(PigContext pigContext, EvalFunc func, EvalItemList args) {
    	super(pigContext);
        this.func = func;
        this.args = args;
    }
    
    @Override
    public void finish(){
    	if (func instanceof ShellBagEvalFunc){
    		((ShellBagEvalFunc)func).finish();
    	}
    }
    
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(func.toString());
        sb.append("("); 
        sb.append(args);
        sb.append(")");
        
        if (nestedEval != null) sb.append(nestedEval);
        
        if (subColSpec != null) sb.append(".(" + subColSpec + ")");
        
        return sb.toString();   
    }

    public List<String> getFuncs() {
        List<String> funcs = new ArrayList<String>();
        
        if (func != null) funcs.add(func.toString());
        
        if (nestedEval != null) funcs.addAll(nestedEval.getFuncs());
        
        if (subColSpec != null) funcs.addAll(subColSpec.getFuncs());
        
        return funcs;
    }

    @Override
    public SchemaItem mapInputSchemaInitial(SchemaItem input){
    	if (func == null)
    		return new SchemaItemList();
    	
    	SchemaItem output = func.outputSchema();
    	
    	if (output == null )
    		return new SchemaItemList();
    	
    	return output;
    }

    
    
}
	