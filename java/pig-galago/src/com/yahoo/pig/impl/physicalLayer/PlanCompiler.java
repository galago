/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.Map;

import com.yahoo.pig.GroupFunc;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.*;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;

public abstract class PlanCompiler {
	PigContext pigContext;
	protected PlanCompiler(PigContext pigContext) {
		this.pigContext = pigContext;
	}
    public abstract PhysicalOperator compile(LogicalOperator lo, Map queryResults) throws IOException;

    // various helper methods for map-reduce and hadoop compilation:

    protected String toString(GroupFunc[] funcs, boolean[] isInner) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < funcs.length; i++) {
            if (i != 0)
                sb.append(';');
            if (isInner[i])
                sb.append('-');
            sb.append(funcs[i].toString());
        }
        return sb.toString();
    }

    protected String joinString(String[] args, char c) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < args.length; i++) {
            if (i > 0)
                sb.append(',');
            sb.append(args[i]);
        }
        return sb.toString();
    }



    protected EvalSpecPipeList addEval(EvalSpecPipeList list, EvalSpecPipeList toAdd) throws IOException {
        if (list == null) list = new EvalSpecPipeList(pigContext);
        
        if (toAdd == null) {  // as a default, use "star"
            EvalItemList newSpec = new EvalItemList(pigContext);
            newSpec.add(new StarEvalItem(pigContext));
            EvalSpecPipe newPipe = new EvalSpecPipe(pigContext);
            newPipe.add(newSpec);
            list.add(newPipe);
        } else {
            if (toAdd.list.size() != 1) throw new IOException("Internal error: method addEval() was expecting single pipe.");
            list.add(toAdd.list.get(0));
        }
        
        return list;
    }
    
    protected String addFile(String target, String file) {
        return (target == null || target.equals("")) ? file : target + ';' + file;
    }

    public static String getTempFile() throws IOException {
        return FileLocalizer.getTemporaryPath(null).toString();
    }
}
