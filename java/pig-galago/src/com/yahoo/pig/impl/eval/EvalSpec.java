/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.util.List;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

public abstract class EvalSpec {
    
    public abstract DataCollector collector(DataCollector output);
    
    public abstract List<String> getFuncs();
    
    public boolean isJustStar() {
        if (this instanceof EvalItemList) return ((EvalItemList) this).isJustStar();
        else return false;
    }
    
    public abstract SchemaItem mapInputSchema(SchemaItem schema);

}
