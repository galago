/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.LinkedList;
import java.util.List;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

abstract public class LogicalOperator {
    public String            alias                = null;
    
    public static final int  FIXED                = 1;
    public static final int  MONOTONE             = 2;
    public static final int  UPDATABLE            = 3;   // Reserved for future use
    public static final int  AMENDABLE            = 4;

    public LogicalOperator[] inputs;
    public int               requestedParallelism = -1;
    public SchemaItemList    schema               = null;
    public PigContext pigContext;

    protected LogicalOperator(PigContext pigContext) {
    	this.pigContext = pigContext;
    }

    public abstract SchemaItemList outputSchema();

    public String name() {
        return "ROOT";
    }

    public LogicalOperator[] children() {
        return inputs;
    }

    public String arguments() {
        return "";
    }

    public List<String> getFuncs() {
        List<String> funcs = new LinkedList<String>();
        for (int i = 0; i < inputs.length; i++) {
            funcs.addAll(inputs[i].getFuncs());
        }
        return funcs;
    }
    
    public abstract int getOutputType();
}
