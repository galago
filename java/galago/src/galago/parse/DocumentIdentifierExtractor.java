/*
 * DocumentIdentifierExtractor
 *
 * April 19, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentIdentifier;
import java.io.IOException;

/**
 * Extracts just the identifier and the document length from a document object,
 * and outputs a DocumentIdentifier.
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.types.DocumentIdentifier")
public class DocumentIdentifierExtractor extends StandardStep<Document, DocumentIdentifier> {
    public void process( Document document ) throws IOException {
        processor.process( new DocumentIdentifier(document.identifier, document.terms.size()) );
    }
}
