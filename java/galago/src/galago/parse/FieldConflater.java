/*
 * FieldConflater
 *
 * April 27, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Parameters.Value;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.parse.Document")
@Verified
public class FieldConflater extends StandardStep<Document, Document> {
    HashMap<String, String> conflations = new HashMap<String, String>();
    
    public FieldConflater( TupleFlowParameters parameters ) {
        List<Value> values = parameters.getXML().list( "field" );
        
        for( Value field : values ) {
            List<String> sources = field.stringList( "source" );
            String destination = field.get( "destination" );

            for( String s : sources ) {
                conflations.put( s, destination );
            }
        }
    }
    
    public void process( Document document ) throws IOException {
        for( Tag tag : document.tags ) {
            if (conflations.containsKey(tag.name)) {
                tag.name = conflations.get(tag.name);
            }
        }
        
        processor.process(document);
    }   
}
