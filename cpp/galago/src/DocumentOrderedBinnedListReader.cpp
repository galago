
//
// DocumentOrderedBinnedListReader
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "DocumentOrderedBinnedListReader.hpp"
#include "indri/Path.hpp"

//
// openRead
//

void DocumentOrderedBinnedListReader::openRead( const std::string& path ) {
    std::string manifestPath = indri::file::Path::combine( path, "manifest" );
    _manifest.loadFile( manifestPath );
    _reader.openRead( path );
}

//
// getTerm
//

DocumentOrderedBinnedIterator* DocumentOrderedBinnedListReader::getTerm( const std::string& term ) const {
    ListRegion region = _reader.getTerm( term );
    
    if( region.data == 0 )
        return 0;

    return new DocumentOrderedBinnedIterator( region.data, region.start, region.end );
}

//
// close
//

void DocumentOrderedBinnedListReader::close() {
    _reader.close();
}

