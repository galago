/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer.schema;

import java.util.ArrayList;
import java.util.List;




/**
 * A SchemaField encapsulates a column alias
 * and its numeric column value.
 * @author dnm
 *
 */
public class SchemaField extends SchemaItem {
    public Type type = Type.string;
    
    public SchemaField(String alias) {
        this.alias = alias;
    }
    public Type getType() { return type; }
    public int colFor(String alias) {
    	return -1;
    }
    public SchemaItem schemaFor(int col) {
        return null;
    }
    
    public SchemaField copy(){
    	return new SchemaField(alias);
    }
    
    public List<SchemaItem> flatten(){
    	List<SchemaItem> ret = new ArrayList<SchemaItem>();
    	ret.add(this);
    	return ret;
    }
}
