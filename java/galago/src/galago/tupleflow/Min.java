package galago.tupleflow;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public class Min<T> implements Processor<T> {
    TopK<T> topk;
    Order<T> order;
    
    /** Creates a new instance of Max */
    public Min( Processor<T> processor, Order<T> key, Order<T> combined, int limit, int batch ) {
        this.order = key;
        topk = new TopK( processor, key.lessThan(), combined.lessThan(), limit, batch );
    }
    
    public Class<T> getInputClass() {
        return order.getOrderedClass();
    }
    
    public void process( T object ) throws IOException {
        topk.process(object);
    }
    
    public void close() throws IOException {
        topk.close();
    }
}
