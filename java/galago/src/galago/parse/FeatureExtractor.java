
//
// FeatureExtractor.java
//
// 11 December 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.parse;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public interface FeatureExtractor {
    public void extract( String id, Document document ) throws IOException;
    public void close() throws IOException;
}
