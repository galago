//
// PriorCombiner.java
//
// Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.binner;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentProbability;
import galago.types.DocumentWordProbability;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.DocumentWordProbability", order={ "+document" })
@OutputClass(className="galago.types.DocumentWordProbability", order={ "+document" })
public class PriorCombiner extends StandardStep<DocumentWordProbability, DocumentWordProbability> {
    TypeReader<DocumentProbability> priorValues;
    DocumentProbability current;
    double weight;
    
    public PriorCombiner( TupleFlowParameters parameters ) throws IOException {
        String priorName = parameters.getXML().get( "prior" );
        weight = Double.parseDouble(parameters.getXML().get( "weight" ) );
        priorValues = parameters.getTypeReader( priorName );
        current = priorValues.read();
    }
    
    @Override
    public void process(DocumentWordProbability object) throws IOException {
        assert current != null;
        int compareResult;
        
        while( (compareResult = object.document.compareTo(current.document)) > 0 )
            current = priorValues.read();
        
        assert compareResult == 0 : "Couldn't find a prior in the list.  Posting:" + object.document + " PriorFile:" + current.document;
        double result = weight * current.probability + object.probability;
        processor.process( new DocumentWordProbability( object.document, object.word, result ) );
    }

    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !Verification.requireParameters( new String[] { "prior", "weight" }, parameters.getXML(), handler ) )
            return;
        
        String priorName = parameters.getXML().get( "prior" );
        Verification.verifyTypeReader( priorName, galago.types.DocumentProbability.class, parameters, handler );
    }
}
