
//
// AccumulatorTypes
//
// 23 February 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_ACCUMULATORTYPES_HPP
#define GALAGO_ACCUMULATORTYPES_HPP

template<class _T>
class AccumulatorTypes {
public:
    typedef UINT64 term_type;
    typedef UINT64 update_type;
};

#endif // GALAGO_ACCUMULATORTYPES_HPP

