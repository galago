// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/DocumentIdentifier.java DocumentIdentifier galago.types String:identifier int:length order:+identifier
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class DocumentIdentifier implements Type<DocumentIdentifier> {
    public String identifier;
    public int length; 
    
    public DocumentIdentifier() {}
    public DocumentIdentifier( String identifier, int length ) {
        this.identifier = identifier;
        this.length = length;
    }  
    
    public String toString() {
            return String.format( "%s,%d",
                                   identifier, length );
    } 

    public Order<DocumentIdentifier> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+identifier" } ) ) {
            return new IdentifierOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<DocumentIdentifier> {
        public void process( DocumentIdentifier object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class IdentifierOrder implements Order<DocumentIdentifier> {
        public int hash( DocumentIdentifier object ) {
            int h = 0;
            h += Utility.hash(object.identifier);
            return h;
        } 
        public Comparator<DocumentIdentifier> greaterThan() {
            return new Comparator<DocumentIdentifier>() {
                public int compare( DocumentIdentifier one, DocumentIdentifier two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.identifier, two.identifier );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentIdentifier> lessThan() {
            return new Comparator<DocumentIdentifier>() {
                public int compare( DocumentIdentifier one, DocumentIdentifier two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.identifier, two.identifier );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentIdentifier> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentIdentifier> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentIdentifier> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentIdentifier > {
            DocumentIdentifier last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentIdentifier object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.identifier, last.identifier) ) { processAll = true; shreddedWriter.processIdentifier( object.identifier ); }
               shreddedWriter.processTuple( object.length );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentIdentifier> getInputClass() {
                return DocumentIdentifier.class;
            }
        } 
        public ReaderSource<DocumentIdentifier> orderedCombiner( Collection<TypeReader<DocumentIdentifier>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentIdentifier> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentIdentifier clone( DocumentIdentifier object ) {
            DocumentIdentifier result = new DocumentIdentifier();
            if ( object == null ) return result;
            result.identifier = object.identifier; 
            result.length = object.length; 
            return result;
        }                 
        public Class<DocumentIdentifier> getOrderedClass() {
            return DocumentIdentifier.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+identifier"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processIdentifier( String identifier ) throws IOException;
            public void processTuple( int length ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastIdentifier;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processIdentifier( String identifier ) {
                lastIdentifier = identifier;
                buffer.processIdentifier( identifier );
            }
            public final void processTuple( int length ) throws IOException {
                if( lastFlush ) {
                    if( buffer.identifiers.size() == 0 ) buffer.processIdentifier( lastIdentifier );
                    lastFlush = false;
                }
                buffer.processTuple( length );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getLength() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushIdentifier( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getIdentifierEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getIdentifier() );
                    output.writeInt( count );
                    buffer.incrementIdentifier();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushIdentifier( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> identifiers = new ArrayList();
            ArrayList<Integer> identifierTupleIdx = new ArrayList();
            int identifierReadIdx = 0;
                            
            int[] lengths;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                lengths = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processIdentifier( String identifier ) {
                identifiers.add( identifier );
                identifierTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int length ) {
                assert identifiers.size() > 0;
                lengths[writeTupleIndex] = length;
                writeTupleIndex++;
            }
            public void resetData() {
                identifiers.clear();
                identifierTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                identifierReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementIdentifier() {
                identifierReadIdx++;  
            }                                                                                              

            public void autoIncrementIdentifier() {
                while( readTupleIndex >= getIdentifierEndIndex() && readTupleIndex < writeTupleIndex )
                    identifierReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getIdentifierEndIndex() {
                if( (identifierReadIdx+1) >= identifierTupleIdx.size() )
                    return writeTupleIndex;
                return identifierTupleIdx.get(identifierReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getIdentifier() {
                assert readTupleIndex < writeTupleIndex;
                assert identifierReadIdx < identifiers.size();
                
                return identifiers.get( identifierReadIdx );
            }
            public int getLength() {
                assert readTupleIndex < writeTupleIndex;
                return lengths[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getLength() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexIdentifier( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processIdentifier( getIdentifier() );
                    assert getIdentifierEndIndex() <= endIndex;
                    copyTuples( getIdentifierEndIndex(), output );
                    incrementIdentifier();
                }
            }  
            public void copyUntilIdentifier( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getIdentifier(), other.getIdentifier() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processIdentifier( getIdentifier() );
                                      
                        copyTuples( getIdentifierEndIndex(), output );
                    } else {
                        output.processIdentifier( getIdentifier() );
                        copyTuples( getIdentifierEndIndex(), output );
                    }
                    incrementIdentifier();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilIdentifier( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentIdentifier>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentIdentifier.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentIdentifier.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentIdentifier>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentIdentifier> getOutputClass() {
                return DocumentIdentifier.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentIdentifier read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentIdentifier result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentIdentifier>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentIdentifier last = new DocumentIdentifier();         
            long updateIdentifierCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getIdentifier(), otherBuffer.getIdentifier() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentIdentifier read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentIdentifier result = new DocumentIdentifier();
                
                result.identifier = buffer.getIdentifier();
                result.length = buffer.getLength();
                
                buffer.incrementTuple();
                buffer.autoIncrementIdentifier();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateIdentifierCount - tupleCount > 0 ) {
                            buffer.identifiers.add(last.identifier);
                            buffer.identifierTupleIdx.add( (int) (updateIdentifierCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateIdentifier();
                        buffer.processTuple( input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateIdentifier() throws IOException {
                if( updateIdentifierCount > tupleCount )
                    return;
                     
                last.identifier = input.readString();
                updateIdentifierCount = tupleCount + input.readInt();
                                      
                buffer.processIdentifier( last.identifier );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentIdentifier.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentIdentifier.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentIdentifier>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentIdentifier> getOutputClass() {
                return DocumentIdentifier.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentIdentifier last = new DocumentIdentifier();
            boolean identifierProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processIdentifier( String identifier ) throws IOException {  
                if( identifierProcess || Utility.compare( identifier, last.identifier ) != 0 ) {
                    last.identifier = identifier;
                    processor.processIdentifier( identifier );
                    identifierProcess = false;
                }
            }  
            
            public void resetIdentifier() {
                 identifierProcess = true;
            }                                                
                               
            public void processTuple( int length ) throws IOException {
                processor.processTuple( length );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentIdentifier last = new DocumentIdentifier();
            public galago.tupleflow.Processor<DocumentIdentifier> processor;                               
            
            public TupleUnshredder( DocumentIdentifier.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentIdentifier> processor ) {
                this.processor = processor;
            }
            
            public DocumentIdentifier clone( DocumentIdentifier object ) {
                DocumentIdentifier result = new DocumentIdentifier();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.length = object.length; 
                return result;
            }                 
            
            public void processIdentifier( String identifier ) throws IOException {
                last.identifier = identifier;
            }   
                
            
            public void processTuple( int length ) throws IOException {
                last.length = length;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentIdentifier last = new DocumentIdentifier();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentIdentifier clone( DocumentIdentifier object ) {
                DocumentIdentifier result = new DocumentIdentifier();
                if ( object == null ) return result;
                result.identifier = object.identifier; 
                result.length = object.length; 
                return result;
            }                 
            
            public void process( DocumentIdentifier object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.identifier, object.identifier ) != 0 || processAll ) { processor.processIdentifier( object.identifier ); processAll = true; }
                processor.processTuple( object.length );                                         
            }
                          
            public Class<DocumentIdentifier> getInputClass() {
                return DocumentIdentifier.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    