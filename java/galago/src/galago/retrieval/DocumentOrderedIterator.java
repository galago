/*
 * DocumentOrderedIterator
 * 
 * October 5, 2007 -- Trevor Strohman
 * 
 * BSD License (http://galagosearch.org)
 */

package galago.retrieval;

/**
 *
 * @author trevor
 */
public interface DocumentOrderedIterator {
    public long listByteLength();
    public int currentDocument();
    public int currentScore();
    public int listBound();
    public void nextDocument() throws java.io.IOException;

    public boolean skipToBound(int bound) throws java.io.IOException;
    public boolean skipToDocument(int skipTo) throws java.io.IOException;
    public boolean skipToDocument(int skipTo, int bound) throws java.io.IOException;
    
    public boolean isDone();
}
