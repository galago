
//
// Normalizer
//
// 9 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_NORMALIZER_HPP
#define GALAGO_NORMALIZER_HPP

#include <string>

class Normalizer {
private:
	static char _ignore[];
	
public:
	void process( std::string& term ) const;
};

#endif // GALAGO_NORMALIZER_HPP

