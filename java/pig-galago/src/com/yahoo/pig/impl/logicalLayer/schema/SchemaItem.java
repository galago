/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer.schema;

import java.util.List;

public abstract class SchemaItem {
    public String alias = null;
    public int    col   = -1;
    public static enum Type {
        complex, number, string
    };
    public abstract Type getType();
    public abstract int colFor(String alias);
    public abstract SchemaItem schemaFor(int col);
    
    public abstract SchemaItem copy();
    public abstract List<SchemaItem> flatten();
}
