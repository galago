/*
 * DenseIntegerListIndexReader
 * 
 * Oct 5, 2007, 2:08:45 PM -- Trevor Strohman
 * 
 * BSD License (http://galagosearch.org)
 */

package galago.retrieval;

import galago.tupleflow.BufferedDataStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class DenseIntegerIndexReader implements DocumentOrderedBinnedListReader {
    IndexReader reader;
    
    public class DenseIntegerIterator implements DocumentOrderedIterator {
        IndexReader.Iterator iterator;
        BufferedDataStream stream;
        int document;
        int score;
        int bound;
        long length;
        
        DenseIntegerIterator( IndexReader.Iterator iterator ) throws IOException {
            this.iterator = iterator;
            load();
        }
        
        void load() throws IOException {
            this.document = 0;
            this.length = iterator.getListEnd() - iterator.getListStart() - 1;
            this.stream = reader.blockStream( iterator );
            this.bound = stream.readUnsignedByte();
        }

        void loadDocument() throws IOException {
            if( document < length ) {
                stream.seekRelative(document + 1);
                score = stream.readUnsignedByte();
            }
        }
        
        public void nextTerm() throws IOException {
            iterator.nextTerm();
            load();
        }
        
        public boolean skipToBound( int bound ) throws IOException {
            while( document < length && score < bound )
                nextDocument();
            return document < length && score >= bound;
        }
        
        public boolean skipToDocument( int d ) throws IOException {
            if( d > document ) {
                if( d < length ) {
                    document = d;
                    loadDocument();
                    return true;
                } else {
                    document = (int) length;
                    return false;
                }
            }
            return false;
        }

        public boolean skipToDocument(int skipTo, int bound) throws IOException {
            boolean result = skipToDocument(skipTo);
            return result && score >= bound;
        }
        
        public void nextDocument() throws IOException {
            document++;
            loadDocument();
        }
        
        public int currentDocument() {
            return document;
        }
        
        public int currentScore() {
            return score;
        }
        
        public boolean isDone() {
            return document >= length;
        }
        
        public boolean isValid() {
            return !isDone();
        }

        public int listBound() {
            return bound;
        }

        public long listByteLength() {
            return iterator.getListEnd() - iterator.getListStart();
        }
    }
    
    public DenseIntegerIndexReader( String pathname ) throws FileNotFoundException, IOException {
        reader = new IndexReader( pathname );
    }
    
    public DenseIntegerIterator getIterator() throws IOException {
        return new DenseIntegerIterator( reader.getIterator() );
    }

    public DenseIntegerIterator getTerm( String term ) throws IOException {
        IndexReader.Iterator iterator = reader.getTerm( term );
        
        if( iterator != null )
            return new DenseIntegerIterator( iterator );
        
        return null;
    }
    
    public void close() throws IOException {
        reader.close();
    }
}
