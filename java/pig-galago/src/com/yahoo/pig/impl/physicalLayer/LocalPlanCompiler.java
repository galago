/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Map;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.LOCogroup;
import com.yahoo.pig.impl.logicalLayer.LOEval;
import com.yahoo.pig.impl.logicalLayer.LOLoad;
import com.yahoo.pig.impl.logicalLayer.LORead;
import com.yahoo.pig.impl.logicalLayer.LOStore;
import com.yahoo.pig.impl.logicalLayer.LOUnion;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;

/**
 * LocalPlanCompliler will general an execution plan for ExecType.LOCAL jobs
 * using the PhysicalOperator framework.  It fulfills as similar role as the 
 * MapreducePlanCompiler, but is far more simple.
 * @author dnm
 *
 */
public class LocalPlanCompiler extends PlanCompiler {

    protected LocalPlanCompiler(PigContext pigContext) {
		super(pigContext);
    }
	@Override
    public PhysicalOperator compile(LogicalOperator lo, Map queryResults) throws IOException {
        PhysicalOperator po = compileOperator(lo, queryResults);
        for (int i = 0; i < lo.inputs.length; i++) {
            po.inputs[i] = compile(lo.inputs[i], queryResults);
        }
        return po;
    }

    protected PhysicalOperator compileOperator(LogicalOperator lo, Map queryResults) throws IOException {

        if (lo instanceof LOEval) {
            return new POEval(((LOEval) lo).spec,lo.getOutputType());
        } else if (lo instanceof LOCogroup) {
            return new POCogroup(((LOCogroup) lo).specs,lo.getOutputType());
        }  else if (lo instanceof LOLoad) {
            LOLoad lol = (LOLoad) lo;
            StorageFunc lf = null;
            // We compile the passed parameters in to a proper load function
            // for local execution.
            // ==============================
            String loaderName = lol.lf.getClass().getName();
            try {
                Class loaderClass = Class.forName(loaderName);
                if (lol.args != null && lol.args.length > 0) {
                    String cleaned[] = new String[lol.args.length];
                    Class paramTypes[] = new Class[lol.args.length];
                    for (int i = 0; i < paramTypes.length; i++) {
                        paramTypes[i] = String.class;
                        cleaned[i] = lol.args[i].replaceAll("\'", "");
                    }
                    Constructor c = loaderClass.getConstructor(paramTypes);
                    lf = (StorageFunc) c.newInstance((Object[]) cleaned);
                } else {
                    lf = (StorageFunc) loaderClass.newInstance();
                }
            } catch (Exception e) {
                e.printStackTrace();
                lf = new PigStorage();
            }
            return new POLoad(((LOLoad) lo).filename, lf,lo.getOutputType());
        } else if (lo instanceof LORead) {
            IntermedResult readFrom = ((LORead) lo).readFrom;

            if (readFrom.executed()) {
                // reading from a materialized databag; use PORead
                return new PORead(readFrom.read(),readFrom.getOutputType());
            } else {
                if (readFrom.compiled()) {
                    // other plan already compiled, so split its output
                    return new POSplitSlave(readFrom.pp.root,readFrom.getOutputType());
                } else {
                    // other plan not compiled yet, so compile it and use it directly
                    readFrom.compile(queryResults);
                    return readFrom.pp.root;
                }
            }
        } else if (lo instanceof LOStore) {
            LOStore los = (LOStore) lo;
            return new POStore(los.filename, los.sf,los.append);
        } else if (lo instanceof LOUnion) {
            return new POUnion(((LOUnion) lo).inputs.length,lo.getOutputType());
        } else {
            throw new IOException("Unknown logical operator.");
        }
    }

}
