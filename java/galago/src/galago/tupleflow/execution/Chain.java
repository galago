/*
 * Chain
 *
 * March 15, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Processor;
import galago.tupleflow.Linkage;
import galago.tupleflow.Source;
import galago.tupleflow.Step;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class Chain {
    ArrayList<Step> items = new ArrayList();

    public void add( Step stage ) throws IncompatibleProcessorException {
        if( items.size() > 0 ) {
            // is this a ShreddedProcessor?
            Object previousSource = items.get(items.size()-1);
            ((Source)previousSource).setProcessor( stage );
        }

        items.add( stage );
    }
    
    public Step getStage() {
        if( items.size() > 0 ) {
            Step first = items.get(0);
            return first;
        }
        return null;
    }
}
