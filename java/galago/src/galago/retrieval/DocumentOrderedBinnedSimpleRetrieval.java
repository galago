/*
 * DocumentOrderedBinnedRetrieval
 *
 * October 4, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/**
 * This evaluates queries against a DocumentOrderedBinnedIndex.  The point of this
 * implementation is to be correct and easy to understand but not necessarily fast.
 *
 * @author trevor
 */
public class DocumentOrderedBinnedSimpleRetrieval extends DocumentOrderedBinnedRetrieval {
    public DocumentOrderedBinnedSimpleRetrieval( DocumentOrderedBinnedIndex index ) {
        super(index);
    }
    
    PriorityQueue<ScoredDocument> runQueueQuery( List<DocumentOrderedIterator> iterators, List<DocumentOrderedIterator> priors, boolean conjunctive, int requested ) throws IOException {
        PriorityQueue<ScoredDocument> queue = new PriorityQueue<ScoredDocument>();
        ArrayList<DocumentOrderedIterator> allIterators = new ArrayList<DocumentOrderedIterator>();
        allIterators.addAll( iterators );
        allIterators.addAll( priors );
        
        int document = moveIterators(-1, conjunctive, iterators, priors);

        while (document < Integer.MAX_VALUE) {
            int score = scoreDocument(document, allIterators);
            
            // push score onto a heap
            queue.add( new ScoredDocument( document, score ) );

            while( queue.size() > requested )
                queue.poll();
            
            // move to next document
            document = moveIterators(document, conjunctive, iterators, priors);
        }
        
        return queue;
    }

    private int scoreDocument(int document, List<DocumentOrderedIterator> iterators) {
        // score minimum document
        int score = 0;
        
        for (DocumentOrderedIterator iter : iterators) {
            if (!iter.isDone() && document == iter.currentDocument()) {
                score += iter.currentScore();
            }
        }

        return score;
    }
    
    private int moveIterators( int document, boolean conjunctive, List<DocumentOrderedIterator> iterators, List<DocumentOrderedIterator> priors ) throws IOException {
        // first, move past the current document
        int minimumDocument = moveMatchingIterators(document, iterators);

        if (conjunctive) {
            // now that we've moved forward, try to make all of the iterators match
            while (true) {
                if( anyIteratorsAreDone(iterators) )
                    return Integer.MAX_VALUE;
                
                if( allIteratorsMatch(iterators) )
                    return minimumDocument;
                
                minimumDocument = moveMatchingIterators(document, iterators);
            }
        }
        
        // now, move the priors to match the new document position
        for( DocumentOrderedIterator prior : priors ) {
            prior.skipToDocument( minimumDocument );
        }
        
        return minimumDocument;
    }
    
    private boolean anyIteratorsAreDone( List<DocumentOrderedIterator> iterators ) throws IOException {
        for( DocumentOrderedIterator iter : iterators ) {
            if( iter.isDone() )
                return true;
        }
        
        return false;
    }
    
    private boolean allIteratorsMatch( List<DocumentOrderedIterator> iterators ) throws IOException {
        int target = -1;
        
        if( iterators.size() == 0 )
            return false;
        
        for( DocumentOrderedIterator iter : iterators ) {
            if( iter.isDone() )
                return true;
            
            if( target < 0 ) {
                target = iter.currentDocument();
            } else if( target != iter.currentDocument() ) {
                return false;
            }
        }
        
        return true;
    }

    private int moveMatchingIterators(int document, List<DocumentOrderedIterator> iterators) throws IOException {
        // first, start by moving forward all iterators that match the last scored document
        int minimumDocument = Integer.MAX_VALUE;

        for (DocumentOrderedIterator iter : iterators) {
            if (iter.isDone())
                continue;

            if (iter.currentDocument() == document) {
                iter.nextDocument();
            }

            minimumDocument = Math.min(minimumDocument, iter.currentDocument());
        }
        
        return minimumDocument;
    }
}
