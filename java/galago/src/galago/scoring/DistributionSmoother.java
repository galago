
//
// DistributionSmoother.java
//
// 1 May 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.scoring;

/**
 *
 * @author trevor
 */
public interface DistributionSmoother {
    public double smooth( String word, int count, int length );
    public double smooth( double background, int count, int length );
}
