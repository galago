/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;

import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.Tuple;

public class DIVIDE extends AtomEvalFunc {

    @Override
    public void exec(Tuple input, DataAtom output) throws IOException {
        double v1 = input.getAtomField(0).numval();
        double v2 = input.getAtomField(1).numval();
        output.setValue(v1/v2);
    }

}
