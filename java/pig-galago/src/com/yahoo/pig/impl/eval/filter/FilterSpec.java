/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.List;

import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.EvalSpec;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;

public class FilterSpec extends EvalSpec {
    public Cond cond;
    
    public FilterSpec(Cond cond) {
        this.cond = cond;
    }

    public List<String> getFuncs() {
        return cond.getFuncs();
    }

    public String toString() {
    	return cond.toString();
    }

    @Override
    public DataCollector collector(final DataCollector output) {
        return new DataCollector() {

            public void add(Tuple t) throws IOException {
                if (t != null) {
                    if (cond.eval(t)) {
                        output.add(t);
                    }
                } else {
                    output.add(null);  // pass on EOF
                }
            }
        };
    }
    
    @Override
	public SchemaItem mapInputSchema(SchemaItem input){
		return input;
	}
}
