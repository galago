/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;
import java.util.StringTokenizer;

import com.yahoo.pig.BagEvalFunc;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class TOKENIZE extends BagEvalFunc {

    @Override
    public void exec(Tuple input, DataCollector output) throws IOException {
        String str = input.getAtomField(0).strval();
        StringTokenizer tok = new StringTokenizer(str, " \",()*", false);
        while (tok.hasMoreTokens()) {
            output.add(new Tuple(tok.nextToken()));
        }
    }

    @Override
    public SchemaItem outputSchema() {
        SchemaItemList schema = new SchemaItemList();
        schema.add(new SchemaField("token"));
        return schema;
    }
}
