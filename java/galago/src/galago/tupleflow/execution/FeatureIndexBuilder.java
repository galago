
//
// FeatureIndexBuilder
//
// 18 October 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

package galago.tupleflow.execution;

import galago.tupleflow.Parameters;
import galago.tupleflow.Parameters.Value;
import galago.tupleflow.execution.Job.StagePoint;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author trevor
 */
public class FeatureIndexBuilder {
    private String stopwordsFile;
    private String requiredWordsFile;
    
    static String[] linkStopwords = { 	"a", "the", "that", "of", "and", "or", "here", "click" };
    private ArrayList<DocumentCountFeature> features;
    private ArrayList<FeatureIndexBuilder.CountFeatureWeighting> weightings;
    private ArrayList<FeatureIndexBuilder.CountFeatureWeighting> phrases = new ArrayList<CountFeatureWeighting>();
    private ArrayList<FeatureIndexBuilder.CountFeatureWeighting> singles = new ArrayList<CountFeatureWeighting>();
    private ArrayList<WeightedPrior> priors;
    
    public FeatureIndexBuilder( ArrayList<DocumentCountFeature> features,
                                ArrayList<CountFeatureWeighting> weightings,
                                ArrayList<WeightedPrior> priors,
                                String stopwordsFile,
                                String requiredWordsFile ) {
        this.features = features;
        this.weightings = weightings;
        this.stopwordsFile = stopwordsFile;
        this.requiredWordsFile = requiredWordsFile;
        this.priors = priors;
    }
    
    public void determinePhrasesAndSingles() {
        phrases.clear();
        singles.clear();
        
        for( CountFeatureWeighting f : weightings ) {
            if( f.feature.getWidth() > 1 ) {
                phrases.add(f);
            } else if( f.feature.getWidth() == 1 ) {
                singles.add(f);
            }
        }
    }

    void addBinningStage(String name, Job job, Parameters jobParameters, List<CountFeatureWeighting> features ) {
        // binning stage
        Stage s = new Stage(name);
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "collectionLength", "galago.types.XMLFragment", new String[]{"+nodePath"}, null));
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "lm", "galago.types.WordCount", new String[]{"+word"}, null));
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "documentNumbers", "galago.types.DocumentIdentifierNumber", new String[]{"+document"}, null));
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "offsets", "galago.types.WordProbability", new String[]{"+word"}, null));

        if (jobParameters.get("binOrdered", true)) {
            s.add(new StageConnectionPoint(ConnectionPointType.Output, "binSortedBinned", "galago.types.NumberWordBin", new String[]{"+word", "-bin", "+document"}, null));
        }

        if (jobParameters.get("documentOrdered", true)) {
            s.add(new StageConnectionPoint(ConnectionPointType.Output, "docSortedBinned", "galago.types.NumberWordBin", new String[]{"+word", "+document"}, null));
        }
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "minimumOffset", "galago.types.XMLFragment", new String[]{"+nodePath"}, null));

        Parameters p = new Parameters();
        ArrayList<Value> featureDesc = new ArrayList<Value>();
        for (CountFeatureWeighting f : features) {
            Value v = new Value();
            v.add("name", f.feature.getName());
            v.add("weight", Double.toString(f.weight));
            v.add("mu", Double.toString(f.mu));
            v.add("length", Integer.toString(f.length));
            v.add("smoothing", "dirichlet");
            featureDesc.add(v);

            s.add(new StageConnectionPoint(ConnectionPointType.Input, f.feature.getName(), "galago.types.DocumentLengthWordCount", new String[]{"+word", "+document"}, null));
        }

        p.add("feature", featureDesc);
        s.add(new Step("galago.binner.WeightedCombination", p));

        s.add(createSort("galago.types.DocumentWordProbability", "+document"));

        for (WeightedPrior f : getWeightedPriors()) {
            s.add(new StageConnectionPoint(ConnectionPointType.Input, f.name, "galago.types.DocumentProbability", new String[]{"+document"}, null));

            p = new Parameters();
            p.add("prior", f.name);
            p.add("weight", Double.toString(f.weight));
            s.add(new Step("galago.binner.PriorCombiner", p));
        }

        p = new Parameters();
        p.add("numbers", "documentNumbers");
        s.add(new Step("galago.parse.ProbabilityPostingsRenumberer", p));
        s.add(createSort("galago.types.NumberWordProbability", "+word"));

        p = new Parameters();
        p.add("bins", jobParameters.get("bins", "64"));
        p.add("saturation", jobParameters.get("saturation", "0.001"));
        s.add(new Step("galago.binner.OffsetBinner", p));

        MultiStep multi = new MultiStep();
        multi.groups = new ArrayList<ArrayList<Step>>();
        ArrayList<Step> group;

        if (jobParameters.get("binOrdered", true)) {
            group = new ArrayList<Step>();
            group.add(createSort("galago.types.NumberWordBin", "+word -bin +document"));
            group.add(new OutputStep("binSortedBinned"));
            multi.groups.add(group);
        }

        if (jobParameters.get("documentOrdered", true)) {
            group = new ArrayList<Step>();
            group.add(createSort("galago.types.NumberWordBin", "+word +document"));
            group.add(new OutputStep("docSortedBinned"));
            multi.groups.add(group);
        }

        s.add(multi);
        job.add(s);
    }

    void addThresholdsStage(String name, Job job, List<CountFeatureWeighting> features) {
        // thresholds stage
        Stage s = new Stage(name);
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "lm", "galago.types.WordCount", new String[]{"+word"}, null));
        s.add(new StageConnectionPoint(ConnectionPointType.Input, "collectionLength", "galago.types.XMLFragment", new String[]{"+nodePath"}, null));
        s.add(new StageConnectionPoint(ConnectionPointType.Output, "offsets", "galago.types.WordProbability", new String[]{"+word"}, null));
        s.add(new StageConnectionPoint(ConnectionPointType.Output, "minimumOffset", "galago.types.XMLFragment", new String[]{"+nodePath"}, null));

        s.add(new InputStep("lm"));
        Parameters p = new Parameters();
        ArrayList<Value> featureDesc = new ArrayList<Value>();
        for (CountFeatureWeighting f :features) {
            Value v = new Value();
            v.add("name", f.feature.getName());
            v.add("weight", Double.toString(f.weight));
            v.add("length", Integer.toString(f.length));
            v.add("mu", Double.toString(f.mu));
            featureDesc.add(v);
        }
        p.add("feature", featureDesc);
        ArrayList<Value> priorDesc = new ArrayList<Value>();
        for (WeightedPrior fp : getWeightedPriors()) {
            Value v = new Value();
            v.add("name", fp.name);
            v.add("weight", Double.toString(fp.weight));
            priorDesc.add(v);

            s.add(new StageConnectionPoint(ConnectionPointType.Input, fp.name, "galago.types.DocumentProbability", new String[]{"+document"}, null));
        }
        p.add("prior", priorDesc);
        s.add(new Step("galago.binner.ThresholdGenerator", p));
        s.add(new OutputStep("offsets"));
        job.add(s);
    }

    private void addOffsetCombinationStage(Job job) {
        Stage s = new Stage("offset-combination");
        s.add( new StageConnectionPoint( ConnectionPointType.Output, "minimumOffset", "galago.types.XMLFragment", new String[] { "+nodePath" }, null ) );
        
        Parameters p = new Parameters();
        p.add("class", "galago.types.XMLFragment");
        p.add("order", "+nodePath");

        if (singles.size() > 0) {
            s.add( new StageConnectionPoint( ConnectionPointType.Input, "singles", "galago.types.XMLFragment", new String[] { "+nodePath" }, null ) );
            p.add("input", "singles");
        }
        if (phrases.size() > 0) {
            s.add( new StageConnectionPoint( ConnectionPointType.Input, "phrases", "galago.types.XMLFragment", new String[] { "+nodePath" }, null ) );
            p.add("input", "phrases");
        }
        s.add(new Step("galago.tupleflow.StreamCombiner", p));
        s.add(new Step("galago.tupleflow.XMLMinimum") );
        s.add(new OutputStep("minimumOffset"));
        job.add(s);
    }

    private String[] generateInputNames( String prefix ) {
        String[] inputs;

        if (phrases.size() > 0 && singles.size() > 0) {
            inputs = new String[]{prefix + "-phrases", prefix + "-singles"};
        } else if (phrases.size() > 0) {
            inputs = new String[]{prefix + "-phrases"};
        } else {
            inputs = new String[]{prefix + "-singles"};
        }
        
        return inputs;
    }
    
    public interface DocumentCountFeature {
        List<StageConnectionPoint> getParsingOutputs();
        ArrayList<Step> getParsingSteps();
        Job getInternalJob();
        String getName();
        int getWidth();
    }
    
    public class WeightedPrior {
        public String name;
        public String filename;
        public double weight;
        
        public WeightedPrior( String name, String filename, double weight ) {
            this.name = name;
            this.filename = filename;
            this.weight = weight;
        }
    }
    
    public class PostingFeature implements DocumentCountFeature {
        public int width;
        public String fieldName;
        public String streamName;
        public int maxLength;
        
        public PostingFeature( int width, int maxLength, String fieldName, String streamName ) {
            this.width = width;
            this.fieldName = fieldName;
            this.streamName = streamName;
            this.maxLength = maxLength;
        }
        
        public int getWidth() {
            return width;
        }
        
        public ArrayList<Step> getParsingSteps() {
            ArrayList<Step> steps = new ArrayList<Step>();
            Parameters parameters = new Parameters();
            
            if( fieldName != null )
                parameters.add( "field", fieldName );
            parameters.add( "width", Integer.toString(width) );
            parameters.add( "maxLength", Integer.toString(maxLength) );
            
            if( requiredWordsFile != null )
                parameters.add( "filter", requiredWordsFile );
    
            steps.add( new Step( "galago.parse.FieldPostingsCounter", parameters ) );
            steps.add( createSort( "galago.types.DocumentLengthWordCount", "+word +document" ) );
            steps.add( new OutputStep( streamName ) );
            
            return steps;
        }
        
        public Job getInternalJob() {
            return null;
        }
        
        public String getName() {
            return streamName;
        }

        public List<StageConnectionPoint> getParsingOutputs() {
            ArrayList<StageConnectionPoint> result = new ArrayList<StageConnectionPoint>();
            StageConnectionPoint point = new StageConnectionPoint( ConnectionPointType.Output,
                                                                   streamName, 
                                                                   "galago.types.DocumentLengthWordCount",
                                                                   new String[] { "+word", "+document" },
                                                                   null );
            result.add( point );
            return result;
        }
    }
    
    public class AnchorTextFeature implements DocumentCountFeature {
        String streamName;
        public AnchorTextFeature( String streamName ) {
            this.streamName = streamName;
        }
        
        public List<StageConnectionPoint> getParsingOutputs() {
            ArrayList<StageConnectionPoint> result = new ArrayList<StageConnectionPoint>();
            StageConnectionPoint documentURLs = new StageConnectionPoint( ConnectionPointType.Output,
                                                                    "documentURLs",
                                                                    "galago.types.DocumentURL",
                                                                    new String[] { "+url" },
                                                                    null );
            
            StageConnectionPoint extractedLinks = new StageConnectionPoint( ConnectionPointType.Output,
                                                                    "extractedLinks",
                                                                    "galago.types.ExtractedLink",
                                                                    new String[] { "+destUrl" },
                                                                    null );
            result.add( documentURLs );
            result.add( extractedLinks );
            return result;
        }
        
        public int getWidth() {
            return 1;
        }
        
        public ArrayList<Step> getParsingSteps() {
            ArrayList<Step> steps;
            MultiStep m = new MultiStep();
            m.groups = new ArrayList<ArrayList<Step>>();
            
            // first, extract URLs
            steps = new ArrayList<Step>();
            steps.add( new Step( "galago.parse.DocumentURLExtractor" ) );
            steps.add( createSort( "galago.types.DocumentURL", "+url" ) );
            steps.add( new OutputStep( "documentURLs" ) );
            m.groups.add(steps);
            
            // also extract anchor text
            steps = new ArrayList<Step>();
            steps.add( new Step( "galago.parse.LinkExtractor" ) );
            steps.add( createSort( "galago.types.ExtractedLink", "+destUrl" ) );
            steps.add( new OutputStep( "extractedLinks" ) );
            m.groups.add(steps);
            
            // return value
            ArrayList<Step> result = new ArrayList<Step>();
            result.add(m);
            return result;
        }

        public Job getInternalJob() {
            Stage s = new Stage( "linkCombine" );
            Parameters p;
            
            Parameters combineParameters = new Parameters();
            combineParameters.add( "extractedLinks", "extractedLinks" );
            combineParameters.add( "documentNames", "documentURLs" );
            
            s.add( new Step( "galago.parse.LinkCombiner", combineParameters ) );
            s.add( new Step( "galago.parse.AnchorTextDocumentCreator" ) );
            p = new Parameters();
            if( stopwordsFile != null ) {
                p.add( "filename", stopwordsFile );
            }
            s.add( createStep( "galago.parse.WordFilter", p ) );
            p = new Parameters();
            if( requiredWordsFile != null ) {
                p.add( "filter", requiredWordsFile );
            }
            s.add( new Step( "galago.parse.FieldPostingsCounter", p ) );
            s.add( createSort( "galago.types.DocumentLengthWordCount", "+word +document" ) );
            s.add( new OutputStep( "anchorPostings" ) );
            
            StageConnectionPoint output = new StageConnectionPoint( ConnectionPointType.Output,
                                                                    streamName,
                                                                    "galago.types.DocumentLengthWordCount",
                                                                    new String[] { "+word", "+document" },
                                                                    null );
            
            StageConnectionPoint documentURLs = new StageConnectionPoint( ConnectionPointType.Input,
                                                                    "documentURLs",
                                                                    "galago.types.DocumentURL",
                                                                    new String[] { "+url" },
                                                                    null );
            
            StageConnectionPoint extractedLinks = new StageConnectionPoint( ConnectionPointType.Input,
                                                                    "extractedLinks",
                                                                    "galago.types.ExtractedLink",
                                                                    new String[] { "+destUrl" },
                                                                    null );
                        
            s.add( output );
            s.add( documentURLs );
            s.add( extractedLinks );
            
            Job job = new Job();
            job.add(s);
            
            return job;
        }
        
        public String getName() {    
            return streamName;
        }
    }
    
    public static class CountFeatureWeighting {
        DocumentCountFeature feature;
        double mu;
        double weight;
        int length;
        
        CountFeatureWeighting( double weight, double mu, int length, DocumentCountFeature feature ) {
            this.weight = weight;
            this.mu = mu;
            this.feature = feature;
            this.length = length;
        }
        
        DocumentCountFeature getFeature() {
            return feature;
        }
        
        public double getWeight() {
            return weight;
        }
        
        public double getMu() {
            return mu;
        }
    }
  
    public static Step createStep( String className ) {
        return new Step( null, className, null );
    }
    
    public static Step createStep( String className, Parameters p ) {
        return new Step( null, className, p );
    }
    
    public static Step createStep( String className, String tag, List<String> items ) {
        Parameters p = new Parameters();
        for( String item : items ) {
            p.add( tag, item );
        }
        return new Step( null, className, p );
    }
    
    public static Step createStep( String className, String tag, String[] items ) {
        return createStep( className, tag, Arrays.asList(items));
    }
    
    public static Step createSort( String sortClass, String sortOrder, String reducer ) {
        Parameters p = new Parameters();
        p.add( "order", sortOrder );
        p.add( "class", sortClass );
        if( reducer != null )
            p.add( "reducer", reducer );
        
        Step s = new Step( null, "galago.tupleflow.Sorter", p );
        return s;
    }
    
    public static Step createSort( String sortClass, String sortOrder ) {
        return createSort( sortClass, sortOrder, null );
    }
    
    ArrayList<DocumentCountFeature> getExtractedFeatures() {
        return features;
    }
    
    ArrayList<CountFeatureWeighting> getWeightedFeatures() {
        return weightings;
    }
    
    ArrayList<WeightedPrior> getWeightedPriors() {
        return priors;
    }
    
    public Job createParsingJob() {
        Job job = new Job();
        Parameters p;
        Stage s = new Stage( "parsing" );
        
        s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                         "filenames",
                                         "galago.types.FileName",
                                         new String[] { "+filename" },
                                         null ) );
        s.add( new StageConnectionPoint( ConnectionPointType.Output,
                                         "counts",
                                         "galago.types.WordCount",
                                         new String[] { "+word" },
                                         null ) );
        s.add( new StageConnectionPoint( ConnectionPointType.Output,
                                         "documentNames",
                                         "galago.types.DocumentIdentifier",
                                         new String[] { "+identifier" },
                                         null ) );
        
        s.steps = new ArrayList<Step>();
        
        s.steps.add( new InputStep( "filenames" ) );
        s.steps.add( createStep( "galago.parse.UniversalParser" ) );
        s.steps.add( createStep( "galago.parse.TagTokenizer" ) );   

        if( stopwordsFile != null ) {
            p = new Parameters();
            p.add( "filename", stopwordsFile );
            s.steps.add( createStep( "galago.parse.WordFilter", p ) );
        }

        s.steps.add( createStep( "galago.parse.Porter2Stemmer" ) );

        if( requiredWordsFile != null ) {
            p = new Parameters();
            p.add( "keepListWords", "true" );
            p.add( "filename", requiredWordsFile );
            s.steps.add( createStep( "galago.parse.WordFilter", p ) );
        }
        
        // conflate fields together
        p = new Parameters();
        p.add( "field/source", "h1" );
        p.add( "field/source", "h2" );
        p.add( "field/source", "h3" );
        p.add( "field/source", "h4" );
        p.add( "field/destination", "heading" );
        Step conflater = new Step( null, "galago.parse.FieldConflater", p );
        s.steps.add(conflater);
        
        MultiStep m = new MultiStep();
        
        // word counts
        ArrayList<Step> steps = new ArrayList<Step>();
        int lmWidth = 1;
        for( DocumentCountFeature feature : features )
            lmWidth = Math.max( feature.getWidth(), lmWidth );
        
        p = new Parameters();
        p.add( "width", Integer.toString(lmWidth) );
        if( requiredWordsFile != null )
            p.add( "filter", requiredWordsFile );
        steps.add( createStep( "galago.parse.WordCounter", p ) );
        steps.add( createSort( "galago.types.WordCount", "+word", "galago.parse.WordCountReducer" ) );
        steps.add( new OutputStep( "counts" ) );
        m.groups.add(steps);
        
        // document identifiers
        steps = new ArrayList<Step>();
        steps.add( createStep( "galago.parse.DocumentIdentifierExtractor" ) );
        steps.add( createSort( "galago.types.DocumentIdentifier", "+identifier" ) );
        steps.add( new OutputStep( "documentNames" ) );
        m.groups.add(steps);
        
        for( DocumentCountFeature f : features ) {
            m.groups.add( f.getParsingSteps() );
            for( StageConnectionPoint cp : f.getParsingOutputs() )
                s.connections.put( cp.getExternalName(), cp );
        }
        s.steps.add( m );
        job.stages.put(s.name, s);
        
        for( DocumentCountFeature f : features ) {
            if( f.getInternalJob() != null ) {
                String jobName = f.getName() + "internal";
                job.add( jobName, f.getInternalJob() );
                job.connect( "parsing", jobName, ConnectionAssignmentType.Each );
            }
        }
        
        for( WeightedPrior prior : getWeightedPriors() ) {
            s = new Stage( "prior-" + prior.name );
            
            p = new Parameters();
            p.add( "filename", prior.filename );
            s.steps.add( createStep( "galago.parse.PriorParser", p ) );
            s.steps.add( createSort( "galago.types.DocumentProbability", "+document" ) );
            s.steps.add( new OutputStep( prior.name ) );
            
            s.add( new StageConnectionPoint( ConnectionPointType.Output,
                                             prior.name, 
                                             "galago.types.DocumentProbability", 
                                             new String[] { "+document" },
                                             null ) );
            
            job.add(s);
        }
        
        return job;
    }
   
    public Job createStatisticsJob() {
        Job statistics = new Job();
        Stage s;
        
        s = new Stage( "collectionLengthStage" );
        s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                         "documentNames",
                                         "galago.types.DocumentIdentifier",
                                         new String[] { "+identifier" },
                                         null ) );
        s.add( new StageConnectionPoint( ConnectionPointType.Output, 
                                         "collectionLength",
                                         "galago.types.XMLFragment",
                                         new String[] { "+nodePath" },
                                         null ) );
        
        s.add( new InputStep( "documentNames" ) );
        s.add( new Step( "galago.parse.CollectionLengthCounter" ) );
        s.add( createSort( "galago.types.XMLFragment", "+nodePath" ) );
        s.add( new OutputStep( "collectionLength" ) );
        statistics.add(s);
        
        s = new Stage( "documentNumbering" );
        s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                         "documentNames",
                                         "galago.types.DocumentIdentifier",
                                         new String[] { "+identifier" },
                                         null ) );
        s.add( new StageConnectionPoint( ConnectionPointType.Output,
                                         "documentNumbers",
                                         "galago.types.DocumentIdentifierNumber",
                                         new String[] { "+document" },
                                         null ) );
                
        s.add( new InputStep( "documentNames" ) );
        s.add( createSort( "galago.types.DocumentIdentifier", "+identifier" ) );
        s.add( new Step( "galago.parse.DocumentNameNumberer" ) );
        s.add( new OutputStep( "documentNumbers" ) );
        statistics.add(s);
        
        s = new Stage( "mergelm" );
        s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                         "counts",
                                         "galago.types.WordCount",
                                         new String[] { "+word" },
                                         null ) );
        s.add( new StageConnectionPoint( ConnectionPointType.Output,
                                         "lm",
                                         "galago.types.WordCount",
                                         new String[] { "+word" },
                                         null ) );
        s.add( new InputStep( "counts" ) );
        s.add( new Step( "galago.parse.WordCountReducer" ) );
        s.add( new OutputStep( "lm" ) );
        statistics.add(s);
        
        return statistics;
    }

    public Job createBinningJob( Parameters jobParameters ) {
        Job job = new Job();
        
        if( singles.size() > 0 ) {
            addThresholdsStage("thresholds-singles", job, singles);
            addBinningStage("binning-singles", job, jobParameters, singles);
        }
        
        if( phrases.size() > 0 ) {
            addThresholdsStage("thresholds-phrases", job, phrases);
            addBinningStage("binning-phrases", job, jobParameters, phrases);
        }
        
        addOffsetCombinationStage(job);
        
        if( phrases.size() > 0 ) {
            job.connect( new StagePoint( "thresholds-phrases", "minimumOffset" ),
                         new StagePoint( "offset-combination", "phrases" ),
                         ConnectionAssignmentType.Combined );
            job.connect( new StagePoint( "offset-combination", "minimumOffset" ),
                         new StagePoint( "binning-phrases", "minimumOffset" ),
                         ConnectionAssignmentType.Combined );
            job.connect( new StagePoint( "thresholds-phrases", "offsets" ),
                         new StagePoint( "binning-phrases", "offsets" ),
                         ConnectionAssignmentType.Combined );
        }
        
        if( singles.size() > 0 ) {
            job.connect( new StagePoint( "thresholds-singles", "minimumOffset" ),
                         new StagePoint( "offset-combination", "singles" ),
                         ConnectionAssignmentType.Combined );
            job.connect( new StagePoint( "offset-combination", "minimumOffset" ),
                         new StagePoint( "binning-singles", "minimumOffset" ),
                         ConnectionAssignmentType.Combined );
            job.connect( new StagePoint( "thresholds-singles", "offsets" ),
                         new StagePoint( "binning-singles", "offsets" ),
                         ConnectionAssignmentType.Combined );
        }
        
        return job;
    }
    
    public void addInputs( Stage s, String[] inputs, String className, String[] order ) {
        assert inputs.length > 0;
        
        if( inputs.length == 1 ) {
            s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                             inputs[0],
                                             className,
                                             order,
                                             null ) );
            s.add( new InputStep( inputs[0] ) );
        } else {
            // inputs.length > 1
            Parameters p = new Parameters();
            for( String input : inputs ) {
                s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                                 input,
                                                 className,
                                                 order,
                                                 null ) );
                p.add( "input", input );
            }
            
            String orderSpec = "";
            for( String o : order ) {
                orderSpec += o;
                orderSpec += " ";
            }
            orderSpec = orderSpec.trim();
            
            p.add( "class", className );   
            p.add( "order", orderSpec );
            s.add( new Step( "galago.tupleflow.StreamCombiner", p ) );
        }
    }
    
    public Job createBinOrderedIndexJob( String outputDirectory, String[] inputs ) {
        return createOrderedIndexJob( "galago.types.NumberWordBin",
                                      "galago.index.BinOrderedBinnedListWriter",
                                      new String[] { "+word", "-bin", "+document" },
                                      inputs,
                                      outputDirectory );
    }
    
    public Job createOrderedIndexJob( String typeName, String writerName, String[] order, String[] inputs, String outputDirectory ) {
        Job job = new Job();
        assert inputs.length >= 1;
        
        if( inputs.length == 1 ) {
            Stage s = new Stage( "mergeIndex" );
            addInputs( s, inputs, typeName, order );
            Parameters p = new Parameters();
            p.add( "index", outputDirectory + File.separator + "default" );
            s.add( new Step( writerName, p ) );
            job.add(s);
        } else {
            Stage s = new Stage( "mergeIndex" );
            s.add( new StageConnectionPoint( ConnectionPointType.Output, "mergedPostings", typeName, order, null ) );
            addInputs( s, inputs, typeName, order );
            s.add( new OutputStep( "mergedPostings" ) );
            job.add(s);
            
            s = new Stage( "finalWrite" );
            s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                             "mergedPostings",
                                             typeName,
                                             order,
                                             null ) );
            Parameters p = new Parameters();
            s.add( new InputStep( "mergedPostings" ) );
            p.add( "index", outputDirectory + File.separator + "default" );
            s.add( new Step( writerName, p ) );
            job.add(s);
            
            job.connect( new StagePoint( "mergeIndex", "mergedPostings" ),
                         new StagePoint( "finalWrite", "mergedPostings" ),
                         ConnectionAssignmentType.Combined );
        }
        
        return job;
    }
    
    public Job createDocumentOrderedIndexJob( String outputDirectory, String[] inputs ) {
        return createOrderedIndexJob( "galago.types.NumberWordBin",
                                      "galago.index.DocumentOrderedBinnedListWriter",
                                      new String[] { "+word", "+document" },
                                      inputs,
                                      outputDirectory );
    }
    
    public Job createIndexJob( String outputDirectory, String indexType, String[] inputs ) {
        Job job = new Job();
        Parameters p;
        Stage s;
        
        // main manifest
        s = new Stage( "mainManifest" );
        p = new Parameters();
        p.add( "class", "galago.types.XMLFragment" );
        s.add( new Step( "galago.tupleflow.NullSource", p ) );
        p = new Parameters();
        p.add( "manifest", outputDirectory + File.separator + "manifest" );
        p.add( "xml/indexType", indexType );
        s.add( new Step( "galago.index.ManifestWriter", p ) );
        job.add(s);
        
        // feature manifest
        s = new Stage( "featureManifest" );
        p = new Parameters();
        p.add( "class", "galago.types.XMLFragment" );
        s.add( new Step( "galago.tupleflow.NullSource", p ) );
        p = new Parameters();
        p.add( "manifest", outputDirectory + File.separator +
                           "default" + File.separator +
                           "manifest" );
        s.add( new Step( "galago.index.ManifestWriter", p ) );
        job.add(s);
        
        // merge document lists
        s = new Stage( "mergeDocumentLists" );
        s.add( new StageConnectionPoint( ConnectionPointType.Input,
                                         "documentNumbers",
                                         "galago.types.DocumentIdentifierNumber",
                                         new String[] { "+document" },
                                         null ) );

        s.add( new InputStep( "documentNumbers" ) );
        p = new Parameters();
        p.add( "filename", outputDirectory + File.separator + "documentNames" );
        s.add( new Step( "galago.index.DocumentNameWriter", p ) );
        job.add(s);
        
        // merge indexes
        if( indexType.equals( "documentOrdered" ) ) {
            Job docOrdered = createDocumentOrderedIndexJob( outputDirectory, inputs );
            job.add( "docOrdered", docOrdered ); 
        } else {
            Job binOrdered = createBinOrderedIndexJob( outputDirectory, inputs );
            job.add( "binOrdered", binOrdered );
        }
        
        return job;
    }
    
    public Job createFilenamesJob( String inputDirectory ) {
        Job job = new Job();
        Stage s = new Stage( "filenamesStage" );
        
        Parameters p = new Parameters();
        File f = new File(inputDirectory);
        
        if( f.exists() && f.isFile() ) {
            p.add( "file", inputDirectory );
        } else {
            p.add( "directory", inputDirectory );
        }

        s.add( new Step( "galago.tupleflow.FileSource", p ) );
        s.add( new OutputStep( "filenames" ) );
        
        s.add( new StageConnectionPoint( ConnectionPointType.Output,
                                         "filenames",
                                         "galago.types.FileName",
                                         new String[] { "+filename" }, 
                                         null ) );
        job.add(s);
        
        return job;
    }
    
    public Job createJob( String inputDirectory, String outputDirectory, int hashCount, Parameters jobParameters ) {
        determinePhrasesAndSingles();
        
        // somewhere in here I also need to add all of the connections for the features, etc.
        Job job = new Job();
        job.properties.put( "hashCount", Integer.toString(hashCount) );
        
        // get some inputs
        Job filenames = createFilenamesJob( inputDirectory );
        job.add( "filenames", filenames );
        
        // have to parse stuff
        Job parsing = createParsingJob();
        job.add( "parsing", parsing );
        job.connect( new Job.StagePoint( "filenames.filenamesStage", "filenames" ),
                     new Job.StagePoint( "parsing.parsing", "filenames" ),
                     ConnectionAssignmentType.Each,
                     new String[] { "+filename" }, -1 );
        
        // collection statistics
        Job statistics = createStatisticsJob();
        job.add( "statistics", statistics );
        
        job.connect( "parsing", "statistics", ConnectionAssignmentType.Combined );
        
        // binning
        Job binning = createBinningJob( jobParameters );
        job.add( "binning", binning );
        for( WeightedPrior prior : getWeightedPriors() ) {
            job.connect( "parsing.prior-" + prior.name, "binning", ConnectionAssignmentType.Combined );
        }

        if( singles.size() > 0 ) {
            job.connect( new Job.StagePoint( "statistics.collectionLengthStage", "collectionLength" ),
                         new Job.StagePoint( "binning.thresholds-singles", "collectionLength" ),
                         ConnectionAssignmentType.Combined );
        }
        
        if( phrases.size() > 0 ) {
            job.connect( new Job.StagePoint( "statistics.collectionLengthStage", "collectionLength" ),
                         new Job.StagePoint( "binning.thresholds-phrases", "collectionLength" ),
                         ConnectionAssignmentType.Combined );
        }
        
        job.connect( "parsing", "binning", ConnectionAssignmentType.Each );
        job.connect( "statistics", "binning", ConnectionAssignmentType.Combined );
        
        // indexes
        Job index;
        
        if( jobParameters.get( "documentOrdered", true ) ) {
            String[] inputs = generateInputNames( "doc" );
            
            index = createIndexJob( outputDirectory + "-doc", "documentOrdered", inputs );
            job.add( "docIndex", index );
            job.connect( "statistics", "docIndex", ConnectionAssignmentType.Combined );
            job.connect( "binning", "docIndex", ConnectionAssignmentType.Combined );

            if( singles.size() > 0 )
                job.connect( new StagePoint( "binning.binning-singles",
                                             "docSortedBinned" ),
                             new StagePoint( "docIndex.docOrdered.mergeIndex",
                                             "doc-singles" ),
                             ConnectionAssignmentType.Combined );

            if( phrases.size() > 0 )
                job.connect( new StagePoint( "binning.binning-phrases",
                                             "docSortedBinned" ),
                             new StagePoint( "docIndex.docOrdered.mergeIndex",
                                             "doc-phrases" ),
                             ConnectionAssignmentType.Combined );
        }
        
        if( jobParameters.get( "binOrdered", true ) ) {
            String[] inputs = generateInputNames( "bin" );
            
            index = createIndexJob( outputDirectory + "-bin", "binOrdered", inputs );
            job.add( "binIndex", index );
            job.connect( "statistics", "binIndex", ConnectionAssignmentType.Combined );
            job.connect( "binning", "binIndex", ConnectionAssignmentType.Combined );
        
            if( singles.size() > 0 )
                job.connect( new StagePoint( "binning.binning-singles",
                                             "binSortedBinned" ),
                             new StagePoint( "binIndex.binOrdered.mergeIndex",
                                             "bin-singles" ),
                             ConnectionAssignmentType.Combined );

            if( phrases.size() > 0 )
                job.connect( new StagePoint( "binning.binning-phrases",
                                             "binSortedBinned" ),
                             new StagePoint( "binIndex.binOrdered.mergeIndex",
                                             "bin-phrases" ),
                             ConnectionAssignmentType.Combined );
        }
        
        return job;
    }
    
    public WeightedPrior addWeightedPrior( String name, String filename, String weight ) {
        return new WeightedPrior( name, filename, Double.parseDouble(weight) );
    }
    
    public PostingFeature addPostingFeature( int width, int maxTokenLength, String tagName, String name ) {
        return new PostingFeature( width, maxTokenLength, tagName, name );
    }
    
    public AnchorTextFeature addAnchorTextFeature( String name ) {
        return new AnchorTextFeature( name );
    }

    public CountFeatureWeighting addWeighting( double weight, double mu, int length, DocumentCountFeature feature ) {
        return new CountFeatureWeighting( weight, mu, length, feature );
    }
    
    public static void main( String[] args ) throws InterruptedException,
                                                    ExecutionException,
                                                    SAXException,
                                                    IOException,
                                                    ParserConfigurationException
    {
        Parameters parameters = new Parameters(args);
        ErrorStore store;
        
        String input = parameters.get("input");
        String output = parameters.get("output");
        
        ArrayList<CountFeatureWeighting> weightings = new ArrayList<CountFeatureWeighting>();
        ArrayList<DocumentCountFeature> features = new ArrayList<DocumentCountFeature>();
        ArrayList<WeightedPrior> priors = new ArrayList<WeightedPrior>();
        String requiredWordsFile = parameters.get( "requiredWords", (String)null );
        String stopwordsFile = parameters.get( "stopwords", (String)null );

        FeatureIndexBuilder builder = new FeatureIndexBuilder( features, weightings, priors, stopwordsFile, requiredWordsFile );
        
        for( Value value : parameters.list( "feature" ) ) {
            String name = value.get( "name" );
            String tagName = value.get( "tagName", null );
            int width = 1;
            int maxTokenLength = 128;
            
            if( value.containsKey("width") )
                width = Integer.parseInt( value.get("width") );
            
            if( value.containsKey("maxTokenLength" ) )
                maxTokenLength = Integer.parseInt( value.get("maxTokenLength" ) );
            
            double weight = Double.parseDouble(value.get("weight"));
            double mu = Double.parseDouble(value.get("mu"));
            int length = 1500;
            
            if( value.containsKey("length") )
                length = Integer.parseInt(value.get("length"));
            
            String featureType = value.get( "type" );
            DocumentCountFeature feature = null;
            CountFeatureWeighting weighting = null;
            
            if( featureType.equals( "postings" ) ) {
                feature = builder.addPostingFeature( width, maxTokenLength, tagName, name );
            } else if( featureType.equals( "anchor" ) ) {
                feature = builder.addAnchorTextFeature( name );
            }

            weighting = builder.addWeighting( weight, mu, length, feature );
            
            weightings.add( weighting );
            features.add( feature );
        }
        
        if( parameters.containsKey( "prior" ) ) {
            for( Value value : parameters.list( "prior" ) ) {
                String name = value.get( "name" );
                String filename = value.get( "filename" );
                String weight = value.get( "weight" );

                priors.add( builder.addWeightedPrior( name, filename, weight ) );
            }
        }
        
        int hashCount = (int) parameters.get( "hashCount", 100 );
        Job job = builder.createJob( input, output, hashCount, parameters );
        
        if( parameters.containsKey("writeFile") ) {
            BufferedWriter writer = new BufferedWriter( new FileWriter( parameters.get("writeFile") ) );
            writer.write(job.toString());
            writer.close();
        }
        
        // BUGBUG: also need to configure indexes here.  can you turn off
        // the creation of a particular kind of index, or does it
        // always create two? can you change stopping, stemming, etc.?  

        // parse the job to test for errors
        if( parameters.get("reparse", true) ) {
            store = new ErrorStore();
            job = JobExecutor.parseText( job.toString(), store );

            if( store.errors.size() > 0 || store.warnings.size() > 0 ) {
                System.out.println(store.toString());
                return;
            }
        }
        
        // optionally run the job directly
        if( parameters.get("execute", false) ) {
            store = new ErrorStore();
            JobExecutor e = new JobExecutor( job, "/tmp/indexjob", store );
            e.prepare();
            
            if( store.errors.size() > 0 || store.warnings.size() > 0 ) {
                System.out.println(store.toString());
                return;
            }

            e.run( new LocalStageExecutor() );
        }
    }
}
