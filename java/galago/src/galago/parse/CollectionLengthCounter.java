/*
 * CollectionLengthCounter
 *
 * May 2, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentIdentifier;
import galago.types.XMLFragment;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.types.DocumentIdentifier")
@OutputClass(className="galago.types.XMLFragment")
public class CollectionLengthCounter extends StandardStep<DocumentIdentifier, XMLFragment> {
    long collectionLength = 0;

    public void process( DocumentIdentifier identifier ) {
        collectionLength += identifier.length;
    }
    
    @Override
    public void close() throws IOException {
        processor.process( new XMLFragment( "collectionLength", Long.toString(collectionLength) ) );
        super.close();
    }
}
