/*
 * PigServerTest.java
 * JUnit based test
 *
 * Created on September 3, 2007, 11:47 AM
 */

package com.yahoo.pig;

import com.yahoo.pig.PigServer.ExecType;
import java.io.BufferedReader;
import java.io.FileInputStream;
import junit.framework.*;

/**
 *
 * @author trevor
 */
public class PigServerTest extends TestCase {
    
    public PigServerTest(String testName) {
        super(testName);
    }

    protected void setUp() throws Exception {
    }

    protected void tearDown() throws Exception {
    }

    public void testRegisterQuery() throws Exception {
        System.out.println("registerQuery");
        
        String lineOne = "a = load '/Users/trevor/Desktop/data.txt' as (f1, f2, f3);";
        String lineTwo = "b = FOREACH a GENERATE f1, f2;";
        String lineThree = "c = COGROUP b BY f1, b BY f2;";
        PigServer instance = null;
        
        instance = new PigServer( ExecType.GALAGO );
        instance.registerQuery( lineOne );
        instance.registerQuery( lineTwo );
        instance.registerQuery( lineThree );
        instance.store( "b", "/tmp/outdb" );
    }
}
