
//
// Retrieval 
//
// 16 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_RETRIEVAL_HPP
#define GALAGO_RETRIEVAL_HPP

#include "indri/ScoredExtentResult.hpp"
#include "Query.hpp"
#include <vector>

class Retrieval {
public:
    virtual ~Retrieval() {}
    virtual std::string getDocument( int document ) = 0;
    virtual std::vector<indri::api::ScoredExtentResult> runQuery( const std::vector<QueryTerm>& terms, int requested, int threshold ) = 0;
    virtual void openRead( const std::string& path ) = 0;
    virtual void close() = 0;
    
    static Retrieval* open( const std::string& path );
};

#endif // GALAGO_RETRIEVAL_HPP
