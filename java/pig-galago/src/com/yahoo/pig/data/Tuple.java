/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.io.WritableComparable;

/**
 * an ordered list of Datums
 */
public class Tuple implements Datum,WritableComparable {
    protected ArrayList<Datum> fields;
    static String              defaultDelimiter = "[,\t]";

    public Tuple() {
        this(0);
    }

    public Tuple(int numFields) {
        fields = new ArrayList<Datum>(numFields);
        for (int i = 0; i < numFields; i++) {
            fields.add(null);
        }
    }

    public Tuple(List<Datum> fieldsIn) {
        fields = new ArrayList<Datum>(fieldsIn.size());
        fields.addAll(fieldsIn);
    }

    /**
     * shortcut, if tuple only has one field
     */
    public Tuple(Datum fieldIn) {
        fields = new ArrayList<Datum>(1);
        fields.add(fieldIn);
    }

    /**
     * Creates a tuple from a delimited line of text
     * 
     * @param textLine
     *            the line containing fields of data
     * @param delimiter
     *            a regular expression of the form specified by String.split(). If null, the default
     *            delimiter "[,\t]" will be used.
     */
    public Tuple(String textLine, String delimiter) {
        if (delimiter == null) {
            delimiter = defaultDelimiter;
        }
        String[] splitString = textLine.split(delimiter, -1);
        fields = new ArrayList<Datum>(splitString.length);
        for (int i = 0; i < splitString.length; i++) {
            fields.add(new DataAtom(splitString[i]));
        }
    }

    /**
     * Creates a tuple from a delimited line of text. This will invoke Tuple(textLine, null)
     * 
     * @param textLine
     *            the line containing fields of data
     */
    public Tuple(String textLine) {
        this(textLine, defaultDelimiter);
    }

    public Tuple(Tuple[] otherTs) {
        fields = new ArrayList<Datum>(otherTs.length);
        for (int i = 0; i < otherTs.length; i++) {
            try {
                appendTuple(otherTs[i]);
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    public void copyFrom(Tuple otherT) {
        this.fields = otherT.fields;
    }

    public int arity() {
        return fields.size();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append('(');
        for (Iterator<Datum> it = fields.iterator(); it.hasNext();) {
            Datum d = it.next();
            if(d != null) {
                sb.append(d.toString());
            } else {
                sb.append("__PIG_NULL__");
            }
            if (it.hasNext())
                sb.append(", ");
        }
        sb.append(')');
        return sb.toString();
    }

    public void setField(int i, Datum val) throws IOException {
        getField(i); // throws exception if field doesn't exist

        fields.set(i, val);
    }

    public void setField(int i, int val) throws IOException {
        setField(i, new DataAtom(val));
    }

    public void setField(int i, double val) throws IOException {
        setField(i, new DataAtom(val));
    }

    public void setField(int i, String val) throws IOException {
        setField(i, new DataAtom(val));
    }

    public Datum getField(int i) throws IOException {
        if (fields.size() >= i + 1)
            return fields.get(i);
        else
            throw new IOException("Column number out of range: " + i + " -- " + toString());
    }

    // Get field i, if it is an Atom or can be coerced into an Atom
    public DataAtom getAtomField(int i) throws IOException {
        Datum field = getField(i); // throws exception if field doesn't exist

        if (field instanceof DataAtom) {
            return (DataAtom) field;
        } else if (field instanceof Tuple) {
            Tuple t = (Tuple) field;
            if (t.arity() == 1) {
                return t.getAtomField(0);
            }
        } else if (field instanceof DataBag) {
            DataBag b = (DataBag) field;
            if (b.cardinality() == 1) {
                Tuple t = b.content().next();
                if (t.arity() == 1) {
                    return t.getAtomField(0);
                }
            }
        }

        throw new IOException("Incompatible type for request getAtomField().");
    }

    // Get field i, if it is a Tuple or can be coerced into a Tuple
    public Tuple getTupleField(int i) throws IOException {
        Datum field = getField(i); // throws exception if field doesn't exist

        if (field instanceof Tuple) {
            return (Tuple) field;
        } else if (field instanceof DataBag) {
            DataBag b = (DataBag) field;
            if (b.cardinality() == 1) {
                return b.content().next();
            }
        }

        throw new IOException("Incompatible type for request getTupleField().");
    }

    // Get field i, if it is a Bag or can be coerced into a Bag
    public DataBag getBagField(int i) throws IOException {
        Datum field = getField(i); // throws exception if field doesn't exist

        if (field instanceof DataBag) {
            return (DataBag) field;
        }

        throw new IOException("Incompatible type for request getBagField().");
    }

    public void appendTuple(Tuple other) throws IOException {
        for (Iterator<Datum> it = other.fields.iterator(); it.hasNext();) {
            this.fields.add(it.next());
        }
    }

    public void appendField(Datum newField) throws IOException {
        this.fields.add(newField);
    }

    public String toDelimitedString(String delim) throws IOException {
        StringBuffer buf = new StringBuffer();
        for (Iterator<Datum> it = fields.iterator(); it.hasNext();) {
            Datum field = it.next();
            if (!(field instanceof DataAtom)) {
                throw new IOException("Unable to convert non-flat tuple to string.");
            }

            buf.append((DataAtom) field);
            if (it.hasNext())
                buf.append(delim);
        }
        return buf.toString();
    }

    public boolean lessThan(Tuple other) {
        return (this.compareTo(other) < 0);
    }

    public boolean greaterThan(Tuple other) {
        return (this.compareTo(other) > 0);
    }
    
    public boolean equals(Datum other){
    	if (other instanceof Tuple){
    		Tuple tOther = (Tuple)other;
    		if (this.arity() != tOther.arity())
                return false;
            for (int i = 0; i < this.fields.size(); i++) {
                if (!this.fields.get(i).equals(tOther.fields.get(i)))
                    return false;
            }
            return true;
    	}
    	else
    		return false;
    }	
    
    public int compareTo(Tuple other) {
        if (other.fields.size() != this.fields.size())
            return other.fields.size() < this.fields.size() ? 1 : -1;

        for (int i = 0; i < this.fields.size(); i++) {
            int c = this.fields.get(i).compareTo(other.fields.get(i));
            if (c != 0)
                return c;
        }
        return 0;
    }

    public int compareTo(Object other) {
        if (other instanceof DataAtom)
            return +1;
        else if (other instanceof DataBag)
            return +1;
        else if (other instanceof Tuple)
            return compareTo((Tuple) other);
        else
        	return -1;
    }

    public int hashCode() {
        int hash = 1;
        for (Iterator<Datum> it = fields.iterator(); it.hasNext();) {
            hash = 31 * hash + it.next().hashCode();
        }
        return hash;
    }

    // WritableComparable methods:
    public static final byte ATOM   = 0x50;
    public static final byte BAG    = 0x51;
    public static final byte TUPLE  = 0x60;
    public static final byte RECORD_1 = 0x21;
    public static final byte RECORD_2 = 0x31;
    public static final byte RECORD_3 = 0x41;

    public void write(DataOutput out) throws IOException {
        write(this, out);
    }

    private static void write(Tuple t, DataOutput out) throws IOException {
        out.write(Tuple.TUPLE);
        encodeInt(out, t.arity());
        for (int i = 0, n = t.arity(); i < n; i++) {
            Datum d = t.getField(i);
            if (d instanceof Tuple) {
                write((Tuple) d, out);
            } else if (d instanceof DataBag) {
                out.write(Tuple.BAG);
                encodeInt(out, ((DataBag) d).cardinality());
                Iterator<Tuple> it = ((DataBag) d).content();
                while (it.hasNext()) {
                    Tuple item = it.next();
                    write(item, out);
                }
            } else if (d instanceof DataAtom) {
                out.write(Tuple.ATOM);
                byte[] data;
                try {
                    data = ((DataAtom) d).strval().getBytes("UTF-8");
                } catch (Exception e) {
                    long size = ((DataAtom) d).strval().length();
                    throw new RuntimeException("Error dealing with DataAtom of size " + size);
                }
                encodeInt(out, data.length);
                out.write(data);
            } else if (d == null) {
                throw new IOException("null field in tuple: " + i + ": " + t.toString());
                // out.write(LFBin.ATOM);
                // out.write(0);
            }
        }
    }

    public void readFields(DataInput in) throws IOException {
        // nuke the old contents of the tuple
        fields = new ArrayList<Datum>();
        in.readByte(); // Skip the 'TUPLE'
        readTuple(this, in);
    }

    private static Tuple readTuple(DataInput in) throws IOException {
        Tuple ret = new Tuple();
        readTuple(ret, in);
        return ret;
    }

    private static void readTuple(Tuple ret, DataInput in) throws IOException {
        int size = decodeInt(in);
        for (int i = 0, n = size; i < n; i++) {
            byte b = in.readByte();
            switch (b) {
                case Tuple.TUPLE:
                    ret.fields.add(readTuple(in));
                    break;
                case Tuple.BAG:
                    ret.fields.add(readBag(in));
                    break;
                case Tuple.ATOM:
                    ret.fields.add(readAtom(in));
                    break;
            }
        }

    }

    private static DataBag readBag(DataInput in) throws IOException {
        int size = decodeInt(in);
        DataBag ret = new DataBag(); // should be this, but doesn't have temp dir: BagFactory.getInstance().getNewBigBag();
        byte[] b = new byte[1];
        for (int i = 0; i < size; i++) {
            in.readFully(b); // skip the 'TUPLE'
            ret.add(readTuple(in));
        }
        return ret;
    }

    private static DataAtom readAtom(DataInput in) throws IOException {
        int len = decodeInt(in);
        DataAtom ret = new DataAtom();
        byte[] data = new byte[len];
        in.readFully(data);
        ret.setValue(new String(data, "UTF-8"));
        return ret;
    }

    // Encode the integer so that the high bit is set on the last
    // byte
    static void encodeInt(DataOutput os, int i) throws IOException {
        if (i >> 28 != 0)
            os.write((i >> 28) & 0x7f);
        if (i >> 21 != 0)
            os.write((i >> 21) & 0x7f);
        if (i >> 14 != 0)
            os.write((i >> 14) & 0x7f);
        if (i >> 7 != 0)
            os.write((i >> 7) & 0x7f);
        os.write((i & 0x7f) | (1 << 7));
    }

    static int decodeInt(DataInput is) throws IOException {
        int i = 0;
        int c;
        while (true) {
            c = is.readUnsignedByte();
            if (c == -1)
                break;
            i <<= 7;
            i += c & 0x7f;
            if ((c & 0x80) != 0)
                break;
        }
        return i;
    }
}
