/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer.schema;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A SchemaItemList encapuslates a group of schema items which may be SchemaFields
 * (data atoms) or complex items, such as bags or tuples. A SchemaItemList will 
 * recursively search itself for the proper column number for a requested alias, matching
 * against its own alias with priority.
 * @author dnm
 *
 */
public class SchemaItemList extends SchemaItem {
    public List<SchemaItem>        fields  = new ArrayList<SchemaItem>();
    public Map<String, SchemaItem> mapping = new HashMap<String, SchemaItem>();
    private boolean                isBag   = false;
    
    public SchemaItemList() {
    }
    
    public Type getType() {
        return Type.complex;
    }

    public int colFor(String alias) {        
        if (mapping.containsKey(alias)) {
            return mapping.get(alias).col;
        }
        return -1;
    }

    public SchemaItem schemaFor(int col) {
        if (col < fields.size()) {
            return fields.get(col);
        }
        return null;
    }

    public void add(SchemaItem sc) {
        
        sc.col = fields.size();
        fields.add(sc);
     
        if (sc.alias == null) return;
        
        if (mapping.containsKey(sc.alias))
        	throw new RuntimeException("Duplicate alias: " + sc.alias);
        
        mapping.put(sc.alias, sc);
    }

    public int numFields() {
        return fields.size();
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (alias!=null){
        	buf.append(alias);
        	buf.append(": ");
        }
        
        buf.append( "(" );
        boolean first = true;
        for (SchemaItem item : fields) {
            if (!first) {
                buf.append(", ");
            }
            first = false;
            if (item instanceof SchemaField) {
                buf.append(item.alias);
            } else if (item instanceof SchemaItemList) {
                SchemaItemList list = (SchemaItemList) item;
                buf.append(list.toString());
            }
        }
        buf.append(" )");
        return buf.toString();
    }

    public SchemaItemList copy(){
        SchemaItemList copy = new SchemaItemList();
        copy.col = col;
        copy.alias = alias;
        copy.isBag = isBag;
        
        for (SchemaItem item: fields){
        	copy.add(item);
        }
        
        return copy;
    }
    
    public List<SchemaItem> flatten(){
    	/*
    	if (alias!=null){
	    	for (SchemaItem item: fields){	
	    		item.alias = alias + "." + item.alias;
	    	}
    	}
    	*/
    	return fields;
    }
    
    public boolean isBag() {
        return isBag;
    }

    public boolean isTuple() {
        return !isBag();
    }

    public void setIsBag(Boolean bag) {
        this.isBag = bag;
    }

}
