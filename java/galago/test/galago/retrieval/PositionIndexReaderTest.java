/*
 * PositionIndexReaderTest.java
 * JUnit based test
 *
 * Created on October 5, 2007, 4:38 PM
 */

package galago.retrieval;

import galago.Utility;
import galago.index.PositionListWriter;
import galago.retrieval.extents.ExtentArrayIterator;
import galago.tupleflow.Parameters;
import galago.util.ExtentArray;
import java.io.File;
import java.io.IOException;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class PositionIndexReaderTest extends TestCase {
    File tempPath;
    static int[][] dataA = {
        { 5, 7, 9 },
        { 19, 27, 300 }
    };
    
    static int[][] dataB = {
        { 149, 15500, 30319 },
        { 555555, 2 }
    };
    
    public PositionIndexReaderTest(String testName) {
        super(testName);
    }

    @Override
    public void setUp() throws Exception {
        // make a spot for the index
        tempPath = File.createTempFile( "galago-test-index", null );
        tempPath.delete();
        tempPath.mkdir();

        new Parameters().write( tempPath.toString() + File.separator + "manifest" );
        
        Parameters p = new Parameters();
        p.add( "index", tempPath.toString() );
        
        PositionListWriter writer = new PositionListWriter( new galago.tupleflow.FakeParameters(p) );
        
        writer.processWord( Utility.makeBytes("a") );
        
        for( int[] doc : dataA ) {
            writer.processDocument( doc[0] );
            
            for( int i=1; i<doc.length; i++ )
                writer.processPosition( doc[i] );
        }
        
        writer.processWord( Utility.makeBytes("b") );
        
        for( int[] doc : dataB ) {
            writer.processDocument( doc[0] );
            
            for( int i=1; i<doc.length; i++ )
                writer.processPosition( doc[i] );
        }
        
        writer.close();
    }
    
    @Override
    public void tearDown() throws Exception {
        Utility.deleteDirectory( tempPath );
    }
    
    public void internalTestIterator( PositionIndexReader.ExtentListIterator termExtents, int[][] data ) throws IOException {
        assertNotNull( termExtents );
        assertFalse( termExtents.isDone() );
        
        for( int[] doc : data ) {
            assertFalse( termExtents.isDone() );
            ExtentArray e = termExtents.extents();
            ExtentArrayIterator iter = new ExtentArrayIterator(e);
            
            for( int i=1; i<doc.length; i++ ) {
                assertFalse( iter.isDone() );
                assertEquals( doc[i], iter.current().begin );
                assertEquals( doc[i]+1, iter.current().end );
                iter.next();
            }
            
            assertTrue( iter.isDone() );
            termExtents.nextDocument();
        }
        
        assertTrue( termExtents.isDone() );
    }
    
    public void testA() throws Exception {
        PositionIndexReader reader = new PositionIndexReader( tempPath.toString() );
        PositionIndexReader.ExtentListIterator termExtents = reader.getTermExtents("a");
        
        internalTestIterator( termExtents, dataA );
        reader.close();
    }
    
    public void testB() throws Exception {
        PositionIndexReader reader = new PositionIndexReader( tempPath.toString() );
        PositionIndexReader.ExtentListIterator termExtents = reader.getTermExtents("b");
        
        internalTestIterator( termExtents, dataB );
        reader.close();
    }
}
