
/*==========================================================================
 * Copyright (c) 2004 University of Massachusetts.  All Rights Reserved.
 *
 * Use of the Lemur Toolkit for Language Modeling and Information Retrieval
 * is subject to the terms of the software license set forth in the LICENSE
 * file included with this software, and also available at
 * http://www.lemurproject.org/license.html
 *
 *==========================================================================
*/

//
// FileMapping
//
// 5 January 2007 -- tds
//

#ifndef INDRI_FILEMAPPING_HPP
#define INDRI_FILEMAPPING_HPP

#include "indri/indri-platform.h"
#include "indri/Mutex.hpp"
#include <string> 

namespace indri
{
  namespace file
  {
    class FileMapping {
    private:
#ifdef WIN32
     HANDLE _handle;    
#endif           

    public:
#ifdef WIN32
      FileMapping( HANDLE handle, const void* region, const UINT64 length );
#else
      FileMapping( const void* region, const UINT64 length );
#endif
      ~FileMapping(); 
      const void* region;
      const UINT64 length;
    };
  }
}

#endif // INDRI_FILEMAPPING_HPP

