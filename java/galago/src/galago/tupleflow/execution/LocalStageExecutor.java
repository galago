/*
 * LocalStageExecutor
 *
 * March 30, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow.execution;

import galago.tupleflow.ExNihiloSource;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

/**
 *
 * @author trevor
 */
public class LocalStageExecutor implements StageExecutor {
    public class ImmediateFuture<T> implements Future<Object> {
        T object;
        Throwable exception;
        
        public ImmediateFuture( T object, Throwable exception ) {
            this.object = object;
            this.exception = exception;
        }
        
        public ImmediateFuture( T object ) {
            this(object, null);
        }
        
        public T get() throws InterruptedException, ExecutionException {
            if( exception != null )
                throw new ExecutionException( "Caught exception while launching jobs: ", exception );

            return object;
        }
        
        public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
            return get();
        }

        public boolean cancel(boolean mayInterruptIfRunning) {
            return false;
        }

        public boolean isDone() {
            return true;
        }

        public boolean isCancelled() {
            return false;
        }
        
        public Throwable getException() {
            return exception;
        }
    }
    
    public ImmediateFuture<Object> execute( StageGroupDescription stage, String temporary ) {
        try {
            List<StageInstanceDescription> instances = stage.getInstances();
            
            for( StageInstanceDescription instance : instances ) {
                ExNihiloSource source = instance.instantiate();
                source.run();
            }
        } catch( Throwable e ) {
            return new ImmediateFuture<Object>( null, e );
        }
        
        return new ImmediateFuture<Object>( null );
    }
    
    public ImmediateFuture<Object> execute( StageInstanceDescription stage ) {
        try {
            ExNihiloSource source = stage.instantiate();
            source.run();
        } catch( Throwable e ) {
            return new ImmediateFuture<Object>( null, e );
        }
        
        return new ImmediateFuture<Object>( null );
    }
    
    public ImmediateFuture execute( String descriptionFile ) {
        File errorFile = new File(descriptionFile + ".error");
        File completeFile = new File(descriptionFile + ".complete" );
        ImmediateFuture result = null;
        
        try {
            Logger logger = Logger.getLogger(JobExecutor.class);
    
            // check for a checkpoint file.  If one exists, we quit.
            if( completeFile.exists()) {
                logger.info( "Exiting early because a complete checkpoint was found." );                
                return new ImmediateFuture<Object>( null );
            }
            
            // get rid of any error checkpoints
            if( errorFile.exists() ) {
                errorFile.delete();
            }
            
            ObjectInputStream stream = new ObjectInputStream( new FileInputStream( new File(descriptionFile) ) );
            StageInstanceDescription stage = (StageInstanceDescription) stream.readObject();
            result = execute( stage );
        } catch( Throwable e ) {
            result = new ImmediateFuture<Object>( null, e );
        }
        
        try {
            if( result.getException() != null ) {
                Throwable e = result.getException();
                BufferedWriter writer = new BufferedWriter( new FileWriter(errorFile) );
                writer.write(e.toString());
                writer.close();
            } else {
                BufferedWriter writer = new BufferedWriter( new FileWriter(completeFile) );
                writer.close();
            }
        } catch( IOException e ) {
            if( result.getException() == null )
                result = new ImmediateFuture<Object>( null, e );
        }
        
        return result;
    }
    
    public void shutdown() {
    }
    
    public static void main( String[] args ) throws UnsupportedEncodingException, ParserConfigurationException, SAXException, IOException, ClassNotFoundException {
        Logger logger = Logger.getLogger(JobExecutor.class);

        String stageDescriptionFile = args[0];
        logger.info( "Initializing: " + stageDescriptionFile );
        ImmediateFuture future = new LocalStageExecutor().execute( stageDescriptionFile );
        
        if( future.getException() != null ) {
            logger.error( "Exception thrown", future.exception );
        } else {
            logger.info( "Job complete" );
        }
    }
}
