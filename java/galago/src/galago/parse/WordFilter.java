/*
 * WordFilter
 *
 * December 5, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Utility;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.InputClass;
import galago.tupleflow.Linkage;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.NullProcessor;
import galago.tupleflow.OutputClass;
import galago.tupleflow.Processor;
import galago.tupleflow.Source;
import galago.tupleflow.Step;
import galago.tupleflow.execution.ErrorHandler;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * WordFilter filters out unnecessary words from documents.  Typically this object
 * takes a stopword list as parameters and removes all the listed words.  However, 
 * this can also be used to keep only the specified list of words in the index, which
 * can be used to create an index that is tailored for only a small set
 * of experimental queries.
 * 
 * @author trevor
 */

@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.parse.Document")
public class WordFilter implements Processor<Document>, Source<Document> {
    Set<String> stopwords = new HashSet<String>();
    boolean keepListWords = false;
    public Processor<Document> processor = new NullProcessor( Document.class );
    
    public WordFilter( HashSet<String> words ) {
        stopwords = words;
    }
    
    public WordFilter( TupleFlowParameters params ) throws IOException {
        if( params.getXML().containsKey( "filename" ) ) {
            String filename = params.getXML().get( "filename" );
            stopwords = Utility.readFileToStringSet( new File(filename) );
        } else {
            stopwords = new HashSet( params.getXML().stringList( "word" ) );
        }
        
        keepListWords = params.getXML().get( "keepListWords", false );
    }
    
    public void process( Document document ) throws IOException {
        List<String> words = document.terms;
        
        for( int i=0; i<words.size(); i++ ) {
            String word = words.get(i);
            boolean wordInList = stopwords.contains(word);
            boolean removeWord = wordInList != keepListWords;
            
            if( removeWord )
                words.set(i, null);
        }
        
        processor.process( document );
    }
    
    public void close() throws IOException {
        processor.close();
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( parameters.getXML().containsKey( "filename" ) )
            return;
        
        if( parameters.getXML().stringList( "word" ).size() == 0 ) {
            handler.addWarning( "Couldn't find any words in the stopword list." );
        }
    }
    
    public void setProcessor(Step processor) throws IncompatibleProcessorException {
        Linkage.link(this, processor);
    }
}
