/*
 * DocumentStoreWriter
 *
 * May 22, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.store;

import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Processor;
import galago.parse.Document;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author trevor
 */
public class DocumentStoreWriter implements Processor<Document> {
    DocumentStore documentStore;
    
    public DocumentStoreWriter( TupleFlowParameters parameters ) throws SQLException, ClassNotFoundException {
        documentStore = new DocumentStore( parameters );
    }
    
    public void process( Document document ) throws IOException {
        documentStore.add( document );
    }

    public void close() throws IOException {
        documentStore.close();
    }    

    public Class<Document> getInputClass() {
        return Document.class;
    }
}
