/*
 * DocumentOrderedBinnedIndex
 * 
 * October 12, 2007 -- Trevor Strohman
 * 
 * BSD License (http://galagosearch.org/license)
 */

package galago.retrieval;

import galago.index.DocumentNameReader;
import galago.tupleflow.Parameters;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author trevor
 */
public class DocumentOrderedBinnedIndex {
    Parameters manifest;
    DocumentNameReader documents;
    HashMap<String, DocumentOrderedBinnedListReader> features;

    public DocumentOrderedBinnedIndex( String filename ) throws IOException {
        manifest = new Parameters();
        manifest.parse( filename + File.separator + "manifest" );
        features = new HashMap<String, DocumentOrderedBinnedListReader>();
        documents = null;
        String documentsPath = filename + File.separator + "documentNames";
        
        if( new File(documentsPath).exists() )
            documents = new DocumentNameReader( documentsPath );
        
        File directory = new File(filename);
        
        for( File subdirectory : directory.listFiles() ) {
            if( subdirectory.isFile() )
                continue;
            
            String name = subdirectory.getName();
            DocumentOrderedBinnedListReader reader = openListReader( subdirectory );
            features.put(name, reader);
        }
    }

    public Set<String> getFeatureNames() {
        return features.keySet();
    }
    
    public String getDocument( int document ) {
        return documents.get(document);
    }
    
    /**
     * Opens a sub-index.  Right now it just looks for an index called "priors", 
     * and opens that as a DenseIntegerIndexReader, otherwise opens a traditional
     * inverted index.  In the future this might actually check out the manifest
     * of the subindex.
     */
    
    public static DocumentOrderedBinnedListReader openListReader( File directory ) throws IOException {
        if( directory.getName().equals( "priors" ) ) {
            return new DenseIntegerIndexReader( directory.toString() );
        } else {
            return new DocumentOrderedBinnedIndexReader( directory.toString() );
        }
    }
    
    /**
     * Returns an iterator to the entire contents of a particular index.
     */
    
    public DocumentOrderedIterator getIterator( String index ) throws IOException {
        if( !features.containsKey(index) )
            return null;
        
        return features.get(index).getIterator();
    }
    
    /** 
     * Returns an iterator for this particular term from the default index.
     */
    
    public DocumentOrderedIterator getTerm( String term ) throws IOException {
        return getTerm( term, "default" );
    }

    /**
     * Returns an iterator for this particular term from the specified index.
     * Returns null if the index doesn't exist, or if the term doesn't exist
     * in the index.
     */
    
    public DocumentOrderedIterator getTerm( String term, String index ) throws IOException {
        if( !features.containsKey(index) )
            return null;
        
        return features.get(index).getTerm(term);
    }
    
    /**
     * Returns a particular index reader.
     */
    
    public DocumentOrderedBinnedListReader getReader( String index ) throws IOException {
        return features.get(index);
    }
    
    /**
     * Returns a default index reader.
     */
    
    public DocumentOrderedBinnedListReader getReader() throws IOException {
        return getReader( "default" );
    }
    
    /**
     * Closes the index.
     */
    
    public void close() throws IOException {
        for( DocumentOrderedBinnedListReader reader : features.values() ) {
            reader.close();
        }
    }
}
