
//
// Order.java
//
// 27 September 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow;

import java.util.Collection;
import java.util.Comparator;

public interface Order<T> {   
    public Class<T> getOrderedClass();
    public String[] getOrderSpec();
    
    public Comparator<T> lessThan();
    public Comparator<T> greaterThan();
    public int hash( T object );
    
    public Processor<T> orderedWriter( ArrayOutput output );
    public TypeReader<T> orderedReader( ArrayInput input );
    public TypeReader<T> orderedReader( ArrayInput input, int bufferSize );
    public ReaderSource<T> orderedCombiner( Collection< TypeReader<T> > readers, boolean closeOnExit );
}
