/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.io;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A simple filter stream that keeps track of the position. It is not at
 * all specific to Pig, but the need for it arises when implementing LoadFuncs.
 * 
 * @author breed
 *
 */
public class InputStreamPosition extends FilterInputStream {
    long pos;
    long markedPos;
    /**
     * Creates a InputStream filter for the given InputStream with
     * the given initial position.
     */
    public InputStreamPosition(InputStream in, long pos) {
       super(in);
       this.pos = pos;
    }
    public synchronized void mark(int readlimit) {
        super.mark(readlimit);
        markedPos = pos;
    }
    public int read() throws IOException {
        int c = super.read();
        pos++;
        return c;
    }
    public int read(byte[] b, int off, int len) throws IOException {
        int rc = super.read(b, off, len);
        if (rc > 0) {
            pos += rc;
        }
        return rc;
    }
    public synchronized void reset() throws IOException {
        super.reset();
        pos = markedPos;
    }
    public long skip(long n) throws IOException {
        long rc = super.skip(n);
        pos += rc;
        return rc;
    }
    
    /**
     * Returns the current position in the tracked InputStream.
     */
    public long getPosition() {
        return pos;
    }
}