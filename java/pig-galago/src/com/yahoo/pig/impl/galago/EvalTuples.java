/*
 * EvalTuples
 *
 * September 1, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.execution.Verified;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="com.yahoo.pig.impl.galago.Tuple")
@OutputClass(className="com.yahoo.pig.impl.galago.Tuple")
public class EvalTuples extends StandardStep<Tuple, Tuple> {
    PigContext context = new PigContext( ExecType.GALAGO ); // probably not the right option
    EvalSpecPipe evalSpecPipe;
    DataCollector collector;
    
    public EvalTuples( TupleFlowParameters parameters ) throws IOException {
        evalSpecPipe = new EvalSpecPipe( context, parameters.getXML().get( "spec" ) );
        collector = evalSpecPipe.collector( new EvalCollector() );
    }
    
    private class EvalCollector extends DataCollector {
        public void add(com.yahoo.pig.data.Tuple t) throws IOException {
            if( t == null )
                return;
            
            Tuple next = new Tuple();
            next.copyFrom(t);
            processor.process( next );
        }
    }
    
    public void process( Tuple t ) throws IOException {
        collector.add(t);
    }
}
