/*
 * StandardStep
 *
 * April 18, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.tupleflow;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public abstract class StandardStep<T, U> implements Processor<T>, Source<U> {
    public Processor<U> processor;
    
    public abstract void process( T object ) throws IOException;
    public void setProcessor( Step next ) throws IncompatibleProcessorException {
        Linkage.link( this, next );
    }

    public void close() throws IOException {
        processor.close();
    }
}
