
//
// Retrieval 
//
// 16 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "indri/Parameters.hpp"
#include "Retrieval.hpp"
#include "indri/Path.hpp"
#include "lemur/Exception.hpp"

#include "BinOrderedBinnedRetrieval.hpp"
#include "DocumentOrderedBinnedRetrieval.hpp"
#include "DocumentOrderedBinnedMaxScoreRetrieval.hpp"

Retrieval* Retrieval::open( const std::string& path ) {
    std::string manifestPath = indri::file::Path::combine( path, "manifest" );
    
    indri::api::Parameters manifest;
    manifest.loadFile( manifestPath );

    std::string listOrder = manifest.get( "indexType", "undefined" );

    if( listOrder == "binOrdered" ) {
        BinOrderedBinnedRetrieval* retrieval = new BinOrderedBinnedRetrieval();
        retrieval->openRead( path );
        return retrieval;
    } else if( listOrder == "documentOrdered" ) {
        bool simple = indri::api::Parameters::instance().get( "simple", false );

        if( simple ) {
            DocumentOrderedBinnedRetrieval* retrieval = new DocumentOrderedBinnedRetrieval();
            retrieval->openRead( path );
            return retrieval;
        }

        bool skipping = indri::api::Parameters::instance().get( "skipping", true );
        bool scoreSkipping = indri::api::Parameters::instance().get( "scoreSkipping", true );
        bool conjunctive = indri::api::Parameters::instance().get( "conjunctive", false );

        DocumentOrderedBinnedMaxScoreRetrieval* retrieval = new DocumentOrderedBinnedMaxScoreRetrieval( conjunctive, scoreSkipping, skipping );
        retrieval->openRead( path );
        return retrieval;
    } else {
        LEMUR_THROW( LEMUR_RUNTIME_ERROR, "Unknown index type: " + listOrder );
        return 0;
    }
}

