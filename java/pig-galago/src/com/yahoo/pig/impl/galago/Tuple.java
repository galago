/*
 * Tuple
 *
 * September 1, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.Order;
import galago.tupleflow.Processor;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Step;
import galago.tupleflow.Type;
import galago.tupleflow.TypeReader;
import java.io.EOFException;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 *
 * @author trevor
 */
public class Tuple extends com.yahoo.pig.data.Tuple
                   implements Type<Tuple> {
    public Tuple() {
        super();
    }
    
    public Tuple(int fieldCount) {
        super(fieldCount);
    }
    
    public Tuple(com.yahoo.pig.data.Tuple t) {
        super(t);
    }
    
    public Order<Tuple> getOrder(String... fields) {
        return new TupleOrder( fields );
    }
    
    public static class TupleOrder implements Order<Tuple> {
        public TupleOrder( String[] orderSpec ) {
        }

        public Class<Tuple> getOrderedClass() {
            return Tuple.class;
        }
        
        public String[] getOrderSpec() {
            return new String[0];
        }

        public int hash(Tuple tuple) {
            return tuple.fields.get(0).hashCode();
        }

        public Comparator<Tuple> lessThan() {
            return new Comparator<Tuple>() {
                public int compare( Tuple one, Tuple two ) {
                    return one.compareTo(two);
                }
            };
        }

        public Comparator<Tuple> greaterThan() {
            return new Comparator<Tuple>() {
                public int compare( Tuple one, Tuple two ) {
                    return -one.compareTo(two);
                }
            };
        }

        public ReaderSource<Tuple> orderedCombiner( final Collection<TypeReader<Tuple>> readers, final boolean closeOnExit) {
            return new TupleOrderedCombiner( readers, closeOnExit );
        }
        
        public static class TupleOrderedCombiner implements ReaderSource<Tuple> {
            public Processor<Tuple> processor;
            public Collection<TypeReader<Tuple>> readers;
            public PriorityQueue<ReaderIterator> iterators;
            public boolean closeOnExit;
            public boolean uninitialized;
            
            public TupleOrderedCombiner( Collection<TypeReader<Tuple>> readers, boolean closeOnExit ) {
                this.readers = readers;
                this.closeOnExit = closeOnExit;
                this.iterators = new PriorityQueue<ReaderIterator>();
                this.uninitialized = true;
            }
            
            public static class ReaderIterator implements Comparable<ReaderIterator> {
                public Tuple top;
                public TypeReader<Tuple> reader;
                
                public ReaderIterator( TypeReader<Tuple> reader ) throws IOException {
                    this.reader = reader;
                    read();
                }

                public boolean isDone() {
                    return top == null;
                }
                
                public Tuple read() throws IOException {
                    top = reader.read();
                    return top;
                }
            
                public int compareTo( ReaderIterator other ) {
                    return top.compareTo( other.top );
                }
            }
            
            public Tuple read() throws IOException {
                if( uninitialized ) {
                    for( TypeReader<Tuple> reader : readers ) {
                        ReaderIterator iterator = new ReaderIterator( reader );
                        
                        if( iterator.top != null ) {
                            iterators.add( iterator );
                        }
                    }
                    uninitialized = false;
                }
                
                Tuple result = null;
                
                if( iterators.size() > 0 ) {
                    ReaderIterator iterator = iterators.poll();
                    result = iterator.top;
                    
                    if( iterator.read() != null )
                        iterators.offer( iterator );
                }
                
                return result;
            }
            
            public void run() throws IOException {
                for( TypeReader<Tuple> reader : readers ) {
                    ReaderIterator iterator = new ReaderIterator( reader );

                    if( iterator.top != null ) {
                        iterators.add( iterator );
                    }
                }
                
                while( iterators.size() > 0 ) {
                    ReaderIterator iterator = iterators.poll();
                    processor.process( iterator.top );
                    
                    if( iterator.read() != null ) {
                        iterators.offer( iterator );
                    }
                }
                
                if( closeOnExit )
                    processor.close();
            }
            
            public void setProcessor( Step step ) throws IncompatibleProcessorException {
                Linkage.link(this,step);
            }
        }

        public Processor<Tuple> orderedWriter( final ArrayOutput output ) {
            return new Processor<Tuple>() {
                public void process( Tuple t ) throws IOException {
                    t.write( output.getDataOutput() );
                }
                
                public void close() {}
            };
        }

        public TypeReader<Tuple> orderedReader(final ArrayInput input) {
            return new TupleOrderedReader( input );
        }

        public TypeReader<Tuple> orderedReader(ArrayInput input, int bufferSize) {
            return orderedReader(input);
        }
        
        public static class TupleOrderedReader implements TypeReader<Tuple> {
            public Processor<Tuple> processor;
            public ArrayInput input;

            public TupleOrderedReader( ArrayInput input ) {
                this.input = input;
            }
            
            public Tuple read() throws IOException {
                try {
                    Tuple t = new Tuple();
                    t.readFields( input.getDataInput() );
                    return t;
                } catch( EOFException e ) {
                    return null;
                }
            }

            public void run() throws IOException {
                while(true) {
                    Tuple t = read();

                    if( t == null )
                        break;
                    
                    processor.process( t );
                }

                processor.close();
            }

            public void setProcessor(Step step) throws IncompatibleProcessorException {
                Linkage.link( this, step );
            }
        }
    } 
}
