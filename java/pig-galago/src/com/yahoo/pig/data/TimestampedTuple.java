/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TimestampedTuple extends Tuple {

    public double timestamp = 0;      // timestamp of this tuple
    public boolean isHeartbeat = false;  // true iff this is a heartbeat (i.e. purpose is just to convey new timestamp; carries no data)
    
    public TimestampedTuple(int numFields) {
        super(numFields);
    }
    
    public TimestampedTuple(String textLine, String delimiter, int timestampColumn, 
    						SimpleDateFormat dateFormat){
        if (delimiter == null) {
            delimiter = defaultDelimiter;
        }
        String[] splitString = textLine.split(delimiter, -1);
        fields = new ArrayList<Datum>(splitString.length-1);
        for (int i = 0; i < splitString.length; i++) {
        	if (i==timestampColumn){
        		try{
        			timestamp = dateFormat.parse(splitString[i]).getTime()/1000.0;
        		}catch(ParseException e){
        			System.err.println("Could not parse timestamp " + splitString[i]);
        		}
        	}else{
        		fields.add(new DataAtom(splitString[i]));
        	}
        }
    }

    
}
