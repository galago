
//
// DocumentOrderedBinnedRetrieval
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#include "DocumentOrderedBinnedRetrieval.hpp"
#include "indri/ScoredExtentResult.hpp"
#include "Query.hpp"
#include <vector>
#include <queue>
#include <assert.h>
#include "lemur/Exception.hpp"
#include "Logging.hpp"

//
// openRead
//

void DocumentOrderedBinnedRetrieval::openRead( const std::string& indexPath ) {
    _index.openRead( indexPath );
}                               

//
// runQuery
//

std::vector<indri::api::ScoredExtentResult>
    DocumentOrderedBinnedRetrieval::runQuery( const std::vector<QueryTerm>& terms,
                                              int requested,
                                              int threshold ) {
    std::priority_queue<indri::api::ScoredExtentResult> results;

    std::vector<DocumentOrderedBinnedIterator*> iterators = getIterators( terms );
    galago_log_query_terms( iterators.size() );
    
    while (true) {
        // determine which document to score
        int document = MAX_INT32;
        
        for( int i=0; i<iterators.size(); i++ ) {
            DocumentOrderedBinnedIterator* iterator = iterators[i];
            if( iterator->isDone() )
                continue;
            
            document = std::min( document, iterator->currentDocument() );
        }

        if( document == MAX_INT32 )
            break;
        
        // move all iterators to the document and score
        int score = 0;
        
        for( int i=0; i<iterators.size(); i++ ) {
            DocumentOrderedBinnedIterator* iterator = iterators[i];
            
            iterator->skipToDocument( document );
            if( iterator->isDone() || iterator->currentDocument() != document )
                continue;
            
            score += iterator->currentScore();
        }

        // store this result
        results.push( indri::api::ScoredExtentResult( score, document, 0, 0 ) );

        while( results.size() > requested )
            results.pop();

        // move the matching iterators forward
        for( int i=0; i<iterators.size(); i++ ) {
            DocumentOrderedBinnedIterator* iterator = iterators[i];

            if( !iterator->isDone() && iterator->currentDocument() == document )
                iterator->nextDocument();
        }
    }

    // convert priority queue into real results
    std::vector<indri::api::ScoredExtentResult> vectorResults;
    
    while( results.size() > 0 ) {
        vectorResults.push_back( results.top() );
        results.pop();
    }
    
    std::sort( vectorResults.begin(), vectorResults.end() );
    return vectorResults;
}

//
// getIterators
//

std::vector<DocumentOrderedBinnedIterator*>
    DocumentOrderedBinnedRetrieval::getIterators( const std::vector<QueryTerm>& terms ) {
    std::vector<DocumentOrderedBinnedIterator*> iterators;
    
    for( int i=0; i<terms.size(); i++ ) {
        const QueryTerm& term = terms[i];
        DocumentOrderedBinnedIterator* iterator = _index.getTerm( term.text, term.field );

        if( iterator )
            iterators.push_back( iterator );
    }
    
    return iterators;
}

//
// getDocument
//

std::string DocumentOrderedBinnedRetrieval::getDocument( int doc ) {
    return _index.getDocument(doc);
}

//
// close
//

void DocumentOrderedBinnedRetrieval::close() {
    _index.close();
}
