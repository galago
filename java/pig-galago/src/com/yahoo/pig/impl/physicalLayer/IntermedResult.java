/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.Map;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.builtin.BinStorage;
import com.yahoo.pig.builtin.PigStorage;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.io.PigFile;
import com.yahoo.pig.impl.logicalLayer.*;
import com.yahoo.pig.impl.mapreduceExec.MapReduceLauncher;

public class IntermedResult {
    public LogicalPlan  lp          = null;
    public PhysicalPlan pp          = null;
    public DataBag      databag     = null;
    public String       file        = null;
    boolean             isTemporary = true;
    private boolean     compiled    = false;
    private boolean     executed    = false;

    public IntermedResult(LogicalPlan lpIn) {
        lp = lpIn;
    }
    
    public IntermedResult() {
        executed = true;
        databag = new DataBag();
    }
    
    public void add(Tuple t) throws IOException {
        if (databag != null) databag.add(t);
        else throw new IOException("Attempt to add tuple to derived relation.");
    }
    
    public void toDFSFile(String filename, String func) throws IOException {
        if (databag == null) throw new IOException("Internal error: No data to store to file.");

        StorageFunc sf;
        try {
            sf = (StorageFunc) lp.getPigContext().instantiateArgFunc(func);
        } catch (Exception e) {
            sf = new PigStorage();
        }
        
        file = FileLocalizer.HADOOP_PREFIX + filename;
        
        PigFile f = new PigFile(file, false);
        f.store(ExecType.MAPREDUCE, databag, sf);
        
        databag = null;
        
        isTemporary = false;
    }
    
    public void compile(Map queryResults) throws IOException {
        pp = new PhysicalPlan(lp, queryResults);
        compiled = true;
    }

    public void exec() throws IOException {
        if (pp == null) {
            throw new IOException("Attempt to execute an uncompiled or already-executed query.");
        } else {
            switch(lp.getPigContext().getExecType()) {
            case MAPREDUCE:
                POMapreduce pom = (POMapreduce) pp.root;

                MapReduceLauncher.initQueryStatus(pom.numMRJobs());  // initialize status, for bookkeeping purposes.
                
                // if the final operator is a MapReduce with no output file, then send to a temp
                // file.
                if (pom.outputFile.equals("")) {
                    pom.outputFile = PlanCompiler.getTempFile();
                    pom.storeFunc = BinStorage.class.getName() + "()";
                }

                // remember the name of the file to which the final mapreduce operator writes
                file = pom.outputFile;
                file = FileLocalizer.HADOOP_PREFIX + file;
                break;
            case PIG:
                POPigbody pop = (POPigbody) pp.root;
                
                MapReduceLauncher.initQueryStatus(1);
                
                // if the final operator has no output file, then send to a temp
                // file.
                if (!(pop.logicalPlan.root() instanceof LOStore)) {
                    pop.logicalPlan.setRoot(new LOStore(pop.logicalPlan.root().pigContext, pop.logicalPlan.root(), PlanCompiler.getTempFile(), BinStorage.class.getName() + "()", false));
                }
                
                // remember the name of the file to which the pig store operator writes
                file = ((LOStore) pop.logicalPlan.root()).filename;
                file = FileLocalizer.HADOOP_PREFIX + file;
                break;
            }

            // execute the plan
            DataBag results = pp.exec(false);

            if (lp.getPigContext().getExecType() == ExecType.LOCAL) {
                databag = results;
            }

            pp = null;
            compiled = false;
            executed = true;
        }
    }

    public DataBag read() throws IOException {

        if (databag == null) {
            if (file == null)
                throw new IOException("Unable to read result.");
            databag = new PhysicalPlan(new POLoad(file, new BinStorage(), getOutputType())).exec(false); // load from temp
                                                                                // file
        }

        return databag;
    }

    public boolean compiled() {
        return compiled;
    }
    
    public void setCompiled( boolean compiled ) {
        this.compiled = compiled;
    }

    public boolean executed() {
        return executed;
    }

    public boolean isTemporary() {
        return isTemporary;
    }

    public void setPermanentFilename(String file) {
        this.file = file;
        isTemporary = false;
    }
    
    public int getOutputType(){
        if (lp == null) return LogicalOperator.FIXED;
        else return lp.getOutputType();
    }
}
