/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig;


public abstract class AlgebraicBagEvalFunc extends BagEvalFunc {

    public abstract TupleEvalFunc getInitial();

    public abstract TupleEvalFunc getIntermed();

    public abstract BagEvalFunc getFinal();
}
