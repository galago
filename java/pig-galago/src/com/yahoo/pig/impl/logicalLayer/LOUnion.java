/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.Iterator;
import java.util.List;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class LOUnion extends LogicalOperator {

    public LOUnion(PigContext pigContext, LogicalOperator[] inputsIn) {
    	super(pigContext);
        inputs = inputsIn;
    }

    public LOUnion(PigContext pigContext, List<LogicalOperator> inputsIn) {
    	super(pigContext);
        inputs = new LogicalOperator[inputsIn.size()];
        int i = 0;
        for (Iterator<LogicalOperator> it = inputsIn.iterator(); it.hasNext();)
            inputs[i++] = it.next();
    }

    public String name() {
        return "Union";
    }

    public SchemaItemList outputSchema() {
    	if (schema == null){
    		SchemaItemList longest = inputs[0].outputSchema();
	        int current = 0;
	        for (LogicalOperator lo : inputs) {
	            if (lo != null && lo.outputSchema() != null && lo.outputSchema().numFields() > current) {
	                longest = lo.outputSchema();
	                current = longest.numFields();
	            }
	        }
	        schema = longest.copy();
    	}
    	
    	schema.alias = alias;
        return schema;
    }
	
	public int getOutputType(){
	 	int outputType = FIXED;
    	for (int i=0; i<inputs.length; i++){
    		switch (inputs[i].getOutputType()){
    			case FIXED: 
    				continue;
    			case MONOTONE: 
    				if (outputType == FIXED)
    					outputType = MONOTONE;
    				continue;
    			case AMENDABLE:
    				outputType = AMENDABLE;
    			default:	
    				throw new RuntimeException("Invalid input type to the UNION operator");
    		} 
    	}
    	return outputType;
	}
}
