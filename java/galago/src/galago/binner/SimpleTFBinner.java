/*
 * SimpleTFBinner
 *
 * April 18, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.binner;

import galago.tupleflow.StandardStep;
import galago.types.DocumentWordBin;
import galago.types.DocumentWordCount;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class SimpleTFBinner extends StandardStep<DocumentWordCount, DocumentWordBin> {
    public void process( DocumentWordCount dwc ) throws IOException {
        processor.process( new DocumentWordBin( dwc.document, dwc.word, (int)dwc.count ) );
    }
    
    public Class<DocumentWordCount> getInputClass() {
        return DocumentWordCount.class;
    }
    
    public Class<DocumentWordBin> getOutputClass() {
        return DocumentWordBin.class;
    }
}
