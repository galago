/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import com.yahoo.pig.StorageFunc;
import com.yahoo.pig.PigServer;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.io.FileLocalizer;
import com.yahoo.pig.impl.io.InputStreamPosition;

public class BinStorage extends StorageFunc {
    Iterator<Tuple>     i              = null;
    DataInputStream     in             = null;
    InputStreamPosition posInputStream = null;
    InputStream         fsis           = null;
    long                end            = Long.MAX_VALUE;

    /**
     * Simple binary nested reader format
     */
    public BinStorage() {
    }

    public Tuple getNext() throws IOException {
        if (fsis == null || (posInputStream != null && posInputStream.getPosition() > end)) {
            return null;
        }
        byte b = 0;
//      skip to next record
        while (true) {
            b = (byte) in.read();
            if(b != Tuple.RECORD_1 && b != -1) {
                continue;
            }
            if(b == -1) return null;
            b = (byte) in.read();
            if(b != Tuple.RECORD_2 && b != -1) {
                continue;
            }
            if(b == -1) return null;
            b = (byte) in.read();
            if(b != Tuple.RECORD_3 && b != -1) {
                continue;
            }
            if(b == -1) return null;
            break;
        }
        Tuple t = new Tuple();
        t.readFields(in);
        return t;
    }

    public void bindTo(InputStream is, long offset, long end) throws IOException {
        this.posInputStream = new InputStreamPosition(is, offset);
        this.in = new DataInputStream(new BufferedInputStream(posInputStream));
        this.end = end;
        this.fsis = is;
        
        // Since we are not block aligned we throw away the first
        // record and count on a different instance to read it
        if (offset != 0) {
            getNext();
        }
    }


    DataOutputStream         out     = null;
  
    @Override
    public void bindTo(OutputStream os) throws IOException {
        this.out = new DataOutputStream(new BufferedOutputStream(os));
    }

    @Override
    public void done() throws IOException {
        out.flush();
    }

    @Override
    public void putNext(Tuple t) throws IOException {
        out.write(Tuple.RECORD_1);
        out.write(Tuple.RECORD_2);
        out.write(Tuple.RECORD_3);
        t.write(out);
    }
}
