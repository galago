/*
 * ExtentExtractor
 *
 * September 14, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentIdentifierExtent;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.types.DocumentIdentifierExtent")
@Verified
public class ExtentExtractor extends StandardStep<Document, DocumentIdentifierExtent> {
    
    /** Creates a new instance of ExtentExtractor */
    public ExtentExtractor() {
    }

    public void process(Document document) throws IOException {
        for( Tag tag : document.tags ) {
            processor.process( new DocumentIdentifierExtent( tag.name,
                                                             document.identifier,
                                                             tag.begin,
                                                             tag.end ) );
        }
    }
}
