
//
// DocumentOrderedBinnedIterator
//
// October 15, 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_DOCUMENTORDEREDBINNEDITERATOR_HPP
#define GALAGO_DOCUMENTORDEREDBINNEDITERATOR_HPP

#include "lemur/RVLCompress.hpp"
#include <assert.h>
#include <iostream>

class DocumentOrderedBinnedIterator {
public:
    struct length_less {
        bool operator() ( DocumentOrderedBinnedIterator* one, DocumentOrderedBinnedIterator* two ) {
            return one->listByteLength() < two->listByteLength();
        }
    };

    struct bound_greater {
        bool operator()  ( DocumentOrderedBinnedIterator* one, DocumentOrderedBinnedIterator* two ) {
            return one->listBound() > two->listBound();
        }
    };

private:
    const char* _data;
    UINT64 _start;
    UINT64 _end;
    
    struct Skip {
    public:
        int document;
        int bound;
        UINT64 offset;
    };
    
    Skip _lastSkip;
    Skip _currentSkip;
    
    int _options;
    int _documentCount;
    int _listBound;
    bool _hasSkips;
    
    UINT64 _skipLength;
    UINT64 _dataLength;
    
    const char* _skips;
    const char* _skipsStart;
    const char* _skipsEnd;
    
    const char* _postings;
    const char* _postingsStart;
    const char* _postingsEnd;
    
    int _document;
    int _score;
    bool _done;
    

private:
    /**
     * Reads the next skip datum.
     */

    void readSkip() {
        if( _skips == _skipsEnd ) {
            _lastSkip = _currentSkip;
            _currentSkip.document = MAX_INT32;
            _currentSkip.bound = _listBound;
            _currentSkip.offset = _dataLength;
        } else {
            _lastSkip = _currentSkip;

            _skips = lemur::utility::RVLCompress::decompress_int( _skips, _currentSkip.bound );
            _skips = lemur::utility::RVLCompress::decompress_int( _skips, _currentSkip.document );
            _skips = lemur::utility::RVLCompress::decompress_longlong( _skips, _currentSkip.offset );

            _currentSkip.document += _lastSkip.document;
            _currentSkip.offset += _lastSkip.offset;

            assert( _currentSkip.offset <= _dataLength );
        }
    }

    void moveToSkip() {
        if( _lastSkip.document == MAX_INT32 ) {
            _done = true;
            return;
        }

        if( _lastSkip.offset > _postings - _postingsStart )
            _postings = _postingsStart + _lastSkip.offset;

        assert( _postings <= _postingsEnd );
        _document = _lastSkip.document;
        _score = 0;
    }

    void readDocument() {
        assert( _postings <= _postingsEnd );

        if( _postings == _postingsEnd ) {
            _done = true;
        } else {
            int delta;

            _postings = lemur::utility::RVLCompress::decompress_int( _postings, delta );
            _postings = lemur::utility::RVLCompress::decompress_int( _postings, _score );
            _document += delta;
            assert( _document <= _lastSkip.document || _document > _currentSkip.document || _score <= _currentSkip.bound );
        }
    }

public:
    DocumentOrderedBinnedIterator( const char* data, UINT64 start, UINT64 end ) :
        _data(data), _start(start), _end(end)
    {
        load();
    }

    void load() {
        const char* header = _data + _start;

        header = lemur::utility::RVLCompress::decompress_int( header, _options );
        header = lemur::utility::RVLCompress::decompress_int( header, _documentCount );
        header = lemur::utility::RVLCompress::decompress_int( header, _listBound );

        if( (_options & 1) > 0 ) {
            header = lemur::utility::RVLCompress::decompress_longlong( header, _skipLength );
            _hasSkips = true;
        } else {
            _skipLength = 0;
            _hasSkips = false;
        }

        header = lemur::utility::RVLCompress::decompress_longlong( header, _dataLength );

        _lastSkip.bound = 0;
        _lastSkip.document = 0;
        _lastSkip.offset = 0;
        _currentSkip = _lastSkip;

        _skips = header;
        _skipsStart = header;
        _skipsEnd = _skips + _skipLength;

        _postings = _skipsEnd;
        _postingsStart = _postings;
        _postingsEnd = _postings + _dataLength;
        _done = false;
        _document = 0;
        _score = 0;

        readSkip();
        readDocument();
    }

    UINT64 listByteLength() {
        return _end - _start;
    }

    int listBound() {
        return _listBound;
    }

    int currentScore() {
        return _score;
    }

    int currentDocument() {
        return _document;
    }
    
    int currentBoundDocument() {
        return _currentSkip.document;
    }
    
    int lastBoundDocument() {
        return _lastSkip.document;
    }
    
    int currentBound() {
        return _currentSkip.bound;
    }
    
    void readSkipsTo( int document ) {
        while( _currentSkip.document < document && _skips != _skipsEnd )
            readSkip();
    }

    bool skipToBound( int bound ) {
        while( _skips != _skipsEnd && _currentSkip.bound < bound )
            readSkip();

        if( _document < _lastSkip.document )
            moveToSkip();
        
        while( !_done && _score < bound )
            readDocument();
        
        return !_done && _score < bound;
    }

    bool skipToDocument( int skipTo ) {
        while( skipTo > _currentSkip.document )
            readSkip();

        if( _document < _lastSkip.document && skipTo > _lastSkip.document )
            moveToSkip();

        while( !_done && skipTo > _document )
            readDocument();

        return !_done && _document == skipTo;
    }

    bool skipToDocument( int skipTo, int bound ) {
        while( skipTo > _currentSkip.document )
            readSkip();

        if( _document < _lastSkip.document && skipTo > _lastSkip.document )
            moveToSkip();

        if( _currentSkip.bound < bound )
            return false;

        while( !_done && skipTo > _document )
            readDocument();

        return !_done && _document == skipTo && _score >= bound;
    }

    void nextDocument() {
        readDocument();
    }

    bool isDone() {
        return _done;
    }

};

#endif // GALAGO_DOCUMENTORDEREDBINNEDITERATOR_HPP

