/*
 * DenseIntegerIndexReaderTest.java
 * JUnit based test
 *
 * Created on October 5, 2007, 3:31 PM
 */

package galago.retrieval;

import galago.index.DenseIntegerListWriter;
import galago.Utility;
import galago.tupleflow.Parameters;
import java.io.File;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class DenseIntegerIndexReaderTest extends TestCase {
    File tempPath;
    
    public DenseIntegerIndexReaderTest(String testName) {
        super(testName);
    }

    @Override
    public void setUp() throws Exception {
        // make a spot for the index
        tempPath = File.createTempFile( "galago-test-index", null );
        tempPath.delete();
        tempPath.mkdir();
        
        // build the index part
        DenseIntegerListWriter writer = new DenseIntegerListWriter( tempPath.toString() );
        
        writer.processWord( Utility.makeBytes("dense") );
        
        for( int i=0; i<100; i++ ) {
            writer.processDocument( i );
            writer.processTuple( i );
        }
        
        writer.processWord( Utility.makeBytes("squares") );
        
        for( int i=0; i<100; i++ ) {
            int document = i*i;
            int score = i;
            writer.processDocument( document );
            writer.processTuple( score );
        }
        
        // close it
        writer.close();

        // put in a generic manifest
        new Parameters().write( tempPath + File.separator + "manifest" );
    }
    
    @Override
    public void tearDown() throws Exception {
        Utility.deleteDirectory( tempPath );
    }

    public void testDense() throws Exception {
        // open it up and read it
        DenseIntegerIndexReader reader = new DenseIntegerIndexReader( tempPath.toString() );
        DocumentOrderedIterator iterator = reader.getTerm( "dense" );
        int readCount = 0;
        
        assertNotNull( iterator );
        
        for( readCount = 0; !iterator.isDone(); readCount++ ) {
            int score = iterator.currentScore();
            int document = iterator.currentDocument();

            assertEquals( document, readCount );
            assertEquals( score, document );
        
            iterator.nextDocument();
        }
        
        assertEquals( 100, readCount );
    }
    
    public void testSquares() throws Exception {
        // open it up and read it
        DenseIntegerIndexReader reader = new DenseIntegerIndexReader( tempPath.toString() );
        DocumentOrderedIterator iterator = reader.getTerm( "squares" );
        int readCount = 0;
        
        assertNotNull( iterator );
        
        for( readCount = 0; !iterator.isDone(); readCount++ ) {
            int score = iterator.currentScore();
            int document = iterator.currentDocument();
            int root = (int) Math.sqrt(document);
            
            assertEquals( document, readCount );
            
            if( root*root == document ) {
                assertEquals( score, root );
            } else {
                assertEquals( score, 0 );
            }
            
            iterator.nextDocument();
        }
        
        assertEquals( 9802, readCount );
        reader.close();
    }
    
    public void testSkipping() throws Exception {
        DenseIntegerIndexReader reader = new DenseIntegerIndexReader( tempPath.toString() );
        DocumentOrderedIterator iterator = reader.getTerm( "squares" );
        int readCount = 0;
        
        assertNotNull( iterator );

        int root = 0;
        for( readCount = 0; !iterator.isDone(); readCount++ ) {
            int doc = iterator.currentDocument();
            int score = iterator.currentScore();
            
            assertEquals( root, score );
            assertEquals( root*root, doc );
            
            root++;
            int nextDocument = root*root;
            iterator.skipToDocument( nextDocument );
        }
        
        assertEquals( 100, readCount );
        reader.close();
    }
}
