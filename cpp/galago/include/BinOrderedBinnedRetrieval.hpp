
//
// BinOrderedBinnedRetrieval
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                   
#ifndef GALAGO_BINORDEREDBINNEDRETRIEVAL_HPP
#define GALAGO_BINORDEREDBINNEDRETRIEVAL_HPP

#include "BinnedIndex.hpp"
#include <vector>
#include "indri/ScoredExtentResult.hpp"
#include "BinOrderedBinnedIterator.hpp"
#include <queue>
#include "Accumulators.hpp"
#include "Query.hpp"
#include "Retrieval.hpp"

//
// ScoredIterator
// 

class ScoredIterator {
public:
    struct order {
        bool operator () ( const ScoredIterator* one, const ScoredIterator* two ) const {
            return one->maxScore < two->maxScore;
        }
    };
    
    BinOrderedBinnedIterator* iterator;
    int index;
    int maxScore;
    int weight;
    
    ScoredIterator( BinOrderedBinnedIterator* _iterator ) {
        iterator = _iterator;
        index = -1;
        weight = 1;
        maxScore = 0xffff;
    }
    
    ~ScoredIterator() {
        delete iterator;
    }
    
    bool operator< ( const ScoredIterator& other ) {
       return maxScore < other.maxScore;
    }
};

class BinOrderedBinnedRetrieval : public Retrieval {
private:
    typedef std::priority_queue<ScoredIterator*,
                                std::vector<ScoredIterator*>,
                                ScoredIterator::order> siter_queue;
    
    BinnedIndex _index;
    indri::api::Parameters _parameters;
    
    void _assignMaxScore( std::vector<ScoredIterator*>& iterators );
    void _assignMaxScore( ScoredIterator* iterator );

    void _calculateQueryWeights( const BinnedIndex& index, std::vector<ScoredIterator*>& iterators, int binCount );
    void _loadQueue( siter_queue& queue, std::vector<ScoredIterator*>& iterators, Accumulators& accumulators );
    void _processExcessTerms( std::vector<ScoredIterator*>& iterators, Accumulators& accumulators );
    void _fetchIterators( std::vector<ScoredIterator*>& iterators, const std::vector<QueryTerm>& queryTerms );
    
public:
    void openRead( const std::string& indexPath );
    void close();
    std::string getDocument( int document );
    
    std::vector<indri::api::ScoredExtentResult> runQuery( const std::vector<QueryTerm>& query, int requested, int threshold );
};

#endif // GALAGO_BINORDEREDBINNEDRETRIEVAL_HPP



