/*
 * DocumentFilter
 *
 * May 25, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.Parameters;
import galago.tupleflow.StandardStep;
import java.io.IOException;
import java.util.HashSet;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.parse.Document")
public class DocumentFilter extends StandardStep<Document, Document> {
    HashSet<String> docnos = new HashSet();
    
    /** Creates a new instance of DocumentFilter */
    public DocumentFilter( TupleFlowParameters parameters ) {
        Parameters p = parameters.getXML();
        
        for( String docno : p.stringList( "identifier" ) ) {
            docnos.add( docno );
        }
    }
    
    public void process( Document document ) throws IOException {
        if ( docnos.contains( document.identifier ) )
            processor.process( document );
    }
    
    public Class<Document> getOutputClass() {
        return Document.class;
    }
    
    public Class<Document> getInputClass() {
        return Document.class;
    }
}
