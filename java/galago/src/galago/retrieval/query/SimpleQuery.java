/*
 * SimpleQuery
 *
 * December 12, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.query;

import galago.parse.Document;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author trevor
 */
public class SimpleQuery {
    public static class QueryTerm {
        public QueryTerm( String text ) {
            this.weight = 1.0;
            this.field = null;
            this.text = text;
        }
        
        public QueryTerm( String text, String field, double weight ) {
            this.text = text;
            this.field = field;
            this.weight = weight;
        }
        
        @Override
        public boolean equals( Object o ) {
            QueryTerm other = (QueryTerm)o;
            return text.equals(other.text) &&
                   ((field != null) ? field.equals(other.field) : other.field == null) &&
                   weight == other.weight;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 41 * hash + (this.text != null ? this.text.hashCode() : 0);
            hash = 41 * hash + (this.field != null ? this.field.hashCode() : 0);
            hash = 41 * hash + (int) (Double.doubleToLongBits(this.weight) ^ (Double.doubleToLongBits(this.weight) >>> 32));
            return hash;
        }
        
        @Override
        public String toString() {
            String term = text;
            
            // if this is a multi-word query, enclose it in quotes
            if( term.contains(" ") )
                term = "\"" + term + "\"";
            
            // use the minimum amount of syntax necessary to
            // express the query.  If everything is specified, 
            // the format is field:term^weight.
            
            if( field != null && weight != 1.0 )
                return String.format( "%s:%s^%f", field, term, weight );
            if( field != null )
                return String.format( "%s:%s", field, term );
            if( weight != 1.0 )
                return String.format( "%s^%f", term, weight );
            return text;
        }
        
        public String text;
        public String field;
        public double weight;
    }
    
    
    /** 
     * The format of the query term is <tt>field:term^weight</tt>.
     * Both the field and the weight are optional, and the term may
     * be enclosed in quotes.
     *
     * @return A QueryTerm object describing the query term.
     */
    
    public static QueryTerm parseQueryTerm( String term ) {
        double weight = 1.0;
        String field = null;
        
        int colon = term.indexOf(':');
        if( colon >= 0 ) {
            field = term.substring( 0, colon );
            term = term.substring( colon+1 );
        }
        
        int caret = term.indexOf('^');
        if( caret >= 0 ) {
            weight = Double.parseDouble(term.substring(caret+1));
            term = term.substring(0, caret);
        }
        
        if( term.startsWith("\"") )
            term = term.substring(1);
        
        if( term.endsWith("\"") )
            term = term.substring(0, term.length()-1);
        
        return new QueryTerm( term, field, weight );
    }
    
    
    public static List<String> textQueryTerms( String query ) {
        boolean inQuote = false;
        int firstNonSpace = query.length()+1;
        int i=0;
        ArrayList<String> results = new ArrayList<String>();

        // each loop parses a single term
        while( i < query.length() ) {
            // parsing goes in two phases; first we're trying to bypass inital
            // spaces before a query term.  after that point, we parse until the
            // next space that's not in quotes.
            for( ; i < query.length(); i++ ) {
                char c = query.charAt(i);
                
                if( Character.isSpaceChar(c) ) {
                    if( !inQuote ) {
                        if( firstNonSpace < i ) {
                            String term = query.substring(firstNonSpace, i);
                            results.add( term );
                        }
                        firstNonSpace = query.length()+1;
                    }
                } else if( c == '"' ) {
                    firstNonSpace = Math.min(firstNonSpace, i);
                    inQuote = !inQuote;
                } else {
                    firstNonSpace = Math.min(firstNonSpace, i);
                }
            }
        }
        
        if( firstNonSpace < query.length() ) {
            results.add( query.substring(firstNonSpace, query.length()) );
        }
        
        return results;
    }
    
    public static List<QueryTerm> parse( String query ) {
        ArrayList<QueryTerm> results = new ArrayList<QueryTerm>();
        int position = 0;
        String term = null;
        
        List<String> textTerms = textQueryTerms( query );
        ArrayList<QueryTerm> parsedTerms = new ArrayList<QueryTerm>();
        
        for( String textTerm : textTerms ) {
            parsedTerms.add( parseQueryTerm( textTerm ) );
        }
        
        return parsedTerms;
    }
    
    // BUGBUG: technically this should also handle weights, field restriction, and
    // maybe phrases too
    public static Node parseTree( String query ) {
        List<QueryTerm> terms = parse( query );
        ArrayList<Node> nodes = new ArrayList<Node>();
        
        for( QueryTerm term : terms ) {
            nodes.add( new Node( "text", term.text ) );
        }
        
        return new Node( "combine", nodes );
    }
    
    public static Document parseAsDocument( String query ) {
        List<QueryTerm> queryTerms = parse( query );
        Document document = new Document();
        ArrayList<String> terms = new ArrayList<String>();
        
        for( QueryTerm term : queryTerms ) {
            terms.add( term.text );
        }
        
        document.terms = terms;
        return document;
    }
    
}
