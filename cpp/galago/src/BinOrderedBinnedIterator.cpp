
//
// InvertedListIterator
//
// 6 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
     
#include "BinOrderedBinnedIterator.hpp"
#include "BinnedFeatureIndex.hpp"
#include "UncompressedRead.hpp"
#include "lemur/RVLCompress.hpp"
#include "indri/RVLDecompressStream.hpp"

//
// InvertedListIterator constructor
//

BinOrderedBinnedIterator::BinOrderedBinnedIterator( const char* data, UINT64 start, UINT64 end ) 
    :
    invertedData(data)
{
    _readTermHeader( start, end );
}

//
// bin
//

int BinOrderedBinnedIterator::bin() {
    return _currentBinInfo().bin;
}        

//
// next
//

bool BinOrderedBinnedIterator::next() {
    _termBinIndex++;
    if (_termBinIndex == _termBins.size()) {
        _termBinIndex--;
        return false;
    }

    return true;
} 

//
// reset
//

void BinOrderedBinnedIterator::reset() {
    _termBinIndex = -1;
}

//
// _currentBinInfo
//

const BinOrderedBinnedIterator::BinInfo& BinOrderedBinnedIterator::_currentBinInfo() const {
    return _termBins[_termBinIndex];
}

//
// binData
//
    
const char* BinOrderedBinnedIterator::binData() {
    const BinInfo& info = _currentBinInfo();
    return invertedData + info.dataStart;
}                                          

//
// binLength
//

UINT64 BinOrderedBinnedIterator::binDataLength() {
    const BinInfo& info = _currentBinInfo();
    return info.dataLength;
}

//
// skipData
//
    
const char* BinOrderedBinnedIterator::skipData() {
    const BinInfo& info = _currentBinInfo();
    return invertedData + info.skipStart;
}                                          

//
// skipDataLength
//

UINT64 BinOrderedBinnedIterator::skipDataLength() {
    const BinInfo& info = _currentBinInfo();
    return info.skipLength;
}

//
// postingsDataLength
//

UINT64 BinOrderedBinnedIterator::postingsDataLength() const {
    return _postingsLength;
}

//
// documentCount
//

UINT64 BinOrderedBinnedIterator::documentCount() const {
    return _documentCount;
}

//
// _readTermHeader
//

void BinOrderedBinnedIterator::_readTermHeader( UINT64 startPosition, UINT64 endPosition ) {
    const char* headerStart = invertedData + startPosition;
    const char* headerNext;
    int headerLength;
    int options;
                           
    headerNext = lemur::utility::RVLCompress::decompress_int( headerStart, headerLength );
    headerNext = lemur::utility::RVLCompress::decompress_int( headerNext, options );
    headerNext = lemur::utility::RVLCompress::decompress_longlong( headerNext, _documentCount );
    bool useSkips = ((options & 0x01) != 0);

    indri::utility::RVLDecompressStream stream( headerNext, headerLength );
    headerLength += (headerNext - headerStart);
    UINT64 totalBinLength = 0;
    _postingsLength = 0;
    
    while( !stream.done() ) {
        int termBin;
        int termSkipLength = 0;
        int termBinLength;
        
        stream >> termBin;
        if( useSkips ) stream >> termSkipLength;
        stream >> termBinLength;

        BinInfo info;

        info.bin = termBin;
        info.binStart = startPosition + headerLength + totalBinLength;
        info.skipStart = info.binStart;
        info.skipLength = termSkipLength;
        info.dataStart = info.skipStart + info.skipLength;
        info.dataLength = termBinLength;
        info.binLength = info.skipLength + info.dataLength;

        _termBins.push_back(info);
        totalBinLength += info.binLength;
        _postingsLength += info.dataLength;
    }     
    
    assert( startPosition + headerLength + totalBinLength == endPosition );
    _termBinIndex = -1;
}        

//
// _word_block_end_offset
//

static UINT16 _word_block_end_offset( const UINT16* wordBlockEnds, int index ) {
    if ( index == 0 ) {
        return 0;
    } else {
        return UncompressedRead::peek_u16( (const UINT8*) (&wordBlockEnds[index-1]) );
    }
}

