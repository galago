/*
 * PositionPostingsNumberer
 *
 * August 21, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import galago.types.DocumentIdentifierNumber;
import galago.types.DocumentWordPosition;
import galago.types.NumberWordPosition;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.DocumentWordPosition")
@OutputClass(className="galago.types.NumberWordPosition")
public class PositionPostingsNumberer extends StandardStep<DocumentWordPosition, NumberWordPosition>
                              implements DocumentWordPosition.Processor, NumberWordPosition.Source {
    HashMap<String, Integer> documentNumbers = new HashMap();
    
    public void process( DocumentWordPosition object ) throws IOException {
        assert documentNumbers.get(object.document) != null : "" + object.document + " has no name, even with " + documentNumbers.size() + " doc names.";
        processor.process( new NumberWordPosition( documentNumbers.get(object.document), object.word, object.position ) );
    }
    
    public PositionPostingsNumberer( TupleFlowParameters parameters ) throws IOException {
        TypeReader<DocumentIdentifierNumber> reader = parameters.getTypeReader( "documentNumbers" );
        DocumentIdentifierNumber dn;
        
        while( (dn = reader.read()) != null ) {
            documentNumbers.put( dn.identifier, dn.document );
        }
    }
    
    public Class<DocumentWordPosition> getInputClass() {
        return DocumentWordPosition.class;
    }
    
    public Class<NumberWordPosition> getOutputClass() {
        return NumberWordPosition.class;
    }
   
    public static String getInputClass( TupleFlowParameters parameters ) {
        return DocumentWordPosition.class.getName();
    }
    
    public static String getOutputClass( TupleFlowParameters parameters ) {
        return NumberWordPosition.class.getName();
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        Verification.verifyTypeReader( "documentNumbers", DocumentIdentifierNumber.class, parameters, handler );
    }
}