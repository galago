
//
// DocumentOrderedBinnedIndex
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_DOCUMENTORDEREDBINNEDINDEX_HPP
#define GALAGO_DOCUMENTORDEREDBINNEDINDEX_HPP

#include <string>
#include <map>
#include "DocumentOrderedBinnedListReader.hpp"
#include "DocumentOrderedBinnedIterator.hpp"
#include "DocumentNameReader.hpp"
#include "indri/Parameters.hpp"

class DocumentOrderedBinnedIndex {
private:
    std::map<std::string, DocumentOrderedBinnedListReader*> _readers;
    std::string _path;

    DocumentNameReader _documents;
    indri::api::Parameters _parameters;

public:
    void openRead( const std::string& path );
    void close();

    DocumentOrderedBinnedIterator* getTerm( const std::string& term ) const;
    DocumentOrderedBinnedIterator* getTerm( const std::string& term, const std::string& field ) const;

    DocumentOrderedBinnedListReader* getReader() const;
    DocumentOrderedBinnedListReader* getReader( const std::string& field ) const;
    
    std::string getDocument( int document );
};

#endif // GALAGO_DOCUMENTORDEREDBINNEDINDEX_HPP
