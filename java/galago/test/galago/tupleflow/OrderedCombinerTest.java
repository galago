/*
 * OrderedCombinerTest.java
 * JUnit based test
 *
 * Created on May 29, 2007, 12:00 PM
 */

package galago.tupleflow;

import galago.tupleflow.OrderedCombiner;
import galago.tupleflow.TypeReader;
import galago.types.DocumentLengthWordCount;
import junit.framework.*;

/**
 *
 * @author trevor
 */
public class OrderedCombinerTest extends TestCase {
    
    public OrderedCombinerTest(String testName) {
        super(testName);
    }

    public void testGetOutputClass() {
        System.out.println("getOutputClass");
        OrderedCombiner instance = new OrderedCombiner( new TypeReader[0], new DocumentLengthWordCount().getOrder( "+document", "+length" ) );
        
        Class expResult = DocumentLengthWordCount.class;
        Class result = instance.getOutputClass();
        assertEquals(expResult, result);
    }

    public void testRun() throws Exception {
        System.out.println("run");
        OrderedCombiner instance = new OrderedCombiner( new TypeReader[0], new DocumentLengthWordCount().getOrder( "+document", "+length" ) );
        instance.run();
    }
}
