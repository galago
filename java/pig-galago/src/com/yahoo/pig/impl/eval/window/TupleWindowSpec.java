/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.window;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.*;

public class TupleWindowSpec extends WindowSpec {
	int numTuples;
    List<Tuple> window;
	
	public TupleWindowSpec(windowType type, int numTuples){
		super(type);
		this.numTuples = numTuples;
        window = new LinkedList<Tuple>();
	}
	
	@Override
	public DataCollector collector(final DataCollector output) {
	    return new DataCollector() {

	        public void add(Tuple t) throws IOException {
	        	if (t!=null){
	        	
		        	if (t instanceof TimestampedTuple){
		        		if (((TimestampedTuple) t).isHeartbeat) return;
		        	}
		        	
	                window.add(t);
	                while (window.size() > numTuples) {
	                    window.remove(0);
	                }
	                
	             
	        	}
	        	
	        	// emit entire window content to output collector
                for (Iterator<Tuple> it = window.iterator(); it.hasNext(); ) {
                    output.add(it.next());
                }
                
                output.add(null);
	        }
	    };
    }
}
