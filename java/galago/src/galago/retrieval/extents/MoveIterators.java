/*
 * MoveIterators
 *
 * August 16, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class MoveIterators {
    
    /**
     * Moves all iterators in the array to the same document.  This method will only
     * stop at documents where all of the iterators have a match.
     *
     * This code assumes that the most selective iterator (the one with the fewest document
     * matches) is the first one.
     *
     * @return The document number that the iterators are now pointing to, or Integer.MAX_VALUE
     *         if one of the iterators is now done.
     */  
    
    public static int moveAllToSameDocument( ArrayList<ExtentIterator> iterators ) throws IOException {
        if( iterators.size() == 0 )
            return Integer.MAX_VALUE;
        
        if( iterators.get(0).isDone() )
            return Integer.MAX_VALUE;
        
        int currentTarget = iterators.get(0).document();
        boolean allMatch = false;
        
        retry:
        while (!allMatch) {
            allMatch = true;
            
            for( ExtentIterator iterator : iterators ) {
                if( iterator.isDone() )
                    return Integer.MAX_VALUE;
                
                int thisDocument = iterator.document();
                
                // this iterator points somewhere before our
                // current target document, so try to move forward to
                // the target
                if( currentTarget > thisDocument ) {
                    iterator.skipToDocument( currentTarget );
                    if( iterator.isDone() )
                        return Integer.MAX_VALUE;
                    thisDocument = iterator.document();
                }
                
                // this iterator points after the target document,
                // so the target document is not a match.
                // we break and try again because we don't want to 
                // touch the longest iterators if we can help it.
                if( currentTarget < thisDocument ) {
                    allMatch = false;
                    currentTarget = thisDocument;
                    continue retry;
                }
            }
        }

        return currentTarget;
    }

    public static boolean allSameDocument( ArrayList<ExtentIterator> iterators ) {
        if( iterators.size() == 0 )
            return true;
        
        int document = iterators.get(0).document();
        
        for( ExtentIterator iterator : iterators ) {
            if( document != iterator.document() )
                return false;
        }
        
        return true;
    }
    
    public static int findMaximumDocument( ArrayList<ExtentIterator> iterators ) {
        int maximumDocument = 0;
        
        for( ExtentIterator iterator : iterators ) {
            maximumDocument = Math.max( maximumDocument, iterator.document() );
        }
        
        return maximumDocument;
    }
    
    public static int findMinimumDocument( ArrayList<ExtentIterator> iterators ) {
        int minimumDocument = Integer.MAX_VALUE;
        
        for( ExtentIterator iterator : iterators ) {
            minimumDocument = Math.min( minimumDocument, iterator.document() );
        }
        
        return minimumDocument;
    }
    
    public static int findMinimumCandidate( ArrayList<ScoreIterator> iterators ) {
        int minimumDocument = Integer.MAX_VALUE;
        
        for( ScoreIterator iterator : iterators ) {
            minimumDocument = Math.min( minimumDocument, iterator.nextCandidate() );
        }
        
        return minimumDocument;
    }
}
