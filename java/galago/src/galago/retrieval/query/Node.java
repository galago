/*
 * Node
 *
 * August 10, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.query;

import galago.tupleflow.Parameters;
import galago.tupleflow.Parameters.Value;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Node {

    /// The query operator represented by this node, like "combine", "weight", "syn", etc.
    private String operator;

    /// Child nodes of the operator, e.g. in #combine(a b), 'a' and 'b' are internal nodes of #combine.
    private ArrayList<Node> internalNodes;

    /// The position in the text string where this operator starts.  Useful for parse error messages.
    private int position;

    /// Additional parameters for this operator; usually these are term statistics and smoothing parameters.
    private Parameters parameters;

    public Node() {
        internalNodes = new ArrayList<Node>();
        parameters = new Parameters();
    }

    public Node(String operator, ArrayList<Node> internalNodes) {
        this(operator, (String) null, internalNodes, 0);
    }

    public Node(String operator, ArrayList<Node> internalNodes, int position) {
        this(operator, (String) null, internalNodes, position);
    }

    public Node(String operator, String argument) {
        this(operator, argument, 0);
    }

    public Node(String operator, String argument, int position) {
        this(operator, argument, new ArrayList<Node>(), position);
    }

    public Node(String operator, String argument, ArrayList<Node> internalNodes) {
        this(operator, argument, internalNodes, 0);
    }

    public Node(String operator, String argument, ArrayList<Node> internalNodes, int position) {
        Parameters p = new Parameters();

        if (argument != null) {
            p.add("default", argument);
        }
        this.operator = operator;
        this.internalNodes = internalNodes;
        this.position = position;
        this.parameters = p;
    }

    public Node(String operator, Parameters parameters, ArrayList<Node> internalNodes, int position) {
        this.operator = operator;
        this.internalNodes = internalNodes;
        this.position = position;
        this.parameters = parameters;
    }

    public String getOperator() {
        return operator;
    }
    
    public String getDefaultParameter() {
        return parameters.get( "default", (String)null );
    }
    
    public String getDefaultParameter( String key ) {
        return parameters.get( key, getDefaultParameter() );
    }

    public ArrayList<Node> getInternalNodes() {
        return internalNodes;
    }

    public int getPosition() {
        return position;
    }

    public Parameters getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append('#');
        builder.append(operator);

        if (parameters.containsKey("default")) {
            builder.append(':');
            builder.append(parameters.get("default"));
        }

        Map<String, List<Value>> parameterMap = parameters.value().map();

        if (parameterMap != null) {
            for (String key : parameterMap.keySet()) {
                if (key.equals("default")) {
                    continue;
                }
                builder.append(':');
                builder.append(key);
                builder.append('=');
                builder.append(parameterMap.get(key).get(0).toString());
            }
        }

        builder.append("( ");
        for (Node child : internalNodes) {
            builder.append(child.toString());
            builder.append(' ');
        }
        builder.append(")");

        return builder.toString();
    }

    public boolean equals(Object o) {
        if (!(o instanceof Node)) {
            return false;
        }
        if (o == this) {
            return true;
        }
        Node other = (Node) o;

        if ((operator == null) != (other.getOperator() == null)) {
            return false;
        }
        if (operator != null && !other.getOperator().equals(operator)) {
            return false;
        }
        if (internalNodes.size() != other.getInternalNodes().size()) {
            return false;
        }
        for (int i = 0; i < internalNodes.size(); i++) {
            if (!internalNodes.get(i).equals(other.getInternalNodes().get(i))) {
                return false;
            }
        }

        return true;
    }
}