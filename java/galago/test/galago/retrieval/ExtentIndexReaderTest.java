/*
 * ExtentIndexReaderTest.java
 * JUnit based test
 *
 * Created on October 5, 2007, 4:36 PM
 */

package galago.retrieval;

import galago.Utility;
import galago.index.ExtentListWriter;
import galago.retrieval.extents.ExtentArrayIterator;
import galago.tupleflow.Parameters;
import galago.util.ExtentArray;
import java.io.File;
import junit.framework.TestCase;

/**
 *
 * @author trevor
 */
public class ExtentIndexReaderTest extends TestCase {
    File tempPath;
    
    public ExtentIndexReaderTest(String testName) {
        super(testName);
    }
    
    @Override
    public void setUp() throws Exception {
        // make a spot for the index
        tempPath = File.createTempFile( "galago-test-index", null );
        tempPath.delete();
        tempPath.mkdir();
        
        new Parameters().write( tempPath.toString() + File.separator + "manifest" );
        
        Parameters p = new Parameters();
        p.add( "index", tempPath.toString() );
        
        ExtentListWriter writer = new ExtentListWriter( new galago.tupleflow.FakeParameters(p) );
        
        writer.processExtentName( Utility.makeBytes( "title" ) );
        writer.processDocument( 1 );
        writer.processTuple( 2, 3 );
        writer.processTuple( 10, 11 );
        
        writer.processDocument( 9 );
        writer.processTuple( 5, 10 );
        
        writer.processExtentName( Utility.makeBytes( "z" ) );
        writer.processDocument( 15 );
        writer.processTuple( 9, 11 );
        
        writer.close();
    }
    
    @Override
    public void tearDown() throws Exception {
        Utility.deleteDirectory( tempPath );
    }

    public void testReadTitle() throws Exception {
        ExtentIndexReader reader = new ExtentIndexReader( tempPath.toString() );
        ExtentIndexReader.ExtentListIterator extents = reader.getExtents("title");
        
        assertFalse( extents.isDone() );
        
        ExtentArray e = extents.extents();
        ExtentArrayIterator iter = new ExtentArrayIterator(e);
        assertFalse( iter.isDone() );
        
        assertEquals( 1, extents.document() );
        
        assertEquals( 2, iter.current().begin );
        assertEquals( 3, iter.current().end );
        
        iter.next();
        assertFalse( iter.isDone() );
        
        assertEquals( 10, iter.current().begin );
        assertEquals( 11, iter.current().end );
        
        iter.next();
        assertTrue( iter.isDone() );
        
        extents.nextDocument();
        assertFalse( extents.isDone() );

        e = extents.extents();
        iter = new ExtentArrayIterator(e);        
        
        assertEquals( 9, extents.document() );
        
        assertEquals( 5, iter.current().begin );
        assertEquals( 10, iter.current().end );
        
        extents.nextDocument();
        assertTrue( extents.isDone() );
        
        reader.close();
    }
    
    public void testReadZ() throws Exception {
        ExtentIndexReader reader = new ExtentIndexReader( tempPath.toString() );
        ExtentIndexReader.ExtentListIterator extents = reader.getExtents("z");
        
        assertFalse( extents.isDone() );
        
        ExtentArray e = extents.extents();
        ExtentArrayIterator iter = new ExtentArrayIterator(e);
        
        assertEquals( 15, extents.document() );
        
        assertEquals( 9, iter.current().begin );
        assertEquals( 11, iter.current().end );
        
        extents.nextDocument();
        assertTrue( extents.isDone() );
        
        reader.close();
    }
    
    public void testSimpleSkipTitle() throws Exception {
        ExtentIndexReader reader = new ExtentIndexReader( tempPath.toString() );
        ExtentIndexReader.ExtentListIterator extents = reader.getExtents("title");
        
        assertFalse( extents.isDone() );
        extents.skipToDocument( 10 );
        assertTrue( extents.isDone() );
        
        reader.close();
    }
}
