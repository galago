// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/DocumentIdentifierNumber.java DocumentIdentifierNumber galago.types int:document String:identifier int:length order:+identifier order:+document
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class DocumentIdentifierNumber implements Type<DocumentIdentifierNumber> {
    public int document;
    public String identifier;
    public int length; 
    
    public DocumentIdentifierNumber() {}
    public DocumentIdentifierNumber( int document, String identifier, int length ) {
        this.document = document;
        this.identifier = identifier;
        this.length = length;
    }  
    
    public String toString() {
            return String.format( "%d,%s,%d",
                                   document, identifier, length );
    } 

    public Order<DocumentIdentifierNumber> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+identifier" } ) ) {
            return new IdentifierOrder();
        }
        if ( Arrays.equals( spec, new String[] { "+document" } ) ) {
            return new DocumentOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<DocumentIdentifierNumber> {
        public void process( DocumentIdentifierNumber object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class IdentifierOrder implements Order<DocumentIdentifierNumber> {
        public int hash( DocumentIdentifierNumber object ) {
            int h = 0;
            h += Utility.hash(object.identifier);
            return h;
        } 
        public Comparator<DocumentIdentifierNumber> greaterThan() {
            return new Comparator<DocumentIdentifierNumber>() {
                public int compare( DocumentIdentifierNumber one, DocumentIdentifierNumber two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.identifier, two.identifier );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentIdentifierNumber> lessThan() {
            return new Comparator<DocumentIdentifierNumber>() {
                public int compare( DocumentIdentifierNumber one, DocumentIdentifierNumber two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.identifier, two.identifier );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentIdentifierNumber> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentIdentifierNumber> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentIdentifierNumber> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentIdentifierNumber > {
            DocumentIdentifierNumber last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentIdentifierNumber object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.identifier, last.identifier) ) { processAll = true; shreddedWriter.processIdentifier( object.identifier ); }
               shreddedWriter.processTuple( object.document, object.length );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentIdentifierNumber> getInputClass() {
                return DocumentIdentifierNumber.class;
            }
        } 
        public ReaderSource<DocumentIdentifierNumber> orderedCombiner( Collection<TypeReader<DocumentIdentifierNumber>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentIdentifierNumber> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentIdentifierNumber clone( DocumentIdentifierNumber object ) {
            DocumentIdentifierNumber result = new DocumentIdentifierNumber();
            if ( object == null ) return result;
            result.document = object.document; 
            result.identifier = object.identifier; 
            result.length = object.length; 
            return result;
        }                 
        public Class<DocumentIdentifierNumber> getOrderedClass() {
            return DocumentIdentifierNumber.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+identifier"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processIdentifier( String identifier ) throws IOException;
            public void processTuple( int document, int length ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastIdentifier;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processIdentifier( String identifier ) {
                lastIdentifier = identifier;
                buffer.processIdentifier( identifier );
            }
            public final void processTuple( int document, int length ) throws IOException {
                if( lastFlush ) {
                    if( buffer.identifiers.size() == 0 ) buffer.processIdentifier( lastIdentifier );
                    lastFlush = false;
                }
                buffer.processTuple( document, length );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeInt( buffer.getDocument() );
                    output.writeInt( buffer.getLength() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushIdentifier( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getIdentifierEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getIdentifier() );
                    output.writeInt( count );
                    buffer.incrementIdentifier();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushIdentifier( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> identifiers = new ArrayList();
            ArrayList<Integer> identifierTupleIdx = new ArrayList();
            int identifierReadIdx = 0;
                            
            int[] documents;
            int[] lengths;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                documents = new int[batchSize];
                lengths = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processIdentifier( String identifier ) {
                identifiers.add( identifier );
                identifierTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( int document, int length ) {
                assert identifiers.size() > 0;
                documents[writeTupleIndex] = document;
                lengths[writeTupleIndex] = length;
                writeTupleIndex++;
            }
            public void resetData() {
                identifiers.clear();
                identifierTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                identifierReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementIdentifier() {
                identifierReadIdx++;  
            }                                                                                              

            public void autoIncrementIdentifier() {
                while( readTupleIndex >= getIdentifierEndIndex() && readTupleIndex < writeTupleIndex )
                    identifierReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getIdentifierEndIndex() {
                if( (identifierReadIdx+1) >= identifierTupleIdx.size() )
                    return writeTupleIndex;
                return identifierTupleIdx.get(identifierReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getIdentifier() {
                assert readTupleIndex < writeTupleIndex;
                assert identifierReadIdx < identifiers.size();
                
                return identifiers.get( identifierReadIdx );
            }
            public int getDocument() {
                assert readTupleIndex < writeTupleIndex;
                return documents[readTupleIndex];
            }                                         
            public int getLength() {
                assert readTupleIndex < writeTupleIndex;
                return lengths[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getDocument(), getLength() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexIdentifier( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processIdentifier( getIdentifier() );
                    assert getIdentifierEndIndex() <= endIndex;
                    copyTuples( getIdentifierEndIndex(), output );
                    incrementIdentifier();
                }
            }  
            public void copyUntilIdentifier( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getIdentifier(), other.getIdentifier() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processIdentifier( getIdentifier() );
                                      
                        copyTuples( getIdentifierEndIndex(), output );
                    } else {
                        output.processIdentifier( getIdentifier() );
                        copyTuples( getIdentifierEndIndex(), output );
                    }
                    incrementIdentifier();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilIdentifier( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentIdentifierNumber>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentIdentifierNumber.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentIdentifierNumber.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentIdentifierNumber>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentIdentifierNumber> getOutputClass() {
                return DocumentIdentifierNumber.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentIdentifierNumber read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentIdentifierNumber result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentIdentifierNumber>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();         
            long updateIdentifierCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getIdentifier(), otherBuffer.getIdentifier() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentIdentifierNumber read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentIdentifierNumber result = new DocumentIdentifierNumber();
                
                result.identifier = buffer.getIdentifier();
                result.document = buffer.getDocument();
                result.length = buffer.getLength();
                
                buffer.incrementTuple();
                buffer.autoIncrementIdentifier();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateIdentifierCount - tupleCount > 0 ) {
                            buffer.identifiers.add(last.identifier);
                            buffer.identifierTupleIdx.add( (int) (updateIdentifierCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateIdentifier();
                        buffer.processTuple( input.readInt(), input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateIdentifier() throws IOException {
                if( updateIdentifierCount > tupleCount )
                    return;
                     
                last.identifier = input.readString();
                updateIdentifierCount = tupleCount + input.readInt();
                                      
                buffer.processIdentifier( last.identifier );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentIdentifierNumber.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentIdentifierNumber.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentIdentifierNumber>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentIdentifierNumber> getOutputClass() {
                return DocumentIdentifierNumber.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();
            boolean identifierProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processIdentifier( String identifier ) throws IOException {  
                if( identifierProcess || Utility.compare( identifier, last.identifier ) != 0 ) {
                    last.identifier = identifier;
                    processor.processIdentifier( identifier );
                    identifierProcess = false;
                }
            }  
            
            public void resetIdentifier() {
                 identifierProcess = true;
            }                                                
                               
            public void processTuple( int document, int length ) throws IOException {
                processor.processTuple( document, length );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();
            public galago.tupleflow.Processor<DocumentIdentifierNumber> processor;                               
            
            public TupleUnshredder( DocumentIdentifierNumber.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentIdentifierNumber> processor ) {
                this.processor = processor;
            }
            
            public DocumentIdentifierNumber clone( DocumentIdentifierNumber object ) {
                DocumentIdentifierNumber result = new DocumentIdentifierNumber();
                if ( object == null ) return result;
                result.document = object.document; 
                result.identifier = object.identifier; 
                result.length = object.length; 
                return result;
            }                 
            
            public void processIdentifier( String identifier ) throws IOException {
                last.identifier = identifier;
            }   
                
            
            public void processTuple( int document, int length ) throws IOException {
                last.document = document;
                last.length = length;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentIdentifierNumber clone( DocumentIdentifierNumber object ) {
                DocumentIdentifierNumber result = new DocumentIdentifierNumber();
                if ( object == null ) return result;
                result.document = object.document; 
                result.identifier = object.identifier; 
                result.length = object.length; 
                return result;
            }                 
            
            public void process( DocumentIdentifierNumber object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.identifier, object.identifier ) != 0 || processAll ) { processor.processIdentifier( object.identifier ); processAll = true; }
                processor.processTuple( object.document, object.length );                                         
            }
                          
            public Class<DocumentIdentifierNumber> getInputClass() {
                return DocumentIdentifierNumber.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
    public static class DocumentOrder implements Order<DocumentIdentifierNumber> {
        public int hash( DocumentIdentifierNumber object ) {
            int h = 0;
            h += Utility.hash(object.document);
            return h;
        } 
        public Comparator<DocumentIdentifierNumber> greaterThan() {
            return new Comparator<DocumentIdentifierNumber>() {
                public int compare( DocumentIdentifierNumber one, DocumentIdentifierNumber two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<DocumentIdentifierNumber> lessThan() {
            return new Comparator<DocumentIdentifierNumber>() {
                public int compare( DocumentIdentifierNumber one, DocumentIdentifierNumber two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.document, two.document );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<DocumentIdentifierNumber> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<DocumentIdentifierNumber> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<DocumentIdentifierNumber> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< DocumentIdentifierNumber > {
            DocumentIdentifierNumber last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( DocumentIdentifierNumber object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.document, last.document) ) { processAll = true; shreddedWriter.processDocument( object.document ); }
               shreddedWriter.processTuple( object.identifier, object.length );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<DocumentIdentifierNumber> getInputClass() {
                return DocumentIdentifierNumber.class;
            }
        } 
        public ReaderSource<DocumentIdentifierNumber> orderedCombiner( Collection<TypeReader<DocumentIdentifierNumber>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<DocumentIdentifierNumber> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public DocumentIdentifierNumber clone( DocumentIdentifierNumber object ) {
            DocumentIdentifierNumber result = new DocumentIdentifierNumber();
            if ( object == null ) return result;
            result.document = object.document; 
            result.identifier = object.identifier; 
            result.length = object.length; 
            return result;
        }                 
        public Class<DocumentIdentifierNumber> getOrderedClass() {
            return DocumentIdentifierNumber.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+document"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processDocument( int document ) throws IOException;
            public void processTuple( String identifier, int length ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            int lastDocument;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processDocument( int document ) {
                lastDocument = document;
                buffer.processDocument( document );
            }
            public final void processTuple( String identifier, int length ) throws IOException {
                if( lastFlush ) {
                    if( buffer.documents.size() == 0 ) buffer.processDocument( lastDocument );
                    lastFlush = false;
                }
                buffer.processTuple( identifier, length );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeString( buffer.getIdentifier() );
                    output.writeInt( buffer.getLength() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushDocument( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getDocumentEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeInt( buffer.getDocument() );
                    output.writeInt( count );
                    buffer.incrementDocument();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushDocument( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<Integer> documents = new ArrayList();
            ArrayList<Integer> documentTupleIdx = new ArrayList();
            int documentReadIdx = 0;
                            
            String[] identifiers;
            int[] lengths;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                identifiers = new String[batchSize];
                lengths = new int[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processDocument( int document ) {
                documents.add( document );
                documentTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( String identifier, int length ) {
                assert documents.size() > 0;
                identifiers[writeTupleIndex] = identifier;
                lengths[writeTupleIndex] = length;
                writeTupleIndex++;
            }
            public void resetData() {
                documents.clear();
                documentTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                documentReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementDocument() {
                documentReadIdx++;  
            }                                                                                              

            public void autoIncrementDocument() {
                while( readTupleIndex >= getDocumentEndIndex() && readTupleIndex < writeTupleIndex )
                    documentReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getDocumentEndIndex() {
                if( (documentReadIdx+1) >= documentTupleIdx.size() )
                    return writeTupleIndex;
                return documentTupleIdx.get(documentReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public int getDocument() {
                assert readTupleIndex < writeTupleIndex;
                assert documentReadIdx < documents.size();
                
                return documents.get( documentReadIdx );
            }
            public String getIdentifier() {
                assert readTupleIndex < writeTupleIndex;
                return identifiers[readTupleIndex];
            }                                         
            public int getLength() {
                assert readTupleIndex < writeTupleIndex;
                return lengths[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getIdentifier(), getLength() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexDocument( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processDocument( getDocument() );
                    assert getDocumentEndIndex() <= endIndex;
                    copyTuples( getDocumentEndIndex(), output );
                    incrementDocument();
                }
            }  
            public void copyUntilDocument( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getDocument(), other.getDocument() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processDocument( getDocument() );
                                      
                        copyTuples( getDocumentEndIndex(), output );
                    } else {
                        output.processDocument( getDocument() );
                        copyTuples( getDocumentEndIndex(), output );
                    }
                    incrementDocument();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilDocument( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<DocumentIdentifierNumber>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentIdentifierNumber.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentIdentifierNumber.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentIdentifierNumber>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentIdentifierNumber> getOutputClass() {
                return DocumentIdentifierNumber.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public DocumentIdentifierNumber read() throws IOException {
                if( uninitialized )
                    initialize();

                DocumentIdentifierNumber result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<DocumentIdentifierNumber>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();         
            long updateDocumentCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getDocument(), otherBuffer.getDocument() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final DocumentIdentifierNumber read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                DocumentIdentifierNumber result = new DocumentIdentifierNumber();
                
                result.document = buffer.getDocument();
                result.identifier = buffer.getIdentifier();
                result.length = buffer.getLength();
                
                buffer.incrementTuple();
                buffer.autoIncrementDocument();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateDocumentCount - tupleCount > 0 ) {
                            buffer.documents.add(last.document);
                            buffer.documentTupleIdx.add( (int) (updateDocumentCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateDocument();
                        buffer.processTuple( input.readString(), input.readInt() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateDocument() throws IOException {
                if( updateDocumentCount > tupleCount )
                    return;
                     
                last.document = input.readInt();
                updateDocumentCount = tupleCount + input.readInt();
                                      
                buffer.processDocument( last.document );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof DocumentIdentifierNumber.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (DocumentIdentifierNumber.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<DocumentIdentifierNumber>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<DocumentIdentifierNumber> getOutputClass() {
                return DocumentIdentifierNumber.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();
            boolean documentProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processDocument( int document ) throws IOException {  
                if( documentProcess || Utility.compare( document, last.document ) != 0 ) {
                    last.document = document;
                    processor.processDocument( document );
                    documentProcess = false;
                }
            }  
            
            public void resetDocument() {
                 documentProcess = true;
            }                                                
                               
            public void processTuple( String identifier, int length ) throws IOException {
                processor.processTuple( identifier, length );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();
            public galago.tupleflow.Processor<DocumentIdentifierNumber> processor;                               
            
            public TupleUnshredder( DocumentIdentifierNumber.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<DocumentIdentifierNumber> processor ) {
                this.processor = processor;
            }
            
            public DocumentIdentifierNumber clone( DocumentIdentifierNumber object ) {
                DocumentIdentifierNumber result = new DocumentIdentifierNumber();
                if ( object == null ) return result;
                result.document = object.document; 
                result.identifier = object.identifier; 
                result.length = object.length; 
                return result;
            }                 
            
            public void processDocument( int document ) throws IOException {
                last.document = document;
            }   
                
            
            public void processTuple( String identifier, int length ) throws IOException {
                last.identifier = identifier;
                last.length = length;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            DocumentIdentifierNumber last = new DocumentIdentifierNumber();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public DocumentIdentifierNumber clone( DocumentIdentifierNumber object ) {
                DocumentIdentifierNumber result = new DocumentIdentifierNumber();
                if ( object == null ) return result;
                result.document = object.document; 
                result.identifier = object.identifier; 
                result.length = object.length; 
                return result;
            }                 
            
            public void process( DocumentIdentifierNumber object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.document, object.document ) != 0 || processAll ) { processor.processDocument( object.document ); processAll = true; }
                processor.processTuple( object.identifier, object.length );                                         
            }
                          
            public Class<DocumentIdentifierNumber> getInputClass() {
                return DocumentIdentifierNumber.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    