/*
 * ScriptStep
 * 
 * 14 November 2007 -- Trevor Strohman
 * 
 * BSD License (http://galagosearch.org/license)
 */

package galago.tupleflow;

import galago.tupleflow.execution.ErrorHandler;
import galago.tupleflow.execution.Verification;
import java.io.IOException;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * ScriptStep allows you to use any Java-compatible scripting language
 * (like Jython, JRuby or Groovy) to create a TupleFlow stage.
 * 
 * <p>To use this, you need to specify three parameters to the ScriptStep
 * in the TupleFlow parameter file:  "language", "script", and "make".
 * The language parameter is the name of your scripting language, e.g.
 * 'jython'.  The 'script' parameter should contain a class definition
 * that extends galago.tupleflow.Step, like this the following Jython code.
 * Notice that you need to make a new instance of the object at the 
 * end of the code block.</p>
 * 
 * <pre>
 * class IdentityStep(galago.tupleflow.Processor):
 *     def setProcessor(self, processor):
 *         self.processor = processor
 *     def process(self, object):
 *         self.processor.process(object)
 *     def close(self):
 *         self.processor.close()
 * 
 * IdentityStep()
 * </pre>
 * 
 * @author trevor
 */
public class ScriptStep implements Processor, ExNihiloSource {
    Step step;
    
    public ScriptStep( TupleFlowParameters parameters ) throws ScriptException {
        String language = parameters.getXML().get("language");
        String script = parameters.getXML().get("script");
        
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName( language );
        
        Object result = engine.eval(script);
        step = (Step) result;
    }

    public void process(Object object) throws IOException {
        Processor processor = (Processor) step;
        processor.process(object);
    }

    public void close() throws IOException {
        Processor processor = (Processor) step;
        processor.close();
    }

    public void setProcessor(Step next) throws IncompatibleProcessorException {
        Source s = (Source) step;
        s.setProcessor(next);
    }

    public void run() throws IOException {
        ExNihiloSource s = (ExNihiloSource) step;
        s.run();
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !Verification.requireParameters( new String[] { "language", "script" }, parameters.getXML(), handler ) )
            return;
        
        Parameters p = parameters.getXML();
        String language = p.get("language");
        String script = p.get("script");
        String make = p.get("make");

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName( language );
        
        if( engine == null ) {
            handler.addError( "The script engine '" + language + "' was not found." );
        } else {
            Object o = null;

            try {
                o = engine.eval(script + "\n" + make);
            } catch (ScriptException ex) {
                handler.addError( "Caught an exception while trying to parse the script: " + ex.toString() );
                return;
            }
            
            if( o == null || !(o instanceof Step) ) {
                handler.addError( "Ran the script code in 'make', but the resulting object was not a galago.tupleflow.Step." );
            }
        }
    }
}
