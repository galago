/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval;

import java.io.IOException;
import java.util.*;

import com.yahoo.pig.data.*;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

final public class ConstEvalItem extends EvalItem {
    public String val;
    public Double dval  = null;
    
    public ConstEvalItem(PigContext pigContext, String val) {
    	super(pigContext);
        this.val = val;
    }
    
    public String toString() {
        return "'" + val + "'";
    }

    public List<String> getFuncs() {
        return new ArrayList<String>();
    }
    
    public boolean isSimple() {
        return true;
    }

    @Override
    public String stringVal(Tuple tup) throws IOException {
        return val;
    }
    
    @Override
    public Double doubleVal(Tuple tup) throws IOException {
        if(dval == null) {
            try{
                dval = Double.parseDouble(val);
            } catch(NumberFormatException exp) {
                exp.printStackTrace();
            }
        }
        return dval;
    }
    
    @Override
    public SchemaItem mapInputSchema(SchemaItem input){
    	if (schema!= null)
    		return schema;
    	return new SchemaField(null);
    }


    @Override
    public void eval(Tuple input, DataCollector output) throws IOException {
        output.add(new Tuple(new DataAtom(val)));
        output.add(null);  // EOF
    }

    @Override
    public Datum simpleEval(Tuple input) throws IOException {
        return new DataAtom(val);
    }
    
}
