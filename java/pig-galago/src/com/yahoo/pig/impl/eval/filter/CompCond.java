/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.eval.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.eval.EvalItem;

public class CompCond extends Cond {
    public EvalItem left, right;
    public String op;   // one of "<", ">", "==", etc.
    
    public CompCond(EvalItem left, String op, EvalItem right) {
        this.left = left;
        this.op = op;
        this.right = right;
    }

    @Override
    public List<String> getFuncs() {
        return new ArrayList<String>();
    }

    @Override
    public boolean eval(Tuple input) throws IOException {
        
        Double d1 = null;
        Double d2 = null;
        String v1 = null;
        String v2 = null;
       
        
        char op1 = op.charAt(0);
        char op2 = op.length() >= 2 ? op.charAt(1) : '0';
        char op3 = op.length() == 3 ? op.charAt(2) : '0';
        
        switch (op1) {
            // numeric ops first
        case '=':
            if (op2 == '=') {
                d1 = left.doubleVal(input);
                d2 = right.doubleVal(input);
                return d1.equals(d2);
            } else {
                throw new IOException("Internal error: Invalid filter operator: " + op);
            }
        case '<':
            d1 = left.doubleVal(input);
            d2 = right.doubleVal(input);
            if (op2 == '=') {
                return d1.compareTo(d2) <= 0;
            } else {
                return d1.compareTo(d2) < 0;
            }
        case '>':
            d1 = left.doubleVal(input);
            d2 = right.doubleVal(input);
            if (op2 == '=') {
                return d1.compareTo(d2) >= 0;
            } else {
                return d1.compareTo(d2) > 0;
            }
        case '!':
            if (op2 == '=') {
                d1 = left.doubleVal(input);
                d2 = right.doubleVal(input);
                return !d1.equals(d2);
            } else {
                throw new IOException("Internal error: Invalid filter operator: " + op);
            }
            // now string ops
        case 'e':
            if (op2 == 'q') {
                v1 = left.stringVal(input);
                v2 = right.stringVal(input);
                return v1.equals(v2);
            } else {
                throw new IOException("Internal error: Invalid filter operator: " + op);
            }
        case 'l':
            v1 = left.stringVal(input);
            v2 = right.stringVal(input);
            if (op2 == 't' && op3 == 'e') {
                return v1.compareTo(v2) <= 0;
            } else {
                return v1.compareTo(v2) < 0;
            }
        case 'g':
            v1 = left.stringVal(input);
            v2 = right.stringVal(input);
            if (op2 == 't' && op3 == 'e') {
                return v1.compareTo(v2) >= 0;
            } else {
                return v1.compareTo(v2) > 0;
            }
        case 'n':
            if (op2 == 'e' && op3 == 'q') {
                v1 = left.stringVal(input);
                v2 = right.stringVal(input);
                return !v1.equals(v2);
            } else {
                throw new IOException("Internal error: Invalid filter operator: " + op);
            }
        default:
            throw new IOException("Internal error: Invalid filter operator: " + op);
        }
    }
    
    public String toString() {
        return "(" + left + " " + op + " " + right + ")";
    }
}
