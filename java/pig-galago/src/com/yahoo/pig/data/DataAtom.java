/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.data;

/**
 * The basic data unit. For now, we represent all atomic data objects as strings
 */
final public class DataAtom implements Datum {
    String stringVal = null;
    Double doubleVal = null;
    public static String EMPTY = "";

    public DataAtom() {
        stringVal = EMPTY;
        doubleVal = Double.POSITIVE_INFINITY;
    }

    public DataAtom(String valIn) {
        setValue(valIn);
    }

    public DataAtom(int valIn) {
        setValue(valIn);
    }

    public DataAtom(long valIn) {
        setValue(valIn);
    }

    public DataAtom(double valIn) {
        setValue(valIn);
    }

    public void setValue(String valIn) {
        stringVal = valIn;
        doubleVal = Double.POSITIVE_INFINITY;
    }

    public void setValue(int valIn) {
        // conversion is cheap, do it now
        doubleVal = new Double(valIn);
        stringVal = Integer.toString(valIn);
    }

    public void setValue(long valIn) {
        // conversion is cheap, do it now
        doubleVal = new Double(valIn);
        stringVal = Long.toString(valIn);
    }

    public void setValue(double valIn) {
        // conversion is cheap, do it now
        doubleVal = new Double(valIn);
        stringVal = Double.toString(valIn);
    }

    public String strval() {
        return stringVal;
    }

    public Double numval() {
        // lazy parse and create the numeric member value
        if (doubleVal == Double.POSITIVE_INFINITY) {
            doubleVal = new Double(stringVal);
        }
        return doubleVal;
    }

    public String toString() {
        return stringVal;
    }

    
    public boolean equals(Datum other) {
        // if (doubleVal != Double.POSITIVE_INFINITY && other != null && other.doubleVal !=
        // Double.POSITIVE_INFINITY) {
        // doubleVal.equals(other.doubleVal);
        // }
 
    	if (other instanceof DataAtom) {
        	return stringVal.equals(((DataAtom)other).stringVal);
        }else{
        	return false;
        }
    }    
    
    public int compareTo(Object other) {
    	if (!(other instanceof DataAtom))
    		return -1;
        DataAtom dOther = (DataAtom) other;
        // if (doubleVal != Double.POSITIVE_INFINITY && other != null && other.doubleVal !=
        // Double.POSITIVE_INFINITY) {
        // return doubleVal.compareTo(other.doubleVal);
        // }
        return stringVal.compareTo(dOther.stringVal);
            
    }

    public int hashCode() {
        return stringVal.hashCode();
    }

}
