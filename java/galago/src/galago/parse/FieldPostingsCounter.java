/*
 * FieldPostingsCounter
 *
 * April 27, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentLengthWordCount;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author trevor
 */

@Verified
@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.types.DocumentLengthWordCount")
public class FieldPostingsCounter extends StandardStep<Document, DocumentLengthWordCount> {
    /// The name of the field to extract from.  If null, it will extract from the whole document.
    String fieldName;
    
    /// The width of the feature we want to extract (1 means single terms, 2 is two-word phrases, etc.)
    int width;
    
    /// The maximum length of a token; all longer tokens are silently dropped.
    int maxLength;
    
    /// Filtered words: if this set exists, only extract these words.
    HashSet<String> filterWords = null;
    
    /**
     * <p>Creates a new FieldPostingsCounter object.</p>
     * 
     * <p>This constructor looks for two parameters in the TupleFlowParameters
     * object.  "fieldName" is the name of the document field to extract from,
     * like 'title' or 'author'.  If it isn't specified, positings will be extracted
     * from the whole document.  "width" is the width of the feature to extract, in 
     * number of terms.  Setting this to 1 (the default) extracts single terms from
     * the document, 2 extracts two-term phrases, etc.</p>
     */
    
    public FieldPostingsCounter( TupleFlowParameters parameters ) throws IOException {
        fieldName = parameters.getXML().get( "field", (String)null );
        width = (int)parameters.getXML().get( "width", 1 );
        maxLength = (int)parameters.getXML().get( "maxLength", 128 );
        
        String filterFilename = parameters.getXML().get( "filter", (String)null );
        if( filterFilename != null ) {
            filterWords = Utility.readFileToStringSet( new File(filterFilename) );
        }
    }
    
    /**
     * Extracts postings of a particular type from the input document.
     * See the constructor documentation for more details.
     */
    
    public void process( Document document ) throws IOException {
        if (fieldName == null) {
            processDocument(document);
        } else {
            processField(document);
        }
    }
    
    /**
     * Counts postings from the whole document (not just within a 
     * particular field).
     */
    
    public void processDocument( Document document ) throws IOException {
        HashMap<String, DocumentLengthWordCount> countObjects = new HashMap<String, DocumentLengthWordCount>();

        if( width > 1 ) {
            countPhrases(countObjects, null, document);
        } else {
            countTerms(countObjects, null, document);
        }

        for( DocumentLengthWordCount wc : countObjects.values() ) {
            assert wc.word != null;
            processor.process(wc);
        }
    }
    
    /**
     * Counts postings from within a particular field (which was specified
     * in the constructor).
     */
    
    public void processField( Document document ) throws IOException {
        HashMap<String, DocumentLengthWordCount> countObjects = new HashMap<String, DocumentLengthWordCount>();
        int length = 0;
        
        for( Tag tag : document.tags ) {
            if( !tag.name.equals(fieldName) )
                continue;
            
            // update the total length for all tags
            length += tag.end - tag.begin;

            if (width > 1) {
                countPhrases(countObjects, tag, document);
            } else {
                countTerms(countObjects, tag, document);
            }
        }
        
        for( DocumentLengthWordCount wc : countObjects.values() ) {
            // since the length may have been incorrect when we created
            // this object, we need to update it here
            assert wc.word != null;
            wc.length = length;
            processor.process(wc);
        }
    }

    private void countTerms(final HashMap<String, DocumentLengthWordCount> countObjects, final Tag tag, final Document document) {
        int begin = 0;
        int end = document.terms.size();
        
        if( tag != null ) {
            begin = tag.begin;
            end = tag.end;
        }
        
        for( int i=begin; i<end; i++ ) {
            String word = document.terms.get(i);
            if (word == null)
                continue;
            
            if (word.length() > maxLength)
                continue;
            
            updateCount(countObjects, document, word);
        }
    }

    private void countPhrases(final HashMap<String, DocumentLengthWordCount> countObjects, final Tag tag, final Document document) {
        int begin = 0;
        int end = document.terms.size() - width + 1;
        
        if( tag != null ) {
            begin = tag.begin;
            end = tag.end - width + 1;
        }
        
        term_loop:
        for( int i=begin; i < end; i++ ) {
            // build a multi-word token
            StringBuilder builder = new StringBuilder();
            String term = document.terms.get(i);
            if (term == null)
                continue;
            
            if (term.length() > maxLength)
                continue;
            
            builder.append( term );
            
            for( int j=1; j<width; j++ ) {
                term = document.terms.get(i+j);
                if (term == null)
                    continue term_loop;
                
                builder.append(' ');
                builder.append( term );
            }
            
            updateCount(countObjects, document, builder.toString());
        }
    }
    
    private void updateCount(final HashMap<String, DocumentLengthWordCount> countObjects, final Document document, final String word) {
        DocumentLengthWordCount wc = countObjects.get(word);
        
        if (filterWords == null || filterWords.contains(word)) {
            if (wc == null) {
                wc = new DocumentLengthWordCount( document.identifier, document.terms.size(), word, 1 );
                countObjects.put(word, wc);
            } else {
                wc.count += 1;
            }
        }
    }
}
