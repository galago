/*
 * PostingsPositionExtractor
 *
 * August 20, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.Utility;
import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentWordPosition;
import java.io.IOException;

/**
 *
 * @author trevor
 */
@Verified
@InputClass(className="galago.parse.Document")
@OutputClass(className="galago.types.DocumentWordPosition")
public class PostingsPositionExtractor extends StandardStep<Document, DocumentWordPosition> {
    public void process(Document object) throws IOException {
        for( int i=0; i<object.terms.size(); i++ ) {
            String term = object.terms.get(i);
            if (term == null)
                continue;
            
            processor.process( new DocumentWordPosition( object.identifier, Utility.makeBytes(term), i ) );
        }
    }
}
