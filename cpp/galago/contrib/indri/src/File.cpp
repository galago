/*==========================================================================
 * Copyright (c) 2004 University of Massachusetts.  All Rights Reserved.
 *
 * Use of the Lemur Toolkit for Language Modeling and Information Retrieval
 * is subject to the terms of the software license set forth in the LICENSE
 * file included with this software, and also available at
 * http://www.lemurproject.org/license.html
 *
 *==========================================================================
*/

//
// File
//
// 15 November 2004 -- tds
//

#include <string>
#include "indri/indri-platform.h"
#include "indri/File.hpp"

#ifndef WIN32
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#endif

#include "lemur/Exception.hpp"
#include "indri/ScopedLock.hpp"  

//
// File constructor
//

indri::file::File::File() :
#ifdef WIN32
  _handle(INVALID_HANDLE_VALUE)
#else
  _handle(-1)
#endif
{
}

//
// File destructor
//

indri::file::File::~File() {
  close();
}

//
// create
// 

bool indri::file::File::create( const std::string& filename ) {
#ifdef WIN32
  _handle = ::CreateFile( filename.c_str(),
                          GENERIC_READ | GENERIC_WRITE,
                          0,
                          NULL,
                          CREATE_ALWAYS,
                          FILE_ATTRIBUTE_NORMAL,
                          NULL );

  if( _handle == INVALID_HANDLE_VALUE )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't create: " + filename );

  return true;
#else 
  _handle = creat( filename.c_str(), 0600 );

  if( _handle < 0 )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't create: " + filename );

  return true;
#endif
}

bool indri::file::File::open( const std::string& filename ) {
#ifdef WIN32
  _handle = ::CreateFile( filename.c_str(),
                          GENERIC_READ | GENERIC_WRITE,
                          0,
                          NULL,
                          OPEN_EXISTING,
                          FILE_ATTRIBUTE_NORMAL,
                          NULL );
  
  if( _handle == INVALID_HANDLE_VALUE )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't open: " + filename );
  
  return true;
#else 
#ifndef O_LARGEFILE
  _handle = ::open( filename.c_str(), O_RDWR );
#else
  _handle = ::open( filename.c_str(), O_LARGEFILE | O_RDWR );
#endif
  
  if( _handle < 0 )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't open: " + filename );

  return true;
#endif
}

bool indri::file::File::openRead( const std::string& filename ) {
#ifdef WIN32
  _handle = ::CreateFile( filename.c_str(),
                          GENERIC_READ,
                          FILE_SHARE_READ,
                          NULL,
                          OPEN_EXISTING,
                          FILE_ATTRIBUTE_NORMAL,
                          NULL );
  
  if( _handle == INVALID_HANDLE_VALUE )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't open for reading: " + filename );

  return true;
#else 
#ifndef O_LARGEFILE
  _handle = ::open( filename.c_str(), O_RDONLY );
#else
  _handle = ::open( filename.c_str(), O_LARGEFILE | O_RDONLY );
#endif
  
  if( _handle < 0 )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't open for reading: " + filename );
  
  return true;
#endif
}

bool indri::file::File::openTemporary( std::string& fileName ) {
#ifdef HAVE_MKSTEMP
  char name[] = "/tmp/indriXXXXXX";
  _handle = ::mkstemp( name );
  fileName = name;

  if( _handle < 0 )
    LEMUR_THROW( LEMUR_IO_ERROR, "Couldn't create temporary file." );
#else
  fileName = tmpnam( NULL );
  open( fileName );
#endif

  return true;
}

size_t indri::file::File::read( void* buffer, UINT64 position, size_t length ) {
#ifdef WIN32
  assert( _handle != INVALID_HANDLE_VALUE );

  indri::thread::ScopedLock sl( _mutex );
  LARGE_INTEGER actual;
  BOOL result;
  LARGE_INTEGER largePosition;
  largePosition.QuadPart = position;

  result = SetFilePointerEx( _handle, largePosition, &actual, FILE_BEGIN ); 
  assert( largePosition.QuadPart == actual.QuadPart );

  if( !result )
    LEMUR_THROW( LEMUR_IO_ERROR, "Failed to seek to some file position" );

  DWORD actualBytes;
  result = ::ReadFile( _handle, buffer, length, &actualBytes, NULL );

  if( !result )
    LEMUR_THROW( LEMUR_IO_ERROR, "Error when reading file" );

  return actualBytes;
#else // POSIX
  assert( _handle > 0 );
  ssize_t result = ::pread( _handle, buffer, length, position ); 

  if( result < 0 )
    LEMUR_THROW( LEMUR_IO_ERROR, "Error when reading file" );

  return size_t(result);
#endif
}

size_t indri::file::File::write( const void* buffer, UINT64 position, size_t length ) {
  if( length == 0 )
    return 0;

#ifdef WIN32
  assert( _handle != INVALID_HANDLE_VALUE );

  indri::thread::ScopedLock sl( _mutex );
  LARGE_INTEGER actual;
  BOOL result;
  LARGE_INTEGER largePosition;
  largePosition.QuadPart = position;

  result = SetFilePointerEx( _handle, largePosition, &actual, FILE_BEGIN ); 
  assert( largePosition.QuadPart == actual.QuadPart );

  if( !result )
    LEMUR_THROW( LEMUR_IO_ERROR, "Failed to seek to some file position" );

  DWORD actualBytes;
  result = ::WriteFile( _handle, buffer, length, &actualBytes, NULL );

  if( !result )
    LEMUR_THROW( LEMUR_IO_ERROR, "Error when writing file" );

  return actualBytes;
#else // POSIX
  assert( _handle > 0 );

  ssize_t result = ::pwrite( _handle, buffer, length, position );

  if( result < 0 )
    LEMUR_THROW( LEMUR_IO_ERROR, "Error when writing file" );

  return size_t(result);
#endif
}

void indri::file::File::close() {
#ifdef WIN32
  if( _handle != INVALID_HANDLE_VALUE ) {
    ::CloseHandle( _handle );
    _handle = INVALID_HANDLE_VALUE;
  }
#else
  if( _handle >= 0 ) {
    ::close( _handle );
    _handle = -1;
  }
#endif
}

UINT64 indri::file::File::size() {
#ifdef WIN32
  if( _handle == INVALID_HANDLE_VALUE )
    return 0;

  LARGE_INTEGER length;
  BOOL result = ::GetFileSizeEx( _handle, &length );

  if( !result )
    LEMUR_THROW( LEMUR_IO_ERROR, "Got an error while trying to retrieve file size" );

  return length.QuadPart;
#else // POSIX
  struct stat stats;
  
  if( _handle == -1 )
    return 0;

  fstat( _handle, &stats );
  return stats.st_size;
#endif
}    

indri::file::FileMapping* indri::file::File::mapRegion( UINT64 position, UINT64 length, bool readOnly, bool wholeFile ) {
#ifdef WIN32       
    if( wholeFile ) {
        position = 0;
        length = 0;
    }
                       
    DWORD createFlags = readOnly ? PAGE_READONLY : PAGE_READWRITE;
    DWORD flags = readOnly ? FILE_MAP_READ : FILE_MAP_ALL_ACCESS;                            
                                                                           
    DWORD offsetHigh = (position >> 32);
    DWORD offsetLow = (position & 0xFFffFFff);
    
    // on 32-bit windows, we may ask for a region that's too big
    SIZE_T maxSizeT = ~( (SIZE_T) 0 );
    if( length > (UINT64)maxSizeT )
        LEMUR_THROW( LEMUR_IO_ERROR, "Requested a file mapping that's too big for your operating system" );
     
    UINT64 end = position + length;
    DWORD endHigh = (end >> 32);
    DWORD endLow = (end & 0xFFffFFff);
    
    HANDLE mappingHandle = CreateFileMapping( _handle, NULL, createFlags, endHigh, endLow, 0 );    
    
    if( mappingHandle == INVALID_HANDLE_VALUE ) {
        LEMUR_THROW( LEMUR_IO_ERROR, "Failed to create file mapping" );
    }
    
    void* map = MapViewOfFileEx( mappingHandle, flags, offsetHigh, offsetLow, (SIZE_T)length ); 
    
    if( map == 0 ) {
        CloseHandle(mappingHandle);
        LEMUR_THROW( LEMUR_IO_ERROR, "Failed to create file mapping" );
    }            
    
    return new FileMapping( mappingHandle, map, length );
#else       
    if( wholeFile ) {  
        position = 0;
        length = size();
    }
 
    size_t maxSizeT = ~( (size_t) 0 );
    if( length > (UINT64)maxSizeT )
        return 0;
      
    int prot = readOnly ? PROT_READ : (PROT_READ|PROT_WRITE);
    void* map = mmap(0, (size_t)length, prot, MAP_SHARED, _handle, position );
    
    if( !map || map == MAP_FAILED ) {
        LEMUR_THROW( LEMUR_IO_ERROR, "Failed to create file mapping" );
    }
   
    return new FileMapping( map, length );
#endif
}

indri::file::FileMapping* indri::file::File::mapFileRead() {
    return mapRegion( 0, 0, true, true );
}                                   

indri::file::FileMapping* indri::file::File::mapFile() {
    return mapRegion( 0, 0, false, true );
}

indri::file::FileMapping* indri::file::File::mapRegion( UINT64 position, UINT64 length ) {
    return mapRegion( position, length, false, false );
}                                               

indri::file::FileMapping* indri::file::File::mapRegionRead( UINT64 position, UINT64 length ) {
    return mapRegion( position, length, true, false );
}
                                            

