/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;

import com.yahoo.pig.AtomEvalFunc;
import com.yahoo.pig.data.DataAtom;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaField;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItem;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class MULTIPLY extends AtomEvalFunc {

    @Override
    public void exec(Tuple input, DataAtom output) throws IOException {
        double v1 = input.getAtomField(0).numval();
        double v2 = input.getAtomField(1).numval();
        output.setValue(v1*v2);
    }
    @Override
    public SchemaItem outputSchema() {
        SchemaItemList schema = new SchemaItemList();
        schema.add(new SchemaField("product"));
        return schema;
    }
}
