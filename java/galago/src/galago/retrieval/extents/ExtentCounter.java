/*
 * ExtentCounter
 *
 * August 17, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license) 
 */

package galago.retrieval.extents;

import galago.util.ExtentArray;
import java.io.IOException;

/**
 *
 * @author trevor
 */
public class ExtentCounter extends CountIterator {
    ExtentIterator iterator;
    int count;
    boolean allowOverlap;
    
    /** Creates a new instance of ExtentCounter */
    public ExtentCounter( ExtentIterator iterator, boolean allowOverlap ) {
        this.iterator = iterator;
        this.allowOverlap = allowOverlap;
    }
    
    private void countExtents() {
        ExtentArray extents = iterator.extents();
        
        if( allowOverlap ) {
            count = extents.getPosition();
        } else {
            int lastEnd = 0;
            count = 0;

            for( int i=0; i<extents.getPosition(); i++ ) {
                if( lastEnd <= extents.getBuffer()[i].begin ) {
                    count++;
                    lastEnd = extents.getBuffer()[i].end;
                }
            }
        }
    }  
    
    @Override
    public boolean skipToDocument( int document ) throws IOException {
        boolean result = iterator.skipToDocument( document );
        countExtents();
        return result;
    }
        
    public void nextDocument() throws IOException {
        iterator.nextDocument();
        countExtents();
    }
    
    public int count() {
        return count;
    }

    public int document() {
        return iterator.document();
    }

    public boolean isDone() {
        return iterator.isDone();
    }
    
    public void reset() throws IOException {
        iterator.reset();
        count = 0;
    }
}
