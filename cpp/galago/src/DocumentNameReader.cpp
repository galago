
//
// DocumentNameReader
//
// 8 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//
                                
#include "DocumentNameReader.hpp"

void DocumentNameReader::getInSlot( std::string& result, const NameSlot& slot, int footerIndex ) const {           
    char buffer[1024];
    int footer = slot.footers[footerIndex-slot.offset];
                                       
    strncpy( buffer, slot.prefix.c_str(), sizeof buffer );
                                                           
    char* start = buffer + slot.prefix.size();
    char* end = start + slot.footerWidth + 1;
        
    *start = '-';
    *end = 0;
	start++;
			         
    for( char* current = end-1; current >= start; current-- ) {
        char digit = (footer % 10) + '0';
        footer /= 10;
        *current = digit;
    }                                          
    
    result = buffer;
}
    
std::string DocumentNameReader::operator[] ( int index ) const {
    return get(index);
}                 
  
std::string DocumentNameReader::get( int index ) const { 
    assert( index >= 0 );
    assert( index < _documentCount );
    
    if( index >= _documentCount )
        return "unknown";
        
    if( index < 0 )
        return "unknown";
    
    int big = _slots.size()-1;
    int small = 0;
    
    while( big-small > 1 ) {
        int middle = small + (big-small)/2;
        
        if( _slots[middle].offset >= index )
            big = middle;
        else
            small = middle;
    }

    const NameSlot& one = _slots[small];
    const NameSlot& two = _slots[big];
    std::string result;
    
    if (two.offset <= index)
        getInSlot(result, two, index);
    else
        getInSlot(result, one, index);
    
    return result;
}                                   
    
void DocumentNameReader::read( const std::string& filename ) {
    int offset = 0;
    
    // mmap the file
    indri::file::File inputFile;                  
    
	inputFile.openRead( filename );
	UINT64 length = inputFile.size();
    indri::file::FileMapping* mapping = inputFile.mapFileRead();
	const UINT8* region = (const UINT8*) mapping->region;
	const UINT8* end = region + length;
	
    while( region < end ) {
        UINT32 prefixLength = UncompressedRead::read_u32( region );
        
		char* prefixData = new char[prefixLength+1];
        memcpy( prefixData, region, prefixLength );
		prefixData[prefixLength] = 0;
		region += prefixLength;
        
        UINT32 footerWidth = UncompressedRead::read_u32( region );
        UINT32 footerCount = UncompressedRead::read_u32( region );
    
        NameSlot slot;
        slot.footerWidth = footerWidth;
        slot.offset = offset;
        slot.prefix = prefixData;
        
        for( int i=0; i<footerCount; i++ ) { 
            UINT32 footer = UncompressedRead::read_u32( region );
            slot.footers.push_back( footer );
        }                                    
        
        _slots.push_back(slot);
        offset += slot.footers.size(); 
        delete[] prefixData;
    }                                 
    
    _documentCount = offset; 
    
    delete mapping;
    inputFile.close();
}  

