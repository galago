// This file was automatically generated with the command: 
//     java galago.tupleflow.TemplateTypeBuilder src/galago/types/WordCount.java WordCount galago.types String:word long:count long:documents order:+word
package galago.types;

import galago.Utility;
import galago.tupleflow.ArrayInput;
import galago.tupleflow.ArrayOutput;
import galago.tupleflow.Order;   
import galago.tupleflow.OrderedWriter;
import galago.tupleflow.Type; 
import galago.tupleflow.ExNihiloSource; 
import galago.tupleflow.TypeReader;
import galago.tupleflow.Step; 
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.ReaderSource;
import galago.tupleflow.Delta;
import java.io.IOException;             
import java.io.EOFException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;   
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collection;

public class WordCount implements Type<WordCount> {
    public String word;
    public long count;
    public long documents; 
    
    public WordCount() {}
    public WordCount( String word, long count, long documents ) {
        this.word = word;
        this.count = count;
        this.documents = documents;
    }  
    
    public String toString() {
            return String.format( "%s,%d,%d",
                                   word, count, documents );
    } 

    public Order<WordCount> getOrder( String... spec ) {
        if ( Arrays.equals( spec, new String[] { "+word" } ) ) {
            return new WordOrder();
        }
        return null;
    } 
      
    public interface Processor extends Step, galago.tupleflow.Processor<WordCount> {
        public void process( WordCount object ) throws IOException;
        public void close() throws IOException;
    }                        
    public interface Source extends Step {
    }
    public static class WordOrder implements Order<WordCount> {
        public int hash( WordCount object ) {
            int h = 0;
            h += Utility.hash(object.word);
            return h;
        } 
        public Comparator<WordCount> greaterThan() {
            return new Comparator<WordCount>() {
                public int compare( WordCount one, WordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return -result;
                }
            };
        }     
        public Comparator<WordCount> lessThan() {
            return new Comparator<WordCount>() {
                public int compare( WordCount one, WordCount two ) {
                    int result = 0;
                    do {
                        result = + Utility.compare( one.word, two.word );
                        if( result != 0 ) break;
                    } while(false);
                    return result;
                }
            };
        }     
        public TypeReader<WordCount> orderedReader( ArrayInput _input ) {
            return new ShreddedReader( _input );
        }    

        public TypeReader<WordCount> orderedReader( ArrayInput _input, int bufferSize ) {
            return new ShreddedReader( _input, bufferSize );
        }    
        public OrderedWriter<WordCount> orderedWriter( ArrayOutput _output ) {
            ShreddedWriter w = new ShreddedWriter( _output );
            return new OrderedWriterClass( w ); 
        }                                    
        public static class OrderedWriterClass extends OrderedWriter< WordCount > {
            WordCount last = null;
            ShreddedWriter shreddedWriter = null; 
            
            public OrderedWriterClass( ShreddedWriter s ) {
                this.shreddedWriter = s;
            }
            
            public void process( WordCount object ) throws IOException {
               boolean processAll = false;
               if ( processAll || last == null || 0 != Utility.compare(object.word, last.word) ) { processAll = true; shreddedWriter.processWord( object.word ); }
               shreddedWriter.processTuple( object.count, object.documents );
               last = object;
            }           
                 
            public void close() throws IOException {
                shreddedWriter.close();
            }
            
            public Class<WordCount> getInputClass() {
                return WordCount.class;
            }
        } 
        public ReaderSource<WordCount> orderedCombiner( Collection<TypeReader<WordCount>> readers, boolean closeOnExit ) {
            ArrayList<ShreddedReader> shreddedReaders = new ArrayList();
            
            for( TypeReader<WordCount> reader : readers ) {
                shreddedReaders.add( (ShreddedReader)reader );
            }
            
            return new ShreddedCombiner( shreddedReaders, closeOnExit );
        }                  
        public WordCount clone( WordCount object ) {
            WordCount result = new WordCount();
            if ( object == null ) return result;
            result.word = object.word; 
            result.count = object.count; 
            result.documents = object.documents; 
            return result;
        }                 
        public Class<WordCount> getOrderedClass() {
            return WordCount.class;
        }                           
        public String[] getOrderSpec() {
            return new String[] {"+word"};
        }
                           
        public interface ShreddedProcessor extends Step {
            public void processWord( String word ) throws IOException;
            public void processTuple( long count, long documents ) throws IOException;
            public void close() throws IOException;
        }    
        public interface ShreddedSource extends Step {
        }                                              
        
        public static class ShreddedWriter implements ShreddedProcessor {
            ArrayOutput output;
            ShreddedBuffer buffer = new ShreddedBuffer();
            String lastWord;
            boolean lastFlush = false;
            
            public ShreddedWriter( ArrayOutput output ) {
                this.output = output;
            }                        
            
            public void close() throws IOException {
                flush();
            }
            
            public void processWord( String word ) {
                lastWord = word;
                buffer.processWord( word );
            }
            public final void processTuple( long count, long documents ) throws IOException {
                if( lastFlush ) {
                    if( buffer.words.size() == 0 ) buffer.processWord( lastWord );
                    lastFlush = false;
                }
                buffer.processTuple( count, documents );
                if( buffer.isFull() )
                    flush();
            }
            public final void flushTuples( int pauseIndex ) throws IOException {
                
                while( buffer.getReadIndex() < pauseIndex ) {
                           
                    output.writeLong( buffer.getCount() );
                    output.writeLong( buffer.getDocuments() );
                    buffer.incrementTuple();
                }
            }  
            public final void flushWord( int pauseIndex ) throws IOException {
                while( buffer.getReadIndex() < pauseIndex ) {
                    int nextPause = buffer.getWordEndIndex();
                    int count = nextPause - buffer.getReadIndex();
                    
                    output.writeString( buffer.getWord() );
                    output.writeInt( count );
                    buffer.incrementWord();
                      
                    flushTuples( nextPause );
                    assert nextPause == buffer.getReadIndex();
                }
            }
            public void flush() throws IOException { 
                flushWord( buffer.getWriteIndex() );
                buffer.reset(); 
                lastFlush = true;
            }                           
        }
        public static class ShreddedBuffer {
            ArrayList<String> words = new ArrayList();
            ArrayList<Integer> wordTupleIdx = new ArrayList();
            int wordReadIdx = 0;
                            
            long[] counts;
            long[] documentss;
            int writeTupleIndex = 0;
            int readTupleIndex = 0;
            int batchSize;

            public ShreddedBuffer( int batchSize ) {
                this.batchSize = batchSize;

                counts = new long[batchSize];
                documentss = new long[batchSize];
            }                              

            public ShreddedBuffer() {    
                this(10000);
            }                                                                                                                    
            
            public void processWord( String word ) {
                words.add( word );
                wordTupleIdx.add( writeTupleIndex );
            }                                      
            public void processTuple( long count, long documents ) {
                assert words.size() > 0;
                counts[writeTupleIndex] = count;
                documentss[writeTupleIndex] = documents;
                writeTupleIndex++;
            }
            public void resetData() {
                words.clear();
                wordTupleIdx.clear();
                writeTupleIndex = 0;
            }                  
                                 
            public void resetRead() {
                readTupleIndex = 0;
                wordReadIdx = 0;
            } 

            public void reset() {
                resetData();
                resetRead();
            } 
            public boolean isFull() {
                return writeTupleIndex >= batchSize;
            }

            public boolean isEmpty() {
                return writeTupleIndex == 0;
            }                          

            public boolean isAtEnd() {
                return readTupleIndex >= writeTupleIndex;
            }           
            public void incrementWord() {
                wordReadIdx++;  
            }                                                                                              

            public void autoIncrementWord() {
                while( readTupleIndex >= getWordEndIndex() && readTupleIndex < writeTupleIndex )
                    wordReadIdx++;
            }                 
            public void incrementTuple() {
                readTupleIndex++;
            }                    
            public int getWordEndIndex() {
                if( (wordReadIdx+1) >= wordTupleIdx.size() )
                    return writeTupleIndex;
                return wordTupleIdx.get(wordReadIdx+1);
            }
            public int getReadIndex() {
                return readTupleIndex;
            }   

            public int getWriteIndex() {
                return writeTupleIndex;
            } 
            public String getWord() {
                assert readTupleIndex < writeTupleIndex;
                assert wordReadIdx < words.size();
                
                return words.get( wordReadIdx );
            }
            public long getCount() {
                assert readTupleIndex < writeTupleIndex;
                return counts[readTupleIndex];
            }                                         
            public long getDocuments() {
                assert readTupleIndex < writeTupleIndex;
                return documentss[readTupleIndex];
            }                                         
            public void copyTuples( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                   output.processTuple( getCount(), getDocuments() );
                   incrementTuple();
                }
            }                                                                           
            public void copyUntilIndexWord( int endIndex, ShreddedProcessor output ) throws IOException {
                while( getReadIndex() < endIndex ) {
                    output.processWord( getWord() );
                    assert getWordEndIndex() <= endIndex;
                    copyTuples( getWordEndIndex(), output );
                    incrementWord();
                }
            }  
            public void copyUntilWord( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                while( !isAtEnd() ) {
                    if( other != null ) {   
                        assert !other.isAtEnd();
                        int c = + Utility.compare( getWord(), other.getWord() );
                    
                        if( c > 0 ) {
                            break;   
                        }
                        
                        output.processWord( getWord() );
                                      
                        copyTuples( getWordEndIndex(), output );
                    } else {
                        output.processWord( getWord() );
                        copyTuples( getWordEndIndex(), output );
                    }
                    incrementWord();  
                    
               
                }
            }
            public void copyUntil( ShreddedBuffer other, ShreddedProcessor output ) throws IOException {
                copyUntilWord( other, output );
            }
            
        }                         
        public static class ShreddedCombiner implements ReaderSource<WordCount>, ShreddedSource {   
            public ShreddedProcessor processor;
            Collection<ShreddedReader> readers;       
            boolean closeOnExit = false;
            boolean uninitialized = true;
            PriorityQueue<ShreddedReader> queue = new PriorityQueue<ShreddedReader>();
            
            public ShreddedCombiner( Collection<ShreddedReader> readers, boolean closeOnExit ) {
                this.readers = readers;                                                       
                this.closeOnExit = closeOnExit;
            }
                                  
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof WordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (WordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<WordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<WordCount> getOutputClass() {
                return WordCount.class;
            }
            
            public void initialize() throws IOException {
                for( ShreddedReader reader : readers ) {
                    reader.fill();                                        
                    
                    if( !reader.getBuffer().isAtEnd() )
                        queue.add(reader);
                }   

                uninitialized = false;
            }

            public void run() throws IOException {
                initialize();
               
                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    ShreddedReader next = null;
                    ShreddedBuffer nextBuffer = null; 
                    
                    assert !top.getBuffer().isAtEnd();
                                                  
                    if( queue.size() > 0 ) {
                        next = queue.peek();
                        nextBuffer = next.getBuffer();
                        assert !nextBuffer.isAtEnd();
                    }
                    
                    top.getBuffer().copyUntil( nextBuffer, processor );
                    if( top.getBuffer().isAtEnd() )
                        top.fill();                 
                        
                    if( !top.getBuffer().isAtEnd() )
                        queue.add(top);
                }              
                
                if( closeOnExit )
                    processor.close();
            }

            public WordCount read() throws IOException {
                if( uninitialized )
                    initialize();

                WordCount result = null;

                while( queue.size() > 0 ) {
                    ShreddedReader top = queue.poll();
                    result = top.read();

                    if( result != null ) {
                        if( top.getBuffer().isAtEnd() )
                            top.fill();

                        queue.offer( top );
                        break;
                    } 
                }

                return result;
            }
        } 
        public static class ShreddedReader implements Step, Comparable<ShreddedReader>, TypeReader<WordCount>, ShreddedSource {      
            public ShreddedProcessor processor;
            ShreddedBuffer buffer;
            WordCount last = new WordCount();         
            long updateWordCount = -1;
            long tupleCount = 0;
            long bufferStartCount = 0;  
            ArrayInput input;
            
            public ShreddedReader( ArrayInput input ) {
                this.input = input; 
                this.buffer = new ShreddedBuffer();
            }                               
            
            public ShreddedReader( ArrayInput input, int bufferSize ) { 
                this.input = input;
                this.buffer = new ShreddedBuffer( bufferSize );
            }
                 
            public final int compareTo( ShreddedReader other ) {
                ShreddedBuffer otherBuffer = other.getBuffer();
                
                if ( buffer.isAtEnd() && otherBuffer.isAtEnd() ) {
                    return 0;                 
                } else if( buffer.isAtEnd() ) {
                    return -1;
                } else if( otherBuffer.isAtEnd() ) {
                    return 1;
                }
                                   
                int result = 0;
                do {
                    result = + Utility.compare( buffer.getWord(), otherBuffer.getWord() );
                    if( result != 0 ) break;
                } while(false);                                             
                
                return result;
            }
            
            public final ShreddedBuffer getBuffer() {
                return buffer;
            }                
            
            public final WordCount read() throws IOException {
                if( buffer.isAtEnd() ) {
                    fill();             
                
                    if( buffer.isAtEnd() ) {
                        return null;
                    }
                }
                      
                assert !buffer.isAtEnd();
                WordCount result = new WordCount();
                
                result.word = buffer.getWord();
                result.count = buffer.getCount();
                result.documents = buffer.getDocuments();
                
                buffer.incrementTuple();
                buffer.autoIncrementWord();
                
                return result;
            }           
            
            public final void fill() throws IOException {
                try {   
                    buffer.reset();
                    
                    if( tupleCount != 0 ) {
                                                      
                        if( updateWordCount - tupleCount > 0 ) {
                            buffer.words.add(last.word);
                            buffer.wordTupleIdx.add( (int) (updateWordCount - tupleCount) );
                        }
                        bufferStartCount = tupleCount;
                    }
                    
                    while( !buffer.isFull() ) {
                        updateWord();
                        buffer.processTuple( input.readLong(), input.readLong() );
                        tupleCount++;
                    }
                } catch( EOFException e ) {}
            }

            public final void updateWord() throws IOException {
                if( updateWordCount > tupleCount )
                    return;
                     
                last.word = input.readString();
                updateWordCount = tupleCount + input.readInt();
                                      
                buffer.processWord( last.word );
            }

            public void run() throws IOException {
                while(true) {
                    fill();
                    
                    if( buffer.isAtEnd() )
                        break;
                    
                    buffer.copyUntil( null, processor );
                }      
                processor.close();
            }
            
            public void setProcessor( Step processor ) throws IncompatibleProcessorException {  
                if( processor instanceof ShreddedProcessor ) {
                    this.processor = new DuplicateEliminator( (ShreddedProcessor) processor );
                } else if( processor instanceof WordCount.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (WordCount.Processor) processor ) );
                } else if( processor instanceof galago.tupleflow.Processor ) {
                    this.processor = new DuplicateEliminator( new TupleUnshredder( (galago.tupleflow.Processor<WordCount>) processor ) );
                } else {
                    throw new IncompatibleProcessorException( processor.getClass().getName() + " is not supported by " + this.getClass().getName() );                                                                       
                }
            }                                
            
            public Class<WordCount> getOutputClass() {
                return WordCount.class;
            }                
        }
        
        public static class DuplicateEliminator implements ShreddedProcessor {
            public ShreddedProcessor processor;
            WordCount last = new WordCount();
            boolean wordProcess = true;
                                           
            public DuplicateEliminator() {}
            public DuplicateEliminator( ShreddedProcessor processor ) {
                this.processor = processor;
            }
            
            public void setShreddedProcessor( ShreddedProcessor processor ) {
                this.processor = processor;
            }

            public void processWord( String word ) throws IOException {  
                if( wordProcess || Utility.compare( word, last.word ) != 0 ) {
                    last.word = word;
                    processor.processWord( word );
                    wordProcess = false;
                }
            }  
            
            public void resetWord() {
                 wordProcess = true;
            }                                                
                               
            public void processTuple( long count, long documents ) throws IOException {
                processor.processTuple( count, documents );
            } 
            
            public void close() throws IOException {
                processor.close();
            }                    
        }
        public static class TupleUnshredder implements ShreddedProcessor {
            WordCount last = new WordCount();
            public galago.tupleflow.Processor<WordCount> processor;                               
            
            public TupleUnshredder( WordCount.Processor processor ) {
                this.processor = processor;
            }         
            
            public TupleUnshredder( galago.tupleflow.Processor<WordCount> processor ) {
                this.processor = processor;
            }
            
            public WordCount clone( WordCount object ) {
                WordCount result = new WordCount();
                if ( object == null ) return result;
                result.word = object.word; 
                result.count = object.count; 
                result.documents = object.documents; 
                return result;
            }                 
            
            public void processWord( String word ) throws IOException {
                last.word = word;
            }   
                
            
            public void processTuple( long count, long documents ) throws IOException {
                last.count = count;
                last.documents = documents;
                processor.process( clone(last) );
            }               
            
            public void close() throws IOException {
                processor.close();
            }
        }     
        public static class TupleShredder implements Processor {
            WordCount last = new WordCount();
            public ShreddedProcessor processor;
            
            public TupleShredder( ShreddedProcessor processor ) {
                this.processor = processor;
            }                              
            
            public WordCount clone( WordCount object ) {
                WordCount result = new WordCount();
                if ( object == null ) return result;
                result.word = object.word; 
                result.count = object.count; 
                result.documents = object.documents; 
                return result;
            }                 
            
            public void process( WordCount object ) throws IOException {                                                                                                                                                   
                boolean processAll = false;
                if( last == null || Utility.compare( last.word, object.word ) != 0 || processAll ) { processor.processWord( object.word ); processAll = true; }
                processor.processTuple( object.count, object.documents );                                         
            }
                          
            public Class<WordCount> getInputClass() {
                return WordCount.class;
            }
            
            public void close() throws IOException {
                processor.close();
            }                     
        }
    } 
}    