
//
// DocumentOrderedBinnedListReader
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_DOCUMENTORDEREDBINNEDLISTREADER_HPP
#define GALAGO_DOCUMENTORDEREDBINNEDLISTREADER_HPP

#include "DocumentOrderedBinnedIterator.hpp"
#include "IndexReader.hpp"
#include "indri/Parameters.hpp"
#include <string>

class DocumentOrderedBinnedListReader {
private:
    indri::api::Parameters _manifest;
    IndexReader _reader;

public:
    void openRead( const std::string& path );
    void close();
    DocumentOrderedBinnedIterator* getTerm( const std::string& term ) const;
};

#endif // GALAGO_DOCUMENTORDEREDBINNEDLISTREADER_HPP

