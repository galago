/*
 * UnorderedWindowIteratorTest.java
 * JUnit based test
 *
 * Created on September 13, 2007, 7:43 PM
 */

package galago.retrieval.extents;

import galago.tupleflow.Parameters;
import java.io.IOException;
import junit.framework.*;
import galago.util.ExtentArray;
import java.util.ArrayList;

/**
 *
 * @author trevor
 */
public class UnorderedWindowIteratorTest extends TestCase {
    
    public UnorderedWindowIteratorTest(String testName) {
        super(testName);
    }

    public void testPhrase() throws IOException {
        System.out.println("testPhrase");
        
        int[][] dataOne = { { 1, 3 } };
        int[][] dataTwo = { { 1, 4 } };
        FakeExtentIterator one = new FakeExtentIterator( dataOne );
        FakeExtentIterator two = new FakeExtentIterator( dataTwo );
        ArrayList<DocumentDataIterator> iters = new ArrayList<DocumentDataIterator>();
        
        iters.add( one );
        iters.add( two );

        Parameters twoParam = new Parameters();
        twoParam.add( "width", "2" );
        UnorderedWindowIterator instance = new UnorderedWindowIterator( twoParam, iters );
        ExtentArray array = instance.extents();
        assertFalse( instance.isDone() );

        assertEquals( array.getPosition(), 1 );
        assertEquals( array.getBuffer()[0].document, 1 );
        assertEquals( array.getBuffer()[0].begin, 3 );
        assertEquals( array.getBuffer()[0].end, 5 );
        
        instance.nextDocument();
        assertTrue( instance.isDone() );
    }

    public void testUnordered() throws IOException {
        System.out.println("testUnordered");
        
        int[][] dataOne = { { 1, 3 } };
        int[][] dataTwo = { { 1, 4 } };
        FakeExtentIterator one = new FakeExtentIterator( dataOne );
        FakeExtentIterator two = new FakeExtentIterator( dataTwo );
        ArrayList<DocumentDataIterator> iters = new ArrayList<DocumentDataIterator>();
        
        iters.add( two );
        iters.add( one );
        
        Parameters twoParam = new Parameters();
        twoParam.add( "width", "2" );
        UnorderedWindowIterator instance = new UnorderedWindowIterator( twoParam, iters );
        ExtentArray array = instance.extents();
        assertFalse( instance.isDone() );
        
        assertEquals( array.getPosition(), 1 );
        assertEquals( array.getBuffer()[0].document, 1 );
        assertEquals( array.getBuffer()[0].begin, 3 );
        assertEquals( array.getBuffer()[0].end, 5 );
        
        instance.nextDocument();
        assertTrue( instance.isDone() );
    }
    
    public void testDifferentDocuments() throws IOException {
        System.out.println("testDifferentDocuments");
        
        int[][] dataOne = { { 2, 3 } };
        int[][] dataTwo = { { 1, 4 } };
        FakeExtentIterator one = new FakeExtentIterator( dataOne );
        FakeExtentIterator two = new FakeExtentIterator( dataTwo );
        ArrayList<DocumentDataIterator> iters = new ArrayList<DocumentDataIterator>();
        
        iters.add( two );
        iters.add( one );
        
        Parameters twoParam = new Parameters();
        twoParam.add( "width", "2" );

        UnorderedWindowIterator instance = new UnorderedWindowIterator( twoParam, iters );
        ExtentArray array = instance.extents();
        assertEquals( 0, array.getPosition() );
        assertTrue( instance.isDone() );
    }
    
    public void testMultipleDocuments() throws IOException {
        System.out.println("testMultipleDocuments");
        
        int[][] dataOne = { { 1, 3 }, { 2, 5 }, { 5, 11 }  };
        int[][] dataTwo = { { 1, 4 }, { 3, 8 }, { 5, 9 } };
        FakeExtentIterator one = new FakeExtentIterator( dataOne );
        FakeExtentIterator two = new FakeExtentIterator( dataTwo );
        ArrayList<DocumentDataIterator> iters = new ArrayList<DocumentDataIterator>();
        
        iters.add( two );
        iters.add( one );
        
        Parameters fiveParam = new Parameters();
        fiveParam.add( "width", "5" );

        UnorderedWindowIterator instance = new UnorderedWindowIterator( fiveParam, iters );
        ExtentArray array = instance.extents();
        assertFalse( instance.isDone() );
        
        assertEquals( array.getPosition(), 1 );
        assertEquals( array.getBuffer()[0].document, 1 );
        assertEquals( array.getBuffer()[0].begin, 3 );
        assertEquals( array.getBuffer()[0].end, 5 );
        
        instance.nextDocument();
        assertFalse( instance.isDone() );
        
        assertEquals( array.getPosition(), 1 );
        assertEquals( array.getBuffer()[0].document, 5 );
        assertEquals( array.getBuffer()[0].begin, 9 );
        assertEquals( array.getBuffer()[0].end, 12 );
        
        instance.nextDocument();
        assertTrue( instance.isDone() );
    }
}
