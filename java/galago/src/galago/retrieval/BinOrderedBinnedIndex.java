
//
// BinOrderedBinnedIndex.java
//
// 28 November 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.retrieval;

import galago.index.DocumentNameReader;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import galago.tupleflow.Parameters;
import galago.tupleflow.Processor;
import galago.parse.Document;
import galago.retrieval.BinOrderedBinnedIndexReader.BinnedListIterator;

/**
 * Provides access to binned indexes build by Indexer.
 *
 * @author Trevor Strohman
 */
public class BinOrderedBinnedIndex {
    DocumentNameReader documents;
    Parameters parameters;
    HashMap<String, BinOrderedBinnedIndexReader> features;
    int blockSize;
    
    /** Creates a new instance of BinnedIndex */
    public BinOrderedBinnedIndex( String pathname ) throws IOException {
        String documentPath = pathname + File.separator + "documentNames";
        documents = new DocumentNameReader( documentPath );
        String parametersPath = pathname + File.separator + "manifest";
        parameters = new Parameters( new File(parametersPath) );
        features = new HashMap<String, BinOrderedBinnedIndexReader>();
        
        File directory = new File(pathname);
        
        for( File subdirectory : directory.listFiles() ) {
            if( subdirectory.isFile() )
                continue;
            
            String name = subdirectory.getName();
            BinOrderedBinnedIndexReader reader = new BinOrderedBinnedIndexReader( subdirectory );
            features.put(name, reader);
        }
    }
    
    public int bins() {
        return (int)parameters.get("bins", 8);
    }
    
    public Set<String> getFeatureNames() {
        return features.keySet();
    }
    
    public BinnedListIterator getIterator( String feature ) throws IOException {
        BinOrderedBinnedIndexReader index = features.get(feature);
        
        if( index == null )
            return null;
        
        return index.getIterator();
    }
    
    public BinnedListIterator getTerm( String term, String feature ) throws IOException {
        if( feature == null )
            feature = "default";
        
        BinOrderedBinnedIndexReader index = features.get(feature);
        
        if( index == null )
            return null;
        
        return index.getTerm(term);
    }
    
    public String getDocument( int index ) throws IOException {
        return documents.get(index);
    }
    
    public List< Processor<Document> > transformations( String feature ) {
        if( feature == null )
            feature = "default";
        
        BinOrderedBinnedIndexReader index = features.get(feature);
        
        if( index == null )
            return null;
        
        return index.transformations();
    }
    
    public boolean needsQueryWeights() {
        return features.get("default").needsQueryWeights();
    }
    
    public long maximumDocumentCount() {
        return features.get("default").maximumDocumentCount();
    }

    void close() throws IOException {
        for( BinOrderedBinnedIndexReader index : features.values() ) {
            index.close();
        }
    }
}
