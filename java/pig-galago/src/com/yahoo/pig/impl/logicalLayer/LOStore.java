/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.util.List;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.schema.SchemaItemList;

public class LOStore extends LogicalOperator {
    public String    filename;
    public String sf;
    public boolean append;

    public LOStore(PigContext pigContext, LogicalOperator input, String filenameIn, String funcIn, boolean append) {
    	super(pigContext);
        inputs = new LogicalOperator[1];
        inputs[0] = input;
        filename = filenameIn;
        sf = funcIn;
        this.append = append;
        getOutputType();
    }

    public String name() {
        return "Store";
    }

    public SchemaItemList outputSchema() {
        throw new RuntimeException("Internal error: Asking for schema of a store operator.");
    }

    public int getOutputType() {
        switch (inputs[0].getOutputType()) {
        case FIXED:
            return FIXED;
        case MONOTONE:
            return MONOTONE;
        default:
            throw new RuntimeException("Illegal input type for store operator");
        }
    }

    public List<String> getFuncs() {
        List<String> funcs = super.getFuncs();
        funcs.add(sf);
        return funcs;
    }
}
