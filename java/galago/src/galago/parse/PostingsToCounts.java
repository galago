/*
 * PostingsToCounts
 *
 * May 9, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import galago.tupleflow.InputClass;
import galago.tupleflow.OutputClass;
import galago.tupleflow.StandardStep;
import galago.tupleflow.execution.Verified;
import galago.types.DocumentLengthWordCount;
import galago.types.WordCount;
import java.io.IOException;

/**
 *
 * @author trevor
 */

@InputClass(className="galago.types.DocumentLengthWordCount")
@OutputClass(className="galago.types.WordCount")
@Verified
public class PostingsToCounts extends StandardStep<DocumentLengthWordCount, WordCount> {
    public void process( DocumentLengthWordCount object ) throws IOException {
        processor.process( new WordCount( object.word, object.count, 1 ) );
    }
    
    public Class<DocumentLengthWordCount> getInputClass() {
        return DocumentLengthWordCount.class;
    }
    
    public Class<WordCount> getOutputClass() {
        return WordCount.class;
    }
}
