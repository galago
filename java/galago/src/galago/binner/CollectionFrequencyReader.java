/*
 * CollectionFrequencyReader
 *
 * December 11, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.binner;

import java.io.IOException;
import java.util.HashMap;
import galago.tupleflow.Processor;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.ErrorHandler;
import galago.types.WordCount;
import galago.types.XMLFragment;

public class CollectionFrequencyReader implements Processor<WordCount> {
    HashMap<String, Double> collectionFrequencies = new HashMap<String, Double>();
    double oneOverTotalCount;
    
    public void process(WordCount count) {
        if( !collectionFrequencies.containsKey(count.word) )
            collectionFrequencies.put( count.word, new Double(count.count * oneOverTotalCount) );
    }
    
    public void close() {}
    
    public void setTotalCount( long totalCount ) {
        this.oneOverTotalCount = 1.0 / totalCount;
    }
    
    public HashMap<String, Double> getCF() {
        return collectionFrequencies;
    }
    
    @SuppressWarnings("unchecked")
    static HashMap<String, Double> read(TupleFlowParameters parameters) throws IOException {
        TypeReader<WordCount> lmReader = parameters.getTypeReader( "lm" );
        TypeReader<WordCount> countsReader = parameters.getTypeReader( "counts" );
        TypeReader<XMLFragment> collectionLength = parameters.getTypeReader( "collectionLength" );
        
        CollectionFrequencyReader cfReader = new CollectionFrequencyReader();
        XMLFragment fragment = collectionLength.read();
        long totalCount = Long.parseLong(fragment.innerText);
        
        cfReader.setTotalCount( totalCount );
        
        filteredRead( lmReader, countsReader, cfReader );
        return cfReader.getCF();
    }

    private static void filteredRead(final TypeReader<WordCount> lmReader, final TypeReader<WordCount> countsReader, final CollectionFrequencyReader cfReader) throws IOException {
        WordCount lmCount = lmReader.read();
        WordCount cCount = countsReader.read();
        
        while( lmCount != null && cCount != null ) {
            int comparison = lmCount.word.compareTo(cCount.word);
            
            if( comparison == 0 ) {
                cfReader.process( lmCount );
                
                lmCount = lmReader.read();
                cCount = countsReader.read();
            } else if( comparison < 0 ) {
                lmCount = lmReader.read();
            } else {
                // cCount < lmCount
                cfReader.process( cCount );
                cCount = countsReader.read();
            }
        }
    }
    
    public Class<WordCount> getInputClass( TupleFlowParameters parameters ) {
        return WordCount.class;
    }
    
    public static void verify( TupleFlowParameters parameters, ErrorHandler handler ) {
        if( !parameters.readerExists( "counts", "galago.types.WordCount", new String[] { "+word" } ) )
            handler.addError( "CollectionFrequencyReader requires a 'counts' parameter, sorted by word." );
        if( !parameters.readerExists( "lm", "galago.types.WordCount", new String[] { "+word" } ) )
            handler.addError( "CollectionFrequencyReader requires a 'lm' parameter, sorted by word." );
    }
}