/*
 * JoinTuples
 *
 * September 3, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package com.yahoo.pig.impl.galago;

import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.data.BagFactory;
import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.DataCollector;
import com.yahoo.pig.data.Datum;
import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.eval.EvalSpecPipe;
import com.yahoo.pig.impl.eval.groupby.GroupBySpec;
import galago.tupleflow.ExNihiloSource;
import galago.tupleflow.IncompatibleProcessorException;
import galago.tupleflow.Linkage;
import galago.tupleflow.OutputClass;
import galago.tupleflow.Parameters.Value;
import galago.tupleflow.Processor;
import galago.tupleflow.Step;
import galago.tupleflow.TupleFlowParameters;
import galago.tupleflow.TypeReader;
import galago.tupleflow.execution.Verified;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.File;

/**
 *
 * @author trevor
 */
@Verified
@OutputClass(className = "com.yahoo.pig.impl.galago.Tuple")
public class JoinTuples implements ExNihiloSource<Tuple> {

    TypeReader[] readers;
    public Processor<Tuple> processor;
    PigContext context;
    DataCollector evalPipe;
    ArrayList<Input> inputs = new ArrayList<Input>();

    public class Input {

        public String name;
        public GroupBySpec group;
        public TypeReader<Tuple> reader;
        public Tuple top;
        public DataBag bag;
        public boolean isInner;

        public void read() throws IOException {
            top = reader.read();
        }
    }

    @SuppressWarnings("unchecked")
    public JoinTuples(TupleFlowParameters parameters) throws IOException {
        context = new PigContext(ExecType.GALAGO);
        // BUGBUG: probably need to freeze-dry the context
        String evalSpec = parameters.getXML().get("eval", "");

        List<Value> inputs = parameters.getXML().list("input");
        for (int i = 0; i < inputs.size(); i++) {
            Input input = new Input();
            input.name = inputs.get(i).get("name");
            String groupSpec = inputs.get(i).get("group");
            input.group = new GroupBySpec(context, groupSpec);
            input.isInner = input.group.isInner;
            input.top = null;
            input.bag = BagFactory.getInstance().getNewBag();
            input.reader = parameters.getTypeReader(input.name);
            this.inputs.add(input);
        }

        evalPipe = (new EvalSpecPipe(context, evalSpec)).collector(new JoinCollector());
    }

    public class JoinCollector extends DataCollector {

        public void add(com.yahoo.pig.data.Tuple t) throws IOException {
            if (t == null) {
                return;
            }
            Tuple g = new Tuple();
            g.copyFrom(t);
            processor.process(g);
        }
    }

    public boolean isDone() {
        for (Input input : inputs) {
            if (input.top != null) {
                return false;
            }
        }

        return true;
    }
    
    private void initializeTemporaryDirectory() {
        String temporary = System.getProperty( "java.io.tmpdir" );
        BagFactory.init( new File(temporary) );
    }

    public void run() throws IOException {
        for (Input input : inputs) {
            input.read();
        }

        initializeTemporaryDirectory();
        
        while (!isDone()) {
            // find the smallest tuple
            Datum groupName = null;

            for (Input input : inputs) {
                if (input.top != null) {
                    Datum first = input.top.getField(0);

                    if (groupName == null) {
                        groupName = first;
                    } else if (groupName.compareTo(first) < 0) {
                        groupName = first;
                    }
                }
            }

            Tuple result = new Tuple(1 + inputs.size());
            result.setField(0, groupName);

            // now, add to the tuple
            for (int i = 0; i < inputs.size(); i++) {
                Input input = inputs.get(i);
                DataBag bag = BagFactory.getInstance().getNewBag();

                while (input.top != null && input.top.getField(0).equals(groupName)) {
                    bag.add((com.yahoo.pig.data.Tuple) input.top.getField(1));
                    input.read();
                }

                input.bag = bag;
                result.setField(i + 1, bag);
            }

            boolean usableTuple = true;

            for (int i = 0; i < inputs.size(); i++) {
                Input input = inputs.get(i);

                if (input.isInner && input.bag.isEmpty()) {
                    usableTuple = false;
                }
            }

            if (usableTuple) {
                evalPipe.add(result);
                evalPipe.add(null);
            }
        }

        processor.close();
    }

    public void setProcessor(Step step) throws IncompatibleProcessorException {
        Linkage.link(this, step);
    }
}
