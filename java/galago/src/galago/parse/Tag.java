
/*
 * Tag
 *
 * December 11, 2006 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.parse;

import java.util.Map;
import java.util.Map.Entry;

public class Tag implements Comparable<Tag> {
    public Tag( String name, Map<String, String> attributes, int begin, int end ) {
        this.name = name;
        this.attributes = attributes;
        this.begin = begin;
        this.end = end;
    }

    public int compareTo( Tag other ) {
        int deltaBegin = begin - other.begin;

        if( deltaBegin == 0 )
            return other.end - end;

        return deltaBegin;
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append( "<" );
        builder.append( name );
        
        for( Entry<String, String> entry : attributes.entrySet() ) {
            builder.append( ' ' );
            builder.append( entry.getKey() );
            builder.append( '=' );
            builder.append( '"' );
            builder.append( entry.getValue() );
            builder.append( '"' );
        }
        
        builder.append( '>' );
        return builder.toString();
    }

    public String name;
    public Map<String, String> attributes;

    public int begin;
    public int end;
}
