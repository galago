/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.io.Serializable;

import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.LogicalOperator;

abstract public class PhysicalOperator implements Serializable {

    public PhysicalOperator[] inputs = null;
    int outputType;
    
    /**
     * 
     * This constructor should go away when we eventually implement CQ even for the mapreduce exec type 
     */
    public PhysicalOperator(){
    	outputType = LogicalOperator.FIXED;
    }
   
    
    public PhysicalOperator(int outputType){
    	this.outputType = outputType;
    }
    
    public boolean open(boolean continueFromLast) throws IOException {
        // call open() on all inputs
        if (inputs != null) {
            for (int i = 0; i < inputs.length; i++) {
                if (!inputs[i].open(continueFromLast))
                    return false;
            }
        }
        return true;
    }

    abstract public Tuple getNext() throws IOException;

    public void close() throws IOException {
        // call close() on all inputs
        if (inputs != null)
            for (int i = 0; i < inputs.length; i++)
                inputs[i].close();
    }

    public int getOutputType(){
    	return outputType;
    }
}
