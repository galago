//
// DocumentOrderedBinnedMaxScoreRetrieval
//
// 5 November 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_DOCUMENTORDEREDBINNEDMAXSCORERETRIEVAL_HPP
#define GALAGO_DOCUMENTORDEREDBINNEDMAXSCORERETRIEVAL_HPP

#include "DocumentOrderedBinnedIndex.hpp"
#include "DocumentOrderedBinnedIterator.hpp"
#include "indri/ScoredExtentResult.hpp"
#include "Query.hpp"
#include "Retrieval.hpp"
#include <vector>
#include <queue>

class DocumentOrderedBinnedMaxScoreRetrieval : public Retrieval {
private:
    std::vector<DocumentOrderedBinnedIterator*> getIterators( const std::vector<QueryTerm>& terms );

    bool _conjunctive;
    bool _scoreSkipping;
    bool _skipping;
    DocumentOrderedBinnedIndex _index;

public:
    DocumentOrderedBinnedMaxScoreRetrieval( bool conjunctive, bool scoreSkipping, bool skipping ) :
        _conjunctive(conjunctive),
        _scoreSkipping(scoreSkipping),
        _skipping(skipping)
    {
    }

    std::vector<indri::api::ScoredExtentResult> runQuery(const std::vector<QueryTerm>& terms,
                                                      int requested,
                                                      int threshold);
    std::string getDocument( int document );
    void openRead( const std::string& path );
    void close();
};

#endif	// GALAGO_DOCUMENTORDEREDBINNEDMAXSCORERETRIEVAL_HPP 

