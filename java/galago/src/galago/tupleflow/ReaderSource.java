
//
// ReaderSource.java
//
// 7 Sep 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow;

/**
 *
 * @author trevor
 */
public interface ReaderSource<T> extends TypeReader<T>, ExNihiloSource<T> {
}
