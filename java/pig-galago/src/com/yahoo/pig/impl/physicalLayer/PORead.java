/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

import java.io.IOException;
import java.util.Iterator;

import com.yahoo.pig.data.DataBag;
import com.yahoo.pig.data.Tuple;

public class PORead extends PhysicalOperator {
    DataBag             bag;
    Iterator<Tuple> it;

    public PORead(DataBag bagIn, int outputType) {
    	super(outputType);
        inputs = new PhysicalOperator[0];

        bag = bagIn;

        it = null;
    }

    public boolean open(boolean continueFromLast) throws IOException {
    	if (continueFromLast){
    		throw new RuntimeException("LOReads should not occur in continuous plans");
    	}
        it = bag.content();

        return true;
    }

    public Tuple getNext() throws IOException {
        if (it.hasNext())
            return it.next();
        else
            return null;
    }

}
