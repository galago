/*
 * FeatureFactory
 *
 * September 11, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval.extents;

import galago.retrieval.query.Node;
import galago.tupleflow.Parameters;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author trevor
 */
public class FeatureFactory {
    static String[][] sOperatorLookup = {
        { "galago.retrieval.extents.ScoreCombinationIterator",   "combine"   },
        { "galago.retrieval.extents.SynonymIterator",            "syn"       },
        { "galago.retrieval.extents.SynonymIterator",            "synonym"   },
        { "galago.retrieval.extents.ExtentInsideIterator",       "inside"    },
        { "galago.retrieval.extents.OrderedWindowIterator",      "ordered"   },
        { "galago.retrieval.extents.OrderedWindowIterator",      "od"        },
        { "galago.retrieval.extents.UnorderedWindowIterator",    "unordered" },
        { "galago.retrieval.extents.UnorderedWindowIterator",    "uw"        },
        { "galago.retrieval.extents.ScaleIterator",              "scale"     }
    };
    
    static String[][] sFeatureLookup = {
        { "galago.scoring.DirichletScorer", "dirichlet" },
        { "galago.scoring.BM25Smoother",    "bm25"      }
    };
    
    HashMap<String, String> featureLookup;
    HashMap<String, String> operatorLookup;
    
    public FeatureFactory() {
        operatorLookup = new HashMap<String, String>();
        featureLookup = new HashMap<String, String>();
        
        for( String[] item : sFeatureLookup ) {
            featureLookup.put( item[1], item[0] );
        }
        
        for( String[] item : sOperatorLookup ) {
            operatorLookup.put( item[1], item[0] );
        }
    }

    public String getClassName(Node node) throws Exception {
        String operator = node.getOperator();
        
        if( operator.equals("feature") )
            return getFeatureClassName(node.getParameters());
        
        String className = operatorLookup.get(operator);
        
        if( className == null )
            throw new IllegalArgumentException( "Unknown operator name: #" + operator );

        return className;
    }
    
    public String getFeatureClassName(Parameters parameters) throws Exception {
        if (parameters.containsKey("class")) {
            return parameters.get("class");
        }

        String name = parameters.get("name", parameters.get("default", (String) null));

        if (name == null) {
            throw new Exception("Didn't find 'class', 'name', or 'default' parameter in this feature description.");
        }

        String className = featureLookup.get( name );

        if (className == null) {
            throw new Exception("Couldn't find a class for the feature named " + name + ".");
        }

        return className;
    }

    @SuppressWarnings("unchecked")
    public Class<DocumentDataIterator> getClass( Node node ) throws Exception {
        String className = getClassName(node);
        Class c = Class.forName(className);

        if (DocumentDataIterator.class.isAssignableFrom(c)) {
            return (Class<DocumentDataIterator>) c;
        } else {
            throw new Exception("Found a class, but it's not a DocumentDataIterator: " + className);
        }
    }
    
    public String getInputIteratorType( Node node ) throws Exception {
        Class c = getClass( node );
        @SuppressWarnings("unchecked")
        InputIterator annotation = (InputIterator) c.getAnnotation( InputIterator.class );
        
        if( annotation == null )
            throw new Exception("Class '" + c.toString() + "' has no InputIterator annotation, so it cannot be typechecked.");
        
        return annotation.className();
    }

    public String[] getRequiredStatistics(Node node) throws Exception {
        Class<DocumentDataIterator> c = getClass(node);
        RequiredStatistics s = c.getAnnotation(RequiredStatistics.class);

        if( s == null )
            return new String[0];

        return s.statistics();
    }
    
    /**
     * Determines whether this feature node would turn into a score node when instantiated,
     * or into some kind of extent node (counts, extents, etc.).
     */
    
    public boolean isScoreNode( Node node ) throws Exception {
        Class c = getClass( node );
        return ScoreIterator.class.isAssignableFrom(c);
    }
    
    /**
     * Given a query node, generates the corresponding iterator object that can be used
     * for structured retrieval.  This method just calls getClass() on the node,
     * then instantiates the resulting class.
     * 
     * If the class returned by geClass() is a ScoringFunction, it must contain
     * a constructor that takes a single Parameters object.  If the class returned by 
     * getFeatureClass() is some kind of DocumentDataIterator (either a ScoreIterator,
     * ExtentIterator or CountIterator), it must take a Parameters object and an
     * ArrayList of DocumentDataIterators as parameters.
     */

    public DocumentDataIterator getObject(Node node, ArrayList<DocumentDataIterator> childIterators) throws Exception {
        Class<DocumentDataIterator> iteratorClass = getClass( node );
        Constructor constructor = iteratorClass.getConstructor( Parameters.class, ArrayList.class );
        return (DocumentDataIterator) constructor.newInstance( node.getParameters(), childIterators );
    }
}
