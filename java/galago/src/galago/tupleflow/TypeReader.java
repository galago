
//
// TypeReader.java
//
// 3 October 2006 -- Trevor Strohman
//
// BSD License (http://galagosearch.org)
//


package galago.tupleflow;

import java.io.IOException;

/**
 *
 * @author trevor
 */
public interface TypeReader<T> extends ExNihiloSource<T> {
    T read() throws IOException;
    void run() throws IOException;
}
