/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.logicalLayer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Map;

import com.yahoo.pig.impl.PigContext;
import com.yahoo.pig.impl.logicalLayer.parser.ParseException;
import com.yahoo.pig.impl.logicalLayer.parser.QueryParser;
import com.yahoo.pig.impl.physicalLayer.IntermedResult;

/**
 * PlanBuilder class outputs a logical plan given a query String and set of ValidIDs
 * 
 */
public class LogicalPlanBuilder {
	public static ClassLoader classloader = LogicalPlanBuilder.class.getClassLoader();
	private PigContext pigContext;
    public LogicalPlanBuilder(PigContext pigContext) {
		this.pigContext = pigContext;
	}

	public LogicalPlan parse(String query, Map<String, IntermedResult> aliases)
		throws IOException, ParseException {		
		ByteArrayInputStream in = new ByteArrayInputStream(query.getBytes());		
		QueryParser parser = new QueryParser(in, pigContext, aliases);
		return parser.Parse();		
	}
}
