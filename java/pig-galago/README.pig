Pig runs on Java 1.5. This version of Pig was designed for Hadoop 12.1.

RUNNING PIG

Pig is designed to run against an Hadoop cluster. The pig.jar file includes
the ability to startup such a cluster if needed (see next section). Important
configuration parameters (such as the ip and port of the jobtracker and
name node) for Hadoop clusters are contained in a file called
hadoop-site.xml. You will need to add your site's hadoop-site.xml to pig.jar
for it to communicate properly with your hadoop cluster. hadoop-site.xml
is added to pig.jar using zip: zip pig.jar hadoop-site.xml.

Run "java -jar pig.jar -" to see if Pig is running correctly. If it is,
you should get a "grunt>" prompt. See the documentation (distributed
separately) for more details

RUNNING HADOOP

The pig.jar file includes everything needed to startup an Hadoop cluster.
You will need to consult the Hadoop website for details, but briefly:

1) "java -jar pig.jar -H setup" will generate an hadoop-site.xml.
2) "java -jar pig.jar -H namenode -format" will format a namenode directory.
3) "java -jar pig.jar -H conf" will output the cluster configuration. This
   information is useful to write startup scripts to startup the namenode,
   jobtracker, datanode, tasktrackers on the proper nodes. (My scripts
   look at hostname of fs.default.name and mapred.job.tracker and compare with
   hostname of the machine that the script is running on. If it matches,
   I startup the name node or jobtracker, otherwise I start a datanode and
   tasktracker.)
4) "java -jar pig.jar -H namenode" starts the namenode; 
   "java -jar pig.jar -H datanode" starts the datanode;
   "java -jar pig.jar -H jobtracker" starts the jobtracker;
   "java -jar pig.jar -H tasktracker" starts the tasktracker.

BUILDING PIG 

To build Pig you will need to build the hadoop-exe target of Hadoop.
You must apply the patch that is attached to HADOOP-435 in the Hadoop issue
tracker. Put the resulting hadoop.jar file in the lib subdirectory of
hadoop. You will also need libraries from the following packages
in the lib subdirectory:

bzip2: http://www.kohsuke.org/bzip2/:Apache license
javacc: https://javacc.dev.java.net/:BSD license
hadoop: http://lucene.apache.org/hadoop/:Apache license

You must also create conf directory. You may put your hadoop-site.xml file
in that directory so that it will be included at build time. You may also
add the hadoop-site.xml file to pig.jar after the build has completed.

Use ant with the build.xml.oss to build pig.
