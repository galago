//
// Step.java
//
// 22 March 2007 -- Trevor Strohman
//
// BSD License (http://galagosearch.org/license)
//
 
package galago.tupleflow;

/**
 * This interface is empty.  Its purpose is as a base interface for
 * any pipelinable object (Source or Processor).
 *
 * @author trevor
 */
public interface Step {
}
