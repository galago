
//
// BinnedFeatureIndex
//
// 5 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//                 

#include "BinnedFeatureIndex.hpp"
#include "indri/Path.hpp"
#include "Stemmer.hpp"
#include "Stopper.hpp"
#include "Query.hpp"

//
// BinnedFeatureIndex constructor
//

BinnedFeatureIndex::BinnedFeatureIndex() :
    stemmer(0),
    stopper(0)
{   
}

//
// BinnedFeatureIndex deconstructor
//

BinnedFeatureIndex::~BinnedFeatureIndex() {
    close();
}
      
//
// openRead
//

void BinnedFeatureIndex::openRead( const std::string& indexPath ) {
    _reader.openRead( indexPath );
                 
    _maximumDocumentCount = _reader.getManifest().get( "maximumDocumentCount", (INT64)1 );
    _build_stemmer();
    _build_stopper();
    _requiresWeighting = (_reader.getManifest().get("queryBins", 8) == 0);
}                    

//
// _build_stopper
//

void BinnedFeatureIndex::_build_stopper() {
    stopper = 0;
    if( _reader.getManifest().exists("stopper") ) {
        stopper = new Stopper( _reader.getManifest() );
    }
}

//
// _build_stemmer
//               

void BinnedFeatureIndex::_build_stemmer() {
    stemmer = 0;
    if( _reader.getManifest().get( "stemmer", "" ) == "porter2" ) {
        stemmer = new Stemmer();
    }
}    

//
// close
//                                      

void BinnedFeatureIndex::close() {
    delete stemmer;
    delete stopper;
    stemmer = 0;
    stopper = 0;
}

//
// processTerm
//

QueryTerm BinnedFeatureIndex::processTerm( const QueryTerm& term ) const {
    std::string processed = term.text;

    normalizer.process( processed );

    if( stopper )
        stopper->process( processed );

    if( stemmer )
        stemmer->process( processed );

    QueryTerm result = term;
    result.text = processed;

    return result;
}
  
//
// getTerm
//

BinOrderedBinnedIterator* BinnedFeatureIndex::getTerm( const std::string& term ) const {
    ListRegion region = _reader.getTerm( term );
    
    if( region.data == 0 )
        return 0;
    
    BinOrderedBinnedIterator* iterator = new BinOrderedBinnedIterator( region.data, region.start, region.end );
    return iterator;
}

//
// maximumDocumentCount
//

UINT64 BinnedFeatureIndex::maximumDocumentCount() const {
    return _maximumDocumentCount;
}

//
// requiresWeighting
//

bool BinnedFeatureIndex::requiresWeighting() const {
    return _requiresWeighting;
}
