/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.tools.grunt;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobSubmissionProtocol;

import com.yahoo.pig.PigServer;
import com.yahoo.pig.PigServer.ExecType;
import com.yahoo.pig.data.Tuple;
import com.yahoo.pig.impl.logicalLayer.parser.ParseException;
import com.yahoo.pig.impl.logicalLayer.parser.TokenMgrError;
import java.io.InputStreamReader;

public class Grunt {
    BufferedReader        in;
    FileSystem            dfs;
    FileSystem            lfs;
    Configuration         conf;
    JobSubmissionProtocol jobTracker;
    PigServer             pig;
    Map<String, Integer> variables;

    public Grunt(BufferedReader in, FileSystem dfs, FileSystem lfs, 
                 Configuration conf, JobSubmissionProtocol jobTracker) {
        this.in = in;
        this.dfs = dfs;
        this.lfs = lfs;
        this.conf = conf;
        this.jobTracker = jobTracker;
        this.pig = new PigServer(ExecType.LOCAL);
    }
    
    public static void main( String[] args ) {
        Grunt g = new Grunt( new BufferedReader( new InputStreamReader( System.in ) ), null, null, null, null );
        g.run();
    }

    public void run() {
        String line = null;
        try {
            System.err.print("grunt> ");
            System.err.flush();
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if( execute(line) < 0 ){
                    System.err.print("Goodbye\n");
                    System.err.flush();
                    return;
                } else {
                    System.err.print("grunt> ");
                    System.err.flush();
                }
            }
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public void exec() {
        try {
            String line;
            while((line = in.readLine()) != null) {
                execute(line);
            }
        } catch (NumberFormatException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public int execute(String line) {
        try {
            line=line.trim();
            while(line.endsWith(";")) {
        	line = line.substring(0, line.length()-1).trim();
            }
            if (line.length() == 0) {
                // noop
            } else if(line.equals("quit") || line.equals("exit")) {
                return -1;
            } else if (line.equals("debugOn")) {
                pig.debugOn();
            } else if (line.equals("debugOff")) {
                pig.debugOff();
            } else if (line.startsWith("define ")) {
            	String words[] = line.split(" |\t", 3);
            	if (words.length != 3) {
            		System.err.println("define functionAlias functionSpec");
            		return -2;
            	}
            	pig.registerFunction(words[1], words[2]);
            } else if (line.startsWith("store ")) {
                String words[] = line.split(" |\t");
                if (words.length < 3) {
                    System.err.println("store alias filename [functionSpec]");
                    return -2;
                }
                String filename = words[2].trim();
                // Strip off quotes, if any, for consistency with load
                if (filename.startsWith("'") && filename.endsWith("'")) {
                    filename = filename.substring(1, filename.length()-1);
                }
                pig.store(words[1].trim(), filename, words.length > 3 ? words[3] : null);
            } else if (line.startsWith("dump ")) {
                String words[] = line.split(" |\t");
                Iterator result = pig.openIterator(words[1].trim());
                while (result.hasNext()) {
                    Tuple t = (Tuple) result.next();
                    System.out.println(t);
                }
            }else if (line.equals("cd")) {
                dfs.setWorkingDirectory(new Path("/user/" + System.getProperty("user.name")));
            } else if (line.startsWith("cd ")) {
                String files[] = line.split(" |\t");
                Path newDir;
                if (files.length != 2) {
                    System.err.println("usage: cd <path>");
                } else if (dfs.exists(newDir = new Path(files[1]))) {
                    dfs.setWorkingDirectory(newDir);
                } else {
                    System.err.println(files[1] + " doesn't exist");
                }
            } else if (line.equals("pwd")) {
                System.out.println(dfs.getWorkingDirectory());
            } else if (line.startsWith("help")){
            	printHelp();
            }else if (line.startsWith("kill ")) {
                String jobargs[] = line.split(" |\t");
                if (jobargs.length != 2) {
                    System.err.println("usage: kill job_id");
                } else {
                    jobTracker.killJob(jobargs[1]);
                    System.err.println("kill submited.");
                }
            } else if (!checkDFS(line)) {
        	line = line + ';';
            	pig.registerQuery(line);
            	
            }
        } catch (IOException e) {
            Throwable cause = e.getCause();
            if (cause != null && cause instanceof ParseException) {
                System.err.println(cause.getLocalizedMessage() + ": " + line);
            } else {
                System.err.println(e.getLocalizedMessage());
            }
        } catch (TokenMgrError e) {
            System.err.println(e.getLocalizedMessage() + ": " + line);
        }
        return 0;
        
    }

    private void printHelp() {
        System.err.println("Commands:");
        System.err.println("<pig latin statement>;");
        System.err.println("define functionAlias functionSpec");
        System.err.println("store id filename");
        System.err.println("dump id");
        System.err.println("kill job_id");
        System.err.println("ls <path>\r\ndu <path>\r\nmv <src> <dst>\r\ncp <src> <dst>\r\nrm <src>");
        System.err.println("copyFromLocal <localsrc> <dst>\r\ncd <dir>\r\npwd");
        System.err.println("cat <src>\r\ncopyToLocal <src> <localdst>\r\nmkdir <path>");
        System.err.println("cd <path>");
        System.err.println("debugOn");
        System.err.println("debugOff");
    }
    
    /**
     * Returns true if the command is a DFS command.
     * 
     * @param cmd
     *            the command to check.
     * @return true if cmd is a DFS command.
     */
    boolean checkDFS(String cmd) throws IOException {
        String args[] = cmd.split(" |\t");
        if (args[0].equals("ls")) {
            Path dir;
            if (args.length > 1) {
                dir = new Path(args[1]);
            } else {
                dir = dfs.getWorkingDirectory();
            }
            Path paths[] = dfs.listPaths(dir);
            for (int j = 0; j < paths.length; j++) {
                Path path = paths[j];
                if (dfs.isDirectory(path)) {
                    System.out.println(path + "\t<dir>");
                } else {
                    System.out.println(path + "<r " + dfs.getReplication(path) + ">\t" + dfs.getLength(path));
                }
            }
        } else if (args[0].equals("mv")) {
            if (args.length != 3) {
                throw new IOException("Must specify src and dst");
            }
            dfs.rename(new Path(args[1]), new Path(args[2]));
        } else if (args[0].equals("cp")) {
            if (args.length != 3) {
            	throw new IOException("Must specify src and dst");
            }
            FileUtil.copy(dfs, new Path(args[1]), dfs, new Path(args[2]), false, conf);
        } else if (args[0].equals("rm")) {
            for (int i = 1; i < args.length; i++) {
                dfs.delete(new Path(args[i]));
            }
        } else if (args[0].equals("copyFromLocal")) {
            if (args.length != 3) {
            	throw new IOException("Must specify src and dst");
            }
            dfs.copyFromLocalFile(new Path(args[1]), new Path(args[2]));
        } else if (args[0].equals("con")) {
        	if (args.length != 2) {
        		throw new IOException("Must specify a destination");
        	}
        	byte buffer[] = new byte[65536];
        	FSDataOutputStream os = dfs.create(new Path(args[1]));
        	int rc;
        	while((rc = System.in.read(buffer))> 0) {
        		os.write(buffer, 0, rc);
        	}
        	os.close();
        } else if (args[0].equals("cat")) {
            byte buffer[] = new byte[65536];
            for (int i = 1; i < args.length; i++) {
                if (dfs.isDirectory(new Path(args[i]))) {
                    Path paths[] = dfs.listPaths(new Path(args[i]));
                    for (int j = 0; j < paths.length; j++) {
                        if (!dfs.isFile(paths[j]))
                            continue;
                        FSDataInputStream is = dfs.open(paths[j]);
                        int rc;
                        while ((rc = is.read(buffer)) > 0) {
                            System.out.write(buffer, 0, rc);
                        }
                        is.close();
                    }
                } else {
                    FSDataInputStream is = dfs.open(new Path(args[i]));
                    int rc;
                    while ((rc = is.read(buffer)) > 0) {
                        System.out.write(buffer, 0, rc);
                    }
                    is.close();
                }
            }
        } else if (args[0].equals("copyToLocal")) {
            if (args.length != 3) {
                throw new IOException("Must specify src and dst");
            }
            dfs.copyToLocalFile(new Path(args[1]), new Path(args[2]));
        } else if (args[0].equals("mkdir")) {
            if (args.length != 2) {
                System.out.println("Must specify only new directory name");
            }
            dfs.mkdirs(new Path(args[1]));
        } else {
            return false;
        }
        return true;
    }

    public void rename(String oldName, String newName) throws IOException {
        System.out.println("Renaming " + oldName + " to " + newName);
        Path dst = new Path(newName);
        if (dfs.exists(dst))
            dfs.delete(dst);
        dfs.rename(new Path(oldName), dst);
    }

    public void copy(String src, String dst, boolean localDst) throws IOException {
        FileUtil.copy(dfs, new Path(src), (localDst ? lfs : dfs), new Path(dst), false, conf);
    }

}
