/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.builtin;

import java.io.IOException;

import com.yahoo.pig.TupleEvalFunc;
import com.yahoo.pig.data.Tuple;

public class EFNoop extends TupleEvalFunc {

	public void exec(Tuple input, Tuple output) throws IOException {
        output.appendTuple(input);
    }
    
}
