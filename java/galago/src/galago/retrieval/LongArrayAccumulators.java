/*
 * LongArrayAccumulators
 *
 * January 4, 2007 -- Trevor Strohman
 *
 * BSD License (http://www.galagosearch.org/license)
 */

package galago.retrieval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;

/**
 *
 * @author trevor
 */
public class LongArrayAccumulators {
    private long[] accumulators;
    private int[] maxUnseen;
    private int threshold;
    private int postings;
    private int hits;
    private boolean canIgnore;
    
    private static final int DOCUMENT_BITS  = 32;
    private static final long DOCUMENT_MASK = 0x00000000ffffffffL;
    
    private static final long VALUE_MASK    = 0x00ffffff00000000L;
    private static final int VALUE_SHIFT    = 32;
    
    private static final long TERM_MASK     = 0xff00000000000000L;
    private static final int TERM_BITS      = 8;
    private static final int TERM_SIZE      = 256;
    private static final int TERM_SHIFT     = 56;
    
    public boolean useAndMode() {
        return totalUnseen() < threshold;
    }
    
    public void processBin( final int score, final int[] binDocuments, final int termIndex ) {
        if( useAndMode() ) {
            processBinAnd( score, binDocuments, termIndex );
        } else {
            processBinOr( score, binDocuments, termIndex );
        }
    }
    
    public void processBinOr( final int score, final int[] binDocuments, final int termIndex ) {
        int j=0;
        int k=0;
        
        assert termIndex <= 7;
        final long bit = (1L << (termIndex + TERM_SHIFT));
        final long shiftedScore = ((long)score) << VALUE_SHIFT;
        int nextAccumulator = Integer.MAX_VALUE;
        
        if( accumulators.length > 0 )
            nextAccumulator = (int) (accumulators[0] & DOCUMENT_MASK);
        
        long[] newAccumulators = new long[accumulators.length + binDocuments.length];
        
        for( int i=0; i<binDocuments.length; i++ ) {
            int current = binDocuments[i];
            postings++;
            
            // copy all accumulators that are less than this
            while( nextAccumulator < current && j < accumulators.length ) {
                newAccumulators[k] = accumulators[j];
                j++;
                k++;

                if( j == accumulators.length )
                    nextAccumulator = Integer.MAX_VALUE;
                else
                    nextAccumulator = (int) (accumulators[j] & DOCUMENT_MASK);
            }
            
            // match the accumulator if it matches
            if( nextAccumulator == current ) {
                newAccumulators[k] = (accumulators[j] + shiftedScore) | bit;
                k++;
                
                j++;
                if( j == accumulators.length )
                    nextAccumulator = Integer.MAX_VALUE;
                else
                    nextAccumulator = (int) (accumulators[j] & DOCUMENT_MASK);
            } else {
                newAccumulators[k] = current | shiftedScore | bit;
                k++;
            }
            hits++;
        }
        
        if( accumulators.length - j > 0 )
            System.arraycopy( accumulators, j, newAccumulators, k, accumulators.length - j );
        int newestLength = k + accumulators.length - j;
        long[] evenNewerAccumulators = new long[newestLength];
        System.arraycopy( newAccumulators, 0, evenNewerAccumulators, 0, newestLength );
        accumulators = evenNewerAccumulators;
    }
    
    private void processBinAnd( final int score, final int[] binDocuments, final int termIndex ) {
        int j=0;
        
        if( accumulators.length == 0 )
            return;
        
        assert termIndex <= 7;
        final long bit = (1L << (termIndex + TERM_SHIFT));
        final long shiftedScore = (((long)score) << VALUE_SHIFT);
        int nextDocument = (int) (accumulators[0] & DOCUMENT_MASK);
        
        for( int i=0; i<binDocuments.length; i++ ) {
            int current = binDocuments[i];
            postings++;
            
            if( current < nextDocument )
                continue;

            while( current > nextDocument ) {
                do {
                    j += 1;
                    if( j == accumulators.length )
                        return;
                    nextDocument = (int) (accumulators[j] & DOCUMENT_MASK);
                } while( (accumulators[j] & bit) != 0 );
            }
            
            if( current == nextDocument ) {
                hits++;
                accumulators[j] += shiftedScore;
                accumulators[j] |= bit;
            }
        }
    }
    
    public void setMaxUnseen( int termIndex, int score ) {
        maxUnseen[termIndex] = score;
    }
    
    public void calculateThresholdScore( int k ) {
        PriorityQueue<Integer> queue = new PriorityQueue<Integer>();
        
        // if we don't have enough accumulators yet, we can't get a threshold
        if( accumulators.length < k ) {
            threshold = -1;
            return;
        }
        
        // fill up the queue with the current threshold score
        for( int i=0; i<k; i++ ) {
            queue.add(threshold);
        }
        
        for( int i=0; i<accumulators.length; i++ ) {
            int value = (int) ((accumulators[i] & VALUE_MASK) >> VALUE_SHIFT);
            
            if( value > queue.peek() ) {
                queue.add(value);
                queue.poll();
            }
        }
        
        threshold = queue.poll();
    }
    
    public void trim() {
        if( threshold < 0 )
            return;
        
        int[] fullUnseen = computeFullUnseen();
        
        // for each accumulator, we compute the max possible value and trim if it can't be achieved
        long[] newAccumulators = new long[accumulators.length];
        int j=0;
        
        for( int i=0; i<accumulators.length; i++ ) {
            int value = (int) ((accumulators[i] & VALUE_MASK) >> VALUE_SHIFT);
            int seenTerms = (int) ((accumulators[i] & TERM_MASK) >> TERM_SHIFT);
            int maxValue = value + fullUnseen[seenTerms];
            assert maxValue >= value;
            
            if (maxValue >= threshold) {
                newAccumulators[j] = accumulators[i];
                j++;
            }
        }
        
        long[] evenNewerAccumulators = new long[j];
        System.arraycopy( newAccumulators, 0, evenNewerAccumulators, 0, evenNewerAccumulators.length );
        accumulators = evenNewerAccumulators;
    }

    private int[] computeFullUnseen() {
        final int[] fullUnseen = new int[TERM_SIZE];
        
        // build the fullUnseen array
        for( int i=0; i<fullUnseen.length; i++ ) {
            for( int j=0; j<maxUnseen.length; j++ ) {
                int termBit = 1<<j;
                
                if( (i & termBit) == 0 ) {
                    fullUnseen[i] += maxUnseen[j];
                }
            }
        }
        return fullUnseen;
    }
    
    public int size() {
        return accumulators.length;
    }
    
    public int totalUnseen() {
        int totalUnseen = 0;
        
        for( int i=0; i<maxUnseen.length; i++ ) {
            totalUnseen += maxUnseen[i];
        }
        
        return totalUnseen;
    }
    
    private class PartiallyScoredDocument implements Comparable<PartiallyScoredDocument> {
        public int terms;
        public int score;
        public int document;
        
        public PartiallyScoredDocument( long accumulator ) {
            terms = (int) ((accumulator & TERM_MASK) >> TERM_SHIFT);
            score = (int) ((accumulator & VALUE_MASK) >> VALUE_SHIFT);
            document = (int) ((accumulator & DOCUMENT_MASK));
        }
        
        public int compareTo( PartiallyScoredDocument other ) {
            return score - other.score;
        }
    }
    
    public boolean canIgnoreFuturePostings( int requested ) {
        if( accumulators.length < requested )
            return false;
        
        if( this.useAndMode() == false )
            return false;
        
        if( accumulators.length > 3*requested )
            return false;
        
        calculateThresholdScore( requested );
        
        // copy accumulators
        ArrayList<PartiallyScoredDocument> partials = new ArrayList<PartiallyScoredDocument>();
        
        for( int i=0; i<accumulators.length; i++ ) {
            PartiallyScoredDocument doc = new PartiallyScoredDocument( accumulators[i] );
            partials.add(doc);
        }
        
        Collections.sort(partials);
        int[] fullUnseen = computeFullUnseen();
        
        for( int i=0; i<partials.size()-1; i++ ) {
            PartiallyScoredDocument one = partials.get(i);
            PartiallyScoredDocument two = partials.get(i+1);
            
            if ( one.score + fullUnseen[one.terms] > two.score )
                return false;
            
            if ( one.score != two.score && one.score + fullUnseen[one.terms] >= two.score )
                return false;
        }
        
        canIgnore = true;
        return true;
    }
    
    public ScoredDocument[] topResults( int k ) {
        // find the top accumulators in the list
        PriorityQueue<ScoredDocument> scores = new PriorityQueue();
        
        for( int i=0; i<accumulators.length; i++ ) {
            int document = (int) (accumulators[i] & DOCUMENT_MASK);
            int score = (int) ((accumulators[i] & VALUE_MASK) >> VALUE_SHIFT);
            ScoredDocument s = new ScoredDocument( document, score );
            
            if (scores.size() < k || s.score > scores.peek().score)
                scores.add(s);
            
            if (scores.size() > k)
                scores.poll();
        }
        
        ScoredDocument[] results = new ScoredDocument[scores.size()];
        
        for( int i=scores.size()-1; i>=0; i-- ) {
            results[i] = scores.poll();
        }
        
        return results;
    }
    
    public String getStats() {
        return String.format( "t: %8d  a: %8d  i: %5b  am: %5b  p: %8d  h: %8d", threshold, accumulators.length, canIgnore, useAndMode(), postings, hits );
    }
    
    /** Creates a new instance of IntArrayAccumulators */
    public LongArrayAccumulators( int termCount ) {
        assert termCount <= 8;
        this.accumulators = new long[0];
        this.maxUnseen = new int[termCount];
        this.threshold = -1;
        this.postings = 0;
        this.hits = 0;
        
        for( int i=0; i<termCount; i++ ) {
            maxUnseen[i] = Short.MAX_VALUE;
        }
    }
}
