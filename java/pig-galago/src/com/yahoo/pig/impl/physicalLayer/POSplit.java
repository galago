/*
 * Copyright (c) 2007 Yahoo! Inc.  All rights reserved.
 * See accompanying LICENSE file.
 */
package com.yahoo.pig.impl.physicalLayer;

// to use POSplit, create a master/slave pair ...
abstract class POSplit extends PhysicalOperator {

	public POSplit(int outputType){
		super(outputType);
	}
}
