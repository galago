
//
// Stopper
//
// 11 January 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_STOPPER_HPP
#define GALAGO_STOPPER_HPP

#include <set>

class Stopper {
private:
    std::set<std::string> _terms;
    
public:
    Stopper( indri::api::Parameters& parameters ) {
        indri::api::Parameters stopwords = parameters["stopper"]["word"];
        
        for( size_t i=0; i<stopwords.size(); i++ ) {
            _terms.insert( (std::string) stopwords[i] );
        }
    }

    bool isStopword( const std::string& word ) {
        return _terms.find(word) != _terms.end();
    }
    
    void process( std::string& word ) {
        if( isStopword(word) ) {
            word = "";
        }
    }
};

#endif // GALAGO_STOPPER_HPP
