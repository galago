
//
// IndexReader
//
// 15 October 2007 -- tds
//
// BSD License (http://galagosearch.org/license)
//

#ifndef GALAGO_INDEXREADER_HPP
#define GALAGO_INDEXREADER_HPP
                              
#include "indri/File.hpp"  
#include "indri/FileMapping.hpp"
#include "VocabularyReader.hpp"
#include "indri/Parameters.hpp"
#include "BinOrderedBinnedIterator.hpp"
#include "Stemmer.hpp"
#include "Normalizer.hpp"
#include "Query.hpp"
#include "Stopper.hpp"

#define INDEXREADER_MAX_TERM_LENGTH (512)
#define INDEXREADER_VOCAB_GROUP (16)
#define INDEXREADER_BLOCK_SIZE (1<<16)

class ListRegion {
public:
    ListRegion() :
        data(0), start(0), end(0)
    {
    }
    
    ListRegion( const char* d, UINT64 s, UINT64 e ) :
        data(d), start(s), end(e)
    {
    }
        
    const char* data;
    UINT64 start;
    UINT64 end;
};

class IndexReader {
private:
    indri::file::File invertedFile;
    indri::file::FileMapping* invertedFileMapping;
    VocabularyReader vocabulary;       
    Stemmer* stemmer;
    Stopper* stopper;
    Normalizer normalizer;
    indri::api::Parameters manifest; 
    UINT64 _maximumDocumentCount;
    bool _requiresWeighting;
    
    void _build_stemmer();
    void _build_stopper();
    
    int _findTermInBlock( const UINT8* vocabBlock, const char* termSuffix, int blockWordIndex, int overlap, int wordCount ) const;
    const UINT8* _findVocabGroup( const char* termSuffix, const UINT8* region, const UINT16* wordBlockEnds, int wordBlockCount, int& blockWordIndex ) const;
    ListRegion _loadTerm( const std::string& term, UINT64 slotBegin ) const;
    
public:
    static const int blockSize = INDEXREADER_BLOCK_SIZE;
    static const int vocabGroup = INDEXREADER_VOCAB_GROUP;
    static const int maxTermLength = INDEXREADER_MAX_TERM_LENGTH;
    
public:
    IndexReader();
    ~IndexReader();
    
    void openRead( const std::string& indexPath );
    void close();
    ListRegion getTerm( const std::string& term ) const;
    
    QueryTerm processTerm( const QueryTerm& term ) const;
    UINT64 maximumDocumentCount() const;
    bool requiresWeighting() const;
    indri::api::Parameters& getManifest();   
};

#endif // GALAGO_INDEXREADER_HPP
